package com.epam.training.student_nastassia_barzykina.java_basics;

import java.util.Scanner;

import static java.lang.Math.sqrt;
/*
программа для решения квадратных уравнений.

Для заданных коэффициентов квадратного уравнения (ax² + bx + c = 0), верните один или два корня уравнения, если они
существуют в области действительных чисел.

Входное значение задается через System.in. Выходное значение должно быть напечатано в System.out.

Формат вывода:

"x₁ x₂" (два корня в любом порядке, разделенные пробелом), если есть два корня
"x" (просто значение корня), если есть единственный корень
"no roots" (просто строковое значение "без корней"), если корня нет.
Параметр a гарантированно не равен нулю
 */

public class QuadraticEquation {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int b = scan.nextInt();
        int c = scan.nextInt();
        double d = (b * b) - (4 * a * c);

        if (d > 0) {
            double x1, x2;
            x1 = ((-b) + sqrt(d)) / (2 * a);
            x2 = ((-b) - sqrt(d)) / (2 * a);
            System.out.println(x1 + " " + x2);
        } else if (d == 0) {
            int x = (-b) / (2 * a);
            System.out.println(x);
        } else {
            System.out.println("no roots");
        }
    }
}
