package com.epam.training.student_nastassia_barzykina.java_basics;

import java.util.Scanner;
/*
программа, которая должна прочитать два значения из System.in:

количество людей
количество кусочков в одной пицце.
Гарантируется, что входные параметры — целые положительные числа.

Программа должна напечатать минимальное количество пицц (не ноль), которое надо заказать, чтобы у всех было одинаковое
количество ломтиков и не осталось ни одного лишнего.
 */

public class PizzaSplit {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int numFriends = scan.nextInt();
        int numPieces = scan.nextInt();
        int i = 1;

        if (numPieces % numFriends == 0) {
            System.out.println("1");
        } else {
            int tmp = 0;
            do {
                tmp = numPieces * (i + 1);
                i++;
            } while (tmp % numFriends > 0);
            System.out.println(i);
        }
    }
}
