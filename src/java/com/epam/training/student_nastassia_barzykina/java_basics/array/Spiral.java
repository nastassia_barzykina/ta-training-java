package com.epam.training.student_nastassia_barzykina.java_basics.array;

/**
 * реализуйте статический метод:
 *
 * int[][] spiral(int rows, int columns)
 * Возвращает двумерный массив в виде таблицы, содержащий числа от 1 до rows * columns. Размер таблицы будет определяться
 * заданными параметрами.
 * Числа заполняют "таблицу" по часовой стрелке от верхнего угла по спирали.
 * Например, для значений параметров (3, 4), выходной массив должен быть:
 *  1  2  3  4
 * 10 11 12  5
 *  9  8  7  6
 */
public class Spiral {
    public static void main(String[] args) {
        int[][] spiral = Spiral.spiral(3, 10);

        for (int i = 0; i < spiral.length; i++) {
            for (int j = 0; j < spiral[i].length; j++) {
                System.out.print(String.format("%4s", spiral[i][j]));// на каждое число 4 символа для корректного вывода
            }
            System.out.println();
        }
    }

    static int[][] spiral(int rows, int columns) {
        int index = 0;
        int num = rows * columns;// кол-во ячеек в матрице
        int[][] matrix = new int[rows][columns];// объявление массива
        for (int is = 0, js = 0; index < num; is++, js++) {// инициализация (заполнение) массива значением index прямоугольник за прямоугольником (0,0; 1,1;...)
            for (int j = js; j < columns - js; j++) {// перебор столбцов в строке (вправо)
                matrix[is][j] = ++index;
            }
            for (int i = is + 1; i < rows - is; i++) {// перебор строк в столбце (вниз)
                matrix[i][columns - 1 - js] = ++index;
            }
            if (index < num) {//проверка конца массива (необх для прямоуг матрицы)
                for (int j = columns - 2 - js; j >= js; j--) {//влево
                    matrix[rows - 1 - is][j] = ++index;
                }
                for (int i = rows - 2 - is; i > is; i--) {//вверх
                    matrix[i][js] = ++index;
                }
            }
        }
        return matrix;
    }
}
