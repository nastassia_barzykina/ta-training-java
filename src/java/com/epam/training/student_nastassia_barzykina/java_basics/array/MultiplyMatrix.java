package com.epam.training.student_nastassia_barzykina.java_basics.array;

import java.util.Arrays;

/**
 * реализуйте метод multiply.
 *
 * Метод берет два прямоугольных целочисленных массива (матрицы) и возвращает результат их умножения.
 *
 * Рассмотрим две целочисленные матрицы, представленные в виде прямоугольных массивов. Задача – перемножить данные
 * матрицы. При таком умножении элементы в i-й строке матрицы A умножаются на соответствующие элементы в j-м столбце
 * матрицы B, а сумма полученных значений сохраняется в ij-й элемент результирующей матрицы.
 *
 * Обратите внимание, что количество столбцов в первой матрице равно количеству строк во второй матрице.
 */
public class MultiplyMatrix {
    public static void main(String[] args) {
        int[][] matrixA = {
                {1, 2, 3},
                {4, 5, 6}};
        int[][] matrixB = {
                {7, 8},
                {9, 10},
                {11, 12}};
        int[][] result = multiply(matrixA, matrixB);
        System.out.println(Arrays.deepToString(result).replace("],", "]\n"));
    }

    static int[][] multiply(int[][] arrayA, int[][] arrayB) {
        int[][] tmp = new int[arrayA.length][arrayB[0].length];
        for (int resultRow = 0; resultRow < arrayA.length; resultRow++) {
            for (int resultColumn = 0; resultColumn < arrayB[0].length; resultColumn++) {
                for (int j = 0; j < arrayB.length; j++) {
                    tmp[resultRow][resultColumn] += arrayA[resultRow][j] * arrayB[j][resultColumn];
                }
            }

        }
        return tmp;
    }

}

