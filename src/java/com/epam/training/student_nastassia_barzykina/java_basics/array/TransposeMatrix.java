package com.epam.training.student_nastassia_barzykina.java_basics.array;

/**
 *  реализуйте метод multiply.
 *
 * Метод принимает в качестве параметра прямоугольный целочисленный массив (матрицу) и возвращает его транспонированным.
 *
 * Рассмотрим целочисленную матрицу, представленную в виде прямоугольного массива. Задача состоит в том, чтобы
 * транспонировать ее по главной диагонали.
 *
 * Результат транспонирования матрицы – это перевернутая по главной дагонали версия исходной матрицы.
 */
public class TransposeMatrix {
    public static void main(String[] args) {
        int[][] matrix = { { 1, 2, 3, 4, 5 },
                { 5, 4, 3, 2, 1 },
                { 0, 2, 0, 4, 0 } };
        int[][] transMatrix = multiply(matrix);
        for (int i = 0; i < transMatrix.length; i++) {
            for (int j = 0; j < transMatrix[0].length; j++) {
                System.out.print(String.format("%4s", transMatrix[i][j]));
            }
            System.out.println();
        }

    }
    static int[][] multiply(int [][] array) {
        int[][] tmp = new int[array[0].length][array.length];
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array.length; j++) {
                tmp[i][j] = array[j][i];
            }
        }
        return tmp;
    }
}
