package com.epam.training.student_nastassia_barzykina.java_basics.array;

/**
 * реализуйте метод sum.
 *
 * Правильная реализация должна получить массив значений int и вернуть сумму четных чисел.
 *
 * Подробности:
 *
 * Если данный массив равен нулю или пуст, метод возвращает 0.
 * Метод sum не должен изменять массив.
 * Входной массив может содержать любое значение типа int от Integer.MIN_VALUE до Integer.MAX_VALUE
 */
public class SumOfEvenNumbers {
    public static void main(String[] args) {
        int[] array = new int[]{1, 3, 2, 8, 15, 199, 100};

        System.out.println(sum(array));
    }
    public static int sum(int[] val) {
        int sum = 0;
        if ((val == null) || (val.length == 0)) {//если =0 или пуст
            return 0;
        } else {
            for (int value : val) {
                if (value % 2 == 0) {
                    sum += value;
                }
            }
            return sum;
        }
    }
}
