package com.epam.training.student_nastassia_barzykina.java_basics.array;

import java.util.Arrays;

/** Методы:
 * void cycleSwap(int[] array)
 * Сдвигает все элементы в данном массиве вправо на 1 позицию.
 * В этом случае последний элемент массива становится первым.
 * Например, 1 3 2 7 4 становится 4 1 3 2 7.
 * void cycleSwap(int[] array, int shift)
 * Сдвигает все элементы в заданном массиве вправо на shift позиций.
 * Гарантируется, что значение сдвига неотрицательное и не больше длины массива.
 * Например, 1 3 2 7 4 со сдвигом 3 становится 2 7 4 1 3.
 * Для большего интереса при выполнении задания попробуйте не использовать циклы в своем коде (это не обязательно).
 *
 * Обратите внимание, что входной массив может быть пустым.
 */

public class CycleSwap {
    public static void main(String[] args) {
        int[] array = new int[]{1, 3, 2, 7, 4};
        System.out.println(Arrays.toString(array));// исходный массив
        cycleSwap(array, 5);
        System.out.println(Arrays.toString(array));// после сдвига на 5 позиций
        cycleSwap(array);
        System.out.println(Arrays.toString(array));// после сдвига на 1 позицию


    }
    static void cycleSwap(int[] array, int shift) {
        if (array == null || array.length == 0) {
            return;// прерывает действие метода
        }
        int[] swap = new int[array.length];// объявление и выделение памяти под вспомогательный массив
        System.arraycopy(array, array.length - shift, swap, 0, shift);//вставка элементов от точки сдвига
        System.arraycopy(array, 0, swap, shift, array.length - shift);// вставка элементов до точки сдвига
        System.arraycopy(swap, 0, array, 0, swap.length);//копирование массива с переставленными элементами
    }

    static void cycleSwap(int[] array) {
        cycleSwap(array, 1);
    }

    }
