package com.epam.training.student_nastassia_barzykina.java_basics.array;

import java.util.Arrays;

public class ArrayExample {
    public static void main(String[] args) {
        int[] numberArray = new int[10];
        int i = 0;
        while (i < 10) {
            numberArray[i] = i;
            i++;
        }
        i = 0;
        while (i < 10) {
            System.out.println((i+1) + "-й элемент массива = " +  numberArray[i]);
            i++;
        }
        /**
         * способы инициализации массивов:
         */
        int [] array = {1,2,3,4,5};
        int [] a = new int[10];
        String [] str = new String[10];
        int [] b = new int[] {11,22,33,44,55,66,77};
        var c = new int[] {1,2,3,4}; /* аналогично предыдущей строке */
 //       int numbers [] = new int[2] {10, 20}; /* некорректно. Присутствует двойное объявление размера в квадратных скобках
 //       и фактическое число элементов в фигурных скобках. И не имеет значения, что размеры совпадают */
        String[][] names;
        float[][] f1;
        float[ ] f2;
        /**
         * Копирование и вставка элементов массива в другой массив
         * преобразование массива в строку и вывод на печать
         */
        System.arraycopy(array, 0, b, 0, array.length);
        System.out.println(Arrays.toString(b));
    }
}
