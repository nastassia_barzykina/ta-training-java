package com.epam.training.student_nastassia_barzykina.java_basics.array;
import java.util.Arrays;
public class ArraysMethods {
    public static void main(String[] args) {
        /**
         * метод copyOf
         */
        System.out.println("метод copyOf:");
        int[] array = {9, 8, 7, 6, 5};
        System.out.println(Arrays.toString(array));

        int[] newArray = Arrays.copyOf(array, 8); // посл 3 эл-та заполнятся 0, т.к. в исх массиве 5 эл-тов
        System.out.println(Arrays.toString(newArray));
        /**
         * метод equals
         */
        System.out.println("метод equals:");
        int[] arr1 = {1,2,3,4,5,6,7,8,9};
        int[] arr2 = {1,2,3,4,5,6,7,8,9};
        int[] arr3 = {1,2,5,5,5,5,5,8,9};
        System.out.println(arr1 == arr2); // сравнение ссылок на массивы
        System.out.println(Arrays.equals(arr1, arr2)); //сравнение массивов
        System.out.println(Arrays.equals(arr1, arr3));
        /**
         * sort
         */
        System.out.println("метод sort:");
        int intArr[] = {55, 57, 61, 66, 18, 19, 27, 38,10, 55, 15, 39, 51, 18, 83, 95};
        Arrays.sort(intArr);
        System.out.println("The sorted int array is:");
        System.out.println(Arrays.toString(intArr));

        /**
         * binarySearch
         */
        System.out.println("метод binarySearch:");
        int intArr2[] = {10, 15, 18, 18, 19, 27, 38, 39, 51, 55, 55, 57, 61, 66, 83, 95};
        int searchVal = 55; // если эл-та нет, вернет -1
        int retVal = Arrays.binarySearch(intArr2, searchVal);
        System.out.println("The index of element 55 is : " + retVal);

        /**
         * fill
         */
        System.out.println("метод fill:");
        int[] array3 = new int[7];
        Arrays.fill(array3, -1);
        System.out.println(Arrays.toString(array3));
        /**
         * deeptoString
         */
        System.out.println("deeptoString:");
        int [][] array2D = {{1, 2, 3},{4, 5, 6}};
        System.out.println(Arrays.deepToString(array2D));
        /**
         * deepEquals
         */
        System.out.println("deepEquals:");
        int[][] arrayDD = { { 1, 2, 3 }, { 4, 5, 6 }, { 7 } };
        int[][] anotherArray = { { 1, 2, 3 }, { 4, 5, 6 }, { 7 } };
        System.out.println(Arrays.equals(arrayDD, anotherArray)); // не подходит для многомерных массивов, false
        System.out.println(Arrays.deepEquals(arrayDD, anotherArray));// true
    }
}
