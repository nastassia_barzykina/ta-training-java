package com.epam.training.student_nastassia_barzykina.java_basics.array;
/*
Правильная реализация должна получить массив значений типа int и вернуть его максимальное значение.

Подробности:

Входной массив гарантированно не будет пустым или null.
Метод max не должен изменять массив.
Входной массив может содержать любое значение типа int от Integer.MIN_VALUE до Integer.MAX_VALUE
 */
    public class MaxMethod {
    public static void main(String[] args) {
        int[] vals = new int[]{ -2, 0, 10, 5 };
        //int result = MaxMethod.max(vals);
        System.out.println(max(vals)); // true
    }

        public static int max(int[] values) { // определение максимального числа в массиве. На вход подается массив

            int max = Integer.MIN_VALUE;// переменной присваивается мин инт значение
            for (int value : values) {
                if (max < value) {
                    max = value;
                }
            }
            return max;
        }
    }
