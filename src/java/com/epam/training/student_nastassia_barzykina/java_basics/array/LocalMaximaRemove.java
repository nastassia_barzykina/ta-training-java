package com.epam.training.student_nastassia_barzykina.java_basics.array;

import java.util.Arrays;

/**
 * реализуйте метод removeLocalMaxima.
 *
 * Правильная реализация должна получить массив значений типа int и вернуть копию данного массива со всеми удаленными
 * локальными максимумами. Исходный массив изменять нельзя.
 *
 * Локальный максимум — это элемент, который больше любого из его соседних элементов. Вам необходимо удалить элементы,
 * которые являются локальными максимумами в исходном массиве.
 *
 * Подробности:
 *
 * Размер данного массива гарантированно больше 1.
 * Данный массив гарантированно не равен null.
 * Если у массива нет локальных максимумов, то вы должны вернуть его копию без изменений.
 * Вы можете использовать методы java.util.Arrays.*
 */

public class LocalMaximaRemove {
    public static void main(String[] args) {
        int[] array = new int[]{-18, 21, 3, 6, 7, 65};

        System.out.println(Arrays.toString(removeLocalMaxima(array)));
    }

    public static int[] removeLocalMaxima(int[] array) {
        int[] tmp = new int[array.length];
        int count = 0;
        if (array[0] <= array[1]) {// сравнение первых двух элементов
            tmp[count++] = array[0];
        }
        for (int i = 1; i < array.length - 1; i++) {
            if (array[i] <= array[i + 1] || array[i] <= array[i - 1]) {
                tmp[count++] = array[i];
            }
        }
        if (array[array.length - 1] <= array[array.length - 2]) {//сравнение последних двух элементов
            tmp[count++] = array[array.length - 1];
        }
        return Arrays.copyOf(tmp, count);//копирование массива длиной count (сам массив tmp м.б. большей длины)
    }
}
