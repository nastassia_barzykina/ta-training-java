package com.epam.training.student_nastassia_barzykina.java_basics.array;

public class TwoD {
    public static void main(String[] args) {
        int[][] multiplicationTable = new int[5][];
        System.out.println("multiplicationTable = " + multiplicationTable );//вывод ссылки
        for (int i = 0; i < multiplicationTable.length; i++) {
            System.out.println("multiplicationTable[" + i + "] = " + multiplicationTable[i]);// значение элемента
        }
        System.out.println("\nCreate array");
        for (int i = 0; i < multiplicationTable.length; i++) {// создание 1М, ссылки на которые сохраняются в 1М
            multiplicationTable[i] = new int[5];//выделение памяти под массивы
            System.out.println("multiplicationTable[" + i + "] = " + multiplicationTable[i]);
        }
        System.out.println("\nInitialization array");
        for (int i = 0; i < multiplicationTable.length; i++) {//вывод на печать содержимого 2М
            for (int j = 0; j < multiplicationTable[i].length; j++) {
                System.out.print(" " + multiplicationTable[i][j]);
            }
            System.out.println();
        }
        /*
        сумма элементов 2М
         */
        int[][] arAdd = {{0,1,2,3,4}, {0,2,4,6,8}, {0,3,6,9,12}};
        int sum = 0;
        for (int [] i: arAdd) {//для перебора элементов массива ссылок
            for (int k:i) {// для перебора элементов
                sum += k;
            }

        }
        System.out.println("сумма элементов массива: " + sum);
        for (int[] elem: arAdd) {// для перебора элементов массива ссылок
            for (int el: elem) {//перебор элементов и вывод на печать
                System.out.print(el + " ");
                
            }
            System.out.println();

        }
        System.out.println("Example");
        int[][] array2D = { { 1, 2, 3, 4, 5 },
                             { 5, 4, 3, 2, 1 },
                            { 0, 2, 0, 4, 0 } };
        sum = 0;
        for (int[]row : array2D) {
            for (int element : row) {
                sum = sum + element;
            }
        }
        for (int[] row : array2D) {
            for (int element : row) {
                System.out.print(element + " ");
            }
            System.out.println();
        }
        System.out.println("sum = " + sum);
        System.out.println("Example 3D: ");
        /**
         * Трехмерный массив, способы инициализации
         */
        int[] numbers = { 1, 3, 5, 7, 9 }; // создали одномерный массив
        int[][] array = new int[3][]; // создали только массив ссылок на другие массивы
        array[0] = numbers; // связали массив numbers с первой строкой массива array
        array[2] = new int[] { 2, 4, 6, 8 }; // можно не указывать кол-во элементов, но тогда их нужно здесь перечислить
        for (int[] row : array) {
            if (row != null) { // чтобы не было ошибки при поытке вывести содержимое отсутствующего массива здесь проверяем
                // его наличие
                for (int element : row) {
                    System.out.print(element + " ");
                }
            } else {
                System.out.print(row); // int[] row  - пустой (array [1] не инициализирован)
            }
            System.out.println();
        }
//        String[] ejgStr = new String[][] { { null }, new String[] { "a", "b", "c" }, { new String() } }[0];
//        String[] ejgStr1 = null; - литерал null не может быть индексирован
//        String[] ejgStr2 = { null };
//        System.out.println(ejgStr[0] + "  " + ejgStr2[0] + "  " + ejgStr1[0]);
    }
}
