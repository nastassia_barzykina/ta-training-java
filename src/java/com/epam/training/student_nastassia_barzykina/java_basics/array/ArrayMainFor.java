package com.epam.training.student_nastassia_barzykina.java_basics.array;

import java.util.Arrays;

public class ArrayMainFor {
    public static void main(String[] args) {
        int[] array; // объявили ссылку
        int[] arrayInt = new int[20]; // объявили ссылку и выделили память
        int[] amountDays = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }; // объявили и инициализировали
        System.out.println("Апрель содержит " + amountDays[3] + " дней");
        for (int i = 0; i < amountDays.length; i++) {
            if (amountDays[i] < 31) {
                amountDays[i] = 0; // изменили значение элемента
            }
            arrayInt[i] = amountDays[i]; // копируем значение в другой массив
        }
        array = amountDays; // инициализация ссылки array - ссылается на массив amountDays
        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.toString(arrayInt));
        System.out.println(Arrays.toString(amountDays));
        System.out.println("_________________");
        int[]  arrInt = { 1, 2, 3, 4, 5, 6 };
        for (int element :  arrInt) {//только для перебора значений, не редактирует и не удаляет
            element *=  element;// меняется только значение переменной element, а не элементы массива
            System.out.println(element);
        }
        System.out.println("_________________");
        for (int valueInt : arrInt) {
            System.out.println(valueInt);//печать исходного массива
        }

    }
}
