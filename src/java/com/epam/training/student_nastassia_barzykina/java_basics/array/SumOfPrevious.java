package com.epam.training.student_nastassia_barzykina.java_basics.array;

import java.util.Arrays;

/**
 * реализуйте метод getSumCheckArray.
 *
 * Правильная реализация должна получить массив значений int и вернуть массив логических значений, где каждый элемент
 * является результатом проверки: является ли соответствующий исходный элемент суммой двух предыдущих элементов в данном массиве.
 *
 * Подробности:
 *
 * Гарантируется, что длина данного массива составляет 2 или более.
 * Данный массив гарантированно не равен null.
 * Метод возвращает массив логических значений, где каждый элемент является результатом для соответствующего элемента в
 * данном массиве.
 * Первые два элемента логического массива всегда false.
 */

public class SumOfPrevious {
    public static void main(String[] args) {
        int[] array = new int[]{1, -1, 0, 4, 6, 10, 15, 25};

        boolean[] sumCheckArray = getSumCheckArray(array);// массив логических значений, метод инициализации массива
        System.out.println(Arrays.toString(sumCheckArray));
    }
    public static boolean[] getSumCheckArray(int[] array){
        boolean[] result = new boolean[array.length];//выделение памяти под массив
        for (int i = 2; i < array.length; i++) {// инициализация. По умолчанию первые 2 элемента false
            if (array[i-2] + array[i - 1] == array[i]) {
                result[i] = true;
            } else {
                result[i] = false;
            }

        }
        return result;

    }
}
