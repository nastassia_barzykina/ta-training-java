package com.epam.training.student_nastassia_barzykina.java_basics.array;

public class ArrayForEach {
    /**
     * использование типа StringBuilder для изменения данных массива
     * @param args
     */
    public static void main(String[] args) {
        StringBuilder [] stringBuilders = new StringBuilder[3];// обьявление массива с измен. типом данных
        stringBuilders[0] = new StringBuilder("Java");// инициализация элементов массива
        stringBuilders[1] = new StringBuilder("Oracle");
        stringBuilders[2] = new StringBuilder("Epam");
        for (StringBuilder elem: stringBuilders) {
            elem.append(11);

        }

        for (StringBuilder elem: stringBuilders) {//вывод измененных элементов
            System.out.println(elem);
        }
        System.out.println("___________");
        /**
         * Найти макс и заменить отриц эл-ты на макс
         */
            int[] array = { 5, 10, 0, -5, 16, -2 };
            int max = array[0];
            for (int value : array) {//находим max
                if (max < value) {
                    max = value;
                }
            }
            for (int i = 0; i < array.length; i++) {// изменяем элементы
                if (array[i] < 0) {
                    array[i] = max;
                }
                System.out.println("array[" + i + "]= "+ array[i]);
            }
        String[] strArray = new String[] {"One", "Two", "Three"};
        strArray[2] = null;
        for (String val : strArray){
            System.out.print(val + ", ");
            for (String x: strArray) {
                x = "11";
                System.out.println(x);
            }

        }


        }
    }

