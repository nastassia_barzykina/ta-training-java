package com.epam.training.student_nastassia_barzykina.java_basics;

public class TestCompile {
    public static void main(String[] args) {
        byte b3 = 50;
        int iVal = -100;
        b3 += iVal--;
        System.out.println(b3);
        int i = 3;
        byte b = 1;
//        byte b1 = 1 + 2;                // line 1
//        short s = 304111;               // line 2 неправильное приведение типов, максимально допустимое значение для short равно 32767
//        short s1 = (short) 304111;       // line 3
//        b = b1 + 1;                     // line 4 неправильное приведение типов, результатом операции сложения будет int. К тому же b1 не объявлена как final
//        b = (byte)  (b1 + 1);           // line 5
//        b = -b;                         // line 6 неправильное приведение типов, результатом унарного отрицания будет значение типа int
//        b = (byte) −b;                  // line 7 ошибка- нет скобок, но приведение правильное!!!
//        b1 *= 2;                       // line 8
//        b = i;                          // line 9 неправильное приведение типов, нет явного преобразования
//        b = (byte) i;
//        b += i++;
//        float f = 1/2;
//        //b /= f;
//        //long x = 5;
//        //long y = 2;
//        //byte b = (byte) x / y;     // не хватает скобок!!!
    }
}
