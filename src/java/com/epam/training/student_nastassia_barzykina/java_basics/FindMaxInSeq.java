package com.epam.training.student_nastassia_barzykina.java_basics;

import java.util.Scanner;

public class FindMaxInSeq {
    /*
    программа считывает последовательность целочисленных значений из стандартного вывода и находит максимальное из них.
    Вы должны поместить свое решение в метод max, чтобы пройти тесты.

Подробности:

Вы должны считывать значения последовательности, пока следующее не станет 0. Нулевое значение означает конец входной
последовательности и не является ее элементом.
Последовательность гарантированно содержит хотя бы одно значение.
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num;
        int max = Integer.MIN_VALUE;
        while (true) {
            num = sc.nextInt();
            if (num == 0) {
                break;
            }
            max = max(num, max);


        }
        System.out.println(max);
    }

    static int max(int a, int b) {
        if (a > b) {
            return a;
        } else
            return b;
    }
}

