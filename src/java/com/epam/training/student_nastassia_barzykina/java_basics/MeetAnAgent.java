package com.epam.training.student_nastassia_barzykina.java_basics;

import java.util.Scanner;

public class MeetAnAgent {
    /*
    программа запрашивает на вход число-пароль
если оно равно секретному числу, программа выводит: "Hello, Agent"
в противном случае печатает: "Access denied".
Секретный пароль хранится в final static int password.
     */
    final static int PASSWORD = 133976;
    public static void main(String[] args) {
        System.out.print("Enter secret password: ");
        Scanner scan = new Scanner(System.in);
        int input = scan.nextInt();
        if (PASSWORD == input) {
            System.out.println("Hello, Agent");
        } else {
            System.out.println("Access denied");
        }

    }
}
