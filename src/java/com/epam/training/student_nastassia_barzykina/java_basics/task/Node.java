package com.epam.training.student_nastassia_barzykina.java_basics.task;

public class Node {
    private Car element;
    private Node next;

    public Node() {
    }

    public Car getElement() {
        return element;
    }

    public void setElement(Car element) {
        this.element = element;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }
}
