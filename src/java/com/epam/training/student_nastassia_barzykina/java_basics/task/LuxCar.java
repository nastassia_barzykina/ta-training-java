package com.epam.training.student_nastassia_barzykina.java_basics.task;

class LuxCar extends Car {
    String indexLux;
    static int numberOfLuxCar;
    LuxCar() {

    }
    LuxCar(String name, int maxSpeed, String indexLux) {
        super(name, maxSpeed);
        this.indexLux = indexLux;
        numberOfLuxCar++;
    }

    public String getIndexLux() {
        return indexLux;
    }

    public void setIndexLux(String indexLux) {
        this.indexLux = indexLux;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("LuxCar{");
        sb.append("indexLux='").append(indexLux).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    void sound() {
        System.out.println("La-la");
    }
    public static int getNumberOfLuxCar() {
        return numberOfLuxCar;
    }
}
