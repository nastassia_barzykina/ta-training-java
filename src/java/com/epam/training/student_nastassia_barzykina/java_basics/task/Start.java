package com.epam.training.student_nastassia_barzykina.java_basics.task;



class Start {
    public static void main(String[] args) {
        Factory bmw = new Factory();
        Factory lada = new Factory();
        Car bmwRed = bmw.createCar("BMW", 120);
        Car bmwBlue = bmw.createCar("BMW", 100);
        LuxCar bmwLux = bmw.createLuxCar("BMW", 160, "LUX");
        Car ladaRed = lada.createCar("Lada", 70);
        Car ladaBlue = lada.createCar("Lada", 80);
        Car ladaWhite = lada.createCar("Lada", 85);
        bmwLux.sound();
        bmwBlue.sound();
        System.out.println(bmwRed);
        System.out.println(bmwBlue);
        System.out.println(bmwLux);
        System.out.println(ladaRed);
        System.out.println(ladaBlue);
        System.out.println(ladaWhite);
        System.out.println("Всего выпущено автомобилей: " + Car.getNumOfCars());
        System.out.println("Выпущено автомобилей BMW: " + bmw.getNumOfCarsOfModel());
        System.out.println("Выпущено автомобилей Lada: " + lada.getNumOfCarsOfModel());
        System.out.println("Всего выпущено автомобилей LUX: " + LuxCar.getNumberOfLuxCar());
        Car[] array = new Car[Car.getNumOfCars()];
        array[0] = bmwRed;
        array[1] = bmwBlue;
        array[2] = bmwLux;
        array[3] = ladaRed;
        array[4] = ladaBlue;
        array[5] = ladaWhite;

        printCars(array, "Initial array of Cars:");

        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j].getMaxSpeed() > array[j+1].getMaxSpeed()) {
                    Car tmp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = tmp;
                }
            }
        }
        printCars(array, "Sorted cars:");
    }

    private static void printCars(Car[] array, String title) {
        System.out.println(title);
        for (Car car : array) {
            System.out.println(car.getMaxSpeed());
        }
    }
}
