package com.epam.training.student_nastassia_barzykina.java_basics.task;

class Factory {
    private int numOfCarsOfModel;
    Factory() {

    }

    public int getNumOfCarsOfModel() {
        return numOfCarsOfModel;
    }

    Car createCar (String name, int maxSpeed){
        Car car = new Car(name, maxSpeed);
        numOfCarsOfModel++;
        return car;
    }
    LuxCar createLuxCar (String name, int maxSpeed, String indexLux) {
        LuxCar luxCar = new LuxCar(name, maxSpeed, indexLux);
        numOfCarsOfModel++;
        return luxCar;
    }
}
