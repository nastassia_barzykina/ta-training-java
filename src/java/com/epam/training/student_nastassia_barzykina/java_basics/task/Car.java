package com.epam.training.student_nastassia_barzykina.java_basics.task;

class Car {
    private String name;
    private int maxSpeed;
    private static int numOfCars;

    Car() {


    }

    Car(String name, int maxSpeed) {
        this.name = name;
        this.maxSpeed = maxSpeed;
        numOfCars++;
    }

    public String getName() {
        return name;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public static int getNumOfCars() {
        return numOfCars;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Car{");
        sb.append("name='").append(name).append('\'');
        sb.append(", maxSpeed=").append(maxSpeed);
        sb.append('}');
        return sb.toString();
    }
    void sound() {
        System.out.println("Bi-Bi");
    }
}
