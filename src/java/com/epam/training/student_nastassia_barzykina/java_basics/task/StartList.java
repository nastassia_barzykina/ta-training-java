package com.epam.training.student_nastassia_barzykina.java_basics.task;

import java.util.ArrayList;
import java.util.List;

public class StartList {
    public static void main(String[] args) {
        Factory bmw = new Factory();
        Factory lada = new Factory();
        Car bmwRed = bmw.createCar("BMW", 120);
        Car bmwBlue = bmw.createCar("BMW", 100);
        LuxCar bmwLux = bmw.createLuxCar("BMW", 160, "LUX");
        Car ladaRed = lada.createCar("Lada", 80);
        Car ladaBlue = lada.createCar("Lada", 80);
        Car ladaWhite = lada.createCar("Lada", 85);
        bmwLux.sound();
        bmwBlue.sound();
        System.out.println(bmwRed);
        System.out.println(bmwBlue);
        System.out.println(bmwLux);
        System.out.println(ladaRed);
        System.out.println(ladaBlue);
        System.out.println(ladaWhite);
        System.out.println("Всего выпущено автомобилей: " + Car.getNumOfCars());
        System.out.println("Выпущено автомобилей BMW: " + bmw.getNumOfCarsOfModel());
        System.out.println("Выпущено автомобилей Lada: " + lada.getNumOfCarsOfModel());
        System.out.println("Всего выпущено автомобилей LUX: " + LuxCar.getNumberOfLuxCar());
        List<Car> carList = new ArrayList<>();
        carList.add(bmwRed);
        carList.add(bmwBlue);
        carList.add(bmwLux);
        carList.add(ladaRed);
        carList.add(ladaBlue);
        carList.add(ladaWhite);

        printCars(carList,"Initial array of Cars:");

        for (int i = 0; i < carList.size() - 1; i++) {
            for (int j = 0; j < carList.size() - i - 1; j++) {
                if (carList.get(j).getMaxSpeed() > carList.get(j + 1).getMaxSpeed()) {
                    Car tmp = carList.get(j);
                    carList.set(j, carList.get(j+1));// присваивание значения j+1 элемента j элементу
                    carList.set(j+1, tmp);
                }
            }
        }
        printCars(carList, "Sorted cars:");
    }

    private static void printCars(List<Car> list, String title) {
        System.out.println(title);
        for (Car car : list) {
            System.out.println(car.getMaxSpeed() + " km/h, " + car.getName());
        }
    }
}
