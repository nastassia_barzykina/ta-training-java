package com.epam.training.student_nastassia_barzykina.java_basics;

public class TestGetMethod {
    /**
     * Способ передачи аргумента в метод по значению
     * @param number
     */
    public static void start(int number) {
        System.out.println("2. Old value of \"number\" into \"start\" method is:" + number);
        number = 3;// только внутри метода!
        System.out.println("3. New value of \"number\" into \"start\" method is:" + number);
    }
    public static void main(String[] args) {
        int number = 5;
        System.out.println("1. Old value of \"number\" into \"main\" method is:" + number);
        start(number);
        System.out.println("4. New value of \"number\" into \"main\" method is:" + number);
    }
}
