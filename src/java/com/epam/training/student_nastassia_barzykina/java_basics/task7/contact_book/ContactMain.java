package com.epam.training.student_nastassia_barzykina.java_basics.task7.contact_book;

public class ContactMain {
    public static void main(String[] args) {
        Contact alex = new Contact("Alex");
        alex.addPhoneNumber(22, "123-33-44");
        alex.addPhoneNumber(55, "122-22-22");
        alex.addEmail("alex", "jjj.com");
        alex.addEpamEmail("alex", "one");
        alex.addTwitter("12345");
        alex.addSocialMedia("FB", "FB222");
        alex.rename("Alex");
        for (ContactInfo contactInfo : alex.getInfo()) {
            System.out.println(contactInfo.getTitle() + " : " + contactInfo.getValue());
        }

    }
}
