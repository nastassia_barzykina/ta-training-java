package com.epam.training.student_nastassia_barzykina.java_basics.task7.arithmetic_expressions;

import java.util.StringJoiner;

public class Expressions {

    public static Variable var(String name, int value) {// обычный метод-конструктор, не анонимный
        return new Variable(name, value);
    }

    public static Expression val(int value) {
        Expression expression = new Expression() {
            @Override
            public int evaluate() {
                return value;
            }

            @Override
            public String toExpressionString() {
                if (value < 0){
                    return "(" + value + ")";
                }
                return String.valueOf(value);
            }
        };
        return expression;
    }

    public static Expression sum(Expression... members){
        Expression result = new Expression() {
            @Override
            public int evaluate() {
                int rez = 0;
                for (Expression el : members ) {
                    rez += el.evaluate();
                }
                return rez;
            }

            @Override
            public String toExpressionString() {
                StringJoiner membersJoiner = new StringJoiner(" + ", "(", ")");
                for (Expression el : members ) {
                    membersJoiner.add("" + el.toExpressionString());
                }
                return membersJoiner.toString();
            }
        };
        return result;
    }

    public static Expression product(Expression... members){
        Expression result = new Expression() {
            @Override
            public int evaluate() {
                int rez = 1;
                for (Expression el : members ) {
                    rez *= el.evaluate();
                }
                return rez;
            }

            @Override
            public String toExpressionString() {
                StringJoiner membersJoiner = new StringJoiner(" * ", "(", ")");
                for (Expression el : members ) {
                    membersJoiner.add("" + el.toExpressionString());
                }
                return membersJoiner.toString();
            }
        };
        return result;
    }

    public static Expression difference(Expression minuend, Expression subtrahend){
        Expression result = new Expression() {
            @Override
            public int evaluate() {
                return minuend.evaluate() - subtrahend.evaluate();
            }

            @Override
            public String toExpressionString() {
                StringJoiner membersJoiner = new StringJoiner(" - ", "(", ")");
                    membersJoiner.add(minuend.toExpressionString() + " - " + subtrahend.toExpressionString());
                return membersJoiner.toString();
            }
        };
        return result;
    }

    public static Expression fraction(Expression dividend, Expression divisor){

        Expression result = new Expression() {
            @Override
            public int evaluate() {
                return dividend.evaluate() / divisor.evaluate();
            }

            @Override
            public String toExpressionString() {
                StringJoiner membersJoiner = new StringJoiner(" / ", "(", ")");
                membersJoiner.add(dividend.toExpressionString() + " / " + divisor.toExpressionString());
                return membersJoiner.toString();
            }
        };
        return result;
    }

}
