package com.epam.training.student_nastassia_barzykina.java_basics.task7.contact_book;

public class Contact {

    private String nameStr;
    private ContactInfo name;
    private ContactInfo numberPhone;
    private Email[] emails = new Email[3];
    private Social[] socials = new Social[5];

    public Contact(String contactName) {
        name = new NameContactInfo(contactName);
    }

    public void rename(String newName) {
        if (newName == null || "".equals(newName)) {
           return;
        }
        nameStr = newName;
    }

    private class NameContactInfo implements ContactInfo {

        public NameContactInfo(String str) {
             nameStr = str;
        }

        @Override
        public String getTitle() {
            return "Name";
        }

        @Override
        public String getValue() {
            return nameStr;
        }
    }

    public static class Email implements ContactInfo{
        public String localPart;
        public String domain;
        public Email(String localPart, String domain){
            this.localPart = localPart;
            this.domain = domain;
        }

        @Override
        public String getTitle() {
            return "Email";
        }

        @Override
        public String getValue() {
            return localPart + "@" + domain;
        }
    }

    public Email addEmail(String localPart, String domain) {

        var email = new Email(localPart, domain);
        for (int i = 0; i < emails.length; i++) {
            if (emails[i] == null){
                emails[i] = email;
                return email;
            }
        }
        return null;
    }

    public Email addEpamEmail(String firstname, String lastname) {
        Email epamEmail = new Email(firstname + "_" + lastname, "epam.com"){
            @Override
            public  String getTitle(){
                return "Epam Email";
            }
        };

        for (int i = 0; i < emails.length; i++) {
            if (emails[i] == null){
                emails[i] = epamEmail;
                return epamEmail;
            }
        }
        return null;
    }

    public ContactInfo addPhoneNumber(int code, String number) {// телефон добавляется только 1 раз
        if (numberPhone != null){
            return null;
        }
        ContactInfo phone = new ContactInfo() {// анонимный класс, в котором переопределяются методы
            @Override
            public String getTitle() {//заголовка
                return "Tel";
            }

            @Override
            public String getValue() {// номера телефона
                return "+" + code + " " + number;
            }
        };
        numberPhone = phone;
        return phone;
    }
    public static class Social implements ContactInfo{//
        String title;
        String id;
        public Social(String title, String id){
            this.title = title;
            this.id = id;
        }

        @Override
        public String getTitle() {
            return this.title;
        }

        @Override
        public String getValue() {
            return this.id;
        }
    }

    public Social addTwitter(String twitterId) {
        var twitter = new Social("Twitter", twitterId);
        return (Social) addToArray(twitter, socials);
    }

    private ContactInfo addToArray(ContactInfo info, ContactInfo[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == null) {
                array[i] = info;
                return info;
            }
        }
        return null;
    }

    public Social addInstagram(String instagramId) {
        var instagram = new Social("Instagram", instagramId);
        return (Social) addToArray(instagram, socials);
    }

    public Social addSocialMedia(String title, String id) {
        var socialMedia = new Social(title, id);
        return (Social) addToArray(socialMedia, socials);
    }

    public ContactInfo[] getInfo() {
        int countPhone = 0;
        if (numberPhone != null){
            countPhone++;
        }
        int countEmail = 0;
        for (int i = 0; i < emails.length; i++) {
            if (emails[i] != null){
                countEmail++;
            }
        }
        int countSocial = 0;
        for (int i = 0; i < socials.length; i++) {
            if (socials[i] != null){
                countSocial++;
            }
        }

        ContactInfo[] result = new ContactInfo[1 + countPhone + countEmail + countSocial];
        int count = 0;

        result[count++] = name;

        if (countPhone > 0){
            result[count++] = numberPhone;
        }

        for (int i = 0; i < countEmail; i++) {
            result[count++] = emails[i];
        }

        for (int i = 0; i < countSocial; i++) {
            result[count++] = socials[i];
        }

        return result;
    }

}
