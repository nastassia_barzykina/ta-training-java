package com.epam.training.student_nastassia_barzykina.java_basics.task7.contact_book;

public interface ContactInfo {
    String getTitle();
    String getValue();
}
