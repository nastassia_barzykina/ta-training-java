package com.epam.training.student_nastassia_barzykina.java_basics.task7.arithmetic_expressions;

public interface Expression {
    int evaluate();
    String toExpressionString();
}
