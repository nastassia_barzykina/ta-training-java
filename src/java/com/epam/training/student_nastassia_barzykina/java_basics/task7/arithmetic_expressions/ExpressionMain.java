package com.epam.training.student_nastassia_barzykina.java_basics.task7.arithmetic_expressions;

import static com.epam.training.student_nastassia_barzykina.java_basics.task7.arithmetic_expressions.Expressions.difference;
import static com.epam.training.student_nastassia_barzykina.java_basics.task7.arithmetic_expressions.Expressions.fraction;

public class ExpressionMain {
    public static void main(String[] args) {
        Variable a = Expressions.var("a", 2);
        Variable b = Expressions.var("b", 5);
        var sumAB = Expressions.sum(a, b);
        System.out.println(sumAB.toExpressionString());
        System.out.println(sumAB.evaluate());
        var prodAB = Expressions.product(a, b);
        System.out.println(prodAB.toExpressionString());
        System.out.println(prodAB.evaluate());
        var difAB = Expressions.difference(a, b);
        System.out.println(difAB.toExpressionString());
        System.out.println(difAB.evaluate());
        Expression c = Expressions.val(3);
        Expression d = Expressions.val(4);
        System.out.println();
        var frAB = fraction(c, d).evaluate();
        System.out.println(fraction(c, d).toExpressionString());
        System.out.println(frAB);
        Expression fraction;

        fraction = Expressions.fraction(Expressions.var("a", 72), Expressions.val(9));
        System.out.println(fraction.toExpressionString());

        Expression expression =
                Expressions.product(
                        Expressions.val(1),
                        Expressions.product(
                                Expressions.val(2),
                                Expressions.product(
                                        Expressions.val(3),
                                        Expressions.product(
                                                Expressions.val(4),
                                                Expressions.val(5)
                                        )
                                )
                        )
                );
        System.out.println(expression.toExpressionString());
        Variable x1 = Expressions.var("x1", 7);
        Variable x2 = Expressions.var("x2", 7);
//        Variable x3 = (Variable)Expressions.val(5);
//        x3.setValue(100);
        fraction = fraction(x1, x2);
        System.out.println(fraction.toExpressionString());
        System.out.println(fraction.evaluate());
        x1.setValue(343);
        x2.setValue(-7);
        Expression dif = difference(x1, x2);
        System.out.println(fraction.toExpressionString());
        System.out.println(fraction.evaluate());
        System.out.println(dif.evaluate());





    }
}
