package com.epam.training.student_nastassia_barzykina.java_basics;

/**
 * пример описания класса Car и создания его объектов. В классе приведено объявление поля carModel и метода доступа к нему
 * getCarModel(). При использовании ключевого слова new система выделяет необходимый объем памяти для нового объекта в "куче".
 * Затем вызывает конструктор для инициализации объекта (установки его полей в начальные значения). После этого поля и
 * методы объекта становятся доступными через полученную ссылку на объект
 */
class Car {
    /**
     * Закрытые конструкторы могут вызываться из других конструкторов или из статических методов этого же класса.
     * конструктор без параметров и с одним параметром вызывают конструктор с двумя параметрами. Такой синтаксис
     * позволяет избежать дублирования кода инициализации полей экземпляра.
     */
    private String model;
    private String brand;
    public Car() {
        this("Camry", "Toyota");//вызов другого конструктора этого же класса
        System.out.println("Init");
        System.out.println(model + brand);
    }
    public Car(String model) {
        this(model, "Toyota");// вариант 2
    }
    public Car(String model, String brand) {
        this.model = model;
        this.brand = brand;
    }
    private  String carModel;
    //    public Car (String carModel){
    //    this.carModel = carModel;
    //}
    public  String getCarModel() {
        return carModel;
    }
}
public class Demo {
    public static void main(String[] arg) {
        Car car1 = new Car("bmw");
        Car car2 = null;
//        car1 = null;
//        car2.getCarModel();
        Car car3 = new Car();
        printModelOfCar(car3);
        String name = car1.getCarModel();
        System.out.println(name);
        System.out.println(null == null);
        System.out.println(car3 == null);
        System.out.println(car1 != car2);
    }

    private static void printModelOfCar(Car car) {
        System.out.println("Model - " + car.getCarModel());
    }
}
