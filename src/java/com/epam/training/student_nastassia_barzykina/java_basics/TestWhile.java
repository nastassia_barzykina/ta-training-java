package com.epam.training.student_nastassia_barzykina.java_basics;

public class TestWhile {
    public static void main(String[] args) {
        int i = 0;
        while (i < 5) {
            System.out.println("Iteration: " + i);
            i++;
        }
        System.out.println(i);
        int j = 0;
        do {
            System.out.println("Iteration: " + j);
            j++;
        } while (j > 5 && j < 10);
        System.out.println(j);
        // оператор break заставляет остановить исполнение операторов текущего блока кода и возобновить исполнение
        // программы после него. Например, создается бесконечный цикл и выполняется выход из него, когда наступает
        // следующая ситуация: переменная k получает значение больше 6:
        int k = 0;
        while (true) {
            if (k > 6) {
                break;
            }
            System.out.println(k++);
        }
        // continue обеспечивает переход к следующей итерации цикла, не завершая исполнения оставшихся операторов текущей
        // итерации цикла. Например, создается цикл на 10 итераций, в котором проверяется переменная p на кратность значению
        // 4 или 8: если переменная p не кратна этим величинам, то выводится ее значение. В противном случае это действие
        // пропускается для текущей итерации
        //
        int p = 0;
        while (p++ < 10) {
            if ((p == 4) || (p == 8)) {
                continue;
            }
            System.out.println(p);
        }
        int m = 0;
        outer:
        while (m < 5) {
            int n = 0;
            while (n < 4) {
                n++;
                if (n == 2) {
                    break outer;
                }
                System.out.println("m=" + m + ", n=" + n);
            }
        }
        int w = 0;
        outer: while (w++ < 3) {
            int y = 0;
            while (y++ < 5) {
                if (y == 2) {
                    continue outer;
                }
                System.out.println("w = " + w + "; y = " + y);
            }
        }
        /*
        цикл FOR
        В параметрах цикла инициализируются две переменные (a и b). От их значений зависит количество итераций цикла.
        После каждой итерации значения переменных a и b изменяются. Когда значения пересекутся – цикл завершится.
         */
        int a, b;
        for (a = 1, b = 4; a < b; a++, b--) {
            System.out.println("\n Начало итерации");
            System.out.println("a = " + a);
            System.out.println("b = " + b);
            System.out.println("Завершение итерации");
        }
    }

    }
