package com.epam.training.student_nastassia_barzykina.java_basics;

import javax.imageio.IIOException;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class ReadConsoleStream {
    public static void main(String[] args) throws IOException {
        // чтение через сканнер:
        Scanner scSc = new Scanner(System.in);
        int input;// для вводимого числа
        int avg = 0;//для среднего значения

        for (int i = 0; ; i++) {//бесконечный цикл
            input = scSc.nextInt();
            avg += input;// avg+input
            if (input == 0) {//условие выхода из цикла
                avg = avg / i;
                break;
            }
        }
        System.out.println(avg);
// чтение файла через его путь построчно:
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String sourceFileName = reader.readLine();
        FileInputStream fileInputStream = new FileInputStream(sourceFileName);

        Scanner scanner = new Scanner(fileInputStream);
        StringBuilder builder = new StringBuilder();
        while (scanner.hasNextLine()) {
            builder.append(scanner.nextLine()).append("\n");
        }

        System.out.print(builder.toString());

        scanner.close();
        reader.close();

        //
        Scanner sc = new Scanner(System.in);//C:\test\project\for_path\file_test.txt
//        //String path = sc.nextLine();
//        //File file = new File(path);
//        FileInputStream fis = new FileInputStream(sc.nextLine());
//        InputStreamReader isr = new InputStreamReader(fis);
//        BufferedReader buff = new BufferedReader(isr);
//        while (buff.ready()) {
//            String line = buff.readLine();
//            System.out.println(line);
//
//        }
//        //buff.lines().forEach(x -> System.out.println(x));// через лямбду - вместо while
//        buff.close();
//        isr.close();
//        fis.close();
        //аналог сканнера:
        BufferedReader reader11 = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String name = reader11.readLine();
            if (name.isEmpty()) break;
        }
    }
}
