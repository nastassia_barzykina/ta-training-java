package com.epam.training.student_nastassia_barzykina.java_basics;

import java.util.Scanner;

public class GoDutch {
    /*
    Дано следующее условие: компания друзей посещает ресторан. Они решили разделить счет поровну и добавить 10 процентов от
    общей суммы счета в качестве чаевых. Далее друзья равными частями покрывают общую сумму платежа.
Перейдите в класс GoDutch и напишите программу, которая считывает общую сумму счета и количество друзей, а затем выводит
 размер части оплаты.

Рассмотрим некоторые детали:
Программа должна читать данные из System.in
Общая сумма счета не может быть отрицательной. Если введенное значение отрицательное, программа останавливается и печатает:
Bill total amount cannot be negative
Количество друзей не может быть отрицательным или нулевым. Если такое происходит, то программа останавливается и выводит:
Number of friends cannot be negative or zero
Общая сумма счета, количество друзей и часть к оплате являются целыми числами.
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int sum = sc.nextInt();
        int friends = sc.nextInt();
        if (sum < 0) {
            System.out.println("Bill total amount cannot be negative");
        } else if (friends <= 0) {
            System.out.println("Number of friends cannot be negative or zero");
        } else {
            int totalAmount = sum + sum / 10;
            int amount = totalAmount / friends;
            System.out.println(amount);
        }
    }
}
