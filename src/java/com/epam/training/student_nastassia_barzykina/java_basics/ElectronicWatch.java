package com.epam.training.student_nastassia_barzykina.java_basics;

import java.util.Scanner;

public class ElectronicWatch {
    public static void main(String[] args) {
        System.out.print("Enter number of seconds: ");
        Scanner scan = new Scanner(System.in);
        int input = scan.nextInt();
        int tmp = input % 86400;    // секунды в текущих сутках
        int h = input / 3600;       // определение целых часов
        h = h % 24;                 // часы в текущих сутках
        tmp = tmp % 3600;           // секунды за вычетом часов
        int mm = tmp / 60;          // количество минут
        int ss = tmp % 60;          // количество секунд
        System.out.printf("%d:%0,2d:%0,2d", h, mm, ss);


    }
}
