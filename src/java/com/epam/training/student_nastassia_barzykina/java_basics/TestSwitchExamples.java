package com.epam.training.student_nastassia_barzykina.java_basics;

public class TestSwitchExamples {
    public static void main(String[] args) {

        int num = 10;

        while (++num > 20) { //В условии цикла будет выполнено сравнение 11>20. По итогам сравнения вход в цикл не будет
            // осуществлен и после цикла будет выведено число 11.
            num++;
        }
        System.out.println("num = " + num);
        var number = 3;
        switch (number) {
            case 1 -> number *= 2;
            case 2 -> number *= 3;
            case 3, 5 -> number *= 4;
            default -> {
                System.out.println("out of range" + number);
            }
        }
        System.out.println("number = " + number);
        /*
        Вместо нескольких инструкций case появилась возможность перечислять их через запятую:
         */
        int value = 1;
        switch (value) {
            case 1, 2, 3, 4 -> System.out.println("1, 2, 3 or 4");
            case 777 -> System.out.println("Range: " + value);
            case 0 -> System.out.println("0");
            default -> System.out.println("Default");
        }
        System.out.println("Example of switch:");
        TestSwitchExamples b = new TestSwitchExamples();
//        b.defineLevel("test");
//        b.defineLevel("admin");
        System.out.println(b.defineLevel("test"));
        System.out.println(b.defineLevel("guest"));
//        int rez = defineLevel("test");
//        System.out.println(rez);
//        int rez2 = defineLevel("admin");
//        System.out.println(rez2);
    }


    //    /*
//    int метод defineLevel, передающий строковую переменную role
//    в метод switch (метод возвращает целочисл значение)
//     */
//    public int defineLevel(String role) {
//        return switch (role) {
//            case "guest" -> 0;
//            case "client" -> 1;
//            case "moderator" -> 2;
//            case "admin" -> 3;
//            default -> {
//                System.out.println("non-authentic role = " + role);
//                yield -1;
/*
или           default: throw new IllegalArgumentException ("non-authentic role = " + role);
 */
//            }
//        };
//    }
//    /*
//    прямое присваивание оператора switch (метод в методе)
//     */
    public static int defineLevel(String role) {
        var result = switch (role) {
            case "guest" -> 0;
            case "client" -> 1;
            case "moderator" -> 2;
            case "admin" -> {
                System.out.println("role = " + role);
                yield 3;
            }
            default -> {
                System.out.println("non-authentic role = " + role);
                yield -1; /* если в блоке есть хоть 1 оператор */
            }
        };
        return result;
    }

}
//   /*
//    Возвращаемое значение оператора switch можно также использовать в выражениях case:
//     */
//    System.out.println(
//            switch (value) {
//        case 2, 3, 4 -> "2,3 or 4";
//        case 777 -> "Range: " + value;
//        case 0 -> "0";
//        default -> "Default";
//    });
//
//}
