package com.epam.training.student_nastassia_barzykina.java_basics;

import java.util.Scanner;

public class InputStringProgram {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String input = scan.next();
        System.out.println("Hello, " + input);
    }
}
