package com.epam.training.student_nastassia_barzykina.java_basics;

import java.util.Scanner;

public class InputProgram {
    public static void main(String[] args) {
        int x;
        Scanner sc = new Scanner(System.in);
        x = sc.nextInt();
        System.out.printf("%0,2d:%0,2d:%0,2d", x, x, x);

    }
}
