package com.epam.training.student_nastassia_barzykina.java_basics;

public class Test {
    /*
    операторы
     */
    public static void main(String[] args) {
        int a = 10;
        int b = 20;
        String s = "Hello";
        long l = 99;
        //double d = 1.11;
        int i = 1;
        int j = 0;
        j = i<<l;
        int c = 25;
        int d = 25;
        System.out.println("a + b  = " + (a + b));
        System.out.println("a - b  = " + (a - b));
        System.out.println("a * b  = " + (a * b));
        System.out.println("b / a  = " + (b / a));
        System.out.println("b % a  = " + (b % a));
        System.out.println("c % a  = " + (c % a));
        System.out.println("a++    = " +  (a++));
        System.out.println("a--    = " +  (a--));
        System.out.println("d++    = " +  (d++));
        System.out.println("++d    = " +  (++d));
        System.out.println("a += b = " + (a += b));
        System.out.println("a      = " + (a));
        System.out.println("a -= b = " + (a -= b));
        System.out.println("a      = " + (a));

        System.out.println("a & b   = " + (a & b)); // 12  = ... 0000 1100
        System.out.println("a | b   = " + (a | b)); // 61  = ... 0011 1101
        System.out.println("a ^ b   = " + (a ^ b)); // 49  = ... 0011 0001
        System.out.println("~a      = " + ~a); // -61 = 1111 1111 1111 1111 1111 1111 1100 0011
        System.out.println("a << 2  = " + (a << 2)); // 240 = ... 1111 0000
        System.out.println("a >> 2  = " + (a >> 2)); // 15  = ... 0000 1111
        System.out.println("a >>> 2 = " + (a >>> 2)); // 15  = ... 0000 1111
        System.out.println(j);
        System.out.println(010 | 4);
        int experience = 5;
        int requirements = 10;
        String result = (experience > requirements) ? "Accept to project" : "Learn more" ;
        System.out.println( result );
        System.out.println("___________");
        /**
         * вызов метода с аргументами
         */
        byte p = 5;
        Test test = new Test();
        long ss = test.pow(2, p);// или test.pow(2, (byte) 5); -- явное приведение типа
        System.out.println(ss);
    }
    long pow(int x, byte p) {
        long result = x;
        int counter = 1;
        while (counter++ < p){
            result = result * x;
        }
        return result;
    }
}
