package com.epam.training.student_nastassia_barzykina.java_basics;

import java.util.Scanner;

public class Snail {
    /*
    Дано следующее условие: улитка поднимается по дереву на a футов в день. Затем каждую ночь улитка сползает вниз на b футов. Высота дерева — h футов.
Перейдите к классу Snail и напишите программу с подсчетом количества дней, которые потребуются улитке, чтобы добраться до вершины дерева.
Программа читает a, b, h построчно. Входные значения гарантированно являются положительными целыми числами.
Если улитка не может добраться до вершины дерева, должно выводиться сообщение: Impossible.
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int b = scan.nextInt();
        int h = scan.nextInt();

        if (a >= h) {
            System.out.println("1");
        } else if (a <= b) {
            System.out.println("Impossible");
        } else {
            int countDay;
            countDay = (h - a) + b;
            int i = 0;
            do {
                countDay = (countDay - a) + b;
                i++;
            } while (countDay >= b);

            System.out.println(i);
        }
    }
}

