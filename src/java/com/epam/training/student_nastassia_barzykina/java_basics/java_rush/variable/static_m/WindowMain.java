package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.variable.static_m;

public class WindowMain {
    public static void main(String[] args) {
        Window window = new Window();
        window.printSize();
        window.changeSize(4, 5);
        window.printSize();
    }
}
