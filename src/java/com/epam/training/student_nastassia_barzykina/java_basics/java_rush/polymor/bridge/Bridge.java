package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.bridge;

public interface Bridge {
    int getCarsCount();
}
