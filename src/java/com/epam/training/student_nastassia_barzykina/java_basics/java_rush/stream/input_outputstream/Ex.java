package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Ex {
    public static void main(String[] args) {
//        Вывод строки на консоль
        System.out.println("Hello");
//        Сохранили поток вывода на консоль в отдельную переменную.
//        Выводим в поток строку.
        PrintStream console = System.out;
        console.println("Hello from PrintStream");
//        Создали динамический (растягивающийся) массив байт в памяти.
//                Связали его с новым потоком вывода – объектов PrintStream
//        Выводим в поток строку.
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        PrintStream consolePS = new PrintStream(stream);
        consolePS.println("Hello from BAOS and PR");
    }
}
