package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex.voice;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList pets = new ArrayList<>();
        pets.add(new Fish());
        pets.add(new Cat());
        pets.add(new Dog());
        for (Object pet : pets){
            if (pet instanceof Sounding){
                Sounding voice = (Sounding) pet;
                voice.sound();
            }
        }

    }
}
