package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.array;

import java.util.Scanner;

/**
 * Считывает из консоли число N — количество строк массива (считай, что это число будет больше 0, можно не проверять).
 * Считывает N чисел из консоли (любые числа, которые будут больше 0, можно не проверять).
 * Инициализирует двумерный массив multiArray:
 * количеством строк N;
 * строки — массивами, размер которых соответствует числам, введенным в п.2 (в порядке ввода).
 * Пример:
 *
 * Введено число 5.
 * Введены числа 1, 7, 5, 9, 3.
 * Получаем такой массив:
 * []
 * [][][][][][][]
 * [][][][][]
 * [][][][][][][][][]
 * [][][]
 */
public class MultiArray {
    public static int[][] multiArray;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();// число строк массива
        multiArray = new int[n][];
        for (int i = 0; i < multiArray.length; i++){
            multiArray[i] = new int[sc.nextInt()];
        }
        for (int i = 0; i < multiArray.length; i++){
            for ( int j = 0; j < multiArray[i].length; j++){
                System.out.print("[]");
            }
            System.out.println();
        }
    }
}
