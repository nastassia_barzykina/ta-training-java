package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.string;

import java.util.Arrays;
import java.util.List;

public class ListStringEx {
    public static void main(String[] args) {
        List<String> people = Arrays.asList(
                "Philip J. Fry",
                "Turanga Leela",
                "Bender Bending Rodriguez",
                "Hubert Farnsworth",
                "Hermes Conrad",
                "John D. Zoidberg",
                "Amy Wong"
        );
        System.out.println(people.get(1));

        String peopleString = String.join("", people);
        System.out.println(peopleString);
    }
}
