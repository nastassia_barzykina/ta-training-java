package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.task_exeption;

public class DogIsNotReadyException extends Exception{
    DogIsNotReadyException(String message){
        super(message);
    }
}
