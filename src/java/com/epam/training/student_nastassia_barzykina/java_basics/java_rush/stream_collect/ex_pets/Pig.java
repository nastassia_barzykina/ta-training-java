package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream_collect.ex_pets;

public class Pig extends Animal{
    public Pig(String name, Color color, int age) {
        super(name, color, age);
    }
}
