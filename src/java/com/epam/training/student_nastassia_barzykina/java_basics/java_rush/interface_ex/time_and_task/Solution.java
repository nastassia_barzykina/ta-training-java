package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex.time_and_task;

import java.util.ArrayList;

public class Solution {
    public static ArrayList<Runnable> list = new ArrayList<>();
    public static void main(String[] args) {
        addToList(new Car());
        addToList(new Car());
        addToList(new Car());
        addToList(new Car());
        addToList(new Plane());

        runList();
    }

    public static void addToList(Runnable runnable) {
        list.add(runnable);
    }

    public static void runList() {
        for (Runnable el : list)
            el.run();
    }
}
