package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex.time_and_task;

public class Car implements Runnable{
    @Override
    public void run() {
        System.out.println("Car is running!");
    }
}
