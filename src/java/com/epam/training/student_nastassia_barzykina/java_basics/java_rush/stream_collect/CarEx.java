package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream_collect;

import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Stream;

public class CarEx {
    private String model;
    private boolean isElectric;

    public CarEx(String model, boolean isElectric) {
        this.model = model;
        this.isElectric = isElectric;
    }

    public String getModel() {
        return model;
    }

    public boolean isElectric() {
        return isElectric;
    }

    @Override
    public String toString() {
        return "Car " + model + ", electric - " + isElectric;
    }
}
class Main {
    public static void main(String[] args) {
        var cars = new ArrayList<CarEx>();
        Collections.addAll(cars,
                new CarEx("Range Rover", false),
                new CarEx("Model S", true),
                new CarEx("Navigator", false),
                new CarEx("Model 3", true),
                new CarEx("Camaro", false),
                new CarEx("Escalade", false),
                new CarEx("Mustang", false),
                new CarEx("Model X", true),
                new CarEx("X5", false),
                new CarEx("Model Y", true));

        var carStream = onlyElectricCars(cars);
        carStream.forEach(System.out::println);

        // повторы слов:
        var words = new ArrayList<String>();
        Collections.addAll(words, "чтобы", "стать", "программистом", "нужно", "программировать",
                "а", "чтобы", "программировать", "нужно", "учиться");

        Stream<String> distinctWords = getDistinctWords(words);
        distinctWords.forEach(System.out::println);
    }

    public static Stream<CarEx> onlyElectricCars(ArrayList<CarEx> cars) {
        return cars.stream()
                .filter(CarEx::isElectric);
    }
    public static Stream<String> getDistinctWords(ArrayList<String> words) {
        return words.stream().
                distinct();
    }
}
