package com.epam.training.student_nastassia_barzykina.java_basics.java_rush;

import java.util.Scanner;

/**
 * Ввести с клавиатуры строку и число number, которое больше 0 и меньше 5.
 * Вывести на экран строку number раз с помощью цикла do-while. Каждое значение нужно вывести с новой строки.
 * Если число number меньше/равно 0 или больше/равно 5, то введенную строку нужно вывести на экран один раз.
 */

public class NumAndLine {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String txt = sc.nextLine();
        int number = sc.nextInt();
//        if (number <= 0 || number >= 5) number = 1;
//        int count = 0;
//        do {
//            System.out.println(txt);
//            count++;
//        } while (number > count);
        do {
            System.out.println(txt);
            number--;
        } while (number > 0 && number < 4);

        System.out.println("Площадь круга");
        int radius = sc.nextInt();
        double PI = 3.14;
        int s = (int)(PI * radius * radius);
        System.out.println(s);

        System.out.println("Задача про колу");
        int num = sc.nextInt();
        int people = sc.nextInt();
        //double res = num * 1.0 / people;
        //System.out.println(res);
        System.out.println(num * 1.0 / people);

        System.out.println("TEST");
        for(int i = 0; i < 10; i++) {
            System.out.println(i);
            continue;
        }

    }
    }
