package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex.task_flyable;

public class Test {
    public static void main(String[] args) {
        System.out.println(getObjectType(new Cat()));
        System.out.println(getObjectType(new Tiger()));
        System.out.println(getObjectType(new Lion()));
        System.out.println(getObjectType(new Bull()));
        System.out.println(getObjectType(new Cow()));
        System.out.println(getObjectType(new Animal()));
    }

    public static String getObjectType(Object o) {
        if (!(o instanceof Animal))
            return null;
        return o.toString();
    }

    public static class Cat extends Animal {
        @Override
        public String toString() {
            return "Кот";
        }
    }

    public static class Tiger extends Cat {
        @Override
        public String toString() {
            return "Тигр";
        }
    }

    public static class Lion extends Cat {
    }

    public static class Bull extends Animal {
    }

    public static class Cow extends Animal {
    }

    public static class Animal {
        @Override
        public String toString() {
            return this.getClass().getSimpleName();
        }
    }
}
