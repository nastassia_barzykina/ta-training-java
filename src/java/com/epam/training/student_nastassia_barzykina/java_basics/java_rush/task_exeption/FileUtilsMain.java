package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.task_exeption;
import java.io.FileNotFoundException;
import java.nio.file.FileSystemException;
/**
 * В методе copyFile перехвати исключения, которые бросают методы readFile и writeFile.
 * Перехваченные исключения оберни в RuntimeException и пробрось дальше.
 *
 * Требования:
 * •	В методе copyFile должны перехватываться FileNotFoundException и FileSystemException.
 * •	Все перехваченные исключения нужно оборачивать в RuntimeException и пробрасывать дальше.
 * •	У метода copyFile не должно быть исключений в секции throws.
 */
public class FileUtilsMain {
    public static void main(String[] args) {
        copyFile("book.txt", "book_final_copy.txt");
        copyFile("book_final_copy.txt", "book_last_copy.txt");
    }

    static void copyFile(String sourceFile, String destinationFile)  {
        try {
            FileUtils.readFile(sourceFile);
            FileUtils.writeFile(destinationFile);
        } catch (FileNotFoundException e){
            throw new RuntimeException(e);
        } catch (FileSystemException e){
            throw new RuntimeException(e);
        }
        //напишите тут ваш код
    }
}
