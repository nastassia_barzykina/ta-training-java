package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.enums;

import java.util.Arrays;

/**
 * getWinterMonths(), getSpringMonths(), getSummerMonths(), getAutumnMonths() которые будут возвращать массив с тремя месяцами.
 */
public enum Month {
    JANUARY,
    FEBRUARY,
    MARCH,
    APRIL,
    MAY,
    JUNE,
    JULY,
    AUGUST,
    SEPTEMBER,
    OCTOBER,
    NOVEMBER,
    DECEMBER;
    public static Month[] getWinterMonths(){
        return new Month []{DECEMBER, JANUARY, FEBRUARY};//так тоже можно
    }
    public static Month[] getSpringMonths(){
//        ArrayList<Month> list = new ArrayList<>();
//        Collections.addAll(list, Month.values()[2], Month.values()[3], Month.values()[4]);
        return new Month []{Month.MARCH, Month.APRIL, Month.MAY};
    }
    public static Month[] getSummerMonths(){
//        ArrayList<Month> list = new ArrayList<>();
//        Collections.addAll(list, Month.values()[5], Month.values()[6], Month.values()[7]);
        return new Month []{Month.JUNE, Month.JULY, Month.AUGUST};
    }
    public static Month[] getAutumnMonths(){
        //        ArrayList<Month> list = new ArrayList<>();
//        Collections.addAll(list, Month.values()[8], Month.values()[9], Month.values()[10]);
        return new Month []{Month.SEPTEMBER, Month.OCTOBER, Month.NOVEMBER};
    }

}
