package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods.statics;

public class ExcepStatic {
    public static int A = 0;

    static {
//        if (A == 0)
//            throw new RuntimeException();
        if (true) throw new RuntimeException("");
        //throw an exception here - выбросьте эксепшн тут
    }

    public static int B = 5;

    public static void main(String[] args) {
        System.out.println(B);
    }
}
