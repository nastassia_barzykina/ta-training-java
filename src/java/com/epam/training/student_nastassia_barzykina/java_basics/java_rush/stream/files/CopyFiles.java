package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.files;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

/**
 * Напиши программу, которая будет считывать с клавиатуры пути к двум директориям и копировать файлы из одной директории в
 * другую (только файлы, директории игнорируй).
 * Используй соответствующие методы класса Files: newDirectoryStream(), isRegularFile() или isDirectory(), copy().
 */
public class CopyFiles {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        Path sourceDirectory = Path.of(scanner.nextLine());
        Path targetDirectory = Path.of(scanner.nextLine());
        try (DirectoryStream<Path> filesCopy = Files.newDirectoryStream(sourceDirectory)) {
            for (Path path : filesCopy) {
                if (Files.isRegularFile(path)) {
                    Files.copy(path, targetDirectory.resolve(path.getFileName()));
                }
            }
        }
//        Path resolve = targetDirectory.resolve(path.getFileName()); - получение полного пути с именем файла для дальнейшего копирования или перемещения
//                    Files.copy(path, resolve);
//        try (DirectoryStream<Path> files = Files.newDirectoryStream(targetDirectory)) {
//            for (Path paths : files)
//                System.out.println(paths);
//        }
    }
}
