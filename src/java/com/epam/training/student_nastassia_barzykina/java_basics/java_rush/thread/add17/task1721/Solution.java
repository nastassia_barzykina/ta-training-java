package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread.add17.task1721;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Сделать метод joinData транзакционным, т.е. если произошел сбой, то данные не должны быть изменены.
 * 1. Считать с консоли 2 имени файла.
 * 2. Считать построчно данные из файлов. Из первого файла - в allLines, из второго - в forRemoveLines.
 * В методе joinData:
 * 3. Если список allLines содержит все строки из forRemoveLines, то удалить из списка allLines все строки,
 * которые есть в forRemoveLines.
 * 4. Если условие из п.3 не выполнено, то:
 * 4.1. очистить allLines от данных
 * 4.2. выбросить исключение CorruptedDataException
 * Метод joinData должен вызываться в main. Все исключения обработайте в методе main.
 * Не забудь закрыть потоки.
 * <p>
 * Требования:
 * •	Класс Solution должен содержать public static поле allLines типа List<String>.
 * •	Класс Solution должен содержать public static поле forRemoveLines типа List<String>.
 * •	Класс Solution должен содержать public void метод joinData() который может бросать исключение CorruptedDataException.
 * •	Программа должна считывать c консоли имена двух файлов.
 * •	Программа должна считывать построчно данные из первого файла в список allLines.
 * •	Программа должна считывать построчно данные из второго файла в список forRemoveLines.
 * •	Метод joinData должен удалить в списке allLines все строки из списка forRemoveLines, если в allLines содержатся
 * все строки из списка forRemoveLines.
 * •	Метод joinData должен очистить список allLines и выбросить исключение CorruptedDataException, если в allLines не
 * содержатся все строки из списка forRemoveLines.
 * •	Метод joinData должен вызываться в main.
 */

public class Solution {
    public static List<String> allLines = new ArrayList<String>();
    public static List<String> forRemoveLines = new ArrayList<String>();

    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String firstFileName = null;//"c:\\test\\chartest.txt";//
        String secondFileName = null;
        try {
            firstFileName = br.readLine();
            secondFileName = br.readLine();//"c:\\test\\test.txt";//
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            BufferedReader fileReader1 = new BufferedReader(new InputStreamReader(new FileInputStream(firstFileName)));
            while (fileReader1.ready()) {
                allLines.add(fileReader1.readLine());
            }
        } catch (FileNotFoundException e) {
            System.out.println("File " + firstFileName + " not found");
        } catch (IOException e) {
            System.out.println("Can't Read File " + firstFileName);
        }
        try {
            BufferedReader fileReader2 = new BufferedReader(new InputStreamReader(new FileInputStream(secondFileName)));
            while (fileReader2.ready()) {
                forRemoveLines.add(fileReader2.readLine());
            }
        } catch (FileNotFoundException e) {
            System.out.println("File " + secondFileName + " not found");
        } catch (IOException e) {
            System.out.println("Can't Read File " + secondFileName);
        }
        try {
            new Solution().joinData();
        } catch (CorruptedDataException ex) {
            System.out.println("Transaction failed");
        }
    }

    public void joinData() throws CorruptedDataException {
        if (allLines.containsAll(forRemoveLines)) {
            allLines.removeAll(forRemoveLines);
            return;
        } else {
            allLines.clear();
            throw new CorruptedDataException();
        }
    }
}
