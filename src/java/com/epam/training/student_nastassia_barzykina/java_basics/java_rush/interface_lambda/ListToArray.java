package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_lambda;

import java.util.ArrayList;
import java.util.Collections;

public class ListToArray {
    public static void main(String[] args) {
        var strings = new ArrayList<String>();
        Collections.addAll(strings, "Ты", "ж", "программист");
        var s = new ArrayList<String>();
        //через приведение типов:
        Collections.addAll(s, "s", "nnn", "hhh");
        String[] stringArray1 = s.toArray(new String[0]);
        for (String string : stringArray1) {
            System.out.println(string);
        }

        var integers = new ArrayList<Integer>();
        Collections.addAll(integers, 1000, 2000, 3000);


        String[] stringArray = toStringArray(strings);
        for (String string : stringArray) {
            System.out.println(string);
        }

        Integer[] integerArray = toIntegerArray(integers);
        for (Integer integer : integerArray) {
            System.out.println(integer);
        }
    }

    public static String[] toStringArray(ArrayList<String> strings) {
        return strings.toArray(String[]::new);// обход стирания типов
//        return new String[]{};
    }

    public static Integer[] toIntegerArray(ArrayList<Integer> integers) {
        return integers.toArray(Integer[]::new);
    }
}
