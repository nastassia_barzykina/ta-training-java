package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.add18;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * CRUD для таблицы внутри файла.
 * Напиши программу, которая считывает с консоли путь к файлу для операций CRUD и при запуске в зависимости от флага, переданного в параметрах обновляет данные товара с заданным id или производит физическое удаление товара с заданным id (удаляет из файла все данные, которые относятся к переданному id).
 * -u id productName price quantity
 * -d id
 * <p>
 * Значения параметров:
 * -u - флаг, который означает обновление данных товара с заданным id
 * -d - флаг, который означает физическое удаление товара с заданным id (из файла удаляются все данные, которые относятся к переданному id)
 * id - id товара, 8 символов
 * productName - название товара, 30 символов
 * price - цена, 8 символов
 * quantity - количество, 4 символа
 * <p>
 * В файле данные хранятся в следующей последовательности (без разделяющих пробелов):
 * id productName price quantity
 * <p>
 * Данные дополнены пробелами до их длины.
 * <p>
 * Для чтения и записи файла нужно использовать FileReader и FileWriter соответственно.
 * <p>
 * Пример содержимого файла:
 * 19847   Шорты пляжные синие           159.00  12
 * 198479  Шорты пляжные черные с рисунко173.00  17
 * 19847983Куртка для сноубордистов, разм10173.991234
 * <p>
 * Требования:
 * •	Программа должна считать имя файла для операций CRUD с консоли.
 * •	При запуске программы без параметров список товаров должен остаться неизменным.
 * •	При запуске программы с параметрами "-u id productName price quantity" должна обновится информация о товаре в файле.
 * •	При запуске программы с параметрами "-d id" строка товара с заданным id должна быть удалена из файла.
 * •	Созданные для файлов потоки должны быть закрыты.
 */
public class Task1828 {
    static boolean flag = false;
    //"C:\test\task.txt"
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String file =  br.readLine();//"C:\\test\\task.txt";
        List<String> list = new ArrayList<>();
        //заполнение листа строками из файла
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            while (reader.ready()) {
                list.add(reader.readLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (args.length == 0) return;
        // дополняем элементы массива args до нужного значения (8, 30,8,4)
        if (args.length == 5) {
            formatArgs(args);
        }
       // System.out.println(Arrays.toString(args));
        // проверка действий
        switch (args[0]) {
            case "-u": {
                update(args, list);
                break;
            }
            case "-d": {
                args[1] = String.format("%-8s", args[1]);
                delete(args[1], list);
                break;
            }
            default: {
                break;
            }
        }
        // запись листа в файл
        if (flag) {
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
                for (String elem : list) {
                    bw.write(elem);
                    bw.newLine();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private static void formatArgs(String[] elem) {
        elem[1] = String.format("%-8s", elem[1]);
        elem[2] = String.format("%-30s", elem[2]);
        elem[3] = String.format("%-8s", elem[3]);
        elem[4] = String.format("%-4s", elem[4]);
    }

    public static List<String> update(String[] args, List<String> list) {
        String upd = "";
        for (int j = 1; j < args.length; j++) {
            upd += args[j];
        }
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).startsWith(args[1])) {
                list.set(i, upd);
                flag = true;
            }
        }
        return list;
    }

    public static List<String> delete(String id, List<String> list) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).startsWith(id)) {
                list.remove(i);
                flag = true;
            }
        }
        return list;
    }
}
