package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.path;

import java.nio.file.Path;

public class PathExample {
    public static void main(String[] args) {
        /*Метод getParent() возвращает путь, который указывает на родительскую директорию для текущего пути. Независимо от того,
        был этот путь директорией или файлом:*/
        String path = "C:\\test\\project\\for_path\\file_test.txt";// C:\test\project\for_path\file_test.txt
        Path path1 = Path.of(path).getParent();
        System.out.println(path1);//C:\test\project\for_path
        Path directory = Path.of(path1.toUri()).getParent();
        System.out.println(directory);//C:\test\project
        Path d1 = Path.of(directory.toUri()).getParent();
        System.out.println(d1);
        Path root = Path.of(d1.toUri()).getParent();
        System.out.println(root);//C:\
        Path r = Path.of(root.toUri()).getParent();
        System.out.println(r);// null

        /*Метод getFileName() возвращает одно имя файла (или директории) — то, что идет после последнего разделителя:*/
        String str = "C:\\test\\file_test.txt";
        Path path2 = Path.of(str).getFileName();//file_test.txt
        System.out.println(path2);
        String s = "C:\\test\\";
        Path path3 = Path.of(s).getFileName();// test
        System.out.println(path3);

        /*Метод getRoot() возвращает путь к корневой директории:*/
        Path path4 = Path.of(path).getRoot();//C:\
        System.out.println(path4);

    }
}
