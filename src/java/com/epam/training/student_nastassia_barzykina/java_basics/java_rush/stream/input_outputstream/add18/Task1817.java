package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.add18;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * В метод main первым параметром приходит путь к файлу.
 * Вывести на экран соотношение количества пробелов к количеству всех символов. Например, 10.45.
 * 1. Посчитать количество всех символов.
 * 2. Посчитать количество пробелов.
 * 3. Вывести на экран п2/п1*100, округлив до 2 знаков после запятой до ближайшего.
 * 4. Закрыть потоки.
 *
 * Требования:
 * •	Считывать с консоли ничего не нужно.
 * •	Создай поток чтения из файла, который приходит первым параметром в main.
 * •	Посчитай отношение пробелов ко всем символам в файле и выведи в консоль это число.
 * •	Выведенное значение необходимо округлить до 2 знаков после запятой до ближайшего.
 * •	Поток чтения из файла должен быть закрыт.
 */
public class Task1817 {
    public static void main(String[] args) throws IOException {
        int countChar = 0;
        int countWhitespase = 0;
        try (FileReader fileReader = new FileReader(args[0])){
            while (fileReader.ready()) {
                char readChar = (char) fileReader.read();
                countChar++;
                //if (readChar == ' ') countWhitespase++;
                if (readChar == 32) countWhitespase++;
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        double res = (double) countWhitespase / countChar * 100;
        System.out.printf("%.2f%n", res);
    }

}
