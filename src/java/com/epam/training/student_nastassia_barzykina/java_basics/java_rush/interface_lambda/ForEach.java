package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_lambda;

import java.util.ArrayList;
import java.util.Collections;

public class ForEach {
    public static void main(String[] args) {
        var numbers = new ArrayList<Integer>();
        Collections.addAll(numbers, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0);

        print(numbers);
    }

    public static void print(ArrayList<Integer> numbers) {
        numbers.forEach((s) -> System.out.println(s));
        numbers.forEach(System.out::println);// только если 1 параметр!
//        for (int i = 0; i < numbers.size(); i++) {
//            System.out.println(numbers.get(i));
//        }
    }
}
