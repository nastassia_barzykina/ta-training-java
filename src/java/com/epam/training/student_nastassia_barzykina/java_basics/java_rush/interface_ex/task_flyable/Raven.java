package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex.task_flyable;

public class Raven implements Flyable{
    @Override
    public int getMaxSpeed() {
        return 48;
    }
}
