package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.constructors_obj;

public class EqualsMethod {
    public static void main(String[] args) {
        String s1 = new String("JavaRush");
        //s1 = "JavaRush";
        String s2 = "JavaRush";
//        s2 = new String("JavaRush");
        String s3 = "JavaRush";
//        s3 = new String("JavaRush");
        System.out.println(s1.hashCode());
        System.out.println(s3.hashCode());
        System.out.println(s1 == s3);// сравниваются ссылки
        System.out.println(s2.equals(s3));//сравниваются ссылки
        String a = new String("Привет");
        String b = new String("Привет");
        System.out.println(a == b);
    }
}
