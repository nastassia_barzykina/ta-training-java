package com.epam.training.student_nastassia_barzykina.java_basics.java_rush;

public class Kratno3 {
    public static void main(String[] args) {
        int i = 1;
        int sum = 0;
        while (i <= 100)
        {
            if ( (i % 3) == 0 )
            {
                i++;
                continue;
            }
            sum = sum+ i;
            i++;
        }
        System.out.println(sum);
        for (int j = 1; j <= 15; j++){
            if (j % 2 == 0){
                System.out.println(j);
            }
        }
    }
}
