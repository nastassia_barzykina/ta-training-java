package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods.overloading;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution1519 {
    public static void main(String[] args) throws IOException {
        BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
        String key;
        while (!(key = buff.readLine()).equals("exit")) {
            if (key.matches("-?\\d+(\\.\\d+)?")) {
//                if (key.indexOf(".") > 0){ //- но точек может быть много, и может быть s.11 -> тогда см. Test1519
                if (key.contains(".")) {
                    print(Double.parseDouble(key));
                } else {
                    if (Integer.parseInt(key) > 0 && Integer.parseInt(key) < 128) {
                        print(Short.parseShort(key));
                    } else
                        print(Integer.parseInt(key));
                }
            } else
                print(key);
        }
    }

    public static void print(Double value) {
        System.out.println("Это тип Double, значение " + value);
    }

    public static void print(String value) {
        System.out.println("Это тип String, значение " + value);
    }

    public static void print(short value) {
        System.out.println("Это тип short, значение " + value);
    }

    public static void print(Integer value) {
        System.out.println("Это тип Integer, значение " + value);
    }

//    public static boolean itsDouble(String s) {
//        try {
//            Double.parseDouble(s);
//        } catch (NumberFormatException e) {
//            return false;
//        }
//        return true;
//    }

//    public static boolean itsInt(String s) {
//        try {
//            Integer.parseInt(s);
//        } catch (NumberFormatException e) {
//            return false;
//        }
//        return true;
//    }

//    public static boolean isNumeric(String string) {
//        return string.matches("-?\\d+(\\.\\d+)?");
//        String regex = "[0-9]+[.]?[0-9]*";
//        String regexMinus = "-*[0-9]+[.]?[0-9]*";
//        return Pattern.matches(regex, string) || Pattern.matches(regexMinus, string);
//    }
}
