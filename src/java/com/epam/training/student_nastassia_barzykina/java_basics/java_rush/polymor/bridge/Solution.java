package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.bridge;

public class Solution {
    public static void main(String[] args) {
        println(new WaterBridge());
        println(new SuspensionBridge());
    }
    public static  void println(Bridge bridge){
//        if (bridge instanceof WaterBridge){// проверка избыточна
//            System.out.println(bridge.getCarsCount());
//        }
//        if (bridge instanceof SuspensionBridge){
            System.out.println(bridge.getCarsCount());
//        }
    }
}
