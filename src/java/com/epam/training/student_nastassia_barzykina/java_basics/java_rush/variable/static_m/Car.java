package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.variable.static_m;

/**
 * исправить программу по производству машин так, чтобы каждому объекту-машине можно было присвоить уникальное имя.
 */
public class Car {
    private /*static*/ String modelName;
    private int speed;

    public /*static*/ String getModelName() {
        return modelName;
    }

    public /*static*/ void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public static void main(String[] args) {
        Car car1 = new Car();
        car1.setModelName("model1");
        Car car2 = new Car();
        car2.setModelName("model2");
        System.out.println(car1.modelName);
        System.out.println(car2.modelName);

    }
}
