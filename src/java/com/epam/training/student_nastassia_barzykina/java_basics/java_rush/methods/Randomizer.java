package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods;

/**
 *  реализовать метод generateNumber(), который будет возвращать случайное число от 0 до 99.
 * В методе generateNumber() используй метод Math.random().
 */
public class Randomizer {
    public static void main(String[] args) {
        for (int i = 0; i < 101; i++) {
            System.out.println(generateNumber());
        }
    }

    public static int generateNumber() {
        //int x = (int) (Math.random() * 100);
        //напишите тут ваш код
        return  (int) (Math.random() * 100);
    }
}
