package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex.repka;

import java.util.List;

public  class RepkaStory  {
//    public RepkaStory(String name, String namePadezh) {
//        super(name, namePadezh);
//    }

    static void tell(List<Person> items) {
        Person first;
        Person second;
        for (int i = items.size() - 1; i > 0; i--) {
            first = items.get(i - 1);
            second = items.get(i);
            second.pull(first);
        }
    }
}
