package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.data.localdate;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.Locale;

public class DayOfWeekTask {
    static LocalDate birthDate = LocalDate.of(2020, 3, 12);
    static LocalDate myBirthDate = LocalDate.of(1980, 4, 13);

    public static void main(String[] args) {
        System.out.println(getDayOfWeek(birthDate));
        System.out.println(getDayOfWeek(myBirthDate));
    }

    static String getDayOfWeek(LocalDate date) {
        DayOfWeek day = date.getDayOfWeek();
        String str =  day.getDisplayName(TextStyle.FULL, Locale.forLanguageTag("ru") );
        return str;
//        return date.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.forLanguageTag("ru"));
    }
}
