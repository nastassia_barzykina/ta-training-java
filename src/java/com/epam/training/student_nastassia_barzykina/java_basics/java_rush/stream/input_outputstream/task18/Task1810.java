package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.task18;

import java.io.*;

/**
 * 1 Считывать с консоли имена файлов.
 * 2 Если файл меньше 1000 байт, то:
 * 2.1 Закрыть потоки работы с файлами.
 * 2.2 Выбросить исключение DownloadException.
 */
public class Task1810 {
    public static void main(String[] args) throws DownloadException, IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String fileName = br.readLine();
//        File file = new File(fileName);
//        long size = file.length();
//        while (size >= 1000){
//            size = new File(br.readLine()).length();
//        }
//        br.close();
//        throw new DownloadException();
        byte[] buffer = new byte[1000];
        while (true) {
            FileInputStream inputStream = new FileInputStream(fileName);
            int size = 0;
            if (inputStream.available() > 0) {
                size = inputStream.read(buffer);
            }
//            System.out.println(size + " -- size");
            if (size >= buffer.length) {
                fileName = br.readLine();
            } else {
                inputStream.close();
                throw new DownloadException();
            }
        }
    }

    public static class DownloadException extends Exception {

    }
}
