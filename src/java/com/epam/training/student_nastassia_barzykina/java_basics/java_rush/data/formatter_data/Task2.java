package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.data.formatter_data;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Task2 {
    static ZonedDateTime zonedDateTime = ZonedDateTime.now();

    /**
     * 4 9.3.19 06:03:07.319180500 Europe/Kiev
     * 4 — номер дня недели, т.е. четверг;
     * 9 — день месяца;
     * 3 — месяц;
     * 19 — год;
     * 06 — часы;
     * 03 — минуты;
     * 07 — секунды;
     * 319180500 — наносекунды;
     * Europe/Kiev — временная зона.
     * @param args
     */
    public static void main(String[] args) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("e d.M.yy HH:mm:ss.n VV");
        String text = dtf.format(zonedDateTime);
        System.out.println(text);//1 26.2.24 12:48:54.309965800 Asia/Tbilisi
    }
}
