package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.comp;

public class Monitor implements CompItem {
    @Override
    public String getName() {
        return getClass().getSimpleName();
    }
}
