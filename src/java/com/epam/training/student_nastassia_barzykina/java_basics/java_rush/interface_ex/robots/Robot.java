package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex.robots;

public class Robot extends AbstractRobot {
    //private static int hitCount;
    private String name;

    public Robot(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
