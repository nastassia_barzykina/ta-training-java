package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.bridge;

public class SuspensionBridge implements Bridge {
    @Override
    public int getCarsCount() {
        return 50;
    }
}
