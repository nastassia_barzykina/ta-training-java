package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.data.localdate.zone;

/**
 * В методе main присвой значение переменной globalTime, используя переменные localDateTime и zoneId.
 */

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class LocalDateTask {
    static LocalDateTime localDateTime = LocalDateTime.of(2020, 3, 19, 9, 17);
    static ZoneId zoneId = ZoneId.of("Zulu");
    static ZonedDateTime globalTime;

    public static void main(String[] args) {
        globalTime = localDateTime.atZone(zoneId);
        System.out.println(globalTime);//2020-03-19T09:17Z[Zulu]
//        ZonedDateTime globalTime = ZonedDateTime.now(zoneId);//2024-02-24T14:34:43.349212500Z[Zulu]
        System.out.println(globalTime);
    }
}

