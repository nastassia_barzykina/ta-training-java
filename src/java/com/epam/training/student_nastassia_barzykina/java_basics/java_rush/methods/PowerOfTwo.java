package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods;

public class PowerOfTwo {
    public static void main(String[] args) {
        System.out.println(getPowerOfTwo(6));
    }

    public static int getPowerOfTwo(int power) {
        if (power >= 0) {
            return 1 << power;
        } else {
            return 1 >> -power;// при power < 0 всегда 0, т.к. равнозначно делению на 2 нацело
                }
        //return 2 << (power - 1);// не работает при power < 0
    }
}
