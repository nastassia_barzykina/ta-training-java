package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream_collect.ex_pets;

import java.util.ArrayList;

public class Owner {
    private String name;
    private ArrayList<Animal> pets = new ArrayList<>();
    public Owner(String name){
        this.name = name;
    }

    public ArrayList<Animal> getPets() {
        return pets;
    }
}
