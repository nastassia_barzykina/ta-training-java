package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread.add17.task1715;

public class Drug {
    private String name;
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
