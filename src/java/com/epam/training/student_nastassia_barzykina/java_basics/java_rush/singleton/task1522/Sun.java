package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.singleton.task1522;

public class Sun implements Planet {
    private static Sun instance;

    private Sun() {

    }

    public static Sun getInstance() {
        if (instance == null) {
            instance = new Sun();
        }
        return instance;
    }
}
