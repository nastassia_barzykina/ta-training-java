package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.hen_task;

public class RussianHen extends Hen {
    private static final int egg = 100;

    @Override
    public int getCountOfEggsPerMonth() {
        return egg;
    }

    @Override
    public String getDescription() {

        return String.format("%s Моя страна - %s. Я несу %d яиц в месяц.", super.getDescription(), Country.RUSSIA, getCountOfEggsPerMonth());
    }
}
