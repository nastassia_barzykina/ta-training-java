package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex.task_compare;

import java.util.Comparator;

public class AgeComparator implements Comparator<Student> {
    @Override
    public int compare(Student student1, Student student2) {
        return student2.getAge() - student1.getAge();
    }
}
