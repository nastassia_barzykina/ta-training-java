package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.data.localdate.zone;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.TreeSet;

/**
 * в методе getSortedZones верни множество TreeSet всех временных зон;
 * в методе getBeijingTime верни текущую дату и время в Пекине (временная зона для него — "Asia/Shanghai").
 */
public class ZoneTask {
    public static void main(String[] args) {
        TreeSet<String> sortedZones = getSortedZones();
        System.out.println();
        System.out.println(sortedZones.size());
        System.out.println(sortedZones.first());
        System.out.println(sortedZones.last());

        System.out.println(getBeijingDateTime());
    }

    static TreeSet<String> getSortedZones() {
        return new TreeSet<>(ZoneId.getAvailableZoneIds());
    }

    static ZonedDateTime getBeijingDateTime() {
        return ZonedDateTime.now(ZoneId.of("Asia/Shanghai"));
    }
}
