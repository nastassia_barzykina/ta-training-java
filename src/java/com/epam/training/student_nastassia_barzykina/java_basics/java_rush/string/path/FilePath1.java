package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.string.path;

import java.nio.file.Path;
import java.util.Scanner;

public class FilePath1 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        String path = console.nextLine();//C:/Program Files/Java/jdk-13.0.0/bin
        char[] chars = path.toCharArray();
        for (int i = 0; i < chars.length; i++){
            if (chars[i] == '/')
                chars[i] = '\\';
        }
        String result = new String(chars);
        Path path1 = Path.of(result).getRoot();
        System.out.println(path1);
    }
}
