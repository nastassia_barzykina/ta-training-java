package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream;

import javax.imageio.IIOException;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyClass {
    private List<Integer> list =
            new ArrayList<>(Arrays.asList(111, 222, 333));

    public void write(int data)
    {
        list.add(data);
    }

    public int read()
    {
        int first = list.get(0);
        list.remove(0);
        return first;
    }

    public int available()
    {
        return list.size();
    }

    public static void main(String[] args) throws IOException {
//        MyClass myObject = new MyClass();
        InputStream inStream = new FileInputStream("c:/my-object-data.txt");//входной
        OutputStream outStream = new FileOutputStream("c:/my-object222-data.txt");// выходной
//        MyClass myObject1 = new MyClass();

        while (inStream.available() > 0)
        {
            int data = inStream.read(); //читаем один int из потока для чтения
            outStream.write(data); //записываем прочитанный int в другой поток.
        }

//        while (outStream.available() > 0)
//        {
//            int data = outStream.read(); //читаем один int из потока для чтения
//            outStream.write(data); //записываем прочитанный int в другой поток.
//        }
        inStream.close(); //закрываем потоки

        outStream.close();

    }
}
