package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.poly_abstract;

public abstract class Bird {
    private String species;
    private int age;
    public abstract void fly();

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
