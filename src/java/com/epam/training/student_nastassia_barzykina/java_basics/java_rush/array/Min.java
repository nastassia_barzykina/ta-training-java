package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.array;

import java.util.Scanner;

public class Min {
    public static int[] array;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int count = sc.nextInt();
        if (count > 0){
            array = new int[count];
            for (int i = 0; i < array.length; i++){
                array[i] = sc.nextInt();
            }
            int min = array[0];
            for (int i = 1; i < array.length; i++){
                if (array[i] < min){
                    min = array[i];
                }
            }
            System.out.println(min);
        }

    }
}
