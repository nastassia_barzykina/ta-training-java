package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stack_trace;

/**
 * Метод printStackTrace принимает в качестве параметра массив stackTrace. Нужно вывести информацию о каждом элементе
 * массива в формате:
 * "Метод <имя метода> вызван из строки <номер строки> класса <имя класса> в файле <имя файла>."
 * Информацию о каждом элементе выводи с новой строки.
 *
 * Пример:
 * Метод addJuice вызван из строки 24 класса com.javarush.task.pro.task14.task1414.Solution в файле Solution.java.
 */
public class PrintStackTrace {
    public static final String OUTPUT_FORMAT = "Метод %s вызван из строки %d класса %s в файле %s.\n";

    public static void main(String[] args) {
        makeScrewdriver();
    }

    public static void printStackTrace(StackTraceElement[] stackTrace) {
//        Thread current = Thread.currentThread(); - не нужно
//        stackTrace = current.getStackTrace();
        for (var info: stackTrace){
            System.out.printf(OUTPUT_FORMAT, info.getMethodName(), info.getLineNumber(), info.getClassName(), info.getFileName());
        }
    }

    static void makeScrewdriver() {
        addJuice();
    }

    static void addJuice() {
        addVodka();
    }

    static void addVodka() {
        printStackTrace(Thread.currentThread().getStackTrace());
    }
}
