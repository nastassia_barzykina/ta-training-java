package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.inherit;

public class HumanInherit {
    public interface HasWeight {
        int getWeight();
    }

    public interface HasHeight {
        int getHeight();
    }

    public static class Human implements HasWeight, HasHeight {

        @Override
        public int getHeight() {
            return 170;
        }

        @Override
        public int getWeight() {
            return 70;
        }
    }

    public static void main(String[] args) {
        Human human = new Human();
        System.out.println(human.getHeight());
        System.out.println(human.getWeight());
    }
}
