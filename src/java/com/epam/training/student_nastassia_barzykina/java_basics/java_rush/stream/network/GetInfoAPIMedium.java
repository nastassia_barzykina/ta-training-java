package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.network;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.io.*;

/**
 * обратиться к публичному API в интернете: отправить и получить данные.
 * Напиши программу, которая будет обращаться по ссылке к публичному API в интернете, отправляя туда данные, получать
 * ответные данные и выводить их на экран.
 * <p>
 * Подсказки:
 * <p>
 * Используй метод openConnection() класса URL.
 * Используй методы setDoOutput(true) и getOutputStream() класса URLConnection.
 * Если не знаешь, какой API использовать, бери http://httpbin.org/post.
 * Требования:
 * •	Программа должна выводить полученную информацию на экран.
 * •	В программе должен вызываться метод openConnection() класса URL.
 * •	В программе должны вызываться методы setDoOutput(true), getOutputStream() и getInputStream() класса URLConnection
 */
public class GetInfoAPIMedium {
    public static void main(String[] args) throws IOException {
        URL url = new URL("http://httpbin.org/post");
        URLConnection connection = url.openConnection();
        connection.setDoOutput(true);

        try (OutputStream output = connection.getOutputStream();
             OutputStreamWriter writer = new OutputStreamWriter(output)) {
            writer.write("Привет");
            writer.flush();// не было
//            PrintStream sender = new PrintStream(output)) {// в решении
//                sender.println("Привет");
        }

        try (InputStream input = connection.getInputStream();
             BufferedReader buffer = new BufferedReader(new InputStreamReader(input))) {
            while (buffer.ready()) {
                System.out.println(buffer.readLine());
            }
        } finally {
            System.out.println("ggg");

        }
    }
}
