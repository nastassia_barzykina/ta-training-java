package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.add18;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Numbers {
    public static void main(String[] args) {
        String fileName = "C:\\test\\num.txt";
        String file2 = "C:\\test\\numInt.txt";
        File file = new File(fileName);
        double numberDouble;
        int numberInt;
        try {
            Scanner scanner = new Scanner(file);
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(file2));
            while (scanner.hasNextDouble()) {
                numberDouble = scanner.nextDouble();
                System.out.print(numberDouble + " "); // => 1 2 3 4 5 6
                numberInt = (int) Math.round(numberDouble);
                System.out.print(numberInt + " ");
                String res = String.valueOf(numberInt);
                writer.write(res);
                writer.write(" ");
            }
            scanner.close();
            writer.close();
        } catch (IOException e) {
            System.out.println("Ошибка чтения файла " + fileName);
        }
    }
}
