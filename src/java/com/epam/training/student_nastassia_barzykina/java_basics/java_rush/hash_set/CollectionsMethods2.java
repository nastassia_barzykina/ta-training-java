package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.hash_set;

import java.util.ArrayList;
import java.util.Collections;

public class CollectionsMethods2 {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        Collections.addAll(list, 1,2,3,4,5);
        Collections.reverse(list);
        for (int i : list)
            System.out.print(i + " ");
        System.out.println();

        Collections.sort(list);
        for (int i : list)
            System.out.print(i + " ");
        System.out.println();

        Collections.rotate(list, 3);
        for (int i : list)
            System.out.print(i + " ");
        System.out.println();

        Collections.shuffle(list);
        for (int i : list)
            System.out.print(i + " ");
        System.out.println();

        int min = Collections.min(list);
        System.out.println(min);

        int max = Collections.max(list);
        System.out.println(max);
        System.out.println();

        int count = Collections.frequency(list, 1);
        System.out.println(count);

        Collections.sort(list);
        int index = Collections.binarySearch(list, 4);// если нету -> -N
        System.out.println(index);

        ArrayList<Integer> list1 = new ArrayList<>();
        Collections.addAll(list1, 99, 95);
        boolean isDifferent = Collections.disjoint(list, list1);
        System.out.println(isDifferent);






    }
}
