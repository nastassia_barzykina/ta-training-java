package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.ex;

public class Cow {
    public void printAll(){
        printColor();
        printName();
    }
    public void printColor ()
    {
        System.out.println("Я — белая");
    }

    public void printName()
    {
        System.out.println("Я — корова");
    }
}
