package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.bridge;

public class WaterBridge  implements Bridge {
    @Override
    public int getCarsCount() {
        return 10;
    }
}
