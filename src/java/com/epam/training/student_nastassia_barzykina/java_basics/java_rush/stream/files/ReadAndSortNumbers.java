package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.files;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Ввести имя файла с консоли.
 * Прочитать из него набор чисел.
 * Вывести в консоли только четные, отсортированные по возрастанию.
 */

public class ReadAndSortNumbers {
    public static void main(String[] args) throws IOException {
        BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
        String fileName = buff.readLine();
        FileInputStream fis = new FileInputStream(fileName);
        BufferedReader sc = new BufferedReader(new InputStreamReader(fis));
        sc.lines()
                .mapToInt(Integer::parseInt)
                .filter(i -> i % 2 == 0)
                .sorted()
                .forEach(System.out::println);
        sc.close();
        fis.close();
        buff.close();
        /**
         * Правильное решение:
         * BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
         *         String sourceFileName = reader.readLine();
         *         Scanner scanner = new Scanner(new FileInputStream(sourceFileName));
         *
         *         List<Integer> data = new ArrayList<>();
         *         while (scanner.hasNext()) {
         *             int value = scanner.nextInt();
         *             if (value % 2 == 0) data.add(value);
         *         }
         *
         *         Collections.sort(data);
         *         for (Integer value : data) {
         *             System.out.println(value);
         *         }
         *
         *         scanner.close();
         */
    }
}

