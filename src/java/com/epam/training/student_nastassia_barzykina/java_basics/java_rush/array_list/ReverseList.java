package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.array_list;

import java.util.ArrayList;

/**
 * массив int[] numbers заполняется числами в методе init(). Затем в методе reverse() происходит перестановка чисел в
 * обратном порядке.
 * Твоя задача — переписать код так, чтобы вместо массива int[] numbers использовался список ArrayList<Integer> numbers.
 */
public class ReverseList {
    //public static int[] numbers = new int[10];
    public static ArrayList<Integer> numbers = new ArrayList<Integer>();

    public static void main(String[] args) {
        init();
        print();
        System.out.println("Reverse List:");

        reverse();
        print();
    }

    public static void init() {
        for (int i = 10; i < 20; i++) {
            numbers.add(i);
        }
    }

    public static void reverse() {
        int n = numbers.size() - 1;
        for (int i = 0; i < numbers.size() / 2; i++) {
            int temp = numbers.get(i);
            numbers.set(i, numbers.get(n - i));//здесь была ошибка!!! (i, n - i)
            numbers.set(n - i, temp);
        }
    }

    private static void print() {
        for (int number : numbers) {
            System.out.println(number);
        }
    }
}
