package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.linkedlist;

public class StringLinkedList1 {
    private Node first = new Node();
    private Node last = new Node();
    public StringLinkedList1(){
        last.prev = first;// замыкаем ноды друг на друга, не требуется доп проверка и определение номера элемента
        first.next = last;
    }

    public void printAll() {
        Node currentElement = first.next;
        while ((currentElement) != null) {
            System.out.println(currentElement.value);
            currentElement = currentElement.next;
        }
    }

    public void add(String value) {
        Node node = new Node();
        node.value = value;
//        if (last.prev == null && first.next == null) {//когда первый
//            last.prev = node;
//            first.next = node;
//            node.next = last;
//            node.prev = first;
//
//        } else {// когда добавляем второй элемент
//            node.next = last;// не нужна. Если ее не будет, не будет null
//            node.prev = last.prev;
//            last.prev.next = node;
//            last.prev = node;
        Node lastNode = last.prev;//указывает на ноду левее ласта, временная переменная
        lastNode.next = node;//на переменной берется некст - на старой предыдущей ноде(строка 31)
        node.prev = lastNode;//вставляемая нода смотрит пердыдущую ноду
        last.prev = node;//ласт смотрит на вставляемую ноду

        //}

    }

    public static class Node {
        private Node prev;
        private String value;
        private Node next;
    }
}
