package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread;

public class Task1602 {
    public static void main(String[] args) {
        TestThread thread = new TestThread();
        thread.start();
    }
    public static class TestThread extends Thread{
        @Override
        public void run() {
            System.out.println("it's a run method");//2
        }
        static {
            System.out.println("it's a static block inside TestThread");//1
        }
    }
}
