package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.add18;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.util.*;

/**
 * Программа запускается с одним параметром - именем файла, который содержит английский текст.
 * Посчитать частоту встречания каждого символа.
 * Отсортировать результат по возрастанию кода ASCII (почитать в инете).
 *
 * Пример:
 * ','=44, 's'=115, 't'=116.
 *
 * Вывести на консоль отсортированный результат:
 * [символ1] частота1
 * [символ2] частота2
 * Закрыть потоки.
 *
 * Пример вывода:
 * , 19
 * - 7
 * f 361
 *
 * Требования:
 * •	Считывать с консоли ничего не нужно.
 * •	Создай поток для чтения из файла, который приходит первым параметром в main.
 * •	В файле необходимо посчитать частоту встречания каждого символа и вывести результат.
 * •	Выведенный в консоль результат должен быть отсортирован по возрастанию кода ASCII.
 * •	Поток для чтения из файла должен быть закрыт.
 */
public class Task1821 {
    public static void main(String[] args) throws Exception {
//        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//        String fileName = br.readLine();//"C:\\test\\dest.txt"
        FileInputStream inputStream = new FileInputStream(args[0]);
//        BufferedReader br = new BufferedReader(new FileReader(args[0]));
        HashMap<Character, Integer> map = new HashMap<>();

        while (inputStream.available() > 0) {
            char data = (char) inputStream.read();
            Integer count = map.get(data);
            if (count == null) {
                map.put(data, 1);
            } else {
                map.put(data, ++count);
            }
        }
        inputStream.close();
        Map<Character, Integer> sortedMap = new TreeMap<>(map);
        sortedMap.forEach((key, value) -> System.out.println(key + " " + value));


//        for (Map.Entry<Character, Integer> entry : sortedMap.entrySet()){
//                System.out.println(entry.getKey() + " " + entry.getValue());
//        }
    }

}
