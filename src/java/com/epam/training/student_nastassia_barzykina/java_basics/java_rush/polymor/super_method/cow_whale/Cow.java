package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.super_method.cow_whale;

class Cow {
    public void printAll()
    {
        printColor();
        printName();
    }

    public void printColor()
    {
        System.out.println("Я — белый");
    }

    public void printName()
    {
        System.out.println("Я — корова");
    }
}
class Whale extends Cow{
    @Override
    public void printName() {
        System.out.print("It is not true:  ");
        super.printName();
        System.out.println("Я - кит!!!");
    }

    public static void main(String[] args) {
        Whale whale = new Whale();
        whale.printAll();
        // Я — белый
        //It is not true:  Я — корова
        //Я - кит!!!
    }
}
