package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.comp;

public class Computer {
    private Keyboard keyboard;
    private  Mouse mouse;
    private  Monitor monitor;

    public Computer(Keyboard keyboard, Mouse mouse, Monitor monitor) {
        this.keyboard = keyboard;
        this.mouse = mouse;
        this.monitor = monitor;
    }

    public Keyboard getKeyboard() {
        return keyboard;
    }

    public Mouse getMouse() {
        return mouse;
    }

    public Monitor getMonitor() {
        return monitor;
    }
}
