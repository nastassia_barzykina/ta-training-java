package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread.add.task1631;

import com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread.add.task1631.common.*;

import java.util.Objects;

public class ImageReaderFactory {
    public static ImageReader getImageReader(ImageTypes types) throws IllegalArgumentException {
        if (types == ImageTypes.JPG)
            return new JpgReader();

        if (types == ImageTypes.BMP)
            return new BmpReader();

        if (types == ImageTypes.PNG)
            return new PngReader();

        throw new IllegalArgumentException("Неизвестный тип картинки");
    }

}
