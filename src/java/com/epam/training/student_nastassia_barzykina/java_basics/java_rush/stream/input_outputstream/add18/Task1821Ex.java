package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.add18;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Task1821Ex {
    public static void main(String[] args) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(args[0]))) {
            Map<Character, Integer> charMap = new HashMap<>();
            int currentChar;

            while ((currentChar = reader.read()) != -1) {
                char ch = (char) currentChar;
                charMap.put(ch, charMap.getOrDefault(ch, 0) + 1);
            }

            // Преобразуем HashMap в TreeMap для сортировки по ключам (кодам ASCII)
            Map<Character, Integer> sortedChar = new TreeMap<>(charMap);

            // Выводим результат
            sortedChar.forEach((key, value) -> System.out.println(key + " - " + value));
        } catch (IOException e) {
            System.err.println("Ошибка при чтении файла: " + e.getMessage());
        }
    }
}
