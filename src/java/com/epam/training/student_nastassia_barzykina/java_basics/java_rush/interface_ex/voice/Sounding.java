package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex.voice;

public interface Sounding {
    void sound();
}
