package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.path;

import java.nio.file.Path;
import java.util.Scanner;

public class FoundRoot {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        Path path = Path.of(str).getRoot();
        String str2 = scanner.nextLine();
        Path path1 = Path.of(str2).getRoot();
        System.out.println("Директории равны?");
        System.out.println(path.equals(path1));
        System.out.println(path);
        //System.out.println(Path.of(new Scanner(System.in).nextLine()).getRoot());
    }
}
