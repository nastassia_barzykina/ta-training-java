package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_lambda;

import java.util.Arrays;
import java.util.List;

public class Ex4Reduce {
    /*
    Дан список, нужно вывести сумму квадратов всех элемента списка. Lambda-выражения позволяет достигнуть этого
    написанием всего одной строки кода. В этом примере применен метод свертки (редукции) reduce(). Мы используем метод
    map() для возведения в квадрат каждого элемента, а потом применяем метод reduce() для свертки всех элементов в одно число.
     */
    public static void main(String[] args) {
        // Старый способ:
        List<Integer> list = Arrays.asList(1,2,3,4,5,6,7);
        int sum = 0;
        for(Integer n : list) {
            int x = n * n;
            sum = sum + x;
        }
        System.out.println(sum);

// Новый способ:
        List<Integer> listNew = Arrays.asList(1,2,3,4,5,6,7);
        int sumNew = listNew.stream()
                .map(x -> x*x)
                .reduce((x,y) -> x + y)// Integer::sum
                .get();
        System.out.println(sumNew);
    }
}
