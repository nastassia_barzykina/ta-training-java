package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.task18;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Программа должна:
 * 1. Переписать все байты одного файла в другой одним куском.
 * 2. Закрывать потоки ввода-вывода.
 *
 * Подсказка:
 * 4 ошибки.
 *
 * Требования:
 * •	Программа должна использовать классы FileInputStream и FileOutputStream.
 * •	Программа должна переписать все байты одного файла в другой одним куском.
 * •	Потоки FileInputStream и FileOutputStream должны быть закрыты.
 * •	Нужно исправить 4 ошибки.
 */
public class Task1806 {
    public static void main(String[] args) throws IOException {
        FileInputStream inputStream = new FileInputStream("C:\\test\\chartest.txt");//("c:/data.txt");
        // Создаем поток-записи-байт-в-файл
        FileOutputStream outputStream = new FileOutputStream("C:\\test\\result.txt");//("c:/result.txt");

//        if (inputStream.read() >= 0) {//ош
        if (inputStream.available() > 0){
            //читаем весь файл одним куском
            byte[] buffer = new byte[inputStream.available()];
            int count = inputStream.read(buffer);
            outputStream.write(buffer, 0, count);
        }
        inputStream.close();
        outputStream.close();

//        inputStream.reset(); //ош
//        outputStream.flush();//ош
    }
}
