package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.array_list;

import java.util.Arrays;

public class CustomStringArrayList {
    private int size;
    private int capacity;
    private String[] elements;

    public CustomStringArrayList() {
        capacity = 10;
        size = 0;
        elements = new String[capacity];
    }

    public void add(String element) {
        if (size == capacity) {
            grow();
        }
        elements[size] = element;
        size++;
    }
//Приватный метод grow(), который должен присвоить полю elements новый массив вместимостью (capacity) в полтора раза больше,
// * чем у старого массива и скопировать данные из старого массива в новый в том же порядке. Поле capacity должно увеличиться
// * точно так же, как и размер массива.
    private void grow() {
        //capacity = (int) (capacity * 1.5);
        capacity = capacity + (capacity >> 1);
        //int newC = capacity + (capacity >> 1);
        System.out.println("capacity = " + capacity);
        //capacity = newC;
        elements = Arrays.copyOf(elements, capacity);
/**
 * Без метода копирования (вместо 32 строки):
 * String[] newElements = new String[capacity];
 *         for (int i = 0; i < elements.length; i++) {
 *             newElements[i] = elements[i];
 *         }
 *         elements = newElements;
  */
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CustomStringArrayList{");
        sb.append("elements=").append(Arrays.toString(elements));
        sb.append('}');
        return sb.toString();
    }
}
