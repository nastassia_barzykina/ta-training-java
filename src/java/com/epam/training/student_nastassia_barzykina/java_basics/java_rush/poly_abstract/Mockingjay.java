package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.poly_abstract;

public class Mockingjay extends Bird{
    @Override
    public void fly() {
        System.out.println("Fly!");
    }

    public static void main(String[] args) {
        Mockingjay someBird = new Mockingjay();
        someBird.setAge(2);
        System.out.println(someBird.getAge());
    }
}
