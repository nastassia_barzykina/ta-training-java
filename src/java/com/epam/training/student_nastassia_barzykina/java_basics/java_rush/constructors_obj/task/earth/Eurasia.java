package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.constructors_obj.task.earth;

public class Eurasia {
    private final int area;

    public Eurasia(int area) {
        this.area = area;
    }
}
