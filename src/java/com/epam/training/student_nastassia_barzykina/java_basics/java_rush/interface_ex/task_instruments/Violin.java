package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex.task_instruments;

public class Violin implements MusicalInstrument{
    @Override
    public void play() {
        System.out.println("Играет скрипка.");
    }
}
