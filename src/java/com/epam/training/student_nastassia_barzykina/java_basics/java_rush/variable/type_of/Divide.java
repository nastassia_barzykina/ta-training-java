package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.variable.type_of;

/**
 * Реализуй метод divide(double a, double b)
 *      Double.POSITIVE_INFINITY, если верен любой из следующих пунктов:
 * a положительное и b равно 0;
 * a равно Double.POSITIVE_INFINITY и b >= 0 и b не равно Double.POSITIVE_INFINITY;
 * a равно Double.NEGATIVE_INFINITY и b < 0 и b не равно Double.NEGATIVE_INFINITY;
 *      Double.NEGATIVE_INFINITY, если верен любой из следующих пунктов:
 * a отрицательное и b равно 0;
 * a равно Double.NEGATIVE_INFINITY и b >= 0 и b не равно Double.POSITIVE_INFINITY;
 * a равно Double.POSITIVE_INFINITY и b < 0 и b не равно Double.NEGATIVE_INFINITY;
 *      Double.NaN, если верен любой из следующих пунктов:
 * a равно 0 и b равно 0;
 * хотя бы один из операндов равен Double.NaN;
 * a равно Double.POSITIVE_INFINITY и b равно Double.POSITIVE_INFINITY;
 * a равно Double.POSITIVE_INFINITY и b равно Double.NEGATIVE_INFINITY;
 * a равно Double.NEGATIVE_INFINITY и b равно Double.POSITIVE_INFINITY;
 * a равно Double.NEGATIVE_INFINITY и b равно Double.NEGATIVE_INFINITY;
 *      Иначе выводить результат деления a на b.
 */
public class Divide {
    public static void main(String[] args) {
        divide(1, 0);
        divide(-1, 0);
        divide(0, 0);
        divide(100, 20);
        divide(200, 20);
        divide(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
    }

    public static void divide(double a, double b) {
//        if (a == 0 && b == 0){
//            return Double.NaN;
//        }
        System.out.println(a / b);// РЖУ!!!
        //напишите тут ваш код
    }
}
