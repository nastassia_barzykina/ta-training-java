package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.hash_set;

import java.util.ArrayList;
import java.util.Collections;

public class CollectionsMethodsSolution {
    public static void copy(ArrayList<String> destination, ArrayList<String> source) {
        if(destination.size() < source.size()) {
            throw new IndexOutOfBoundsException("Source does not fit in dest");
        }
        Collections.copy(destination, source);
//        for (int i = 0; i < source.size(); i++) {
//            destination.set(i, source.get(i));
//        }
    }

    public static void addAll(ArrayList<String> list, String... strings) {
//        for (String string : strings) {// цикл не нужен!
            //list.add(string);
            Collections.addAll(list, strings);
//        }
    }

    public static void replaceAll(ArrayList<String> list, String oldValue, String newValue) {
//        for (int i = 0; i < list.size(); i++) {
//            String string = list.get(i);
//            if(string.equals(oldValue)) {
//                list.set(i, newValue);
//            }
//        }
        Collections.replaceAll(list, oldValue, newValue);
    }

    public static void main(String[] args) {
        ArrayList<String> words = new ArrayList<>();
        addAll(words, "1111", "2222", "ssss", "jjjj", "ssss777");
        for (String s : words)
            System.out.print(s + "  ");
        System.out.println();
        replaceAll(words, "ssss", "DDDD");
        for (String s : words)
            System.out.print(s + "  ");
        System.out.println();
        ArrayList<String> newW = new ArrayList<>();
        Collections.addAll(newW, " ", "dest");
        copy(words, newW);
        for (String s : words)
            System.out.print(s + "  ");
    }
}
