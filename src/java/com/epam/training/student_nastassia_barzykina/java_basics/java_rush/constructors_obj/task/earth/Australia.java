package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.constructors_obj.task.earth;

public class Australia {
    private final int area;
    public Australia(int area){
        this.area = area;
    }
}
