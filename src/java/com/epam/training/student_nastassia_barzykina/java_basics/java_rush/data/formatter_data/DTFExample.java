package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.data.formatter_data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class DTFExample {
    public static void main(String[] args) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM-dd-yy");
        String text = dtf.format(LocalDate.now());
        System.out.println(text);//02-26-24

        DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("MMMM-dd-yyyy");
        text = dtf1.format(LocalDate.now());
        System.out.println(text);//февраля-26-2024

        DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("MMMM-dd-yyyy", Locale.ENGLISH);
        LocalDate date = LocalDate.parse("February-23-2019", dtf2);
        System.out.println(date.format(dtf2));// 2019-02-23, if date; February-23-2019, if date.format

        DateTimeFormatter dtf3 = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalTime time = LocalTime.parse("23:59:50", dtf3);
        System.out.println(time.format(dtf3));
    }
}
