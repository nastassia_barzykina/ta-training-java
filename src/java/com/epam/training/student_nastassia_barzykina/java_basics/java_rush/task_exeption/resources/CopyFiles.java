package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.task_exeption.resources;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyFiles {
    public static void main(String[] args) throws IOException {// проверяемое исключение
        /** Длинный код:
         * String src = "c:\\projects\\log.txt";
         * String dest = "c:\\projects\\copy.txt";
         *
         * FileInputStream input = null;
         * FileOutputStream output = null;
         *
         * try
         * {
         *    input = new FileInputStream(src);
         *    output = new FileOutputStream(dest);
         *
         *    byte[] buffer = input.readAllBytes();
         *    output.write(buffer);
         * }
         * finally
         * {
         *    if (input != null)
         *       input.close();
         *    if (output != null)
         *       output.close();
         * }
         * Короткий код:
         */
        String src = "c:\\test\\log.txt";
        String dest = "c:\\test\\copy.txt";

        try (FileInputStream input = new FileInputStream(src);

             FileOutputStream output = new FileOutputStream(dest)) {
            // byte[] buffer = input.readAllBytes();
//            output.write(buffer);
            byte[] buffer = new byte[65536]; // 64Kb Буфер, в который будем считывать данные
            while (input.available() > 0)//   Пока данные есть в потоке
            {
                int real = input.read(buffer);// Считываем данные в буфер, real - количество считанных в буфер байт
                output.write(buffer, 0, real);// Записываем данные из буфера во второй поток
            }
        }
        /**
         * Еще один интересный момент — это переменная real. Когда из файла будет читаться последний блок данных, легко
         * может оказаться, что его длина меньше 64Кб. Поэтому в output нужно тоже записать не весь буфер, а только его
         * часть: первые real байт. Именно это и делается в методе write().
         */
    }
}
