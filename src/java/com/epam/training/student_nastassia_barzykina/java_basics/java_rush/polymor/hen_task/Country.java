package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.hen_task;

public interface Country {
    String UKRAINE = "Ukraine";
    String RUSSIA = "Russia";
    String MOLDOVA = "Moldova";
    String BELARUS = "Belarus";
}
