package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.singleton.task1522;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) {

    }

    public static Planet thePlanet;

    static {
        try {
            readKeyFromConsoleAndInitPlanet();
//            if (thePlanet != null) {
//                System.out.println(thePlanet.getClass().getSimpleName());
//            } else {
//                System.out.println("The Planet not found");
//            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    //add static block here - добавьте статический блок тут

    public static void readKeyFromConsoleAndInitPlanet() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String planet = reader.readLine();
        switch (planet) {
            case Planet.SUN:
                thePlanet = Sun.getInstance();
                break;
            case Planet.MOON:
                thePlanet = Moon.getInstance();
                break;
            case Planet.EARTH:
                thePlanet = Earth.getInstance();
                break;
            default:
                thePlanet = null;
                break;
        }
        // implement step #5 here - реализуйте задание №5 тут
    }
}
