package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.enums;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public enum Season {
    WINTER("Зима"), //Есть ещё один прикол с enum, так называемый супер enum, он делает так что при обращении,
                            // допустим Season.WINTER.toString будет выведено Зима, очень удобно получается
    SPRING("Осень"),
    SUMMER("Лето"),
    AUTUMN("Весна");
    final String toString; // создаем значение в котором будет храниться наше текстовое определение

    Season(String toString) {
        this.toString = toString; // при создании экземпляра будет присваивать текстовое значение
    }
}
enum Days{
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;
    public static List<Days> asList(){
        ArrayList<Days> list = new ArrayList<>();//создаем AL
        Collections.addAll(list, values());// Добавляем в него значения из массива: его возвращает метод values().
        return list;
    }

}