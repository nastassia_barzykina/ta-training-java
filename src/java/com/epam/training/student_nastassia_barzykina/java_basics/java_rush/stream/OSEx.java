package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.StringWriter;

public class OSEx {
    public static void main(String[] args) {
        /**
         * Пример SW:
         */
        StringWriter writer = new StringWriter();
        writer.write("Hello");
        writer.write(String.valueOf(123));

        String result = writer.toString();
        System.out.println(result);


        String src = "c:\\test\\log.txt";
        String dest = "c:\\test\\test.txt";
        try(FileInputStream input = new FileInputStream(src);
            FileOutputStream out = new FileOutputStream(dest))
        /**Аналог OS:
         * FileReader reader = new FileReader(src);
         * FileWriter writer = new FileWriter(dest)
         * char[] buffer;
         */
        {
            byte[] buffer = new byte[65536];//64 KB
            while (input.available() > 0 ){
                int realSize = input.read(buffer);//размер входящего потока
                out.write(buffer, 0, realSize);// в исходящий поток пишем массив buffer[] с 0 позиции размера realSize
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
