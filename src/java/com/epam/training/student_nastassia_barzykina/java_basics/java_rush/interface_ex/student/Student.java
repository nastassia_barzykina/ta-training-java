package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex.student;

public class Student {
    private String name;
    public Student (String name){
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        Student student = new Student("Alibaba");
        System.out.println(student.getName());
    }
}
