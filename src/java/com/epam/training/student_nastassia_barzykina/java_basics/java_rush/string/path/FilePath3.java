package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.string.path;

import java.util.Scanner;

public class FilePath3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String path = sc.nextLine();
        String result = path.replace('/', '\\');
        System.out.println(result);
    }
}
