package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.variable.solar_system;

public class Solution {
    public static void main(String[] args) {
        System.out.println("Человечество живет в Солнечной системе.");
        System.out.printf("Ее возраст около %d лет.%n", SolarSystem.age);
        System.out.printf("В Солнечной системе %d известных планет.%n", SolarSystem.planetsCount);
        System.out.printf("Как и большинство звездных систем, состоит из %d звезды.%n", SolarSystem.starsCount);
        System.out.printf("Звезды по имени %s.%n", SolarSystem.starName);
        System.out.println(String.format("Расстояние к центру галактики составляет %d световых лет.", SolarSystem.galacticCenterDistance));
        System.out.println("Каждый обитатель Солнечной системы должен знать эту информацию!");


    }
}
