package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.hash_map;
import java.util.ArrayList;
import java.util.HashMap;
/**
 * метод getProgrammingLanguages, который возвращает список языков программирования. Тебе нужно переписать этот метод,
 * чтобы он возвращал HashMap<Integer, String>. Ключом в этой коллекции будет индекс элемента в ArrayList.
 */
public class ProgrammingLanguages {
    public static void main(String[] args) {
        System.out.println(getProgrammingLanguages());
    }

    public static HashMap<Integer, String> getProgrammingLanguages() {
        //напишите тут ваш код
        ArrayList<String> programmingLanguages = new ArrayList<>();
        programmingLanguages.add("Java");// programmingLanguages.put(0, "Java");
        programmingLanguages.add("Kotlin");
        programmingLanguages.add("Go");
        programmingLanguages.add("Javascript");
        programmingLanguages.add("Typescript");
        programmingLanguages.add("Python");
        programmingLanguages.add("PHP");
        programmingLanguages.add("C++");
        HashMap<Integer, String> map = new HashMap<>();
        for (String el : programmingLanguages){
            map.put(programmingLanguages.indexOf(el), el);
        }
        return map;
    }
}
