package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.variable.static_m;

public class CatMain {
    static Integer first = 1000;
    static Integer second = 1000;
    static int third = 1000;
    public static void main(String[] args) {
        System.out.println(first.equals(second));// т.к. объекты
        System.out.println(third == second);
        System.out.println(third == first);

        Integer x = Integer.valueOf(7);//все равно 7, т.к. класс immutable
        //int x = 7;
        incrementNumber(x);
        System.out.println(x);

        Cat cat = new Cat(7);
        catLevelUp(cat);
        System.out.println(cat.getAge());

    }

    public static void catLevelUp(Cat cat) {

        cat.setAge(cat.getAge()+1);
    }

    public static void incrementNumber(Integer x) {
        Integer c = Integer.valueOf(x++);// все равно не будет работать
        x++;// не будет работать
        System.out.println("x = " + x);
        System.out.println("c = " + c);
        /**
         * Потому что примитивы в Java передаются в методы по значению.
         *Когда мы вызываем метод goToPast(), и передаем туда нашу переменную int currentYear = 2020, в метод попадает
         * не сама переменная currentYear, а ее копия.
         *
         * Значение этой копии, конечно, тоже равно 2020, но все изменения, которые происходят с копией, никак не влияют
         * на нашу изначальную переменную currentYear!
         * Аналогично и для x.
         */
    }
}
