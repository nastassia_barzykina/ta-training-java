package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods.new_swap;

public class MainSwapXOR {
    public static void main(String[] args) {
        Pair pair = new Pair(4, 5);
        System.out.println(pair);
        pair.swap();
        System.out.println(pair);
    }
}
