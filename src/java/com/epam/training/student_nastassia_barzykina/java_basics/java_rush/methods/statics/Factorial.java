package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods.statics;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;

public class Factorial {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int input = Integer.parseInt(reader.readLine());
        reader.close();

        System.out.println(factorial(input));
    }

    public static String factorial(int n) {
        if (n < 0) {
            return "0";
        }
        if (n == 0){
            return "1";
        }
//        int[] factorialNum = new int[n];
//        for (int i = 0; i < factorialNum.length; i++){
//            factorialNum[i] = i + 1;
//        }
//        BigDecimal result = BigDecimal.valueOf(1);
//        for (int elem : factorialNum){
//            result =  result.multiply(BigDecimal.valueOf(elem));
//        }
        BigDecimal result = BigDecimal.valueOf(1);// BigDecimal bd = new BigDecimal(1);
        for (int i = 1; i <= n; i++){
            result = result.multiply(BigDecimal.valueOf(i));//bd = bd.multiply(new BigDecimal(i));
        }
        return String.valueOf(result);// bd.toString();
    }
}
