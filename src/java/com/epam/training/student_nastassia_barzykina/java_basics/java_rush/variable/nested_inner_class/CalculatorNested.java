package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.variable.nested_inner_class;

public class CalculatorNested {
    public static final String EQUAL = " = ";
    static class Calculator{
        public static void add(int a, int b) {
            System.out.println(a + " + " + b + EQUAL + (a + b));
        }
        public static void subtract(int a, int b) {
            System.out.println(a + " - " + b + EQUAL + (a - b));
        }

        public static void multiply(int a, int b) {
            System.out.println(a + " * " + b + EQUAL + (a * b));
        }

        public static void divide(int a, int b) {
            System.out.println(a + " / " + b + EQUAL + (a / b));
        }
    }

    public static void main(String[] args) {
        int a = 45;
        int b = 15;
        CalculatorNested.Calculator.add(a, b);
        CalculatorNested.Calculator.subtract(a, b);
        CalculatorNested.Calculator.multiply(a, b);
        CalculatorNested.Calculator.divide(a, b);
        a = 15;
        CalculatorNested.Calculator.divide(a, b);
    }




}
