package com.epam.training.student_nastassia_barzykina.java_basics.java_rush;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static java.lang.Integer.parseInt;

public class GCDnod {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String numA = null;
        String numB = null;
        int a, b;
//        while (true) {
            numA = reader.readLine();
            boolean uA = !numA.isEmpty() && itsNumber(numA) && parseInt(numA) >= 0;
            if (!uA) {
                System.exit(1);
               // break;
            }
            a = parseInt(numA);
            numB = reader.readLine();
            boolean uB = !numB.isEmpty() && itsNumber(numB) && parseInt(numB) >= 0;
            if (!uB) {
                System.exit(1);
               // break;
            }
            b = parseInt(numB);
            System.out.println(gcd(a, b));
            //break;
//        }

//        int first = Integer.parseInt(reader.readLine());
//        int second = Integer.parseInt(reader.readLine());
//
//        System.out.println(gcd(first, second));
    }

    public static int gcd(int a, int b) {
        if (a < 0) a = -a;
        if (b < 0) b = -b;
        if (0 == b) return a;
        else return gcd(b, a % b);
//        if (a < 1 || b < 1) {
//            throw new IllegalArgumentException();
//        }
//        while (a != b) {
//            if (a > b) {
//                a -= b;
//            }
//            if (b > a) {
//                b -= a;
//            }
//        }
//        return a;
    }

    public static boolean itsNumber(String s) {
        try {
            parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}
