package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.adapt.ex1;

/**
 * Класс «адаптер» (т.е. переходник) от интерфейса AlphaList к интерфейсу BetaList
 * Класс ListAdapter реализует интерфейс BetaList из второго фреймворка.
 *
 * Когда кто-то вызывает эти методы, код класса перевызывает методы переменной list, которая имеет тип AlphaList
 * из первого фреймворка.
 *
 * Объект типа AlphaList передается в конструктор ListAdapter в момент создания
 *
 * Метод setSize работает по принципу: если нужно увеличить размер списка – добавим туда пустых (null) элементов.
 * Если нужно уменьшить – удалим несколько последних.
 */
public class ListAdapter implements BetaList {
    private AlphaList list;

    ListAdapter(AlphaList list) {
        this.list = list;
    }

    public int getValue(int index) {
        return this.list.get(index);
    }

    public void setValue(int index, int value) {
        this.list.set(index, value);
    }

    public int getSize() {
        return this.list.count();
    }

    public void setSize(int newSize) {
        if (newSize > this.list.count()) {
            while (this.list.count() < newSize) {
                this.list.add(0);//null
            }
        } else if (newSize < this.list.count()) {
            while (this.list.count() > newSize) {
                list.remove(list.count() - 1);
            }
        }
    }
    public static void main(String[] args)//пример использования
    {
        AlphaList listAlpha = AlphaListManager.createList();
        BetaList listBeta = new ListAdapter(listAlpha);
        BetaSaveManager.saveList(listBeta);
    }
}

