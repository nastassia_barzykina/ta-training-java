package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.hash_map;

import java.util.ArrayList;
import java.util.HashMap;

public class ALvsHM {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("Ivan");
        list.add("Petr");
        String s = list.get(0);
        list.set(0, s + "!");
        System.out.println("ArrayList:");
        for (String el : list){
            System.out.println(el);
        }
        System.out.println("HashMap:");
        HashMap<Integer, String> listMap = new HashMap<>();
        listMap.put(0, "Ivan");
        listMap.put(1, "Petr");
        String string = listMap.get(0);
        listMap.put(0, string + "!");
        for (String item : listMap.values()){
            //Integer key = el.getKey();
            //String name = el.getValue();
            System.out.println(item);
        }


    }
}
