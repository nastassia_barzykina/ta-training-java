package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.add18;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Читайте с консоли имена файлов, пока не будет введено слово "exit".
 * Передайте имя файла в нить ReadThread.
 * Нить ReadThread должна найти байт, который встречается в файле максимальное число раз (если таких байтов несколько,
 * выбрать наименьший), и добавить его в словарь resultMap,
 * где параметр String - это имя файла, параметр Integer - это искомый байт.
 * Закрыть потоки.
 * <p>
 * Требования:
 * •	Программа должна считывать имена файлов с консоли, пока не будет введено слово "exit".
 * •	Для каждого файла создай нить ReadThread и запусти ее.
 * •	После запуска каждая нить ReadThread должна создать свой поток для чтения из файла.
 * •	Затем нити должны найти максимально встречающийся байт в своем файле и добавить его в словарь resultMap.
 * •	Поток для чтения из файла в каждой нити должен быть закрыт.
 */
public class Task1823 {
    public static Map<String, Integer> resultMap = new HashMap<String, Integer>();

    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = null;
        while (true) {
            try {
//                if (reader.readLine().equals("")) {
//                    break;
//                }
                s = reader.readLine();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            if ("exit".equals(s)) {
                break;
            } else {
                new ReadThread(s).start();
            }
        }

    }

    public static class ReadThread extends Thread {
        private String filename;

        public ReadThread(String fileName) {
            this.filename = fileName;
            //implement constructor body
        }

        // implement file reading here - реализуйте чтение из файла тут
        //C:\\test\\dest.txt
        @Override
        public void run() {
            Map<Integer, Integer> map = new HashMap<>();
            TreeSet<Integer> treeSet = new TreeSet<>();
            try {
                FileInputStream inputStream = new FileInputStream(this.filename);
                while (inputStream.available() > 0) {
                    int data = inputStream.read();
                    Integer count = map.get(data);
                    if (count == null) {
                        map.put(data, 1);
                    } else {
                        map.put(data, ++count);
                    }
                }
                inputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            List<Integer> list = new ArrayList<>(map.values());
            Collections.sort(list);
            Integer max = list.get(list.size() - 1);
            for (Map.Entry entry : map.entrySet()) {
                if (max == entry.getValue()) {
                    treeSet.add((Integer) entry.getKey());
                }
            }
            synchronized (resultMap) {
                resultMap.put(this.filename, treeSet.first());
            }
        }
    }
}
