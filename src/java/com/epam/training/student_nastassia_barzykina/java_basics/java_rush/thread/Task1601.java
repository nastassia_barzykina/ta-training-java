package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread;

/**
 * Создать public static class TestThread - нить с интерфейсом Runnable.
 * TestThread должен выводить в консоль "My first thread".
 */
public class Task1601 {
    public static void main(String[] args) {
        TestThread task = new TestThread();
        new Thread(task).start();
    }
    public static class TestThread implements Runnable{

        @Override
        public void run() {
            System.out.println("My first thread");
        }
    }
}
