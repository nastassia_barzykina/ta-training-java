package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.files;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class TestBW {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        String fileName = sc.nextLine();
        BufferedWriter bufferedWriter = new BufferedWriter(
                new FileWriter(fileName));
        while (true) {
            String text = sc.nextLine();
            if (text.equals("exit")) {
                bufferedWriter.write(text);
                break;
            }
            bufferedWriter.write(text + '\n');
        }
        bufferedWriter.close();

    }
}
