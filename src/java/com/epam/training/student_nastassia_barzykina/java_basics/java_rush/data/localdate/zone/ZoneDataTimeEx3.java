package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.data.localdate.zone;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ZoneDataTimeEx3 {
    //В 12 часов 1 января 2020 года вы вылетаете из Вьетнама в Японию, длительность полета составляет 6 часов 15 минут.
    // Вопрос в том, во сколько вы приедете в Японию?
    public static void main(String[] args) {
        LocalDateTime localDateTime = LocalDateTime.of(2020, Month.JANUARY, 1, 12, 0, 0);

        System.out.println("LocalDateTime: " + localDateTime); // 2020-01-01T12:00
        System.out.println();

// UTC+7 (Ho Chi Minh Vietnam).
        ZoneId vnZoneId = ZoneId.of("Asia/Ho_Chi_Minh");
// Add time zone to it!
        ZonedDateTime vnDateTime = ZonedDateTime.of(localDateTime, vnZoneId);

        System.out.println("Depart from Vietnam at: " + vnDateTime); // 2020-01-01T12:00+07:00[Asia/Ho_Chi_Minh]

// UTC+9 (Tokyo Japan).
        ZoneId jpZoneId = ZoneId.of("Asia/Tokyo");
        ZonedDateTime jpDateTime = vnDateTime.withZoneSameInstant(jpZoneId).plusHours(6).plusMinutes(15);

        System.out.println("Arrive to Japan at: " + jpDateTime); // 2020-01-01T20:15+09:00[Asia/Tokyo]
    }
}
