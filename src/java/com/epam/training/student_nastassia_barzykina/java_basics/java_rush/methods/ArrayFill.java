package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods;

import java.util.Arrays;

public class ArrayFill {
    public static void main(String[] args) {
        Integer[] array = new Integer[7];
        fillArray(array, 3);
        System.out.println(Arrays.toString(array));
    }
    public static void fillArray(Integer[] array, int value){
        for (int i = 0; i < array.length; i++){
            array[i] = value;
        }
    }
}
