package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods.magic_ball;

import java.util.concurrent.TimeUnit;

public class MainMagicBall {
    public byte h = -128;
    public short s = -32768;
    public int i = 1_234_567_890;
    public long l = 2_345_678_900l;


    public double a = 0.;
    public double b = .0;
    public double c = 100D;
    public double d = 100.0;
    public double e = 1.11E5;
    public float f = 200F;
    public float g = 0.F;
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Заработаю ли я себе на BMW?");
        System.out.println(MagicBall.getPrediction());
        setTimer(0,0,0, 5, 5);
        int x = (16 >> 2);
        System.out.println(x);
        System.out.println("""
                ntcn "ntcn ggg"fghg
                """);

    }
    public static void setTimer(int days, int hours, int minutes, int seconds, int millis) throws InterruptedException {
        System.out.println("Таймер запущен!");
        TimeUnit.DAYS.sleep(days);
        TimeUnit.HOURS.sleep(hours);
        TimeUnit.MINUTES.sleep(minutes);
        TimeUnit.SECONDS.sleep(seconds);
        TimeUnit.MILLISECONDS.sleep(millis);
        //напишите тут ваш код
        System.out.println("♬ ♪ ♬♬♬♬ ♪♪♪♪");
    }
}
