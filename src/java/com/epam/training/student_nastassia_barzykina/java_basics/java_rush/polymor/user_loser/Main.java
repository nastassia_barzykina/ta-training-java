package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.user_loser;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Person person = null;
        String key = null;
        label:
        while (true)
        //тут цикл по чтению ключей, пункт 1
        {
            key = reader.readLine();
            switch (key) {
                case "user":
                    //создаем объект, пункт 2
                    person = new Person.User();
                    break;
                case "loser":
                    person = new Person.Loser();
                    break;
                case "coder":
                    person = new Person.Coder();
                    break;
                case "proger":
                    person = new Person.Proger();
                    break;
                default:
                    break label;
            }
            //вызываем doWork
            doWork(person);
        }
    }

    public static void doWork(Person person) {
        if (person instanceof Person.User) {
            ((Person.User) person).live();
        } else if (person instanceof Person.Coder) {
            ((Person.Coder) person).writeCode();
        } else if (person instanceof Person.Loser) {
            ((Person.Loser) person).doNothing();
        } else if (person instanceof Person.Proger) {
            ((Person.Proger) person).enjoy();
        } //else
           // System.out.println("unknown person!");
        // пункт 3
    }
}
