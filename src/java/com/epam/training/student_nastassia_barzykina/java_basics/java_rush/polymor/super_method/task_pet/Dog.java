package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.super_method.task_pet;

public class Dog extends Pet{
    public static final String DOG = "Я люблю людей.";

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.println(DOG);
    }
}
