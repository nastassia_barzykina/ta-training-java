package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream_collect.ex_pets;

public class Parrot extends Animal{
    public Parrot(String name, Color color, int age) {
        super(name, color, age);
    }
}
