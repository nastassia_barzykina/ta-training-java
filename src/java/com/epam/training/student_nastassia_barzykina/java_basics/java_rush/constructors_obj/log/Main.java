package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.constructors_obj.log;

/**
 * Логирование
 */
public class Main {
    public static void main(String[] args)
    {
        ChildClass obj = new ChildClass();
    }

    public static String print(String text)// Этот метод пишет в консоль переданный текст и возвращает его же
    {
        System.out.println(text);
        return text;
    }
}
class ParentClass{
    public String a = Main.print("ParentClass.a");// Пишем текст и им же инициализируем переменные
    public String b = Main.print("ParentClass.b");

    public ParentClass()//Пишем в консоль сообщение о вызове конструктора. Возвращаемое значение игнорируем.
    {
        Main.print("ParentClass.constructor");
    }
}
class ChildClass extends ParentClass{
    public String c = Main.print("ChildClass.c");
    public String d = Main.print("ChildClass.d");

    public ChildClass()
    {
        Main.print("ChildClass.constructor");
    }
}
