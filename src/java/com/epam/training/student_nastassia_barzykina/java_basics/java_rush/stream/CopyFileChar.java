package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream;

import java.io.*;

public class CopyFileChar {
    public static void main(String[] args)  {
        String src = "c:\\test\\char.txt";
        String dest = "c:\\test\\chartest.txt";
        try (FileReader reader = new FileReader(src) ; FileWriter writer = new FileWriter(dest)) {
            char[] buffer = new char[256];
            while(reader.ready())
            {
                int real = reader.read(buffer);
                writer.write(buffer, 0, real);
            }
        } catch (Exception e){
            System.out.println("Что-то пошло не так");
        }

    }
}

