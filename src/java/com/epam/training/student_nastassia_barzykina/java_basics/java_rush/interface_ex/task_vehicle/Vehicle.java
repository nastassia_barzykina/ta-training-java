package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex.task_vehicle;

public interface Vehicle {
    default void start(){
        System.out.println("Начинаю движение.");
    }
    void move();
    default void stop(){
        System.out.println("Останавливаюсь.");
    }
}
