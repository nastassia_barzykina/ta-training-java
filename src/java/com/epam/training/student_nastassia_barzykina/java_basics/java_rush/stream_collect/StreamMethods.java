package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream_collect;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.valueOf;

public class StreamMethods {
    public static void main(String[] args) {
        Stream<Integer> integerStream = Stream.of(1);// поток из 1 элемента
        Stream<Integer> integer = Stream.of(1, 3, 4, 2, 0);// поток из  элементов
        Stream<Double> doubleStreamRandom = Stream.generate(Math::random);
//        System.out.println(valueOf(doubleStreamRandom) + " - random");
        doubleStreamRandom
                .limit(10)
                .forEach(System.out::println);
        Stream<Integer> result = Stream.concat(integerStream, integer);
        result.forEach(System.out::println);
        System.out.println("___");
        Stream<Integer> integerF = Stream.of(1, 3, 4, 2, 0, -1);
        Stream<Integer> filter = integerF.filter(x -> (x < 3));
        filter.forEach(System.out::println);

        Stream<Integer> integerC = Stream.of(1, 3, 4, 2, 0, -1, 11);

        Stream<Integer> compare = integerC.sorted();
        compare.forEach(System.out::println);
        System.out.println("++++");

        Stream<Integer> stream = Stream.of(1, 2, 3, 4, 5, 2, 2, 2, 3, 4);
        Stream<Integer> unic = stream.distinct();
        unic.forEach(System.out::println);

        Stream<Integer> stream1 = Stream.of(1, 2, 3, 4, 5, 2, 2, 2, 3, 4);
        Stream<Integer> stream2 = stream1.limit(3); // 1, 2, 3
        System.out.println("Преобразование данных:");
        Stream<Integer> s1 = Stream.of(1, 2, 3, 4, 5, 2, 2, 2, 3, 4);
        //Stream<String> s2 = s1.map(x -> String.valueOf(x)); или:
        Stream<String> s2 = s1.map(String::valueOf);
        Stream<Integer> s3 = s2.map(Integer::parseInt);
        s3.forEach(System.out::println);

        List<String> list = new ArrayList<>();
        list.add("https://google.com");
        list.add("https://linkedin.com");
        list.add("https://yandex.com");
//        в конструкторe(!) класса URI checked-исключение URISyntaxException. А такие исключения нужно обязательно
//        заворачивать в try-catch:
        Stream<URI> uri = list.stream().map(str ->
        {
            try {
                return new URI(str);
            } catch (URISyntaxException e){
                e.printStackTrace();
                return null;
            }
        });
        uri.forEach(System.out::println);

        Stream<Integer> st = Stream.of(1, -2, 3, -4, 5);
        boolean res = st.filter(x -> x < 0).allMatch(x -> x < 0);//сначала пропускаем через фильтр только элементы меньше
                                                                // нуля, а потом уже среди них выполняем проверку
        System.out.println(res);
        System.out.println("Find-metods:");
        ArrayList<String> f = new ArrayList<>();
        String rez = list.stream().findFirst().get();
        System.out.println(rez);



    }
}
