package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.constructors_obj.task.earth;

public class SouthAmerica {
    private final int area;

    public SouthAmerica(int area) {
        this.area = area;
    }
}
