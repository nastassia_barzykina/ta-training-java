package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods;

public class Cube {
    public static void main(String[] args) {
        long number = 2;
        System.out.println(cube(number));
        System.out.println(ninthDegree(number));
    }
    public static long cube(long a){
      return a * a * a;
    }
    public static long ninthDegree(long n){
        return cube(cube(n));
    }
}
