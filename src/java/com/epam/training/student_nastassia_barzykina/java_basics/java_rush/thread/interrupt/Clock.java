package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread.interrupt;

public class Clock implements Runnable {
    /**
     * Т.к. много нитей могут вызвать метод run одного объекта, то объект Clock в своем методе run получает объект
     * вызвавшей его нити («текущей нити»).
     * Класс Clock (часы) будет писать в консоль раз в секунду слово «Tik», пока переменная isInterrupted текущей нити
     * равна false.
     *
     * Когда переменная isInterrupted станет равной true, метод run завершится.
     */
    public void run() {
        Thread current = Thread.currentThread();

        while (!current.isInterrupted()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                current.interrupt();
            }
            System.out.println("Tik");
        }
    }

    /**
     * Главная нить, запускает дочернюю нить – часы, которая должна работать вечно.
     * Ждет 10 секунд и отменяет задание, вызовом метода interrupt.
     *
     * Главная нить завершает свою работу.
     *
     * Нить часов завершает свою работу.
     */
    public static void main(String[] args) throws InterruptedException {
        Clock clock = new Clock();
        Thread clockThread = new Thread(clock);
        clockThread.start();

        Thread.sleep(10000);
        clockThread.interrupt();
        /**
         * в методе sleep есть автоматическая проверка переменной isInterrupted. Если нить вызовет метод sleep,
         * то этот метод сначала проверит, а не установлена ли для текущей (вызвавшей его нити) переменная isInterrupted
         * в true. И если установлена, то метод не будет спать, а выкинет исключение InterruptedException.
         *
         * myThread.isInterrupted(); // для проверки прерван ли поток
         * myThread.interrupt(); // команда для прерывания потока
         * //после прерывания потока у него генерируется исключение InterruptedException
         * // и в блоке catch можно что-то выполнить, например вывести сообщение
         * System.out.println("Поток был прерван");
         */
    }
}
