package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.constructors_obj.car;

public class Solution {
    public static void main(String[] args) {
        new ElectricCar();
        new GasCar();
        new HybridCar();

    }
}
