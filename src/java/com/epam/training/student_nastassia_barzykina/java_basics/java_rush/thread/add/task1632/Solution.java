package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread.add.task1632;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * 1. Создай 5 различных своих нитей (наследников класса Thread):
 * 1.1. Нить 1 должна бесконечно выполняться;
 * 1.2. Нить 2 должна выводить "InterruptedException" при возникновении исключения InterruptedException;
 * 1.3. Нить 3 должна каждые полсекунды выводить "Ура";
 * 1.4. Нить 4 должна реализовать интерфейс Message, при вызове метода showWarning нить должна останавливаться;
 * 1.5. Нить 5 должна читать с консоли числа пока не введено слово "N", а потом вывести в консоль сумму введенных чисел.
 * 2. В статическом блоке добавь свои нити в List<Thread> threads в перечисленном порядке.
 * 3. Нити не должны стартовать автоматически.
 * <p>
 * Подсказка:
 * Нить 4 можно проверить методом isAlive()
 */
public class Solution {
    public static List<Thread> threads = new ArrayList<>(5);

    static {
        threads.add(new ThreadFirst());
        threads.add(new ThreadSecond());
        threads.add(new ThreadThird());
        threads.add(new ThreadFourth());
        threads.add(new ThreadFive());
    }

    public static void main(String[] args) throws InterruptedException {
        threads.get(2).start();
        //Thread.sleep(3000);
        //System.out.println(threads.get(3).isAlive());
        threads.get(2).interrupt();


    }

    static class ThreadFirst extends Thread {
        @Override
        public void run() {
            while (true){

            }
        }
    }

    static class ThreadSecond extends Thread {
        @Override
        public void run() {
            try {
                Thread.sleep(0);
            } catch (InterruptedException e) {
                System.out.println("InterruptedException");
            }
//            try {//правильное решение
//                throw new InterruptedException();
//            } catch (InterruptedException e) {
//                System.out.println(e);
//            }
        }
    }

    static class ThreadThird extends Thread {
        @Override
        public void run() {
            while (true) {
                System.out.println("Ура");
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    static class ThreadFourth extends Thread implements Message {
        private boolean isCancel;
        public ThreadFourth(){
            this.isCancel = false;
        }
        @Override
        public void run() {
            while (isCancel == false){

            }
        }

        @Override
        public void showWarning() {
            isCancel = true;
//            if (isAlive()) {
//                interrupt();
//            }
        }
//        public void run() {//правильное решение
//            while (!isInterrupted()) {
//            }
//        }
//
//        public void showWarning() {
//            this.interrupt();
//        }
    }

        static class ThreadFive extends Thread {
            @Override
            public void run() {
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                String s = null;
                int num = 0;
                while (true) {
                    try {
                        s = reader.readLine();
                        if (s.equals("N")) {
                            break;
                        } else {
                            num += Integer.parseInt(s);
                        }
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
                System.out.println(num);
                try {
                    reader.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
