package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.task_exeption.resources;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

public class TaskTryWR2 {
    public static void main(String[] args) //throws IOException
    {
        try(Scanner scanner = new Scanner(System.in);
            BufferedReader bufferedReader = Files.newBufferedReader(Path.of(scanner.nextLine()))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            System.out.println("Something went wrong : " + e);
        }
        /**
         *         Scanner scanner = null;
         *         BufferedReader bufferedReader = null;
         *         try {
         *             scanner = new Scanner(System.in);
         *             String fileName = scanner.nextLine();
         *             bufferedReader = Files.newBufferedReader(Path.of(fileName));
         *             String line;
         *             while ((line = bufferedReader.readLine()) != null) {
         *                 System.out.println(line);
         *             }
         *         } catch (IOException e) {
         *             System.out.println("Something went wrong : " + e);
         *         } finally {
         *             if (bufferedReader != null) {
         *                 bufferedReader.close();
         *             }
         *             if (scanner != null) {
         *                 scanner.close();
         *             }
         *         }
         */
    }
}
