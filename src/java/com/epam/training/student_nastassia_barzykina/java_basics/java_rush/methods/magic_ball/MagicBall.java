package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods.magic_ball;

import java.util.Random;

/**
 * реализовать метод getPrediction() в классе MagicBall, который будет работать как шар предсказаний.
 * Он случайным образом возвращает фразу-ответ на заданный вопрос. В методе getPrediction() нужно использовать
 * метод nextInt(int) класса Random, который должен возвращать значения от 0 до 7 включительно.
 * В зависимости от результата работы метода nextInt(int) возвращается один из вариантов ответа:
 * 0 -> Бесспорно
 * 1 -> Определённо да
 * 2 -> Вероятнее всего
 * 3 -> Хорошие перспективы
 * 4 -> Спроси позже
 * 5 -> Попробуй снова
 * 6 -> Мой ответ — нет
 * 7 -> Весьма сомнительно
 * Иначе вернуть null.
 */
public class MagicBall {
    private static final String CERTAIN = "Бесспорно";//0
    private static final String DEFINITELY = "Определённо да";//1
    private static final String MOST_LIKELY = "Вероятнее всего";//2
    private static final String OUTLOOK_GOOD = "Хорошие перспективы";//3
    private static final String ASK_AGAIN_LATER = "Спроси позже";
    private static final String TRY_AGAIN = "Попробуй снова";
    private static final String NO = "Мой ответ — нет";
    private static final String VERY_DOUBTFUL = "Весьма сомнительно";

    public static String getPrediction() {
        Random r = new Random();
        int x = r.nextInt(8);
        //System.out.println("x = " + x);
        return switch (x){
            case 0 -> CERTAIN;
            case 1 -> DEFINITELY;
            case 2 -> MOST_LIKELY;
            case 3 -> OUTLOOK_GOOD;
            case 4 -> ASK_AGAIN_LATER;
            case 5 -> TRY_AGAIN;
            case 6 -> NO;
            case 7 -> VERY_DOUBTFUL;

            default -> null;
        };
        // вариант через массив:
//        String[] array = new String[]{CERTAIN,DEFINITELY,MOST_LIKELY,OUTLOOK_GOOD,ASK_AGAIN_LATER,TRY_AGAIN,NO,VERY_DOUBTFUL} ;
//        if (r <= 7 && r >= 0)
//            return array[r];
//        else
//            return null;
        // вариант тернарного оператора:
//        return x<8 && x>=0? array[x] : null;

        //напишите тут ваш код
        //return null;
    }
}
