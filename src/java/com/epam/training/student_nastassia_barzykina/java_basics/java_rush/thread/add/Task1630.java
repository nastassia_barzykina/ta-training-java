package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread.add;

import java.io.*;
import java.util.ArrayList;

/**
 * 1. Разберись, что делает программа.
 * 2. В статическом блоке считай 2 имени файла firstFileName и secondFileName.
 * 3. Внутри класса Solution создай нить public static ReadFileThread, которая реализует
 * интерфейс ReadFileInterface (Подумай, что больше подходит - Thread или Runnable).
 * 3.1. Метод setFileName должен устанавливать имя файла, из которого будет читаться содержимое.
 * 3.2. Метод getFileContent должен возвращать содержимое файла.
 * 3.3. В методе run считай содержимое файла, закрой поток. Раздели пробелом строки файла.
 * 4. Подумай, в каком месте нужно подождать окончания работы нити, чтобы обеспечить последовательный вывод файлов.
 * 4.1. Для этого добавь вызов соответствующего метода.
 * <p>
 * Ожидаемый вывод:
 * [все тело первого файла]
 * [все тело второго файла]
 * <p>
 * (квадратные скобки выводить не нужно)
 * Требования:
 * •	Статический блок класса Solution должен считывать с консоли имена двух файлов и сохранять их
 * в переменные firstFileName и secondFileName.
 * •	Объяви в классе Solution public static класс ReadFileThread.
 * •	Класс ReadFileThread должен реализовывать интерфейс ReadFileInterface.
 * •	Класс ReadFileThread должен быть унаследован от подходящего класса.
 * •	Метод run класса ReadFileThread должен считывать строки из файла, установленного методом setFileName.
 * А метод getFileContent, этого же класса, должен возвращать вычитанный контент. Возвращаемое значение - это одна строка,
 * состоящая из строк файла, разделенных пробелами.
 * •	Метод systemOutPrintln должен вызывать метод join у созданного объекта f.
 * •	Вывод программы должен состоять из 2х строк. Каждая строка - содержимое одного файла.
 */
public class Task1630 {
    public static String firstFileName;
    public static String secondFileName;

    static {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            firstFileName = reader.readLine();//"c:\\test\\chartest.txt"
            secondFileName = reader.readLine();//"c:\\test\\test.txt"
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
//напишите тут ваш код

public static void main(String[] args) throws InterruptedException {
    systemOutPrintln(firstFileName);
    systemOutPrintln(secondFileName);
}

public static void systemOutPrintln(String fileName) throws InterruptedException {
    ReadFileInterface f = new ReadFileThread();
    f.setFileName(fileName);
    f.start();
    f.join();
    System.out.println(f.getFileContent());
}

public static class ReadFileThread extends Thread implements ReadFileInterface {
    private String name;
    private ArrayList<String> content;

    public ReadFileThread() {
        this.name = null;
        this.content = new ArrayList<>();
    }

    public void run() {
        try {
            BufferedReader fileReader = new BufferedReader(new InputStreamReader(new FileInputStream(name)));
            while (fileReader.ready()) {
                content.add(fileReader.readLine());
            }
            fileReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("File "+ name +" not found");
        } catch (IOException e) {
            System.out.println("File "+ name +" can't read");
        }

    }

    @Override
    public void setFileName(String fullFileName) {
        this.name = fullFileName;
    }

    @Override
    public String getFileContent() {
        StringBuffer sbuf = new StringBuffer();
        for (String s : content) {
            sbuf.append(s).append(" ");
        }
        return sbuf.toString();
    }
}

public interface ReadFileInterface {

    void setFileName(String fullFileName);

    String getFileContent();

    void join() throws InterruptedException;

    void start();
}

//напишите тут ваш код
}
