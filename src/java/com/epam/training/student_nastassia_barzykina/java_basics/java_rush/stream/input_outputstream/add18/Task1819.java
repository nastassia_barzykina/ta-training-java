package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.add18;

import java.io.*;

/**
 * Считать с консоли 2 имени файла.
 * В начало первого файла записать содержимое второго файла так, чтобы получилось объединение файлов.
 * Закрыть потоки.
 *
 * Требования:
 * •	Программа должна два раза считать имена файлов с консоли.
 * •	Не используй в программе статические переменные.
 * •	Для первого файла создай поток для чтения и считай его содержимое.
 * •	Затем, для первого файла создай поток для записи(поток для записи должен быть один). Для второго - для чтения.
 * •	Содержимое первого и второго файла нужно объединить в первом файле.
 * •	Сначала должно идти содержимое второго файла, затем содержимое первого.
 * •	Созданные для файлов потоки должны быть закрыты.
 */
public class Task1819 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String file1 = /*br.readLine();*/"C:\\test\\dest.txt";//
        String file2 = /*br.readLine();*/"C:\\test\\file2.txt";//
        br.close();
        FileInputStream fis1 = new FileInputStream(file1);
        byte[] buffer = new byte[fis1.available()];
        while (fis1.available() > 0){
            fis1.read(buffer);
        }
        fis1.close();
        FileOutputStream fos = new FileOutputStream(file1);
        FileInputStream fis2 = new FileInputStream(file2);
        byte[] buffer2 = new byte[fis2.available()];
        while (fis2.available() > 0){
            fis2.read(buffer2);
        }
        fis2.close();
        fos.write(buffer2);
        fos.write(buffer);
        fos.close();
    }

}
