package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.abstrakt_money;

public class Hryvnia extends Money {
    public Hryvnia(double amount) {
        super(amount);
    }

    @Override
    public String getCurrencyName() {
        return "UAH";
    }
}
