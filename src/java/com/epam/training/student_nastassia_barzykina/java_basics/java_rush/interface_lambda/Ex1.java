package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Ex1 {
    public static void main(String[] args) {
        new Thread(() -> System.out.println("Hello world!")).start();

        String[] array1 = {"мама", "мыла", "раму"};
        String[] array2 = {"я", "очень", "люблю", "java"};
        String[] array3 = {"мир", "труд", "май"};

        List<String[]> arrays = new ArrayList<>();
        arrays.add(array1);
        arrays.add(array2);
        arrays.add(array3);

//        Comparator<String[]> sortByLength = new Comparator<String[]>() {
//            @Override
//            public int compare(String[] o1, String[] o2) {
//                return o1.length - o2.length;
//            }
//        };

//        Comparator<String[]> sortByWordsLength = new Comparator<String[]>() {
//            @Override
//            public int compare(String[] o1, String[] o2) {
//                int length1 = 0;
//                int length2 = 0;
//                for (String s : o1) {
//                    length1 += s.length();
//                }
//                for (String s : o2) {
//                    length2 += s.length();
//                }
//                return length1 - length2;
//            }
//        };

        arrays.sort((o1, o2) -> o1.length - o2.length);
        arrays.forEach(x -> {
            for (String el : x){
                System.out.print(el + " ");
            }
            System.out.println();
        });
        System.out.println();
        arrays.sort(((o1, o2) -> {
            int length1 = 0;
            int length2 = 0;
            for (String s : o1) {
                length1 += s.length();
            }
            for (String s : o2) {
                length2 += s.length();
            }
            return length1 - length2;
        }));
        arrays.forEach(x -> {
            for (String el : x){
                System.out.print(el + " ");
            }
            System.out.println();
        });


    }

}
