package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.enums;

/**
 * В методе main получи список всех констант из enum JavarushQuest с помощью метода values().
 * В методе main выведи порядковый номер каждого элемента с новой строки, используя метод ordinal().
 */
public class JQMain {
    public static void main(String[] args) {
        JavarushQuest[] quests = JavarushQuest.values();
        for (JavarushQuest num : quests){
            System.out.println(num.ordinal());
        }
//        String s = JavarushQuest.CS_50.toString();
//        System.out.println(s);
        JavarushQuest jr = JavarushQuest.valueOf("CS_50");
        System.out.println(jr);
        JavarushQuest javarushQuest = JavarushQuest.values()[5];
        System.out.println(javarushQuest);

    }
}
