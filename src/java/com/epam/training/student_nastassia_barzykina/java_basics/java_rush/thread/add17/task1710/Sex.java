package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread.add17.task1710;

public enum Sex {
    MALE("м"),
    FEMALE("ж");

    private final String value;// изм
    Sex(String s){
        value = s;
    }

    public String getValue() {// add
        return value;
    }

    public static Sex getSexByString(String s) {//add
        switch (s) {
            case "m": return Sex.MALE;//м
            case "f": return Sex.FEMALE;//ж
            default: throw new RuntimeException();
        }
    }
}
