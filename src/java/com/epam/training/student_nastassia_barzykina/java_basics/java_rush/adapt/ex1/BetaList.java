package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.adapt.ex1;

public interface BetaList {
    int getValue(int index);
    void setValue(int index, int value);
    int getSize();
    void setSize(int newSize);
}
class BetaSaveManager
{
    public static void saveList(BetaList list)
    {
        //какой-то код по сохранению объекта
        //типа BetaList в файл на диске
    }
    /**
     * BetaList – это один из интерфейсов, для взаимодействия кода фреймворка и кода, который будет использовать этот фреймворк.
     *
     * BetaSaveManager – класс фреймворка, метод которого saveList сохраняет на диск объект типа BetaList
     */
}
