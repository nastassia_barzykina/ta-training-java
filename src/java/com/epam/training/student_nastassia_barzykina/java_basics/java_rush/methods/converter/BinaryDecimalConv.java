package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods.converter;

/**
 * Публичный статический метод toBinary(int) должен переводить целое число, полученное в качестве входящего параметра,
 * из десятичной системы счисления в двоичную и возвращать его строковое представление. А публичный статический метод
 * toDecimal(String) наоборот — из строкового представления двоичного числа в десятичное число.
 * Методы работают только с положительными числами и не пустыми строками. Если входящий параметр меньше или равен 0,
 * метод toBinary(int) возвращает пустую строку. Если входящий параметр — пустая строка или null, то метод toDecimal(String)
 * возвращает 0.
 */
public class BinaryDecimalConv {
    public static void main(String[] args) {
        int decimalNumber = Integer.MAX_VALUE;
        //int decimalNumber = -1;
        System.out.println("Десятичное число " + decimalNumber + " равно двоичному числу " + toBinary(decimalNumber));
        String binaryNumber = "1111111111111111111111111111111";
        //String binaryNumber = "100";
        System.out.println("Двоичное число " + binaryNumber + " равно десятичному числу " + toDecimal(binaryNumber));
    }

    public static String toBinary(int decimalNumber) {
        String binaryNumber = "";
        if (decimalNumber <= 0){
        return binaryNumber;// not null!
        }
        while (decimalNumber != 0) {
            binaryNumber = (decimalNumber % 2) + binaryNumber;
            decimalNumber = decimalNumber / 2;
        }
        return binaryNumber;
    }

    public static int toDecimal(String binaryNumber) {
        int decimalNumber = 0;
        if (binaryNumber == null || binaryNumber == ""){
        return decimalNumber;
        }
        for (int i = 0; i < binaryNumber.length(); i++){
            int v = binaryNumber.charAt(binaryNumber.length()- 1 - i);
            //int value = Character.getNumericValue(binaryNumber.charAt(binaryNumber.length() - 1 - i));// возвращает числовое значение из указанной позиции строки
            decimalNumber = (int) (decimalNumber + v * Math.pow(2, i));
        }
        return decimalNumber;
    }

}
