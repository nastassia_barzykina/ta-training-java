package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor;

public class SolutionMin {
    public static void main(String[] args) {

    }

    public static int min(int a, int b){
//        return Math.min(a, b);
        return a < b ? a : b;
    }
    public static long min(long a, long b){
        return Math.min(a, b);
    }
    public static double min(double a, double b){
        return Math.min(a, b);

    }
}
