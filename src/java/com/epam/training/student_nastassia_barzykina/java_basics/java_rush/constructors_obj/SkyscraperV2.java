package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.constructors_obj;

public class SkyscraperV2 {
    private int floorsCount;
    private String developer;
    public SkyscraperV2(){
        this.floorsCount = 5;
        this.developer = "JavaRushDevelopment";
    }
    public SkyscraperV2(int floorsCount, String developer){
        this.floorsCount = floorsCount;
        this.developer = developer;
    }

    public static void main(String[] args) {
        SkyscraperV2 skyscraper = new SkyscraperV2();
        SkyscraperV2 skyscraperUnknown = new SkyscraperV2(50, "Неизвестно");
    }
}
