package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.variable.static_m;

public class Cat {
    private int age;
    Cat(int age){
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
