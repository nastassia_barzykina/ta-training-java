package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Разобраться, что программа умеет делать.
 * Все классы должны быть внутри класса Solution.
 * Добавить классы Cartoon, Thriller.
 * Разобраться, как мы получаем объект класса SoapOpera по ключу "soapOpera".
 * Аналогично получению объекта SoapOpera сделать:
 * добавить в MovieFactory.getMovie получение объекта Cartoon для ключа "cartoon".
 * добавить в MovieFactory.getMovie получение объекта Thriller для ключа "thriller".
 * 7. Считать с консоли несколько ключей (строк).
 * Важно: ввод заканчивается, как только вводится строка не совпадающая с одной из: "cartoon", "thriller", "soapOpera".
 * <p>
 * Создать переменную movie типа Movie и для каждой введенной строки (ключа):
 * получить объект используя MovieFactory.getMovie и присвоить его переменной movie.
 * вывести на экран movie.getClass().getSimpleName().
 * Требования:
 * •	Классы Cartoon и Thriller должны быть статическими и существовать внутри класса Solution.
 * •	Метод MovieFactory.getMovie должен возвращать объект типа Cartoon при передаче ему строки "cartoon" в качестве
 * параметра.
 * •	Метод MovieFactory.getMovie должен возвращать объект типа Thriller при передаче ему строки "thriller" в качестве параметра.
 * •	Метод main должен считывать строки с клавиатуры.
 * •	Метод main должен прекращать считывать строки с клавиатуры, если была введена некорректная строка
 * (не "cartoon", не "thriller" или не "soapOpera").
 * •	Для каждой введенной строки (в том числе для некорректной) необходимо вызвать метод MovieFactory.getMovie().
 * •	Для всех введенных корректных строк необходимо вывести на экран простые имена
 * (movie.getClass().getSimpleName()) типов объектов, возвращаемых методом MovieFactory.getMovie().
 * cartoon
 * thriller
 * soapOpera
 * movie
 */
public class MoveTask {
    public static void main(String[] args) throws Exception {
        BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
        String key;
        while (true) {
            key = read.readLine();
            Movie movie = MovieFactory.getMovie(key);
            boolean u = key.equals("cartoon") || key.equals("thriller") || key.equals("soapOpera");
            if (!u) {
                break;
            }
            /** правильное решение:
             * String key = null;
             *         while ((key = reader.readLine()) != null) {
             *             Movie movie = MovieFactory.getMovie(key);
             *             if (movie == null) {
             *                 return;
             *             }
             */
            System.out.println(movie.getClass().getSimpleName());
        }
        //ввести с консоли несколько ключей (строк), пункт 7

        /*
8 Создать переменную movie класса Movie и для каждой введенной строки(ключа):
8.1 получить объект используя MovieFactory.getMovie и присвоить его переменной movie
8.2 вывести на экран movie.getClass().getSimpleName()
        */

    }

    static class MovieFactory {

        static Movie getMovie(String key) {
            Movie movie = null;
            //создание объекта SoapOpera (мыльная опера) для ключа "soapOpera"
            if ("soapOpera".equals(key)) {
                movie = new SoapOpera();
            } else if ("cartoon".equals(key)) {
                movie = new Cartoon();
            } else if ("thriller".equals(key)){
                movie = new Thriller();
            }
            //напишите тут ваш код, пункты 5,6
            return movie;
        }
    }

    static abstract class Movie {
    }

    static class SoapOpera extends Movie {
    }

    static class Cartoon extends Movie {
    }

    static class Thriller extends Movie {
    }
}
