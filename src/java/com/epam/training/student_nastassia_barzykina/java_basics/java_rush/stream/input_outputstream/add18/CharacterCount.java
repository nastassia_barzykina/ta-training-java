package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.add18;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class CharacterCount {
    public static void main(String[] args) {
        String filePath = args[0];//"C:\\test\\cipher.txt"; //  фактический путь к файлу

        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            int englishLetterCount = 0;
            int currentChar;
            while ((currentChar = reader.read()) != -1) {
                char ch = (char) currentChar;
                if ((ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122)) {
                    // Проверяем, что символ - это буква английского алфавита
                    englishLetterCount++;
                }
            }
            System.out.println("Количество символов, соответствующих буквам английского алфавита: " + englishLetterCount);
        } catch (IOException e) {
            System.err.println("Ошибка при чтении файла: " + e.getMessage());
        }
    }
    public static boolean isAllEnglishChars(String str) {
        for (char c : str.toCharArray()) {
            //System.out.println("take: " + c + "->" + (int)c);
            if ((c >= 65 && c <= 90) || (c >= 97 && c <= 122)) {
                return true;
            }
        }
        return false;

    }
}
