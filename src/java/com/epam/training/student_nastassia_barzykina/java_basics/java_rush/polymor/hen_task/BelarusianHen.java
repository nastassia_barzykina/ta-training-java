package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.hen_task;

public class BelarusianHen extends Hen {
    private static int egg = 250;

    @Override
    public int getCountOfEggsPerMonth() {
        return egg;
    }

    @Override
    public String getDescription() {

        return String.format("%s Моя страна - %s. Я несу %d яиц в месяц.", super.getDescription(), Country.BELARUS, getCountOfEggsPerMonth());
    }
}
