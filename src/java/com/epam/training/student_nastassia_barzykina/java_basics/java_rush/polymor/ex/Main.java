package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.ex;

public class Main {
    public static void main(String[] args) {
        Cow cow = new Cow();
        cow.printName();//Я — корова

        Whale whale = new Whale();
        whale.printTest();
        whale.printName();//Я — кит
        whale.printAll();// Я — белая  Я — кит
        System.out.println("______");

        //Т.к. класс при наследовании получает все методы и данные класса-родителя, объект этого класса разрешается
        // сохранять (присваивать) в переменные класса-родителя (и родителя родителя, и т.д., вплоть до Object)
        Cow cow1 = new Whale();
        cow1.printName();//Я — кит
        cow1.printColor();// Я — белая
        // cow1.printTest(); -- недоступен, только методы типа переменной(кл. Cow)
        Whale whaleTest = (Whale) cow1;
        whaleTest.printTest(); // после сужающего приведения методы кл. Whale доступны

        Object o = new Whale();
        System.out.println(o.toString());//com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.ex.Whale@1b28cdfa

    }
}
