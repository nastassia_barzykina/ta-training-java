package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex.task_vehicle;

public class Car implements Vehicle{
//    @Override
//    public void start() {
//        System.out.println("Начинаю движение.");
//    }

    @Override
    public void move() {
        System.out.println("Еду со средней скоростью 70 км/ч.");
    }

//    @Override
//    public void stop() {
//        System.out.println("Останавливаюсь.");
//    }
}
