package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.hash_set;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;

public class PrintHashSet {
    public static void main(String[] args) {
        HashSet<String> set = new HashSet<>();
        set.add("Привет");
        set.add("Hello");
        set.add("Namaste");
        set.add("Hola");
        set.add("Bonjuor");
        set.add("Hi");
        Iterator<String> it = set.iterator();
        while (it.hasNext()){
            String str = it.next();
            System.out.println(str);
        }
        System.out.println("Task:");
        /**    метод print(HashSet<String>), который должен выводить в консоли все элементы множества, используя iterator().
         *
         */
        HashSet<String> words = new HashSet<>(Arrays.asList("Программированию обычно учат на примерах.".split(" ")));
        print(words);
    }
    public static void print(HashSet<String> words){
        Iterator<String> iterator = words.iterator();
        while (iterator.hasNext()){
            //String s = iterator.next();
            System.out.println(iterator.next());
        }

    }

}
