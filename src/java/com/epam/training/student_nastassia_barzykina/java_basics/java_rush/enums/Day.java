package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.enums;

public class Day {
    public static final Day MONDAY = new Day(0);// Список статических констант-значений
    public static final Day TUESDAY = new Day(1);
    public static final Day WEDNESDAY = new Day(2);
    public static final Day THURSDAY = new Day(3);
    public static final Day FRIDAY = new Day(4);
    public static final Day SATURDAY = new Day(5);
    public static final Day SUNDAY = new Day(6);

    private static final Day[] array = {MONDAY, TUESDAY,//Массив со всеми значениями типа Day
            WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY};

    private final int value;//Переменная со значением конкретного объекта Day

    private Day (int value)//private конструктор класса Day — объекты класса Day можно создавать только внутри класса Day.
    {
        this.value = value;
    }

    public int ordinal()//Метод ordinal нужно вызвать у объекта Day. Он возвращает значение объекта — value.
    {
        return this.value;
    }

    public static Day[] values()//Метод возвращает статический массив со всеми значениями класса Day
    {
        return array;
    }

    public static void main(String[] args) {
        Day[] days = Day.values();
        System.out.println(days[2]);
    }
}
