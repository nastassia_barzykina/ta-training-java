package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.wrapper;

import java.io.*;

/**
 * 1 Измени класс AmigoOutputStream так, чтобы он стал Wrapper-ом для класса FileOutputStream. Используй наследование.
 * 2 При вызове метода close() должны выполняться следующая последовательность действий:
 * 2.1 Вызвать метод flush().
 * 2.2 Записать в конец файла фразу "JavaRush © All rights reserved.", используй метод getBytes().
 * 2.3 Закрыть поток методом close().
 *
 * Требования:
 * •	Метод main изменять нельзя.
 * •	Класс AmigoOutputStream должен наследоваться от класса FileOutputStream.
 * •	Класс AmigoOutputStream должен принимать в конструкторе объект типа FileOutputStream.
 * •	Все методы write(), flush(), close() в классе AmigoOutputStream должны делегировать свое выполнение объекту FileOutputStream.
 * •	Метод close() должен сначала вызвать метод flush(), затем записать в конец файла текст, затем закрыть поток.
 */
public class Task1813 extends FileOutputStream{
    public static String fileName = "C:\\test\\result.txt";//"C:/tmp/result.txt";
    private FileOutputStream fileOutputStream;

    public Task1813(FileOutputStream fileOutputStream) throws FileNotFoundException {
        super(fileName);
        this.fileOutputStream = fileOutputStream;
    }

    @Override
    public void write(int b) throws IOException {
        fileOutputStream.write(b);
    }

    @Override
    public void write(byte[] b) throws IOException {
        fileOutputStream.write(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        fileOutputStream.write(b, off, len);
    }

    @Override
    public void flush() throws IOException {
        fileOutputStream.flush();
    }

    @Override
    public void close() throws IOException {
        flush();
        write("JavaRush © All rights reserved.".getBytes());
        fileOutputStream.close();
    }

    public static void main(String[] args) throws FileNotFoundException {
            new Task1813(new FileOutputStream(fileName));
    }

}
