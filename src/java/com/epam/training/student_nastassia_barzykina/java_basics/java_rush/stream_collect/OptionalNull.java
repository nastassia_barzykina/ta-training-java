package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream_collect;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class OptionalNull {
    public static void main(String[] args) {
        List<String> strings = new ArrayList<>();
        Collections.addAll(strings, "first", "second", null, "fourth", "fifth");

        printList(strings);
    }

    public static void printList(List<String> list) {
        String text = "Этот элемент равен null";
//        for (String el : list) {
//            Optional<String> s = Optional.ofNullable(el);
//            System.out.println(s.orElse(text));
//        }
        // без цикла:
//        list.stream()
//                .map(s -> Optional.ofNullable(s).orElse(text))
//                .forEach(System.out::println);
        // решение:
        list.forEach(s -> System.out.println(Optional.ofNullable(s).orElse(text)));

    }
}
