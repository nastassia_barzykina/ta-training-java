package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.constructors_obj;

public class EqualsStep {
    public boolean equals(Object obj)
    {
        if (this == obj)//1.Сравниваем ссылки.  В метод передали тот же самый объект, у которого вызвали метод
            return true;

        if (obj == null)//2. Переданный объект — null?
            return false;

        if (!(obj instanceof Integer))//3.Если переданный объект не типа Integer(класса)
            return false;

        Integer person = (Integer) obj;//4. Операция приведения типа
        // return this.name == person.name && this.age == person.age; - сравнение полей объектов
        // НО! this.name != null && this.name.equals(person.name), т. к. String
        return true;
        /**
         * Либо:
         * Person person = (Person) obj;
         *
         * if (this.age != person.age) - Если возрасты не равны, сразу return false
         *    return false;
         *
         * if (this.name == null) - Если this.name равно null, нет смысла сравнивать через equals. Тут либо второе поле name равно null, либо нет.
         *    return person.name == null;
         *
         * return this.name.equals(person.name);
         */

        //остальной код метода equals
    }
}
