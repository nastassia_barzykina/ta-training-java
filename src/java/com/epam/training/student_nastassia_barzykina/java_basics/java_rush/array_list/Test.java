package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.array_list;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static com.epam.training.student_nastassia_barzykina.java_basics.java_rush.array_list.Glasses.print;

public class Test {
    private int number = 54;
    private String string = "string";

    public static void main(String[] args) {
        var integer = 22;
        var string = "string";
        var array = new int[5];
        var strings = new ArrayList<String>();
        var bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        var maxValue = Integer.valueOf(Integer.MAX_VALUE);
        /**
         * преобразование строки в массив чаров:
         */
        String str = "Hello, World!";
        char[] arr = new char[str.length()];

        for (int i = 0; i< str.length(); i++){
        arr[i] = str.charAt(i);
        }
        for (char el:arr){
            System.out.println(el);
        }
//
//        print(array)
    }
}
