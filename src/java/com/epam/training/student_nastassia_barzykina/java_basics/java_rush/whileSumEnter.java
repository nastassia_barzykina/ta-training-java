package com.epam.training.student_nastassia_barzykina.java_basics.java_rush;

import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class whileSumEnter {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean exit = true;
        int sum = 0;
//        while (exit){// с проверкой на конкретно EXIT
//            String number = sc.nextLine();
//            if (!(number.equals("ENTER"))) {
//                sum += parseInt(number);
//            } else {
//                exit = false;
//            }
//        }
//        System.out.println(sum);
        while (sc.hasNextInt()){// пока с клавы вводится число(без проверки на exit)
            int number = sc.nextInt();
            sum += number;
            }
        System.out.println(sum);
//        while (!isExit)
//        {
//            String s = sc.nextLine();
//            sum = sum + parseInt(s);// когда s = exit, ошибка, не может распарсить
//            s += parseInt(s);
//            System.out.println(sum);
//            isExit = s.equals("EXIT");
//        }

        }
    }
