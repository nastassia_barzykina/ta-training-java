package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ExCount {
    public static void main(String[] args) throws IOException {
        /**
         * Подсчет суммы всех байт файла на диске
         */
        //создаем объект FileInputStream, привязанный к файлу «c:/data.txt». "C:\test\char.txt"
        FileInputStream inputStream = new FileInputStream("C:\\test\\char.txt");
        long sum = 0;

        while (inputStream.available() > 0) //пока остались непрочитанные байты
        {
            int data = inputStream.read(); //прочитать очередной байт
            sum += data; //добавить его к общей сумме
        }
        inputStream.close(); // закрываем поток

        System.out.println(sum); //выводим сумму на экран.
        // дополнить строку пробелами:
        String s = "Hello";
        s = String.format("%-6s", s);
        System.out.println(1 + s + 1);
        System.out.println(s.length() + " -- length of s");

    }
}
