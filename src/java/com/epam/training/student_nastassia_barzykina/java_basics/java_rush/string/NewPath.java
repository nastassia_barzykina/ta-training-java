package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.string;

public class NewPath {
    public static void main(String[] args) {
        String path = "/usr/java/jdk1.8/bin/";

        String jdk13 = "jdk-13";
        System.out.println(changePath(path, jdk13));
    }
//Версия jdk начинается со строки "jdk" и заканчивается на "/".
    public static String changePath(String path, String jdk) {
        return path.replaceFirst("jdk.+?/", jdk + "/");// - работает, но не прошло проверку. А так - "jdk.+?/", jdk + "/" - норм.
//        int index = path.indexOf("jdk");
//        int index2 = path.indexOf("/", index);// ищем "/" после символов "jdk"
//        String first = path.substring(0, index);
//        String second = path.substring(index2);
//        return first + jdk + second;
//        String oldVersion = path.substring(index, index2);//v3
//        return path.replace(oldVersion, jdk);//v3
    }
}
