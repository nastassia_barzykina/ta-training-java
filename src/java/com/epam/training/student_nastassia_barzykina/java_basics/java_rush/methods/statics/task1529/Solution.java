package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods.statics.task1529;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) {

    }

    static {
        Solution.reset();
        //add your code here - добавьте код тут
    }

    public static CanFly result;

    public static void reset() {
        BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
        try {
        String key = buff.readLine();
        if (key.equals("helicopter")) result = new Helicopter();
        if (key.equals("plane")) {
            int count = Integer.parseInt(buff.readLine());
            result = new Plane(count);
            //System.out.println("plane!!!");
        }
        buff.close();
        } catch (IOException e){
            e.printStackTrace();
        }
        //add your code here - добавьте код тут. Правильное решение:
//        try {
//            String key = buff.readLine();
//            if ("plane".equals(key)) {
//                key = buff.readLine();
//                result = new Plane(Integer.parseInt(key));
//            } else if ("helicopter".equals(key)) {
//                result = new Helicopter();
//            }
//            buff.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }
}
