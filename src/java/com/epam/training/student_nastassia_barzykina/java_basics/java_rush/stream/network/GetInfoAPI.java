package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Напиши программу, которая будет обращаться по ссылке к публичному API в интернете, получать данные и выводить их на экран.
 */
public class GetInfoAPI {
    public static void main(String[] args) throws IOException {
        URL url = new URL("https://javarush.ru/api/1.0/rest/projects");
        try (InputStream input = url.openStream();
             BufferedReader buffer = new BufferedReader(new InputStreamReader(input))) {
            while (buffer.ready()) {
                System.out.println(buffer.readLine());
            }
        }
        /**
         * Без BR:
         * byte[] buffer = input.readAllBytes();
         * String str = new String(buffer);
         * System.out.println(str);
         */

    }
}
