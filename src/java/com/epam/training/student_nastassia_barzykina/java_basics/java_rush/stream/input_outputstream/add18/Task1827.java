package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.add18;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Программа должна считать имя файла для операций CRUD с консоли.
 * •	В классе Solution не должны быть использованы статические переменные.
 * •	При запуске программы без параметров список товаров должен остаться неизменным.
 * •	При запуске программы с параметрами "-c productName price quantity" в конец файла должна добавится новая строка с товаром.
 * •	Товар должен иметь следующий id, после максимального, найденного в файле.
 * •	Форматирование новой строки товара должно четко совпадать с указанным в задании.
 * •	Созданные для файлов потоки должны быть закрыты.
 */
public class Task1827 {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String file = br.readLine();
        //String file = "C:\\test\\task.txt";
        List<String> list = new ArrayList<>();
        String newLine = "";
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            while (reader.ready()) {
                list.add(reader.readLine());
            }
        }
        if (args.length == 0) {
            return;
        }
        formatArgs(args);
        if ("-c".equals(args[0])) {
            newLine = create(list, args);
            for (String elem : list){
                System.out.println(elem);
            }
        }
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            for (String elem : list) {
                bw.write(elem);
                bw.newLine();
            }
            // write the last string
            bw.write(newLine);
            //BufferedWriter fout = new BufferedWriter(new FileWriter(fileName));
            //                for (String str : buffer)
            //                    fout.write(str + "\n");
            //
            //                // write the last string
            //                fout.write(String.format("%-8s%-30s%-8s%-4s",
            //                        id, args[1], args[2], args[3]));
            //
            //                fout.close();
        }
    }

    private static void formatArgs(String[] elem) {
        String name = "";
        for (int i = 1; i < elem.length - 2; i++) {
            name += elem[i] + " ";
        }
        elem[1] = String.format("%-30s", name);

        elem[2] = elem[elem.length - 2];
        elem[2] = String.format("%-8s", elem[2]);

        elem[3] = elem[elem.length - 1];
        elem[3] = String.format("%-4s", elem[3]);
    }

    public static String create(List<String> list, String[] args) {
        int[] id = new int[list.size()];
        int max = 0;
        for (int i = 0; i < list.size(); i++) {
            String isStr = list.get(i).substring(0, 8).trim();
            id[i] = Integer.parseInt(isStr);
            if (id[i] > max) {
                max = id[i];
            }
        }
        String newId = String.format("%-8s", max + 1);
        if (args[1].length() > 30) args[1] = args[1].substring(0, 30);
        if (args[2].length() > 8) args[2] = args[2].substring(0, 8);
        if (args[3].length() > 4) args[3] = args[3].substring(0, 4);
        String newProduct = newId + args[1] + args[2] + args[3];
//        list.add(newProduct);
        return newProduct;
    }
}
