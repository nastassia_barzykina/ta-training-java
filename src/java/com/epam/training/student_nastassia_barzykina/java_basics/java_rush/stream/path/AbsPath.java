package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.path;

import java.nio.file.Path;
import java.util.Scanner;

public class AbsPath {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        Path path = Path.of(str);
        System.out.println(path.isAbsolute()? path : path.toAbsolutePath());
    }
}
