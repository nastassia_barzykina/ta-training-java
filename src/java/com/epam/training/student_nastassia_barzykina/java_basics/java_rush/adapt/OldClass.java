package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.adapt;

// Класс, реализующий OldInterface
public class OldClass implements OldInterface {
    public void oldMethod() {
        System.out.println("Old Method");
    }
}

// Интерфейс, который необходимо адаптировать
interface OldInterface {
    void oldMethod();
}


// Новый интерфейс, который мы хотим использовать
interface NewInterface {
    void newMethod();
}

// Адаптер, преобразующий OldInterface в NewInterface
class Adapter implements NewInterface {
    private OldInterface oldObject;

    public Adapter(OldInterface oldObject) {
        this.oldObject = oldObject;
    }

    public void newMethod() {
        // Вызываем старый метод через адаптер
        oldObject.oldMethod();
    }
    //использование
    public static void main(String[] args) {
        // Создаем объект класса, реализующего OldInterface
        OldInterface oldObject = new OldClass();

        // Создаем адаптер, передавая ему объект OldInterface
        NewInterface adapter = new Adapter(oldObject);

        // Теперь можем использовать объект NewInterface, вызывая newMethod(),
        // который в свою очередь вызывает oldMethod() через адаптер
        adapter.newMethod();
    }
    /**
     * Здесь Adapter позволяет использовать объект класса OldClass (реализующего OldInterface) как объект,
     * реализующий NewInterface.
     */
}
