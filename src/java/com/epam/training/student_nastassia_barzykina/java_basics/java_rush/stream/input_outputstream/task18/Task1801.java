package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.task18;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 * Ввести с консоли имя файла.
 * Найти максимальный байт в файле, вывести его на экран.
 * Закрыть поток ввода-вывода.
 * <p>
 * Требования:
 * •	Программа должна считывать имя файла с консоли.
 * •	Для чтения из файла используй поток FileInputStream.
 * •	В консоль должен выводиться максимальный байт, считанный из файла.
 * •	Поток чтения из файла должен быть закрыт.
 */
public class Task1801 {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String fileName = br.readLine();
        FileInputStream inputStream = new FileInputStream(fileName);
        int max = 0;

        while (inputStream.available() > 0) //пока остались непрочитанные байты
        {
            int data = inputStream.read(); //прочитать очередной байт
            if (data > max) {
                max = data;
            }
        }
        inputStream.close(); // закрываем поток

        System.out.println(max);

    }

}
