package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.constructors_obj;

/**
 * нужно сделать так, что бы здание было универсальным, и его назначение можно было менять, не создавая нового.
 * Для этого создай метод initialize, который будет устанавливать значение полю type (определять тип здания),
 * а конструктор убери.
 */
public class Building {
    private String type;

//    public Building(String type) {
//        this.type = type;
//    }
    public void initialize(String type){
        this.type = type;
    }

    //напишите тут ваш код

    public static void main(String[] args) {
        //Building building = new Building("Ресторан");
        Building building = new Building();
        building.initialize("Барбершоп");
        System.out.println(building.type);
    }

}
