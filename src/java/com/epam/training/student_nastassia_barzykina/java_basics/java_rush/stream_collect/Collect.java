package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream_collect;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

public class Collect {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        Collections.addAll(list, "Hello", "how", "are", "You?");
        Set<String> result = list
                .stream()
                .filter(s -> Character.isUpperCase(s.charAt(0)))
                .collect(toSet());
        result.forEach(System.out::println);
        var numbers = Stream.of(-1, 10, 43, 0, -32, -4, 22);

        getPositiveNumbers(numbers).forEach(System.out::println);

        var stringStream = Stream.of("JavaRush", "CodeGym", "Amigo", "Elly", "Kim", "Risha");

        getFilteredStrings(stringStream).forEach(System.out::println);
    }
    public static List<Integer> getPositiveNumbers(Stream<Integer> numbers) {
        return numbers
                .filter(x -> (x > 0))
//                .collect(Collectors.toList());
        .collect(toList()); //- if import static method
    }

    public static Set<String> getFilteredStrings(Stream<String> stringStream) {
        return stringStream
                .filter(x -> (x.length() > 6))
                .collect(toSet());
    }
}
