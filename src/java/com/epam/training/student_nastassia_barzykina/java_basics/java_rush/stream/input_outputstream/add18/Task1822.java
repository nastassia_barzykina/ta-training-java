package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.add18;

import java.io.*;

/**
 * Считать с консоли имя файла.
 * Найти в файле информацию, которая относится к заданному id, и вывести ее на экран в виде, в котором она записана в файле.
 * Программа запускается с одним параметром: id (int).
 * Закрыть потоки.
 *
 * В файле данные разделены пробелом и хранятся в следующей последовательности:
 * id productName price quantity
 * где id - int.
 * productName - название товара, может содержать пробелы, String.
 * price - цена, double.
 * quantity - количество, int.
 *
 * Информация по каждому товару хранится в отдельной строке.
 *
 * Пример содержимого файла:
 * 194 хлеб 12.6 25
 * 195 масло сливочное 52.2 12
 * 196 молоко 22.9 19
 *
 * Пример вывода для id = 195:
 * 195 масло сливочное 52.2 12
 *
 * Требования:
 * •	Программа должна считать имя файла с консоли.
 * •	В методе main создай для файла поток для чтения.
 * •	Программа должна найти в файле и вывести информацию о id, который передается первым параметром.
 * •	Поток для чтения из файла должен быть закрыт.
 */
public class Task1822 {
    public static void main(String[] args)throws IOException {
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        String fileName = reader.readLine();//"C:\\test\\task1822.txt";//
//
//        try (BufferedReader fileReader = new BufferedReader(new FileReader(fileName))) {
//            while (fileReader.ready()) {
//                String s = fileReader.readLine();
//                if (s.startsWith(args[0])){
//                    System.out.println(s);
//                }
//            }
//        }не прошло
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String line;
        String f = reader.readLine();
        BufferedReader fr = new BufferedReader(new InputStreamReader(new FileInputStream(f)));

        while((line = fr.readLine()) != null)
            if (line.startsWith(args[0] + " "))// обязательно нужен пробел!!!
                System.out.println(line);

        fr.close();
        //не прошло:
//        BufferedReader file = new BufferedReader(new FileReader(reader.readLine()));
//        reader.close();
//
//        while (file.ready()) {
//            String s = file.readLine();
//            if (s.startsWith(args[0]))
//                System.out.println(s);
//        }
//        file.close();
    }
}
