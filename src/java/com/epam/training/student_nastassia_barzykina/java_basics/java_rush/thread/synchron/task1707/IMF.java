package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread.synchron.task1707;

public class IMF {
    private static IMF imf;
//    private int foo;
//    private String bar;
//    private static volatile IMF imf; - гарантирует отношение happens-before, напр, если в к-ре есть еще переменные
    public static IMF getFund() {
        if (imf == null){// см строку 7, иначе без volatile можем потерять значения переменных в случае многопоточности
            synchronized (IMF.class){
                if (imf == null){
                    imf = new IMF();
                }
            }
        }
        return imf;
    }

    private IMF(){
//        foo = 15;
//        bar = "ttt";
    }
}
