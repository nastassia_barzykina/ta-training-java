package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.data.formatter_data;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * В методе main выведи на экран переменную localDateTime в таком виде:
 * 19.03.2020г. 5ч.4мин
 */
public class TaskPattern {
    static LocalDateTime localDateTime = LocalDateTime.now();

    public static void main(String[] args) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyyг. Hч.mмин");
        String text = dtf.format(localDateTime);
        System.out.println(text);//26.02.2024г. 12ч.28мин
    }

}
