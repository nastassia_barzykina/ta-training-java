package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread.interrupt;

/**
 * 1. Разберись, что делает программа.
 * 2. Реализуй логику метода run так, чтобы каждую секунду через пробел
 * выдавался отсчет начиная с numSeconds до 1, а потом слово [Марш!] (см примеры).
 * 3. Если нить работает 3.5 секунды или более, прерви ее методом interrupt и внутри нити выведи в консоль слово [Прервано!].
 * <p>
 * Пример для numSeconds=4 :
 * "4 3 2 1 Прервано!"
 * <p>
 * 4. Если нить работает менее 3.5 секунд, она должна завершиться сама.
 * Пример для numSeconds=3 :
 * "3 2 1 Марш!"
 * <p>
 * PS: метод sleep выбрасывает InterruptedException.
 */
public class Task1617 {
    public static volatile int numSeconds = 4;

    public static void main(String[] args) throws InterruptedException {
        RacingClock clock = new RacingClock();
        Thread.sleep(3500);
        clock.interrupt();
        //add your code here - добавь код тут
    }

    public static class RacingClock extends Thread {
        public RacingClock() {
            start();
        }

        public void run() {
//            int count = numSeconds;
            while (numSeconds > 0) {
                System.out.print(numSeconds-- + " ");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    System.out.println("Прервано!");
                    return;
                }
            }
            System.out.println("Марш!");

//                    if (count == 3){
//                    }
//                    if (count  == 4){
//                        System.out.println("Прервано!");
//                    }
//            try {// правильное решение:
//                while (!isInterrupted() && numSeconds >= 0) {
//                    if (numSeconds == 0) {
//                        System.out.println("Марш!");
//                    } else {
//                        System.out.print(numSeconds + " ");
//                        Thread.sleep(1000);
//                    }
//                    numSeconds--;
//                }
//            } catch (InterruptedException e) {
//                if (numSeconds != -1) {
//                    System.out.println("Прервано!");
//                }
//            }
        }
        //add your code here - добавь код тут
    }
}
