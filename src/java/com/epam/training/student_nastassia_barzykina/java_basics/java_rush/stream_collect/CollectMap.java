package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream_collect;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectMap {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<String>();
        Collections.addAll(list, "a=2", "b=3", "c=4", "d==3", "e");

        Map<String, String> result = list.stream()
                .map( e -> e.split("=") )
                .filter( e -> e.length == 2 )
                .collect( Collectors.toMap(e -> e[0], e -> e[1]) );
        String res = list.stream().collect( Collectors.joining(", ") );
        result.forEach((k, v) -> System.out.println(k + " - " + v));
        System.out.println("Joining res string: " + res);
//        for (var key : result.entrySet()){// print Map
//            String sym = key.getKey();
//            String num = key.getValue();
//            System.out.println(sym + " : " + num);
//        }
//        System.out.println("Print trow forEach:");

        //Task:
        var stringStream = Stream.of("JavaRush", "CodeGym", "Amigo", "Elly", "Kim", "Risha");

        getMap(stringStream).forEach((s, i) -> System.out.println(s + " - " + i));
    }

    public static Map<String, Integer> getMap(Stream<String> stringStream) {
        return stringStream
                .collect(Collectors.toMap(e -> e, e -> e.length()));// String::length
    }

}
