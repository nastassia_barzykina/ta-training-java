package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods;

/**
 * метод printArray() выводит в консоли все элементы массива.
 * реализовать метод reverseArray(). Он должен менять порядок элементов массива на обратный.
 * Метод должен работать только с массивами целочисленных значений (int[]).
 *
 * Пример:
 * Если массив содержал элементы:
 * 1, 2, 3, 4, 5, 6, 7, 8, 9, 0
 * то после вызова метода reverseArray() должен содержать:
 * 0, 9, 8, 7, 6, 5, 4, 3, 2, 1
 */
public class reverse_array {
    public static void main(String[] args) {
        int[] array = {11, 22, 33, 44, 55, 66, 77, 88, 99, 101};
        printArray(array);
        reverseArray(array);
        printArray(array);
    }

    public static void reverseArray(int[] array) {
//        int[] tmp = new int[array.length];
//        int j = array.length - 1;
//        for (int i = 0; i <= j; i++){
//            tmp[i] = array[j - i];
//        }
//        for (int i = 0; i <= j ; i++){
//            array[i] = tmp[i];
//        }
        /**
         * правильное решение:
         */
        for (int i = 0; i < array.length / 2; i++) {
            int temp = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = temp;
        }
    }

    public static void printArray(int[] array) {
        for (int i : array) {
            System.out.print(i + ", ");
        }
        System.out.println();
    }
}
