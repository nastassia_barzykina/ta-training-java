package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.data.class_date;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class DataEx {
    public static void main(String[] args)throws Exception {
//        текущая дата
        Date current = new Date();
        System.out.println(current);

//        Задание определенной даты (Год нужно задавать от 1900. Месяцы нумеруются с нуля.)
        Date birthday = new Date(80, 3, 13);
        System.out.println(birthday);

//        Задание определенного времени
        Date date = new Date(124, 13, 18, 11, 11, 0);
        System.out.println(date);// Sat Apr 13 12:15:00 GET 2024
        System.out.println(date.getYear() + 1900);
        System.out.println(date.getMonth());// 12 == 0, 13 == 1 ,etc
//        System.out.println(date.getDate());
//        System.out.println(date.getDay());// 6 = Sat

        Locale ru = new Locale("ru", "RU");
//        получение названия дня недели даты на русском:
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE", ru);// шаблон и локация
        String dayOfWeek = formatter.format(birthday);
        System.out.println(dayOfWeek + " - день недели");

        System.out.println(" - проверка");

//        GregorianCalendar calendar = new GregorianCalendar(2017, Calendar.JANUARY , 24);
//        DayOfWeek dow = LocalDate.now().getDayOfWeek();
        LocalDate localDate = LocalDate.of(1980, Month.APRIL, 13);//год в норм формате
        DayOfWeek birth = localDate.getDayOfWeek();
        System.out.println("день недели - " + birth);


        System.out.println(birth.getDisplayName(TextStyle.FULL, ru));  //  день недели
        System.out.println(birth.getDisplayName(TextStyle.SHORT, ru)); //
    }
}
