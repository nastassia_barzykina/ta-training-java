package com.epam.training.student_nastassia_barzykina.java_basics.java_rush;

import java.util.Scanner;

public class MinOfTwoNumberInput {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] ints = {8, 4, 7, 4, 5, 9, 5};
        var tmp = getTmp(ints);
        System.out.println(tmp);
    }

    private static int getTmp(int[] ints) {
        int i = 0;
        int MIN = Integer.MAX_VALUE;
        int tmp = MIN;
        while (i < ints.length){
            int num = ints[i++];
//            int num = sc.nextInt();
//            if (num < MIN){
//                tmp = MIN;
//                MIN = num;
//            } else if (num > MIN && num <= tmp){
//                tmp = num;
            if (num < tmp) {
                if (num < MIN) {
                    tmp = MIN;
                    MIN = num;
                } else {
                    tmp = num;
                }
            }
        }
        return tmp;



    }

}




