package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.constructors_obj;

import java.util.Objects;

public class Iphone {
    private String model;
    private String color;
    private int price;

    public Iphone(String model, String color, int price) {
        this.model = model;
        this.color = color;
        this.price = price;
    }
    public boolean equals(Object object){
        if (this == object){
            return true;
        }
        if (object == null){
            return false;}
        if (!(object instanceof Iphone)){
            return false;
        }
        Iphone iphone = (Iphone) object;
        if (this.price != iphone.price){
            return false;
        }
//        if (this.model == null){
//            return iphone.model == null;
//        }
//        if (this.color == null){
//            return iphone.color == null;
//        }
       // return this.color.equals(iphone.color) && this.model.equals(iphone.model);// nullPointerEx
          return Objects.equals(this.color, iphone.color) && Objects.equals(this.model, iphone.model);

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) {
//            return true;
//        }
//        if (o == null || getClass() != o.getClass()) {
//            return false;
//        }
//        Iphone iphone = (Iphone) o;
//        return price == iphone.price &&
//                Objects.equals(model, iphone.model) &&
//                Objects.equals(color, iphone.color);
//    }
}

    //напишите тут ваш код

    public static void main(String[] args) {
        Iphone iphone1 = new Iphone(null, "Black", 999);
        Iphone iphone2 = new Iphone(null, "Black", 999);

        System.out.println(iphone1.equals(iphone2));
        int a = 5;
        int b = 5;
        System.out.println(a == b);
    }
}
