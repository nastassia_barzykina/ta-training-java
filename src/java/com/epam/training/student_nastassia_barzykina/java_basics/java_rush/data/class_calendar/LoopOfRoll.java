package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.data.class_calendar;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class LoopOfRoll {
    public static void main(String[] args) {
        Calendar start = new GregorianCalendar(2014, Calendar.JANUARY, 2);
        Calendar end = new GregorianCalendar(2014, Calendar.FEBRUARY, 2);

        System.out.print("Старт");
        while (start.before(end)) {
            start.add(Calendar.DATE, 1);// not roll()!!!
            System.out.print(".");
        }
        System.out.print("Финиш");
    }
}
