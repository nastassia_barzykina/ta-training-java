package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.data.localdate;

import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;

public class ExamplesLocalDate {
    public static void main(String[] args) {
        LocalDate today = LocalDate.now();
        System.out.println(today);

        ZoneId timezone = ZoneId.of("Asia/Shanghai");
        LocalDate todayS = LocalDate.now(timezone);
        System.out.println("В Шанхае сейчас " + todayS);

        LocalDate date = LocalDate.of(2019, Month.MARCH, 19);
        System.out.println(date);

        LocalDate date100 = LocalDate.ofYearDay(2024, 100);
        System.out.println("Day №100 : " + date100);

        LocalDate dateUnix = LocalDate.EPOCH;
        System.out.println(dateUnix);
        LocalDate dateFromUnix = LocalDate.ofEpochDay(15481);
        System.out.println(dateFromUnix + " - 15481 day of Unix-time");

        System.out.println(today.getYear());
        System.out.println(today.getMonth());
        System.out.println(today.getMonthValue());
        System.out.println(today.getDayOfMonth());
        System.out.println(today.getDayOfWeek());
        System.out.println(today.getEra());// Возвращает эру: константа BC (Before Current Era) и CE(Current Era)

//Объект birthday, чьи методы мы вызываем, не меняется.
// Вместо этого его методы возвращают новые объекты, которые и содержат нужные данные.
        LocalDate birthday = LocalDate.of(2019, 2, 28);
        LocalDate nextBirthday = birthday.plusYears(1);
        LocalDate firstBirthday = birthday.minusYears(30);

        System.out.println(birthday + " - birthday");
        System.out.println(nextBirthday + " - next birthday");
        System.out.println(firstBirthday + " - first birthday");


    }
}
