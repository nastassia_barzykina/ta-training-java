package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.wrapper.task1812;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Используя шаблон проектирования Wrapper (Decorator) расширь функциональность AmigoOutputStream.
 * В классе QuestionFileOutputStream при вызове метода close() должна быть реализована следующая функциональность:
 * 1. Вывести в консоль фразу "Вы действительно хотите закрыть поток? Д/Н".
 * 2. Считай строку.
 * 3. Если считанная строка равна "Д", то закрыть поток.
 * 4. Если считанная строка не равна "Д", то не закрывать поток.
 */

public class QuestionFileOutputStream implements AmigoOutputStream {
    private AmigoOutputStream amigo;

    QuestionFileOutputStream(AmigoOutputStream amigo) {
        //super();
        this.amigo = amigo;
    }

    @Override
    public void flush() throws IOException {
        amigo.flush();
    }

    @Override
    public void write(int b) throws IOException {
        amigo.write(b);
    }

    @Override
    public void write(byte[] b) throws IOException {
        amigo.write(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        amigo.write(b, off, len);
    }

    @Override
    public void close() throws IOException {
        System.out.println("Вы действительно хотите закрыть поток? Д/Н");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String answer = br.readLine();
        br.close();
        if ("Д".equals(answer)) {
            amigo.close();
        }
    }
}
