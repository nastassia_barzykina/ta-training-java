package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.task18;

import java.io.*;

/**
 * Считать с консоли три имени файла: файл1, файл2, файл3.
 * Разделить файл1 по следующему критерию:
 * Первую половину байт записать в файл2, вторую половину байт записать в файл3.
 * Если в файл1 количество байт нечетное, то файл2 должен содержать большую часть.
 * Закрыть потоки.
 */

public class Task1808 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String file1 = /*br.readLine();*/"C:\\test\\test.txt";//
        String file2 = /*br.readLine();*/"C:\\test\\file2.txt";//
        String file3 = /*br.readLine();*/"C:\\test\\file3.txt";//
//        File file = new File(file1);
//        long size = file.length();
//        System.out.println(size);
        FileInputStream inputStream = new FileInputStream(file1);
        FileOutputStream outputStream1 = new FileOutputStream(file2);
        FileOutputStream outputStream2 = new FileOutputStream(file3);
        int size = inputStream.available();
        byte[] buffer = new byte[size];
        System.out.println(inputStream.available());
        while (inputStream.available() > 0) {
            inputStream.read(buffer);
            if (size % 2 == 0){
            outputStream1.write(buffer, 0, (size / 2));
            outputStream2.write(buffer, (size / 2), (size - (size / 2)));
            } else {
                outputStream1.write(buffer, 0, (size / 2 + 1));
                outputStream2.write(buffer, (size / 2 + 1), (size - (size / 2 + 1)));
            }
            // решение:
//            int halfOfFile = (fileInputStream.available() + 1) / 2;
//            int numberOfByte = 0;
//            while (fileInputStream.available() > 0) {
//                if (numberOfByte < halfOfFile) {
//                    fileOutputStream1.write(fileInputStream.read());
//                    numberOfByte++;
//                } else fileOutputStream2.write(fileInputStream.read());
//            }
        }
        inputStream.close();
        outputStream2.close();
        outputStream1.close();
    }
}
