package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;
/**
 *  необходимо развернуть вывод в обратном порядке с помощью PrintStream. Метод printSomething(String) выводит в поле stream
 *  переданную строку. Необходимо развернуть вывод в обратном порядке с помощью PrintStream.
 * В методе main(String[] args) считывается строка с клавиатуры и передается в метод printSomething(String), который записывает
 * полученную строку в поток stream.
 * Твоя задача — в методе main(String[]) развернуть переданную строку с помощью поля outputStream (то есть, outputStream
 * должен хранить в себе перевернутую строку) и вывести в консоли.
 *
 * Пример:
 * Входные данные:
 * it's a text for testing
 * Вывод:
 * gnitset rof txet a s'ti
 *
 * Метод printSomething(String) не изменяй
 */
public class OSReverse {
    public static ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    public static PrintStream stream = new PrintStream(outputStream);

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        var input = scanner.nextLine();
        StringBuilder sb = new StringBuilder(input);
        String result = sb.reverse().toString();
        printSomething(result);// запись в поток outputStream при помощи метода через переменную stream
        System.out.println(result);
        /** Правильное решение:
         * Scanner scanner = new Scanner(System.in);
         *         printSomething(scanner.nextLine());
         *         String result = outputStream.toString();// это было
         *         outputStream.reset();// очистка потока OS
         *         StringBuilder stringBuilder = new StringBuilder(result);
         *         String reverse = stringBuilder.reverse().toString();
         *         printSomething(reverse);
         *         System.out.println(outputStream);
         *
         *         Вариант через цикл:
         *         Scanner scanner = new Scanner(System.in);
         *         var input = scanner.nextLine();
         *         var bytes = input.getBytes();
         *         for (int i = 0; i < bytes.length / 2; i++){
         *             byte tmp = bytes[i];
         *             bytes[i] = bytes[bytes.length - i - 1];
         *             bytes[bytes.length - i - 1] = tmp;
         *         }
         *         String result = new String(bytes);
         *          printSomething(result);
         *         System.out.println(result);
         */
    }
    public static void printSomething(String str) {
        stream.print(str);
    }
}
