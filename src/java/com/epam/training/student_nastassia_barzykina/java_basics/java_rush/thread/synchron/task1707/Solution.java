package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread.synchron.task1707;

public class Solution {
    public static void main(String[] args) {
        IMF fund = IMF.getFund();
        IMF anotherFund = IMF.getFund();
        System.out.println(fund == anotherFund);
    }
}
