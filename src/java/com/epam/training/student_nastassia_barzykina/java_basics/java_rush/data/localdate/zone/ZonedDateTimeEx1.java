package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.data.localdate.zone;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ZonedDateTimeEx1 {
    public static void main(String[] args) {
        //С помощью объекта LocalDateTime, содержащего только информацию о дате и времени, мы добавляем информацию о
        // часовом поясе, чтобы создать объект ZonedDateTime, содержащий информацию о дате, времени и часовом поясе.
        // LocalDateTime:
        LocalDateTime myLocalDateTime = LocalDateTime.of(2020, Month.MAY, 15, 13, 45, 30);

        System.out.println("My LocalDateTime: " + myLocalDateTime); // 2020-05-15T13:45:30

        // UTC+7 (Ho Chi Minh Vietnam).
        ZoneId vnZoneId = ZoneId.of("Asia/Ho_Chi_Minh");

        // Add time zone information to LocalDateTime.
        ZonedDateTime vnDateTime = ZonedDateTime.of(myLocalDateTime, vnZoneId);

        System.out.println("VN DateTime: " + vnDateTime); // 2020-05-15T13:45:30+07:00[Asia/Ho_Chi_Minh]

        // Same as:
        ZonedDateTime vnDateTime2 = myLocalDateTime.atZone(vnZoneId);

        System.out.println("VN DateTime: " + vnDateTime2); // 2020-05-15T13:45:30+07:00[Asia/Ho_Chi_Minh]
    }
}
