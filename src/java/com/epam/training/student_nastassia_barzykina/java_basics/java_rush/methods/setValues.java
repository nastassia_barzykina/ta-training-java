package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods;

/**
 * Реализуй метод setValues(long value), чтобы он устанавливал полученное значение параметра value переменным a, b, c и d.
 * Изменять типы переменных a, b, c и d нельзя.
 */
public class setValues {
    public static byte a;
    public static short b;
    public static int c;
    public static long d;

    public static void main(String[] args) {
        setValues(100000);
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);
        System.out.println();
        long x = 109 + 15;
        int y = (int) (x * 2);
        short z = (short) (x / y);
        byte q = (byte) (x + y - z);
        System.out.println(x);
        System.out.println(y);
        System.out.println(z);
        System.out.println(q);
    }

    public static void setValues(long value){
        a = (byte) value;
        b = (short) value;
        c = (int) value;
        d = value;
        //напишите тут ваш код
    }
}
