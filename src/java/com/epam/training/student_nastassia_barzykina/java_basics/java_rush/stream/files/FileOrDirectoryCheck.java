package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.files;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

/**
 * считывать с клавиатуры строки, и если данная строка — это путь к существующему файлу, выводить в консоли "<введенная строка> - это файл". Если путь к существующей директории, выводить в консоли "<введенная строка> - это директория".
 * Если строка не является путем к файлу или директории, то выходим из программы. Треугольные скобки и кавычки выводить не нужно.
 * Для проверки файлов и директорий используй методы isRegularFile() и isDirectory() класса Files.
 * <p>
 * Пример вывода:
 * C:\javarush\text.txt - это файл
 * C:\javarush\ - это директория
 * <p>
 * Программа должна считывать из консоли пути к файлам/директориям до тех пор, пока не будет введен некорректный путь
 */
public class FileOrDirectoryCheck {
    private static final String THIS_IS_FILE = " - это файл";
    private static final String THIS_IS_DIR = " - это директория";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Path path;
        try {
            while (true) {
                String str = scanner.nextLine();
                path = Path.of(str);
                if (!Files.exists(path) || str.isEmpty())
                    break;
                if (Files.isDirectory(path)) {
                    System.out.println(path + THIS_IS_DIR);
                } else if (Files.isRegularFile(path)) {
                    System.out.println(path + THIS_IS_FILE);
                }
//            if (Files.exists(path)){
//                System.out.println(Files.isDirectory(path) ? path + THIS_IS_DIR : path + THIS_IS_FILE);
            }
        } catch (Exception e) {
            //
        }
/**Правильное решение (и без try-catch):
 * while (true) {
 *             String str = scanner.nextLine();
 *             if (str.isEmpty()) {
 *                 break;
 *             }
 *             if (Files.isRegularFile(Path.of(str))) {
 *                 System.out.println(str + THIS_IS_FILE);
 *             } else if (Files.isDirectory(Path.of(str))) {
 *                 System.out.println(str + THIS_IS_DIR);
 *             } else {
 *                 break;
 *             }
 *         }
 *
 */


    }
}
