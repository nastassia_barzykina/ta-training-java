package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_lambda.tasks;

public class Task2 {
    public static void main(String[] args) {
        //double diskr = b^2 — 4ac;
        DoubleInt<Double> diskr = ((a, b, c) -> ((b * b) - 4 * a * c));
        DoubleInt<Double> res = ((a, b, c) -> (a * Math.pow(b, c)));
        System.out.println(diskr.test(2.0, 3.0, 5.0));
        System.out.println(res.test(2.0, 3.0, 5.0));

    }
}
interface DoubleInt <Double>{
    double test(double d1, double d2, double d3);
}
