package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.string.path;

import java.util.Scanner;

public class FilePath2 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        String path = console.nextLine();
        String[] array = path.split("/");//Преобразование строки в массив строк. В качестве разделителя используется символ /
        String result = String.join("\\", array);// Объединяем все строки из массива строк, в качестве разделителя используется символ \ (экранирован)
        System.out.println(result);
    }
}
