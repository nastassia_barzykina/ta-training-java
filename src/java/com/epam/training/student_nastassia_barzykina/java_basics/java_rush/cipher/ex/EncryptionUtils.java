package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.cipher.ex;

public class EncryptionUtils {// XOR-шифрование
    public static byte[] encrypt(String text, String key) {
        byte[] textBytes = text.getBytes();
        byte[] keyBytes = key.getBytes();
        byte[] result = new byte[textBytes.length];
        for (int i = 0; i < textBytes.length; i++) {
            result[i] = (byte) (textBytes[i] ^ keyBytes[i % keyBytes.length]);
        }
        return result;
    }

    public static String decrypt(byte[] encryptedBytes, String key) {
        byte[] result = new byte[encryptedBytes.length];
        byte[] keyBytes = key.getBytes();
        for (int i = 0; i < encryptedBytes.length; i++) {
            result[i] = (byte) (encryptedBytes[i] ^ keyBytes[i % keyBytes.length]);
        }
        return new String(result);
    }
}
