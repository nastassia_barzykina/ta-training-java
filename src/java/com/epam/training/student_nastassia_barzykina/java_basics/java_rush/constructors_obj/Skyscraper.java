package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.constructors_obj;

public class Skyscraper {
    public static final String SKYSCRAPER_WAS_BUILD = "Небоскреб построен.";
    public static final String SKYSCRAPER_WAS_BUILD_FLOORS_COUNT = "Небоскреб построен. Количество этажей - ";
    public static final String SKYSCRAPER_WAS_BUILD_DEVELOPER = "Небоскреб построен. Застройщик - ";
    //public int floor;
    //public String developer;

    public Skyscraper(){
        System.out.println(SKYSCRAPER_WAS_BUILD);
    }
    public Skyscraper(int floor){
        //this.floor = floor;
        System.out.println(SKYSCRAPER_WAS_BUILD_FLOORS_COUNT + floor);
    }
    public Skyscraper(String developer){
        //this.developer = developer;
        System.out.println(SKYSCRAPER_WAS_BUILD_DEVELOPER + developer);

    }

    public static void main(String[] args) {
        Skyscraper skyscraper = new Skyscraper();
        Skyscraper skyscraperTower = new Skyscraper(50);
        Skyscraper skyscraperSkyline = new Skyscraper("JavaRushDevelopment");
        //System.out.println(skyscraper);
    }
}
