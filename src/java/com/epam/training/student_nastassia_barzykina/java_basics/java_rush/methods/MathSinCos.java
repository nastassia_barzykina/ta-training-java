package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods;

/**
 * Реализуй методы sin(double), cos(double), tan(double) которые возвращают синус, косинус и тангенс угла соответственно,
 * полученного как параметр. Угол задан в градусах.
 * В этом тебе помогут соответствующие методы класса Math, которые принимают параметром угол, заданный в радианах.
 */
public class MathSinCos {
    public static double sin(double a) {
        //напишите тут ваш код
        return Math.sin(Math.toRadians(a));
    }

    public static double cos(double a) {
        //напишите тут ваш код
        return Math.cos(Math.toRadians(a));
    }

    public static double tan(double a) {
        //напишите тут ваш код
        return Math.tan(Math.toRadians(a));
    }

    public static void main(String[] args) {
        double grees = 45;
        System.out.println(sin(grees));
        System.out.println(cos(grees));
        System.out.println(tan(grees));
    }
}
