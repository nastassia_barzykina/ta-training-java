package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods.overloading.task1523;

public class SubSolution extends Solution {
    public SubSolution() {
    }

    protected SubSolution(int i) {
        super(i);
    }

    SubSolution(String s) {
        super(s);
    }
}
