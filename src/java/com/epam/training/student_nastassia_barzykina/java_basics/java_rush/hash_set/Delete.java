package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.hash_set;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class Delete {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("5");
        list.add("5");
        list.add("kkk");
        list.add("Hello");
        list.add("dfgdf");
//        for (int i = 0; i < list.size(); i++)
//        {
//            String str = list.get(i);
//
//            if (str.equals("Hello"))
//            {
//                list.remove(str);
//                i--;    // нужно уменьшить i, т.к. после удаления элементы сдвинулись
//            }
//        }
        ArrayList<String> listCopy = new ArrayList<>(list);
        for (String s : listCopy){
            if (s.equals("Hello")){
                list.remove(s);
            }
        }
        for (String el: list){
            System.out.println(el);
        }
        HashSet<String> stringHashSet = new HashSet<>(list);
        for (String el: stringHashSet){
            System.out.println(el);
        }
        HashSet<String> stringHashSetCopy = new HashSet<>(stringHashSet);
        for (String sSet : stringHashSetCopy){
            if (sSet.equals("5")){
                stringHashSet.remove(sSet);
            }
        }
//        Iterator<String> iterator = stringHashSet.iterator();
//        while (iterator.hasNext()){
//            String string = iterator.next();
//            if (string.equals("5")){
//                iterator.remove();
//            }
//        }
        for (String el: stringHashSet){
            System.out.println(el);
        }

    }


}
