package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.array;

import java.util.Scanner;

public class ArrayFirstHard {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int count = sc.nextInt();
        int[] numbers;
        if (count > 0) {
            numbers = new int[count];
            for (int k = 0; k < count; k++) {
                numbers[k] = sc.nextInt();
            }
            if (count % 2 != 0) {
                for (int i = 0; i < numbers.length; i++) {
                    System.out.println(numbers[i]);
                }
            } else {
                for (int i = numbers.length - 1; i >= 0; i--) {
                    System.out.println(numbers[i]);
                }
            }
        }
    }
}
