package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread.add17.task1712;

public class Dishes {
    private byte tableNumber;

    public Dishes(byte tableNumber) {
        this.tableNumber = tableNumber;
    }

    public byte getTableNumber() {
        return tableNumber;
    }
}
