package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.data.localdate;

import java.time.LocalTime;

/**
 * Цикл while в методе main должен отработать ровно четыре раза, чтобы на экран вывелись 4 строки.
 * Изменения можно вносить только в метод amazingMethod.
 */
public class AmazingMethod {
    public static void main(String[] args) throws InterruptedException {
        LocalTime localTime = LocalTime.MIDNIGHT;
        LocalTime next = amazingMethod(localTime);
        while (next.isAfter(localTime)) {
            System.out.println(next);
            next = amazingMethod(next);
            Thread.sleep(500);
        }
    }

    static LocalTime amazingMethod(LocalTime base) {
        return base.plusHours(4).plusMinutes(48);// или plusMinutes(288)
    }
}
