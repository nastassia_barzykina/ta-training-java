package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor;

public class MinMaxUtil {
    public static int max(int a, int b){
        if (a > b) {
            return a;
        } else
            return b;//Math.max(a, b)
    }
    public static int max(int a, int b, int c){
        if (c > max(a, b)) {
            return c;
        } else
            return max(a, b);//return max(c, max(a, b));
    }
    public static int max(int a, int b, int c, int d){
        if (d > max(a, b, c)) {
            return d;
        } else
            return max(a, b, c);
    }
    public static int max(int a, int b, int c, int d, int e){
        if (e > max(a, b, c, d)) {
            return e;
        } else
            return max(a, b, c, d);
    }

    public static int min(int a, int b){
        if (a < b) {
            return a;
        } else
            return b;
    }
    public static int min(int a, int b, int c){
        if (c < min(a, b)) {
            return c;
        } else
            return min(a, b);
    }
    public static int min(int a, int b, int c, int d){
        if (d < min(a, b, c)) {
            return d;
        } else
            return min(a, b, c);
    }
    public static int min(int a, int b, int c, int d, int e){
        if (e < min(a, b, c, d)) {
            return e;
        } else
            return min(a, b, c, d);
    }

    public static void main(String[] args) {
        System.out.println(min(-2, -2));
        System.out.println(min(10, -7, 11));
        System.out.println(min(155, 15, -1, -155));
        System.out.println(min(-155, 15, -1, 155, -1));
        System.out.println(max(-2, 10));
        System.out.println(max(0, -7, 0));
        System.out.println(max(155, 15, 155, 155));
        System.out.println(max(155, 15, -1, 155, 1000));
    }

}
