package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.array_list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DifferencesBetweenLists {
    private static  List<String> listOne = new ArrayList<>();
//    Arrays.asList("Jack", "Daniel", "Sam", "Alan", "James", "George");
    private static  List<String> listTwo = new ArrayList<>();
//    Arrays.asList("Jack", "Sam", "Alan", "James", "Jack");

    public static void main(String[] args) {
        listOne.add("Jack");
        listOne.add("Daniel");
        listOne.add("Alan");
        listOne.add("Sam");

        listTwo.add("Jack");
        listTwo.add("Sam");
        listTwo.add("Alan");
        listTwo.add("Alan111");

        //создать копию одного списка, а затем удалить все элементы, общие с другим
        List<String> differences = new ArrayList<>(listTwo);
        differences.removeAll(listOne);
        System.out.println(differences + " - после удаления всех эл, сод в 1 листе");
        if (differences.isEmpty()){

            listOne.removeAll(listTwo);
            System.out.println(listTwo + " 2 лист");
            System.out.println(listOne + " 1 лист");
        } else {
            listOne.clear();
            System.out.println(listOne);
        }
//API:
//        List<String> differences = (List<String>) listOne.stream()
//                .filter(element -> !listTwo.contains(element))
//                .collect(Collectors.toList());
//        System.out.println(differences);
//        boolean b = differences.isEmpty();
//        System.out.println(b);
//        System.out.println(listTwo);

    }


}
