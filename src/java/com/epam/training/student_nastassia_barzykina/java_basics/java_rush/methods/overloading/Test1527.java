package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods.overloading;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * Считать с консоли URL-ссылку.
 * Вывести на экран список всех параметров через пробел (Параметры идут после ? и разделяются &, например, lvl=15).
 * URL содержит минимум 1 параметр.
 * Выводить параметры нужно в той же последовательности, в которой они представлены в URL.
 * Если присутствует параметр obj, то передать его значение в нужный метод alert():
 * alert(double value) - для чисел (не забывай о том, что число может быть дробным);
 * alert(String value) - для строк.
 * Обрати внимание на то, что метод alert() необходимо вызывать после вывода списка всех параметров на экран.
 * Пример 1
 * <p>
 * Ввод:
 * http://javarush.ru/alpha/index.html?lvl=15&view&name=Amigo
 * <p>
 * Вывод:
 * lvl view name
 * <p>
 * Пример 2
 * <p>
 * Ввод:
 * http://javarush.ru/alpha/index.html?obj=3.14&name=Amigo
 * <p>
 * Вывод:
 * obj name
 * double: 3.14
 */
public class Test1527 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String url = reader.readLine();
        //int point = url.indexOf("?");
        //System.out.println(point);
        String data = url.substring(url.indexOf("?") + 1);
        //System.out.println(data);
        //String[] tmp = (url.split("\\?"));
        //String data = tmp[1];
//        System.out.println(data);
        String[] dataNew = data.split("&");
        String[] elem = new String[dataNew.length];
        int index = 0;
        for (int i = 0; i < dataNew.length; i++) {
            if (dataNew[i].indexOf("=") > 0) {
                index = dataNew[i].indexOf("=");
                elem[i] = dataNew[i].substring(0, index);
            } else {
                elem[i] = dataNew[i];
            }
//            System.out.println(dataNew[i] + "  nnn - " + i);
            //elem[i] = Arrays.toString(dataNew[i].split("="));
            //System.out.println(dataNew[i]);
        }
        String res = String.join(" ", elem);
        System.out.println(res);
        String val;
        for (int i = 0; i < dataNew.length; i++) {
            if (dataNew[i].indexOf("=") > 0){
            index = dataNew[i].indexOf("=");
            val = dataNew[i].substring(index + 1);
            } else {
                val = "";
            }
            if ("obj".equals(elem[i])) {
                try {
                    double d = Double.parseDouble(val);
                    alert(d);
                } catch (Exception e) {
                    alert(val);
                }
            }
        }

//        Pattern pattern = Pattern.compile("&|=");
//        String[] words = pattern.split(data);
//        Arrays.asList(words).forEach(s -> System.out.print(s + " "));


        //напишите тут ваш код
    }

    public static void alert(double value) {
        System.out.println("double: " + value);
    }

    public static void alert(String value) {
        System.out.println("String: " + value);
    }

}
