package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.network;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

/**
 * считывать с клавиатуры ссылку на файл в интернете, скачивать его и сохранять во временный файл.
 * Используй методы createTempFile(null, null) и write(Path, byte[]) класса Files, а также метод openStream() класса URL.
 */
public class TmpFile {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        URL url = new URL(line);
        byte[] buffer = url.openStream().readAllBytes();
        Path tmp = Files.createTempFile(null, null);//C:\Users\rando\AppData\Local\Temp - по умолчанию
//        либо указывать путь и имя (префикс - начало имени, суффикс - расширение)
        Path tmpName = Files.createTempFile(Path.of("C:\\Users\\rando\\My_projects\\for_test\\"), "data", "temp");
        Files.write(tmpName, buffer);
        Files.write(tmp, buffer);
//        System.out.println(Files.exists(tmp));
//        System.out.println(Files.size(tmp));

    }


}
