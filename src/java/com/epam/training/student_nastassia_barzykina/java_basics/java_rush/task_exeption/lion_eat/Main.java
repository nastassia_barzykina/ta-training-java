package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.task_exeption.lion_eat;

/**
 * В методе eat добавь блок finally, чтобы лев ложился спать, даже если ему не удалось перекусить.
 *
 * Ожидаемый вывод в случае, когда food != null:
 * ищет еду
 * нашел мясо
 * все съел
 * лег спать
 *
 * Ожидаемый вывод когда food == null:
 * ищет еду
 * ничего не нашел
 * лег спать голодным
 */
public class Main {
    public static void main(String[] args) {
        Lion lion = new Lion();
        lion.eat(new Food("мясо"));
        lion.eat(null);

    }
}
