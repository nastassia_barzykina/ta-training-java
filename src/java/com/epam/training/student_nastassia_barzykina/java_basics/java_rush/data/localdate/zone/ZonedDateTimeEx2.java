package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.data.localdate.zone;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ZonedDateTimeEx2 {
    public static void main(String[] args) {
        //Какое время в канун Нового 2020 года во Вьетнаме эквивалентно времени в Японии и Франции?
        LocalDateTime localDateTime = LocalDateTime.of(2000, Month.JANUARY, 1, 0, 0, 0);

        System.out.println("LocalDateTime: " + localDateTime); // 2000-01-01T00:00
        System.out.println();

// UTC+7 (Ho Chi Minh Vietnam).
        ZoneId vnZoneId = ZoneId.of("Asia/Ho_Chi_Minh");
// Add time zone to it!
        ZonedDateTime vnDateTime = ZonedDateTime.of(localDateTime, vnZoneId);

        System.out.println("Vietnam: " + vnDateTime); // 2000-01-01T00:00+07:00[Asia/Ho_Chi_Minh]

// UTC+9 (Tokyo Japan).
        ZoneId jpZoneId = ZoneId.of("Asia/Tokyo");
        ZonedDateTime jpDateTime = vnDateTime.withZoneSameInstant(jpZoneId);

        System.out.println("Japan: " + jpDateTime); // 2000-01-01T02:00+09:00[Asia/Tokyo]

// UTC+1 (Paris France).
        ZoneId frZoneId = ZoneId.of("Europe/Paris");
        ZonedDateTime frDateTime = vnDateTime.withZoneSameInstant(frZoneId);

        System.out.println("France: " + frDateTime); // 1999-12-31T18:00+01:00[Europe/Paris]
    }
}
