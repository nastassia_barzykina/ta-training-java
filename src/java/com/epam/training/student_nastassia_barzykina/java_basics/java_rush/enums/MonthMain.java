package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.enums;

import java.util.List;

/**
 * метод getNextMonth, который должен возвращать следующий месяц, а если передан последний месяц, возвращать первый.
 * Используй методы values и ordinal.
 * Метод main не участвует в проверке.
 *
 * Требования:
 * •	Реализуй метод public static Month getNextMonth(Month): он должен работать согласно условию.
 * •	В методе getNextMonth должны использоваться методы ordinal() и values().
 */
public class MonthMain {
    public static void main(String[] args) {
        System.out.println(getNextMonth(Month.JANUARY));
        System.out.println(getNextMonth(Month.DECEMBER));
        System.out.println(getNextMonth(Month.JULY));
        //List<Month> winter = Month.getWinterMonths();
        System.out.println("Winter: ");
        for (int i = 0; i < Month.getWinterMonths().length; i++){
            System.out.println(Month.getWinterMonths()[i]);
        }
        System.out.println(Month.getWinterMonths());
        System.out.println("Spring: ");
        System.out.println(Month.getSpringMonths());
        System.out.println("Summer: ");
        System.out.println(Month.getSummerMonths());
        System.out.println("Autumn:");
        System.out.println(Month.getAutumnMonths());
    }
    public static Month getNextMonth(Month month){
        /*Month[] monthsArray = Month.values();
        int numberOfMonth = month.ordinal();
        if (numberOfMonth == monthsArray.length - 1){
            return Month.values()[0];
        } else {
            return Month.values()[numberOfMonth + 1];
        }*/ //Правильное решение:
        int ordinal = month == Month.DECEMBER ? 0 : month.ordinal() + 1;
        return Month.values()[ordinal];
    }

}
