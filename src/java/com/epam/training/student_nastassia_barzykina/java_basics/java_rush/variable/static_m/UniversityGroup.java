package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.variable.static_m;

import java.util.ArrayList;
import java.util.List;

public class UniversityGroup {
    public List<String> students;

    public UniversityGroup() {
        students = new ArrayList<>();
        students.add("Сергей Фрединский");
        students.add("Виталий Правдивый");
        students.add("Максим Козыменко");
        students.add("Наталия Андрющенко");
        students.add("Ира Величенко");
        students.add("Николай Соболев");
        students.add("Снежана Слободенюк");
    }
    public void exclude(String excludedStudent) {
//        List<String> tmp = new ArrayList<String>();
//        tmp.addAll(0, students);
//        for (String student : tmp) {
//            if (student.equals(excludedStudent)) {
//                students.remove(excludedStudent);
//            }
//        }
//        for (int i = 0; i < students.size(); i++) {// правильное решение
//            if (students.get(i).equals(excludedStudent)) {
//                students.remove(i);
//            }
//        }
        students.removeIf( student -> student.equals(excludedStudent));//через лямбда-выражение


    }

    public static void main(String[] args) {
        UniversityGroup universityGroup = new UniversityGroup();
        universityGroup.exclude("Виталий Правдивый");
        universityGroup.students.forEach(System.out::println);
    }
}
