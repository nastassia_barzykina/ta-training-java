package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_lambda.tasks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.function.Predicate;

/**
 * Написать функциональный интерфейс с методом, который принимает число и возвращает булево значение. Написать реализацию
 * такого интерфейса в виде лямбда-выражения, которое возвращает true если переданное число делится без остатка на 13.
 */
class Task1 {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        Collections.addAll(list, 1, 13, 25, 26);
        printBy(list, x -> (x % 13 == 0));
    }
    public static void printBy(ArrayList<Integer> list, Predicate<Integer> num){
        for (Integer x : list){
            System.out.println(num.test(x));
        }
    }
}
