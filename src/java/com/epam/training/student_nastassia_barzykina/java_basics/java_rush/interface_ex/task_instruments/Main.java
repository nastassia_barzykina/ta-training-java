package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex.task_instruments;

import java.util.ArrayList;
import java.util.List;

public class Main {
    static List<MusicalInstrument> orchestra = new ArrayList<>();
    public static void main(String[] args) {
        createKeyboardOrchestra();
        createStringedOrchestra();
        playOrchestra();
    }
    public static void createKeyboardOrchestra() {
        orchestra.add(new Organ());
        orchestra.add(new Piano());
        orchestra.add(new Piano());
        orchestra.add(new Piano());
    }

    public static void createStringedOrchestra() {
        orchestra.add(new Violin());
        orchestra.add(new Violin());
        orchestra.add(new Guitar());
    }

    public static void playOrchestra() {
        for (MusicalInstrument instruments : orchestra){//Object, если с проверкой
            instruments.play();
//            if (instruments instanceof MusicalInstrument){
//                MusicalInstrument instrument = (MusicalInstrument) instruments;
//                instrument.play();
//            }
        }
    }
}
