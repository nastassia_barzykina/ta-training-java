package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread.volatile_task;

public class Ex {
    public static void main(String[] args) {
        Clock clock = new Clock();
        Thread clockThread = new Thread(clock);
        clockThread.start();

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        clock.cancel();
        /**
         * Нить «не знает» о существовании других нитей.
         * В методе run переменная isCancel при первом использовании будет помещена в кэш дочерней нити.
         * Эта операция эквивалентна коду:
         *
         * public void run()
         * {
         * boolean isCancelCached = this.isCancel;
         * while (!isCancelCached)
         * {
         * Thread.sleep(1000);
         * System.out.println("Tik");
         * }
         * }
         * Вызов метода cancel из другой нити поменяет значение переменной isCancel в обычной (медленной) памяти,
         * но не в кэше остальных нитей.
         */

    }

    static class Clock implements Runnable {
        private volatile boolean isCancel = false;
        //Из-за модификатора volatile чтение и запись значения переменной всегда будут происходить в обычной,
        // общей для всех нитей, памяти.

        public void cancel() {
            this.isCancel = true;
        }

        public void run() {
            while (!this.isCancel) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("Tik");
            }
        }
    }
}
