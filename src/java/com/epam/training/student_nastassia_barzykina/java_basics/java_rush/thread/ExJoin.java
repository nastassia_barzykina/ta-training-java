package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread;

public class ExJoin {
    static class Printer implements Runnable
    {
        private String name;
        public Printer(String name)
        {
            this.name = name;
        }
        public void run()
        {
            System.out.println("I’m " + this.name);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Printer printer = new Printer("Nick");
        Thread thread = new Thread(printer);
        thread.start();

        thread.join();
        /**
         * Главная нить создает дочернюю нить – объект thread.
         * Затем запускает ее – вызов thread.start();
         *
         * И ждет ее завершения – thread.join();
         */
    }
}
