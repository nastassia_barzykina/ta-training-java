package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ExFOS {
    public static void main(String[] args) throws IOException {
        File file = new File("C:\\test\\dest.txt");
        FileOutputStream fos = new FileOutputStream(file, true);
        String phrase = "Have a good time!\r\n";
        fos.write(phrase.getBytes());
        fos.close();

    }
}
