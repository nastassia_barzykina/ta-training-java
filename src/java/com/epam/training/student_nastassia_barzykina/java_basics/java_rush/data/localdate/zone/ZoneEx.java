package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.data.localdate.zone;

import java.time.*;

public class ZoneEx {
    public static void main(String[] args) {
        for (String s: ZoneId.getAvailableZoneIds())
            System.out.println(s);
        ZoneId zone = ZoneId.of("Africa/Cairo");
        System.out.println(zone);
        System.out.println("+++++");
        ZoneId zone1 = ZoneId.of("Africa/Cairo");
        ZonedDateTime cairoTime = ZonedDateTime.now(zone1);

        LocalDate localDate = cairoTime.toLocalDate();
        LocalTime localTime = cairoTime.toLocalTime();
        LocalDateTime localDateTime = cairoTime.toLocalDateTime();
        System.out.println(localDate);
        System.out.println(localTime);
        System.out.println(localDateTime);
    }
}
