package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.drink;

public class Main {
    public static void main(String[] args) {
        getDeliciousDrink().taste();
        System.out.println(getWine().getHolidayName());
        System.out.println(getSparklingWine().getHolidayName());
        System.out.println(getWine().getHolidayName());
    }

    public static Drink getDeliciousDrink() {

        return new Wine();

    }

    public static Wine getWine() {
        return new Wine();

    }

    public static Wine getSparklingWine() {
        return new SparklingWine();
    }
}
