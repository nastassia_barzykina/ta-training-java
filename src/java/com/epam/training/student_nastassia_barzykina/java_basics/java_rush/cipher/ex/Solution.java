package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.cipher.ex;

import java.util.Arrays;

public class Solution {
    public static void main(String[] args) {
        String text = "Hello world!";
        String key = "Code";
        System.out.println(Arrays.toString(EncryptionUtils.encrypt(text, key)) + " -- encrypt text");
        System.out.println(EncryptionUtils.decrypt(EncryptionUtils.encrypt(text, key), key) + " -- decrypt text");
    }
}
