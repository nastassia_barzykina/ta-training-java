package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.wrapper;

public class ExInit {
    public static void main(String[] args) {
        new Thread(new ThirdRunnableImpl(new SecondRunnableImpl(new FirstRunnableImpl()))).start();
    }

    public static class FirstRunnableImpl implements Runnable {

        static {
            System.out.println("FirstRunnableImpl - Static Block");
        }

        public FirstRunnableImpl() {
            System.out.println("FirstRunnableImpl - Constructor Block");
        }


        @Override
        public void run() {
            System.out.println("FirstRunnableImpl - run() Block");
        }
    }

    public static class SecondRunnableImpl implements Runnable {

        private Runnable component;

        static {
            System.out.println("SecondRunnableImpl - Static Block");
        }

        public SecondRunnableImpl(Runnable component) {
            System.out.println("SecondRunnableImpl - Constructor Block");
            this.component = component;
        }


        @Override
        public void run() {
            System.out.println("SecondRunnableImpl - run() Block starts method run() of " + component.getClass().getSimpleName());
            component.run();
        }
    }

    public static class ThirdRunnableImpl implements Runnable {
        private Runnable component;

        static {
            System.out.println("ThirdRunnableImpl - Static Block");
        }

        public ThirdRunnableImpl(Runnable component) {
            System.out.println("ThirdRunnableImpl - Constructor Block");
            this.component = component;
        }

        @Override
        public void run() {
            System.out.println("ThirdRunnableImpl - run() Block starts method run() of " + component.getClass().getSimpleName());
            component.run();
        }
    }
}
