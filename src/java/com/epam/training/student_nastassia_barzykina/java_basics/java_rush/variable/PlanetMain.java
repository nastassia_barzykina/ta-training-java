package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.variable;

public class PlanetMain {
    public static void main(String[] args) {
        Planet earth = new Planet();
        earth.name = "Земля";
        earth.age = 4_540_000_000L;
        earth.speed = 170_218;
        earth.area = 510_072_000;
        earth.printInformation();
    }
}
