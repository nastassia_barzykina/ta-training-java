package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.array_list;

import java.util.ArrayList;
import java.util.Scanner;

public class ListVsArray {
    /**
     * Array:
     */
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
// ввод строк с клавиатуры
        String[] list = new String[10];

        for (int i = 0; i < list.length; i++){
            String s = console.nextLine();
            list[i] = s;
        }
// вывод содержимого массива на экран
        for (int i = 0; i < list.length; i++){
            int j = list.length - i - 1;
            System.out.println(list[j]);
        }
        /**
         * ArrayList:
          */
        // ввод строк с клавиатуры
        ArrayList<String> listAr = new ArrayList<String>();

        for (int i = 0; i < 10; i++)
        {
            String s = console.nextLine();
            listAr.add(s);
        }

// вывод содержимого коллекции на экран
        for (int i = 0; i < listAr.size(); i++)
        {
            int j = listAr.size() - i - 1;
            System.out.println(listAr.get(j));
        }
    }
}
