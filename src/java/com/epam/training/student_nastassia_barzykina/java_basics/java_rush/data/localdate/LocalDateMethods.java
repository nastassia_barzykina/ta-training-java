package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.data.localdate;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * Метод nowExample должен вернуть текущую дату.
 * •	Метод ofExample должен вернуть дату 12 сентября 2020 года с помощью метода LocalDate.of.
 * •	Метод ofYearDayExample должен вернуть дату 12 сентября 2020 года с помощью метода LocalDate.ofYearDay.
 * •	Метод ofEpochDayExample должен вернуть дату 12 сентября 2020 года с помощью метода LocalDate.ofEpochDay.
 */
public class LocalDateMethods {
    public static void main(String[] args) {
        System.out.println(nowExample());
        System.out.println(ofExample());
        System.out.println(ofYearDayExample());
        System.out.println(ofEpochDayExample());
    }

    static LocalDate nowExample() {
        return LocalDate.now();
    }

    static LocalDate ofExample() {
        return LocalDate.of(2020, 9, 12);
    }

    static LocalDate ofYearDayExample() {
        return LocalDate.ofYearDay(2020, LocalDate.of(2020, 9, 12).getDayOfYear());
    }

    static LocalDate ofEpochDayExample() {
//        Date date1 = format.parse(dateOfPurchase);
//        Date date2 = format.parse(dateOfSale);
//
//        DateTime dt1 = new DateTime(date1);
//        DateTime dt2 = new DateTime(date2);
//
//        storeDays = Days.daysBetween(dt1, dt2).getDays();
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
//        LocalDate startDate = LocalDate.parse("01/01/1970", formatter);
//        LocalDate endDate = LocalDate.parse("12/09/2020", formatter);
//
//        System.out.println(ChronoUnit.DAYS.between(startDate,endDate));   // 18517

        LocalDate unix = LocalDate.of(1970, 1, 1);
        LocalDate ex = LocalDate.of(2020, 9, 12);
        long count = ChronoUnit.DAYS.between(unix, ex);
        LocalDate date = LocalDate.ofEpochDay(count);
        return date;
    }
}
