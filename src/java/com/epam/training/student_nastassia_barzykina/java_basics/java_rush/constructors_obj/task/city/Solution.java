package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.constructors_obj.task.city;

/**
 * Реализуй метод showWeather, который должен выводить в консоли информацию в таком формате: В городе "название города"
 * сегодня температура воздуха "температура в городе"
 * В методе main один раз нужно вызвать метод showWeather с нужным аргументом.
 *
 * Пример вывода:
 * В городе Дубай сегодня температура воздуха 40
 */
public class Solution {
    public static void showWeather(City city) {
        System.out.println(String.format("В городе %s сегодня температура воздуха %d" , city.getName(), city.getTemperature()));
    }

    public static void main(String[] args) {
        showWeather(new City("Дубай", 40));
    }
}
