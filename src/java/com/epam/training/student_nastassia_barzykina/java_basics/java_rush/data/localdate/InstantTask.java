package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.data.localdate;

import java.time.Instant;
import java.time.temporal.ChronoField;

public class InstantTask {
    public static void main(String[] args) {
        System.out.println(getMaxFromMilliseconds());
        System.out.println(getMaxFromSeconds());
        System.out.println(getMaxFromSecondsAndNanos());
    }

    static Instant getMaxFromMilliseconds() {

//        Instant maxInstant = Instant.ofEpochSecond(Long.MAX_VALUE / 1000, Long.MAX_VALUE % 1000_000_000);
//
//        Instant maxInstant = Instant.ofEpochSecond(Long.MAX_VALUE, 999_999_999);
//        long maxMillis = maxInstant.toEpochMilli();

//        System.out.println("Максимальное количество миллисекунд: " + maxMillis);
//
//        return Instant.ofEpochMilli(j.getEpochSecond());//+1001968-01-12T20:06:43.199Z
//        return  Instant.from(Instant.ofEpochSecond(Instant.MAX.getEpochSecond()));//+1000000000-12-31T23:59:59Z
//        return Instant.ofEpochMilli(Instant.MAX.getEpochSecond());//+1001968-01-12T20:06:43.199Z
//        return Instant.ofEpochSecond(Instant.MAX.toEpochMilli());//ex
        //return Instant.from((Instant.ofEpochMilli(Instant.MAX.getEpochSecond())));
//        return Instant.ofEpochMilli(Instant.MAX.getLong(ChronoField.MILLI_OF_SECOND)).plusSeconds(getMaxFromSeconds().getEpochSecond());
        return Instant.ofEpochMilli(Long.MAX_VALUE);//+292278994-08-17T07:12:55.807Z -- TRUE!!!
    }

    static Instant getMaxFromSeconds() {
        return Instant.ofEpochSecond(Instant.MAX.getEpochSecond());
    }

    static Instant getMaxFromSecondsAndNanos() {
        return Instant.ofEpochSecond(Instant.MAX.getEpochSecond(), Instant.MAX.getNano());
//        return Instant.MAX; - аналогично
    }
}
