package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread.interrupt;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 1. Напиши реализацию метода run в нити Stopwatch (секундомер).
 * 2. Stopwatch должен посчитать количество секунд, которое прошло от создания нити до ввода строки.
 * 3. Выведи количество секунд в консоль.
 */
public class Task1616 {
    public static void main(String[] args) throws IOException {
        InputStreamReader in = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(in);
        //create and run thread
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.start();
        //read a string
        reader.readLine();
        stopwatch.interrupt();
        //close streams
        reader.close();
        in.close();
    }

    public static class Stopwatch extends Thread {
        private int seconds;

        public void run() {
            try {
//                Thread current = Thread.currentThread();// правильное решение
//
//                while (!current.isInterrupted()) {
//                    Thread.sleep(1000);
//                    seconds++;
                while (true){
                    seconds++;
                    Thread.sleep(1000);
                    if (isInterrupted()) {
                        return;
                    }
                }
                //напишите тут ваш код
            } catch (InterruptedException e) {
                System.out.println(seconds);
            }
        }
    }

}
