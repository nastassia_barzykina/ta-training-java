package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * считывает с консоли путь к файлу1 и путь к файлу2. Далее все байты из файла1 записывает в файл2, но при этом
 * меняет их
 * местами по такому принципу: первый со вторым, третий с четвертым, и т.д.
 * Если последний байт в файле1 нечетный, то пишем его в файл2 как есть.
 * Для чтения и записи файлов используй методы  newInputStream и newOutputStream класса Files.
 */
public class CopyFilesTask {
    public static void main(String[] args) {
//        String src = "c:\\test\\log.txt";
//        String dest = "c:\\test\\test.txt";
        try (Scanner scanner = new Scanner(System.in);
            var input = Files.newInputStream(Paths.get(scanner.nextLine()));
            var output = Files.newOutputStream(Paths.get(scanner.nextLine()));)
        {
            while (input.available() > 0) {
                int a = input.read();
                if (input.available() > 0) {
                    int b = input.read();
                    output.write(b);
                }
                output.write(a);
            }
        } catch (IOException e) {
            System.out.println("Something went wrong : " + e);
        }
    }
    /** Решение:
     * byte[] bytesIn = inputStream.readAllBytes();// создание массива, куда зачитали ВЕСЬ файл
     *             byte[] bytesOut = new byte[bytesIn.length];//результирующий массив на выход
     *             for (int i = 0; i < bytesIn.length; i += 2) {
     *                 if (i < bytesIn.length - 1) {// если не последний элемент массива
     *                     bytesOut[i] = bytesIn[i + 1];
     *                     bytesOut[i + 1] = bytesIn[i];
     *                 } else {
     *                     bytesOut[i] = bytesIn[i];
     *                 }
     *             }
     *             outputStream.write(bytesOut);
     *         }
     */
}
