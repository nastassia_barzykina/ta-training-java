package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread;

public class Ex {
    public static void main(String[] args) {
        //Создаем две нити, каждая на основе своего объекта типа Printer.
        Printer printer = new Printer("XXX");
        Thread childThread = new Thread(printer);
        childThread.start();
        Printer printer2 = new Printer("YYY");
        Thread childThread2 = new Thread(printer2);
        childThread2.start();
        // можно и на 1 обьекте создать разные нити
        Thread childThread3 = new Thread(printer);
        childThread3.start();
        // Более того, можно совместить это все в одном классе.
        // Класс Thread унаследован от интерфейса Runnable, и достаточно просто переопределить его метод run:
        /**
         * class Printer extends Thread
         * {
         * private String name;
         * public Printer(String name)
         * {
         * this.name = name;
         * }
         * public void run()
         * {
         * System.out.println("I’m " + this.name);
         * }
         * }
         * public static void main(String[] args)
         * {
         * Printer printer = new Printer("Вася");
         * printer.start();
         *
         * Printer printer2 = new Printer("Коля");
         * printer2.start();
         *
         * }
         * минусы:
         *
         * 1) Вам может понадобиться запустить несколько нитей на основе одного единственного объекта, как это сделано в
         * «примере с Наташей».
         *
         * 2) Если вы унаследовались от класса Thread, вы не можете добавить еще один класс-родитель к своему классу.
         *
         * 3) Если у вашего класса есть класс-родитель, вы не можете добавить второго – Thread.
         *
         * run нужен чтобы прописать какие команды будут исполняться.
         * start чтобы запустить новый тред.
         * Если вы не запускаете новый тред, то просто исполните всё из run последовательно.
         * Внутри start объект Thread сам запустит код из run в отдельно созданном новом треде, вместо вас.
         */

    }
    static class Printer implements Runnable
    {
        private String name;
        public Printer(String name)
        {
            this.name = name;
        }
        public void run()
        {
            System.out.println("I’m " + this.name);
        }
    }
}
