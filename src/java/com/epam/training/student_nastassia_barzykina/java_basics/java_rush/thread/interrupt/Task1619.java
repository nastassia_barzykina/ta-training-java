package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread.interrupt;

public class Task1619 {
    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(new TestThread());
        t.start();
        Thread.sleep(3000);
        ourInterruptMethod();
    }
    private static boolean isCancel = false;
    public static void ourInterruptMethod() {
        Task1619.isCancel = true;

    }

    public static class TestThread implements Runnable {
        public void run() {
            while (!isCancel) {
                try {
                    System.out.println("he-he");
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                }
            }
        }
    }
}
