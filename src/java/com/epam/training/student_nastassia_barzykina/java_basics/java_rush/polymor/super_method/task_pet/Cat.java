package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.super_method.task_pet;

public class Cat extends Pet{
    public static final String CAT = "Я не люблю людей.";

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.println(CAT);
    }
}
