package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.array;

/**
 * Считать 6 строк и заполнить ими массив strings.
 * Удалить повторяющиеся строки из массива strings, заменив их на null (null должны быть не строками "null").
 * Я это сделала! Жжесть... Проблема была для XXXYYY...
 */
public class StringArray {
    public static String[] strings = {"Hello", "Hello", "World", "Java", "Tasks", "World"};
    public static void main(String[] args) {
        //strings = new String[]{"Hello", "Hello", "World", "Java", "Tasks", "World"};
//        Scanner sc = new Scanner(System.in);
//        strings = new String[6];
//        for (int i = 0; i < 6; i++){
//            strings[i] = sc.nextLine();
//        }
        //strings = new String[] {"a", "x", "x", "x", "V", "N", "y", "y", "y", "F"};
        int count = 0;
        for (int i = 0; i < strings.length; i++) {
                for (int j = i + 1; j <= strings.length - 1; j++) {
                    if (strings[i] != null) {
                        if (strings[i].equals(strings[j])) {
                            strings[j] = null;
                            count++;
                        }
                    }
                }
            if (count > 0){
                strings[i] = null;
                count = 0;
            }
        }
        for (int i = 0; i < strings.length; i++) {
            System.out.print(strings[i] + ", ");
        }
    }
}
