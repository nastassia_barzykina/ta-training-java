package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.files;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class FilesEx {
    public static void main(String[] args) throws IOException {
//        создание файла и директорий. Создается в активной директории, иначе - ошибка! Если файл существует - тоже ошибка!

//        Files.createFile(Path.of("C:\\Users\\rando\\My_projects\\ta-traning-java\\toFiles.txt"));
//        Files.createDirectory(Path.of("C:\\Users\\rando\\My_projects\\ta-traning-java\\JavaRush"));
//        Files.createDirectories(Path.of("C:\\Users\\rando\\My_projects\\ta-traning-java\\JavaRush\\1\\2\\3"));
//        Копирование, перемещение и удаление. Директории должны быть пустые.

        Path path1 = Path.of("C:\\Users\\rando\\My_projects\\ta-traning-java\\toFiles.txt");
        Path path2 = Path.of("C:\\Users\\rando\\My_projects\\toFiles-copy.txt");// копирует (если такого файла там нет)
        //Files.copy(path1, path2);
//        Files.move(path1, path2);//Перемещает и переименовывает файл(если файла такого там нет)
//        Files.delete(path1);
//        Проверка типа файла и факта существования:

        System.out.println(Files.isDirectory(path1));// false
        System.out.println(Files.isDirectory(Path.of("C:\\Users\\rando\\My_projects\\ta-traning-java\\JavaRush")));//true
        System.out.println(Files.isRegularFile(Path.of("c:\\readme.txt")));
        System.out.println(Files.isRegularFile(Path.of(path1.toUri())));// если файл есть по этому пути и это файл
  //      System.out.println(Files.size(path1)); //если файл есть
//        Files.delete(path1);
//Работа с содержимым файла:
        List<String> list = Files.readAllLines(path1); //список строк из файла
        for (String el : list){
            System.out.println(el);
        }
//        Получение содержимого директории
//        Path path = Path.of("c:\\windows");
//
//        try (DirectoryStream<Path> files = Files.newDirectoryStream(path)) {
//            for (Path paths : files)
//                System.out.println(paths);
//        }
//        Path soursePath = Path.of("C:\\Users\\rando\\My_projects\\source");
//        Files.createTempFile("tmp", "lg");
//        Files.createTempFile("tmpTest", "lg");
Path src = Path.of("C:\\Users\\rando\\My_projects\\for_test\\");
        Path dest = Path.of("C:\\Users\\rando\\My_projects\\target");
        DirectoryStream<Path> files = Files.newDirectoryStream(src);
        for(Path p : files){
            if (Files.isRegularFile(p)){
                Path resolve = dest.resolve(p.getFileName());// получение полного имени файла(директория + имя файла) для новой директории
            Files.copy(p, resolve);
            }
        }

    }
}
