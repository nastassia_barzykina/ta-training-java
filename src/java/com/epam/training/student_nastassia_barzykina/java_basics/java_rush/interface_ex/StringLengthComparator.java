package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

//public class StringLengthComparator implements Comparator<String> {
//    @Override
//    public int compare(String o1, String o2) {
//        return o1.length() - o2.length();
//    }
//}
class Main {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        Collections.addAll(list, "Привет", "как", "дела?");
        for (String el : list){
            System.out.println(el);
        }
//        через анонимный класс
//        Comparator<String> comparator = new Comparator<String>() {
//            @Override
//            public int compare(String o1, String o2) {
//                return o1.length() - o2.length();
//            }
//        };
        //через лямбду
        Comparator<String> comparator = (String o1, String o2) ->{
                return o1.length() - o2.length();
        };
//        Collections.sort(list, comparator);// через анонимный класс или лямбду
        Collections.sort(list, (obj1, obj2) ->  obj1.length() - obj2.length() );// сокращ запись
//        Collections.sort(list, new StringLengthComparator());// по длине строки
        for (String el : list){
            System.out.println(el);
        }
    }
}
