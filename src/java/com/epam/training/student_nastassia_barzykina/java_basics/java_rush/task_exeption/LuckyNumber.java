package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.task_exeption;

/**
 * Перехвати исключение в методе main, чтобы программа компилировалась.
 * При возникновении исключения нужно вывести на экран строку errorMessage.
 *
 * Требования:
 * •	В методе main нужно добавить блок try-catch.
 * •	Метод main не должен бросать проверяемые исключения.
 * •	Метод main должен вызывать метод generateLuckyNumber.
 * •	Метод generateLuckyNumber изменять нельзя.
 * •	При возникновении исключения в методе main нужно вывести на экран строку errorMessage.
 */
public class LuckyNumber {
    public static String errorMessage = "не повезло";

    public static void main(String[] args) /*throws Exception*/{
        try {
            generateLuckyNumber();
        } catch (Exception e){
            System.out.println(errorMessage);
        }
        try {
            System.out.println("Программа работает от забора");
            Thread.sleep(1000);
            System.out.println("до обеда");
        } catch (NullPointerException | NumberFormatException e) {
            System.out.println("Произошло исключение на букву N");
        } catch (IllegalArgumentException | IllegalStateException | InterruptedException e) {
            System.out.println("Произошло исключение на букву I");
        } catch (ClassCastException e) {
            System.out.println("Произошло исключение на букву C");
        }

    }

    static void generateLuckyNumber() throws Exception {
        int luckyNumber = 13;//(int) (Math.random() * 100);
        if (luckyNumber == 13) {
            throw new Exception();
        }
        System.out.println("твое счастливое число: " + luckyNumber);
    }
}
