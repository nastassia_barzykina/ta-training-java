package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.variable.nested_inner_class;

public class OuterMain {
    public static void main(String[] args) {
        //Outer outer = new Outer();// создание объекта род класса
        Outer.Inner  inner = new Outer().new Inner();// создание объекта внутр класса
        Outer.Nested nested = new Outer.Nested();// создание объекта влож класса
    }
}
