package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods.statics;

import java.util.HashMap;
import java.util.Map;

public class Test {
    public static Map<Double, String> labels = new HashMap<>();
    static {
        labels.put(1.0, "a");
        labels.put(1.1, "b");
        labels.put(1.2, "c");
        labels.put(1.3, "d");
        labels.put(1.4, "e");
    }
    public static Cat cat;
    public static class Cat {
        public String name;
    }
    static {
        Test.cat = new Cat();
        cat.name = "Murka";
        System.out.println(cat.name);
    }

    public static void main(String[] args) {
        //labels.put(1.0, "a");
        System.out.println(labels);
    }
}
