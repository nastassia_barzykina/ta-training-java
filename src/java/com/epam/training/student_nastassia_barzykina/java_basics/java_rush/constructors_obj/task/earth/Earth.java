package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.constructors_obj.task.earth;

public class Earth {
    public static void main(String[] args) {
        new Africa(100);
        new Antarctica(200);
        new Australia(50);
        new Eurasia(400);
        new NorthAmerica(120);
        new SouthAmerica(130);

    }
}
