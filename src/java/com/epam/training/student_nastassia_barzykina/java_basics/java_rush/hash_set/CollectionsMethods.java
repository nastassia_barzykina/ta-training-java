package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.hash_set;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CollectionsMethods {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        Collections.addAll(list, 1, 2, 3, 4, 5);
        for (int i : list)
            System.out.println(i);
        System.out.println("_____");

        Collections.fill(list, 10);
        for (int i : list)
            System.out.println(i);
        System.out.println("_____");

        List<String> fake = Collections.nCopies(5, "Hi");
        ArrayList<String> arrayList = new ArrayList<>(fake);
        for (String i : arrayList)
            System.out.println(i);
        System.out.println("_____");

        list.add(1);
        list.add(2);
        list.add(3);
        Collections.replaceAll(list, 10, 20);
        Collections.replaceAll(list, 2, 202);
        for (int i : list)
            System.out.println(i);
        System.out.println("_____");

        ArrayList<Integer> srcList = new ArrayList<Integer>();
        Collections.addAll(srcList, 99, 98, 97);
        ArrayList<Integer> destList = new ArrayList<Integer>();
        Collections.addAll(destList, 1, 2, 3, 4, 5, 6, 7);
        Collections.copy(destList, srcList);
        for (int i: destList)
            System.out.println(i);
    }
}
