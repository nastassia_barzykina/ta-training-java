package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream_collect.ex_pets;

public enum Color {
    WHITE,
    BLACK,
    DARK_GREY,
    LIGHT_GREY,
    FOXY,
    GREEN,
    YELLOW,
    BLUE,
    MAGENTA
}
