package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.data.localdate;

import java.time.Instant;

/**
 * в методе plusMinutes нужно увеличить параметр instant на minutes минут и вернуть результат;
 * в методе plusHours нужно увеличить параметр instant на hours часов и вернуть результат;
 * в методе plusDays нужно увеличить параметр instant на days дней и вернуть результат;
 * в методе minusMinutes нужно уменьшить параметр instant на minutes минут и вернуть результат;
 * в методе minusHours нужно уменьшить параметр instant на hours часов и вернуть результат;
 * в методе minusDays нужно уменьшить параметр instant на days дней и вернуть результат.
 */
public class ALotOfMethodsTask {
    public static void main(String[] args) {
        Instant instant = Instant.ofEpochSecond(10);
        System.out.println(instant);
        System.out.println(plusMinutes(instant, 2));
        System.out.println(plusHours(instant, 2));
        System.out.println(plusDays(instant, 2));
        System.out.println(minusMinutes(instant, 2));
        System.out.println(minusHours(instant, 2));
        System.out.println(minusDays(instant, 2));
    }

    static public Instant plusMinutes(Instant instant, long minutes) {
        long tmp = 60 * minutes;
        return instant.plusSeconds(tmp);
        //return instant.plus(minutes, ChronoUnit.MINUTES);- правильное решение
    }

    static public Instant plusHours(Instant instant, long hours) {
        long tmp = 60 * hours * 60;
        return instant.plusSeconds(tmp);
    }

    static public Instant plusDays(Instant instant, long days) {
        long tmp = 24 * 60 * 60 * days;
        return instant.plusSeconds(tmp);
    }

    static public Instant minusMinutes(Instant instant, long minutes) {
        long tmp = 60 * minutes;
        return instant.minusSeconds(tmp);
    }

    static public Instant minusHours(Instant instant, long hours) {
        long tmp = 60 * hours * 60;
        return instant.minusSeconds(tmp);
    }

    static public Instant minusDays(Instant instant, long days) {
        long tmp = 24 * 60 * 60 * days;
        return instant.minusSeconds(tmp);
    }
}

