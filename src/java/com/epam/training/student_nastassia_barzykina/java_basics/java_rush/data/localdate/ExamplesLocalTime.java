package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.data.localdate;

import java.time.LocalTime;

public class ExamplesLocalTime {
    public static void main(String[] args) {
        LocalTime time = LocalTime.now();
        System.out.println(time + " - текущее время");
        time = LocalTime.of(10, 20, 30, 500);// 10:20:30.000000500
        System.out.println(time);

        time = LocalTime.ofSecondOfDay(10000);
        System.out.println(time + " -  по номеру секунды(10000)");

        LocalTime now = LocalTime.now();
        System.out.println(now.getHour());
        System.out.println(now.getMinute());
        System.out.println(now.getSecond());
        System.out.println(now.getNano());

        LocalTime time2 = time.plusHours(2);
        LocalTime time3 = time.minusMinutes(40);
        LocalTime time4 = time.plusSeconds(3600);

        System.out.println(time);
        System.out.println(time2);
        System.out.println(time3);
        System.out.println(time4);


    }
}
