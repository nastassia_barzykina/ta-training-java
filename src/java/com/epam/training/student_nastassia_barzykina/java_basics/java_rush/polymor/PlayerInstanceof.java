package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * измени haveFun так, чтобы он вызывал метод:
 * play(), если person имеет тип Player.
 * dance(), если person имеет тип Dancer.
 */

public class PlayerInstanceof {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Person person = null;
        String key;
        while (!(key = reader.readLine()).equals("exit")) {
       // while (!"exit".equals(key = reader.readLine())) {// best practice - если константа сравнивается со строкой (переменной), то equals лучше вызывать на константе
            if ("player".equals(key)) {
                person = new Player();
            } else if ("dancer".equals(key)) {
                person = new Dancer();
            }
            haveFun(person);
        }
    }

    public static void haveFun(Person person) {
        if (person instanceof Player){
            ((Player) person).play();
        }
        if (person instanceof Dancer){
            ((Dancer) person).dance();
        }
        //else System.out.println("Something wrong...");
        //напишите тут ваш код
    }

    interface Person {
    }

    static class Player implements Person {
        void play() {
            System.out.println("playing");
        }
    }

    static class Dancer implements Person {
        void dance() {
            System.out.println("dancing");
        }
    }
}
