package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;

public class ExFIS {
    public static void main(String[] args) throws IOException {
        Date date = new Date();
        FileInputStream fis = new FileInputStream("C:\\test\\dest.txt");
        int i;
        while ((i = fis.read()) != -1){
            System.out.print((char) i);
        }
        fis.close();
        Date date1 = new Date();
        System.out.println(date1.getTime() - date.getTime());
    }
}
