package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.linkedlist;

/**
 * есть класс StringLinkedList, в котором есть поля first и last, указывающие на первый и последний элемент соответственно.
 * Тебе нужно реализовать логику метода add, который будет добавлять элементы в список.
 *
 * Подсказки:
 *
 * Для правильной реализации важно понимать, что по списку можно проходить как с первого элемента до последнего, так и
 * наоборот, поэтому при добавлении новых элементов не забудь менять ссылки рядом стоящих.
 * Метод add должен создавать новый объект класса Node, в который он поместит переданное значение. Перед тем как добавлять
 * новую ноду, нужно изменить ссылку последней ноды на новосозданную, а новосозданная станет последней.
 * Если в списке один элемент, то на него указывает first.next и last.prev одновременно.
 *
 * Важно:
 * Node first не должна иметь значение, она указывает только на первый элемент(first.next);
 * Node last также не содержит значения, а только указывает на последний элемент(last.prev).
 *
 * Требования:
 * •	При добавлении нового элемента предыдущие элементы должны получать на него ссылку.
 * •	При добавлении нового элемента он должен получать ссылку на предыдущий.
 * •	Поле first класса StringLinkedList должно всегда ссылаться на первый элемент, не содержать значение (value) и
 * ссылку на предыдущий элемент (prev).
 * •	Поле last класса StringLinkedList должно всегда ссылаться на последний элемент, не содержать значение (value) и
 * ссылку на следующий элемент (next).
 */
public class StringLL {
    public static void main(String[] args) {
        StringLinkedList stringLinkedList = new StringLinkedList();
        stringLinkedList.add("1");
        stringLinkedList.add("2");
        stringLinkedList.add("3");
        stringLinkedList.add("hhh");
        stringLinkedList.add("5");
        stringLinkedList.add("6");
        stringLinkedList.add("7");
        stringLinkedList.add("8");
        stringLinkedList.add("9");
        //stringLinkedList.printAll();
        System.out.println(stringLinkedList.get(0));//1
        System.out.println(stringLinkedList.get(8));//9
        System.out.println(stringLinkedList.get(3));//hhh
        System.out.println(stringLinkedList.get(9)); // null
    }
}
