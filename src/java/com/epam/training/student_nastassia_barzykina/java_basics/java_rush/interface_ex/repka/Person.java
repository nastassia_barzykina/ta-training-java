package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex.repka;

public class Person implements RepkaItem {
    private String name;
    private String namePadezh;

    public Person(String name, String namePadezh) {
        this.name = name;
        this.namePadezh = namePadezh;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNamePadezh() {
        return namePadezh;
    }

    public void setNamePadezh(String namePadezh) {
        this.namePadezh = namePadezh;
    }
//    public void pull (Person person){
//        System.out.println(this.getName() + " за " + person.getNamePadezh());
        //System.out.printf("%s за %s", this.getName(), person.getNamePadezh() + '\n');
        public void pull(Person second) {
                System.out.println(name + " за " + second.getNamePadezh());
        //    }

    }

}
