package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.task_exeption.file_manager;

import java.io.FileNotFoundException;
import java.nio.file.FileSystemException;

/**
 * Распаковка исключения
 */

public class Main {
    public static final String FAILED_TO_READ = "Не удалось прочесть файл.";
    public static final String FAILED_TO_WRITE = "Не удалось записать в файл.";

    public static FileManager fileManager = new FileManager();

    public static void main(String[] args) {
        try {
            fileManager.copyFile("book.txt", "book_final_copy.txt");
            fileManager.copyFile("book_final_copy.txt", "book_last_copy.txt");
        } catch (RuntimeException e){
            Throwable cause = e.getCause();
            if (e.getCause() instanceof FileNotFoundException){ // можно и так.
                System.out.println(FAILED_TO_READ);
            } else if (cause instanceof FileSystemException) {
                System.out.println(FAILED_TO_WRITE);
            }
        }
    }
}
