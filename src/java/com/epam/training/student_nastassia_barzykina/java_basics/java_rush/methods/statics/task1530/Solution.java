package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods.statics.task1530;

public class Solution {
    public static void main(String[] args) {
        DrinkMaker latte = new LatteMaker();
        DrinkMaker tea = new TeaMaker();
        latte.makeDrink();
        tea.makeDrink();

    }
}
