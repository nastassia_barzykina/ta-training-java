package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.hash_set;

import java.util.*;

public class HashSetSolution {
    public static void main(String[] args) {
        String[] array = {"Через", "три", "года", "я", "буду", "Senior", "Java", "Developer"};
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
        System.out.println("___________________________________");

        HashSet<String> hashSet = arrayToHashSet(array);
        for(String s : hashSet) {
            System.out.println(s);
        }
        String[] test = new String[] {"Привет", "Hello", "Hola", "Bonjour", "Cialo", "Namaste" };
        System.out.println("Array: ");
        for (int i = 0; i < test.length; i++){
            System.out.println(test[i]);
        }
        ArrayList<String> testList = new ArrayList<>(Arrays.asList(test));// преобразование в лист
        System.out.println("ArrayList: ");
        System.out.println(testList);
        System.out.println("Обратное преобразование листа в массив:");
        String[] testArray = testList.toArray(new String[0]);
        for (int i = 0; i < testArray.length; i++){
            System.out.print(testArray[i] + " ");
        }
    }

    public static HashSet<String> arrayToHashSet(String[] strings) {
        HashSet<String> result = new HashSet<>();
        for (int i = 0; i < strings.length; i++){
            result.add(strings[i]);
        }
        return result;
        //напишите тут ваш код
    }
}
