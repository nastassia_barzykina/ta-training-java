package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread.add;

public class Task1633 {
    public static Thread.UncaughtExceptionHandler handler = new OurUncaughtExceptionHandler();

    public static void main(String[] args) {
        TestedThread commonThread = new TestedThread(handler);
        //Thread.UncaughtExceptionHandler uncaughtExceptionHandler = new OurUncaughtExceptionHandler();
        //Thread.UncaughtExceptionHandler uncaughtExceptionHandler2 = new OurUncaughtExceptionHandler();

        Thread threadA = new Thread(commonThread, "Нить 1");
        Thread threadB = new Thread(commonThread, "Нить 2");

        threadA.start();
        threadB.start();
        threadA.setUncaughtExceptionHandler(handler);
        threadB.setUncaughtExceptionHandler(handler);

        threadA.interrupt();
        threadB.interrupt();
    }

    public static class TestedThread extends Thread {
        public TestedThread(Thread.UncaughtExceptionHandler handler) {
            setUncaughtExceptionHandler(handler);
            start();
        }

        public void run() {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                throw new RuntimeException("My exception message");
            }
            //throw new RuntimeException();
        }
    }

    public static class OurUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
        @Override
        public void uncaughtException(Thread t, Throwable e) {
            System.out.println(t.getName() + ": " + e.getMessage());
        }
    }
}
/**
 * Я понял так: =)
 * у программы в данном случае 4 нити, а именно:
 * 1) что создается и запускается в public static void main
 * 2) что создается при создании TestedThread commonThread и запускается в конструкторе TestedThread
 * 3) threadA, что создается в первой нити и запускается в ней же
 * 4) threadB, что создается в первой нити и запускается в ней же
 *
 * Первая нить:
 * создала и запустила 3 оставшиеся нити, а затем принудительно поставила флаг остановки на нити threadA и threadB, после успешно завершилась.
 *
 * Вторая нить (commonThread):
 * Установила себе в конструкторе отловщика setUncaughtExceptionHandler(handler);
 * запустилась, проспала 3 секунды и успешно завершилась, т.к. ее никто принудительно не пытался "выключить"
 *
 * Третяя нить (threadA):
 * была создана на основе commonThread, но setUncaughtExceptionHandler(handler) в конструкторе TestedThread на нее не распространился, т.к. она создана на основе уже существующего объекта и конструктор отработал всего 1 раз (при создании commonThread)
 * Т.к. конструктор TestedThread  на нее не распространился, то и запустить нам пришлось ее в первой нити строкой threadA.start();
 * она заснула на 3 секунды
 * но в первой нити ее принудительно остановили, вызвав тем самым код из catch (InterruptedException e), а он в свою очередь пробросил исключение new RuntimeException;
 * которое нам ловить было нечем, т.к. настройки setUncaughtExceptionHandler(handler) в этой нити нет.
 *
 * Четвертая нить (threadB)
 * Полностью аналогична третье.
 *
 * Итого, чтобы поставить настройку отлавливания setUncaughtExceptionHandler(handler) мы можем:
 * - написать в методе run класса TestedThread
 * Thread.currentThread().setUncaughtExceptionHandler(handler);
 * таким образом 2,3,4 нити при старте поставят себе настройку "отлавливателя"
 *
 * - после создания или старта (но обязательно до остановки) прописать каждой нити
 * threadA.setUncaughtExceptionHandler(handler);
 * threadB.setUncaughtExceptionHandler(handler);
 */
