package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.task_animal;

public class Solution {
    public static void main(String[] args) {
        printRation(new Cow());
        printRation(new Elephant());
        printRation(new Wolf());
        printRation(new Lion());
    }
    public static void printRation(Animal animal){
        String herbivore = "Любит траву";
        String predator = "Любит мясо";
        if (animal instanceof Herbivore){
            System.out.println(herbivore);
        } else if (animal instanceof Predator){
            System.out.println(predator);
        }
    }
}
