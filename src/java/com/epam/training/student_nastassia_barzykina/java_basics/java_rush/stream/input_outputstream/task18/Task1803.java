package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.task18;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Ввести с консоли имя файла.
 * Найти байт или байты с максимальным количеством повторов.
 * Вывести их на экран через пробел.
 * Закрыть поток ввода-вывода.
 * <p>
 * Требования:
 * •	Программа должна считывать имя файла с консоли.
 * •	Для чтения из файла используй поток FileInputStream.
 * •	В консоль через пробел должны выводиться все байты из файла с максимальным количеством повторов.
 * •	Данные в консоль должны выводится в одну строку.
 * •	Поток чтения из файла должен быть закрыт.
 */
public class Task1803 {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String fileName = "C:\\test\\dest.txt"; //br.readLine();
        FileInputStream inputStream = new FileInputStream(fileName);
        HashMap<Integer, Integer> map = new HashMap<>();

        while (inputStream.available() > 0) {
            int data = inputStream.read();
            Integer count = map.get(data);
            if (count == null){
                map.put(data, 1);
            } else {
                map.put(data, ++count);
            }
        }
        inputStream.close();
        int max = 0;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (entry.getValue() > max){
                max = entry.getValue();
            }
        }
        for (Map.Entry<Integer, Integer> entry : map.entrySet()){
            if (entry.getValue() == max){
                System.out.println(entry.getKey() + " -> " + entry.getValue());
            }
        }
        TreeSet<Integer> treeSet = new TreeSet<>();
        List<Integer> list = new ArrayList<>(map.values());
        Collections.sort(list);
        Integer maxVal = list.get(list.size() - 1);
        for (Map.Entry entry : map.entrySet()){
            if (maxVal == entry.getValue()){
                treeSet.add((Integer) entry.getKey());
            }
        }
        System.out.println(treeSet.first() + " first");
    }
}
