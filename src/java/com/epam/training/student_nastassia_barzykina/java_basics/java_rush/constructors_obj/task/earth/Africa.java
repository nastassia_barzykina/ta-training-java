package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.constructors_obj.task.earth;

public class Africa {
    private final int area;

    public Africa(int area) {
        this.area = area;
    }
}
