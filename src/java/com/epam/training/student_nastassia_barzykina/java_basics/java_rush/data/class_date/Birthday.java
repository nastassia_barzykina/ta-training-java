package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.data.class_date;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Birthday {
    static Date birthday = new Date(80, Calendar.APRIL, 13);
    public static void main(String[] args) {
        System.out.println(getDaysOfWeek(birthday));
        Date day = new Date("6 January 2010");
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");// если "mm" -> minutes
        System.out.println(f.format(day));

    }
    static String getDaysOfWeek(Date date){
        Locale ru = new Locale("ru", "RU");
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE", ru);
        String dayOfWeek = formatter.format(date);
        return dayOfWeek;
    }
}
