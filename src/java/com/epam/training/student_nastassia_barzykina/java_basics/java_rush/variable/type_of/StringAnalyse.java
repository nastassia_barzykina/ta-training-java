package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.variable.type_of;

import static java.lang.Character.isDigit;

/**
 * Реализуй методы countDigits(String), countLetters(String), countSpaces(String), которые должны возвращать
 * количество цифр, букв и пробелов в строке.
 */
public class StringAnalyse {
    public static void main(String[] args) {
        String string = "Думаю, это будет новой фичей." +
                "Только не говорите никому, что она возникла случайно.";

        System.out.println("Количество цифр в строке : " + countDigits(string));
        System.out.println("Количество букв в строке : " + countLetters(string));
        System.out.println("Количество пробелов в строке : " + countSpaces(string));
    }

    public static int countDigits(String string) {
        if (string.isEmpty()) {
            return 0;
        }
//            char[] text = string.toCharArray();
        int count = 0;
        for (int i = 0; i < string.length(); i++) {
            if (Character.isDigit(string.charAt(i))) {
                count++;
            }
        }
        return count;
    }

    public static int countLetters(String string) {
        int count = 0;
        for (int i = 0; i < string.length(); i++) {
            if (Character.isLetter(string.charAt(i))) {
                count++;
            }
        }
        return count;
    }

    public static int countSpaces(String string) {
        int count = 0;
        for (int i = 0; i < string.length(); i++) {
            if (Character.isWhitespace(string.charAt(i))) {
                count++;
            }
        }
        return count;
    }

}
