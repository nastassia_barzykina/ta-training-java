package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods;

/**
 * setFlag(int number, int flagPos) — устанавливает значение "1" биту под индексом flagPos числа number и возвращает
 * новое значение (a | (1 << b)).
 * resetFlag(int number, int flagPos) — устанавливает значение "0" биту под индексом flagPos числа number и возвращает
 * новое значение (a & ~(1 << b)).
 * checkFlag(int number, int flagPos) — проверяет значение бита под индексом flagPos числа number и возвращает true,
 * если значение "1" и false, если "0" ((a & (1 << b)) == (1 << b) - равен ли бит b 1).
 */
public class Flag {

    public static int setFlag(int number, int flagPos) {
        //напишите тут ваш код
        return number | (1 << flagPos);
    }

    public static int resetFlag(int number, int flagPos) {
        //напишите тут ваш код
        return number & ~(1 << flagPos);
    }

    public static boolean checkFlag(int number, int flagPos) {
        return (number & (1 << flagPos)) == (1 << flagPos);
    }
}
