package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.inherit;

public class ConstantsCars {
    public static void main(String[] args) {
        new ConstantsCars.LuxuriousCar().printlnDesire();
        new ConstantsCars.CheapCar().printlnDesire();
        new ConstantsCars.Ferrari().printlnDesire();
        new ConstantsCars.Lanos().printlnDesire();
    }

    public static class Ferrari extends LuxuriousCar {
       // public
        protected void printlnDesire() {
            //System.out.println("Я хочу ездить на Феррари");
            System.out.println(Constants.WANT_STRING + Constants.FERRARI_NAME);
            //add your code here
        }
    }

    public static class Lanos extends CheapCar {
        public void printlnDesire() {
//            System.out.println("Я хочу ездить на Ланосе");
            System.out.println(Constants.WANT_STRING + Constants.LANOS_NAME);
            //add your code here
        }
    }

    public static class Constants {
        public static String WANT_STRING = "Я хочу ездить на ";
        public static String LUXURIOUS_CAR = "роскошной машине";
        public static String CHEAP_CAR = "дешевой машине";
        public static String FERRARI_NAME = "Феррари";
        public static String LANOS_NAME = "Ланосе";
    }
    public static class LuxuriousCar{
        //protected
        void printlnDesire(){
            //System.out.println("Я хочу ездить на роскошной машине");
            System.out.println(Constants.WANT_STRING + Constants.LUXURIOUS_CAR);
        }
    }
    public static class CheapCar{
        //protected
        private void printlnDesire(){
            //System.out.println("Я хочу ездить на дешевой машине");
            System.out.println(Constants.WANT_STRING + Constants.CHEAP_CAR);
        }
    }
}
