package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.task_exeption.resources;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileTryWR {
    public static void main(String[] args) throws IOException {
        String path = "c:\\test\\project\\log.txt";
        try(FileOutputStream output = new FileOutputStream(path))
        {
            output.write(1);
        }
    }
}
