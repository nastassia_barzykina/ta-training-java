package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.string.sb;

public class StringReverse {
    public static void main(String[] args) {
        String string = "Ходит кот задом наперед";
        System.out.println(string);
        System.out.println(reverseString(string));
    }

    public static String reverseString(String string) {
//        StringBuilder stringBuilder = new StringBuilder(string);
//        stringBuilder.reverse();
//        String reverse = stringBuilder.toString();
//        return reverse;
        return new StringBuilder(string).reverse().toString();
    }
}
