package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex.voice;

public class Cat implements Sounding{
    @Override
    public void sound() {
        System.out.println("Мяу");
    }
}
