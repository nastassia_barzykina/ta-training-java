package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.variable.nested_inner_class;

/**
 * В классе Car создай внутренний класс (inner class) Engine. В классе Engine создай boolean поле isRunning,
 * которое принимает значение true, если двигатель запущен, и false в противном случае. Также в этот класс добавь методы
 * start и stop, которые будут запускать/останавливать двигатель (устанавливать соответствующее значение полю isRunning).
 * Чтобы использовать двигатель, создай поле engine типа Engine в классе Car.
 */
public class Car {
     static class Engine{
        private boolean isRunning;
        public void start(){
            isRunning = true;
        }
        public void stop(){
            isRunning = false;
        }
    }
    Engine engine = new Engine();// но обратится к методам внутр класса можно только через создание обьекта внешнего класса - 23 и 25 строки

    public static void main(String[] args) {
        Engine car = new Car().engine;
//        Car.Engine engine1 = car.new Engine();// либо:
//        Car.Engine engine2 = car.engine;
        car.start();
        System.out.println(car.isRunning);
        //System.out.println(engine2.isRunning);
    }
//    public static void main(String...errors){
//        System.out.println("ddd");
//    }

}
