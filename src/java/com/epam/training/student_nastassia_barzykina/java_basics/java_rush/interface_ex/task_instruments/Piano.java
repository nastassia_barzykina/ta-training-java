package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex.task_instruments;

public class Piano implements MusicalInstrument{
    @Override
    public void play() {
        System.out.println("Играет пианино.");
    }
}
