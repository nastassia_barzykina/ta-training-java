package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.add18;

import java.io.*;
import java.util.*;

/**
 * Собираем файл из кусочков.
 * Считывать с консоли имена файлов.
 * Каждый файл имеет имя: [someName].partN.
 * <p>
 * Например, Lion.avi.part1, Lion.avi.part2, ..., Lion.avi.part37.
 * <p>
 * Имена файлов подаются в произвольном порядке. Ввод заканчивается словом "end".
 * В папке, где находятся все прочтенные файлы, создать файл без суффикса [.partN].
 * <p>
 * Например, Lion.avi.
 * <p>
 * В него переписать все байты из файлов-частей используя буфер.
 * Файлы переписывать в строгой последовательности, сначала первую часть, потом вторую, ..., в конце - последнюю.
 * Закрыть потоки.
 * <p>
 * Требования:
 * •	Программа должна считывать имена файлов с консоли, пока не будет введено слово "end".
 * •	Создай поток для записи в файл без суффикса [.partN] в папке, где находятся все считанные файлы.
 * •	В новый файл перепиши все байты из файлов-частей *.partN.
 * •	Чтение и запись должны происходить с использованием буфера.
 * •	Созданные для файлов потоки должны быть закрыты.
 * •	Не используй статические переменные.
 */
public class Task1825 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = null;//"C:\test\files\Lion.avi.part1"
        String fileNameResult = null;// итоговый файл
        List<String> files = new ArrayList<>();// список частей файла
        while (true) {
            try {
                s = reader.readLine();
                if (!("end".equals(s))) {
                    files.add(s);
                    int end = s.lastIndexOf(".");
                    fileNameResult = s.substring(0, end);
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            if ("end".equals(s)) {
                break;
            }
        }
//        System.out.println(fileNameResult);
//        System.out.println("_____");
        Collections.sort(files, new Comparator<String>() {
            public int compare(String o1, String o2) {
                return extractInt(o1) - extractInt(o2);
            }

            int extractInt(String s) {
                String num = s.replaceAll("\\D", "");// - удаление всех букв
                return num.isEmpty() ? 0 : Integer.parseInt(num);
            }
        });
//        System.out.println(files);
//        System.out.println("_____");

        try (FileOutputStream fos = new FileOutputStream(fileNameResult)){
            for (int i = 0; i < files.size(); i++){
                FileInputStream fis = new FileInputStream(files.get(i));
                byte[] buffer = new byte[fis.available()];
                while (fis.available() > 0){
                    fis.read(buffer);
                    fos.write(buffer);
                }
                fis.close();
            }
        }
    }
    /**
     * Метод парсит значения в целочисленный массив, а затем сортирует
     */
    private static String[] sArr = {"5", "2", "11", "1", "21", "55", "15", "25", "6"};// массив строк с числами
    public static void sortMethod1() {
        int[] intArr = Arrays.stream(sArr).mapToInt(Integer::parseInt).toArray();
        Arrays.sort(intArr);
        System.out.println(Arrays.toString(intArr));
    }
}