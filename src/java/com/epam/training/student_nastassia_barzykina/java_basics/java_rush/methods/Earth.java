package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods;

public class Earth {
    static final String  NAME = "Земля";
    static final double SQUARE = 510_100_000;
    static final long POPULATION = 7_594_000_000L;
    static final long EQUATOR_LENGTH = 40_075_696;// не хватало подчеркивания!!!!
    public static void main(String[] args) {
        short s = Short.MAX_VALUE;
        long l = 9_223_372_036_854_775_807l;
        //long l = Long.MAX_VALUE;
        byte b = Byte.MAX_VALUE;
        System.out.println(s);
        System.out.println(l);
        double earthDiameter = 1.2742E+4;
        //12742.0;
        double lightSpeed = 2.99792458E+8;
        long v = (long) lightSpeed;
        //299792458.0;
        double uraniumAtomicMass = 2.380289E+2;
                //238.0289;
        double averageBeeWeight = 8.5E-2;
                //0.085;
        double javaDeveloperSalary = 1.0E+4;
                //10000.0;
        System.out.println(lightSpeed + " " + v + " V " +  " " + "299792458");
        System.out.println(earthDiameter +" " + "12742");
        System.out.println(uraniumAtomicMass +" "+ "238.0289");
        System.out.println(averageBeeWeight +" "+ "0.085");
        System.out.println(javaDeveloperSalary +" "+ "10000.0");
        System.out.println( 100.0 / 0.0 );//Infinity
        System.out.println(0.0 / 0.0);//NaN
        div(0.0, 10.0);
        div(0.0, -1.0);
        //напишите тут ваш код
    }

    public static void div(double a, double b){
        System.out.println(b/a);
    }

    }
