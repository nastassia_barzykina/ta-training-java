package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread.add;

public class Ex1633 extends Thread {
    static Thread.UncaughtExceptionHandler h = new UncaughtExceptionHandler() {
        @Override
        public void uncaughtException(Thread t, Throwable e) {
            System.out.println("UncEx: " + e);
        }
    };

        Thread t = new Thread();

        public void run() {
            System.out.println("sleep...");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println("Inter");
            }
            System.out.println("Throwing ...");
            throw new RuntimeException();
        }
        //t.setUncaughtExceptionHandler(t);
        //t.start();


        public static void main(String[] args) {
            Thread ex = new Thread();
            try {
                ex.start();
                ex.join();
            } catch (InterruptedException e) {
                System.out.println("from main");
            }
            System.out.println("main stopped");
            ex.setUncaughtExceptionHandler(h);
            // ex.start();
        }
//        public void run() {
//            try {
//                while (true) {
//                    System.out.println("started thread");
//                    sleep(2000);
//                    throw new RuntimeException("ex from thread");
//                }
//            } catch (RuntimeException e) {
//                System.out.println("***");
//                throw e;
//            } catch (InterruptedException e) {
//            }
//        }
    }

