package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.variable.static_m;

/**
 * В методе main конвертируй строку string в тип double с помощью статического метода parseDouble класса Double.
 * Затем округли полученное значение, используя статический метод round класса Math.
 * Результат выведи на экран.
 */
public class StaticMethod {
    public static void main(String[] args) {
        String string = "12.84";
        System.out.println((Math.round(Double.parseDouble(string))));

        //напишите тут ваш код
    }
}
