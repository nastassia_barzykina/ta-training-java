package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.constructors_obj.task.earth;

public class NorthAmerica {
    private final int area;

    public NorthAmerica(int area) {
        this.area = area;
    }
}
