package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread.add17.task1710;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Программа запускается с одним из следующих наборов параметров:
 * -c name1 sex1 bd1 name2 sex2 bd2 ...
 * -u id1 name1 sex1 bd1 id2 name2 sex2 bd2 ...
 * -d id1 id2 id3 id4 ...
 * -i id1 id2 id3 id4 ...
 * <p>
 * Значения параметров:
 * name - имя, String
 * sex - пол, "м" или "ж", одна буква
 * bd - дата рождения в следующем формате 15/04/1990
 * -с - добавляет всех людей с заданными параметрами в конец allPeople, выводит id (index) на экран в соответствующем порядке
 * -u - обновляет соответствующие данные людей с заданными id
 * -d - производит логическое удаление человека с id, заменяет все его данные на null
 * -i - выводит на экран информацию о всех людях с заданными id: name sex bd
 * <p>
 * id соответствует индексу в списке.
 * Формат вывода даты рождения 15-Apr-1990
 * Все люди должны храниться в allPeople.
 * Порядок вывода данных соответствует вводу данных.
 * Обеспечить корректную работу с данными для множества нитей (чтоб не было затирания данных).
 * Используй Locale.ENGLISH в качестве второго параметра для SimpleDateFormat.
 * <p>
 * Пример вывода для параметра -і с двумя id:
 * Миронов м 15-Apr-1990
 * Миронова ж 25-Apr-1997
 * <p>
 * Требования:
 * •	Класс Solution должен содержать public static volatile поле allPeople типа List<Person>.
 * •	Класс Solution должен содержать статический блок, в котором добавляются два человека в список allPeople.
 * •	При параметре -с программа должна добавлять всех людей с заданными параметрами в конец списка allPeople, и выводить id каждого (index) на экран.
 * •	При параметре -u программа должна обновлять данные людей с заданными id в списке allPeople.
 * •	При параметре -d программа должна логически удалять людей с заданными id в списке allPeople.
 * •	При параметре -i программа должна выводить на экран данные о всех людях с заданными id по формату указанному в задании.
 * •	Метод main класса Solution должен содержать оператор switch по значению args[0].
 * •	Каждый case оператора switch должен иметь блок синхронизации по allPeople.
 */
public class Task1711 {
    public static volatile List<Person> allPeople = new ArrayList<Person>();
    static SimpleDateFormat formatD = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
    static SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
    static Date bd;

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) {
        int index;
        int count;
        switch (args[0]) {
            case "-c":
                count = (args.length - 1) / 3;
                synchronized (allPeople) {
                    for (int i = 1; i <= count * 3; i += 3) {
                        create(args[i], args[i + 1], args[i + 2]);
                    }
                }
                break;
            case "-u":
                count = (args.length - 1) / 4;
                synchronized (allPeople) {
                    for (int i = 1; i <= count * 4; i += 4) {
                        index = Integer.parseInt(args[i]);
                        update(index, args[i + 1], args[i + 2], args[i + 3]);
                        //info(index);
                    }
                }
                break;
            case "-d":
                synchronized (allPeople) {
                    for (int i = 1; i < args.length; i++) {
                        index = Integer.parseInt(args[i]);
                        delete(index);
                    }
                }
                break;
            case "-i":
                synchronized (allPeople) {
                    for (int i = 1; i < args.length; i++) {
                        index = Integer.parseInt(args[i]);
                        info(index);
                    }
                }
                break;
        }
        //start here - начни тут
    }

    public static void create(String name, String sex, String date) {//добавляет человека с заданными параметрами в конец allPeople,
        // выводит id (index) на экран
        try {
            bd = formatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (sex.equals("m")) {
            allPeople.add(Person.createMale(name, bd));
        } else {
            allPeople.add(Person.createFemale(name, bd));
        }
        int lastIndex = allPeople.size() - 1;
        System.out.println(lastIndex);
    }

    public static void info(int index) {//выводит на экран информацию о человеке с id: name sex (м/ж) bd (формат 15-Apr-1990)
        Person person = allPeople.get(index);
        System.out.println(person.getName() + " "
                + person.getSex().getValue() + " "
                + formatD.format(person.getBirthDate()));
    }

    public static void update(int index, String name, String sex, String date) {//обновляет данные человека с данным id
        try {
            bd = formatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Person up = allPeople.get(index);
        up.setName(name);
        up.setBirthDate(bd);
        up.setSex(Sex.getSexByString(sex));

    }

    public static void delete(int index) {//производит логическое удаление человека с id, заменяет все его данные на null
        Person up = allPeople.get(index);
        up.setName(null);
        up.setBirthDate(null);
        up.setSex(null);
        //update(index, null, null, null);
    }

}
