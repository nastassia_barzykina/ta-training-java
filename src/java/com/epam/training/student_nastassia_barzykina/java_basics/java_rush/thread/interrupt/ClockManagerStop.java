package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread.interrupt;

public class ClockManagerStop {
    public static boolean isClockRun = true;
    public static void main(String[] args)
    {
        Clock clock = new Clock();
        Thread clockThread = new Thread(clock);
        clockThread.start();
/**
 * Главная нить, запускает дочернюю нить – часы, которая должна работать вечно.
 * Ждет 10 секунд и подает часам сигнал на завершение.
 *
 * Главная нить завершает свою работу.
 *
 * Нить часов завершает свою работу.
 */
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        isClockRun = false;
    }
    static class Clock implements Runnable {
        /**
         * Если несколько нитей, лучше завести такую переменную для каждой нити. Удобнее всего будет добавить ее прямо в класс.
         * private boolean isCancel = false;
         *
         * public void cancel(){
         *      this.isCancel = true;
         * }
         *
         * public void run(){
         *      while (!isCancel){
         *          Thread.sleep(1000);
         *          System.out.println("Tik");
         *      }
         * }
         * а в main - clock.cancel();
         */
        @Override
        public void run() {
            while (true)
            {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("Tik");

                if (!ClockManagerStop.isClockRun)//Если переменная ClockManagerStop.isClockRun равна false –
                    // метод run завершится.
                    return;
            }
        }
    }
}
