package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.data.localdate.convert;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
// для теста
public class Test {
    public static void main(String[] args) {
        Map<String, Integer> map = Map.of("Ivan", 2000, "Petr", 2010, "Egor", 2000);
        Map<LocalDate, List<LocalTime>> test = Map.of(
                LocalDate.parse("2000-09-13"), List.of(
                        LocalTime.parse("01:49:12.325374794")),
                LocalDate.parse("2008-05-29"),List.of(
                        LocalTime.parse("08:30:03.643082444"),
                        LocalTime.parse("13:39:28.294915730"),
                        LocalTime.parse("23:54:27.305936976"),
                        LocalTime.parse("12:46:20.363230376")),
                LocalDate.parse("1996-12-10"), List.of(
                        LocalTime.parse("05:21:55.814521303"),
                        LocalTime.parse("01:24:02.758211901"),
                        LocalTime.parse("13:14:00.438289670")),
                LocalDate.parse("1994-08-06"), List.of(
                LocalTime.parse("05:57:41.189791001"),
                        LocalTime.parse("04:57:30.884706342"))
        );
        Set<LocalDate> testSet = test.keySet();

        Set<String> mapKeys = map.keySet();
        System.out.println(mapKeys); // => [Petr, Ivan, Egor]

        Set<Integer> mapValues = map.values().stream().collect(Collectors.toSet());
        System.out.println(mapValues); // => [2000, 2010]

        Set<Map.Entry<String, Integer>> mapSet = map.entrySet();
        System.out.println(mapSet); // => [Petr=2010, Ivan=2000, Egor=2000]
    }
}
