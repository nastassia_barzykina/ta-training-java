package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread.add17.task1710;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * CRUD - Create, Read, Update, Delete.
 * <p>
 * Программа запускается с одним из следующих наборов параметров:
 * -c name sex bd
 * -r id
 * -u id name sex bd
 * -d id
 * <p>
 * Значения параметров:
 * name - имя, String
 * sex - пол, "м" или "ж", одна буква
 * bd - дата рождения в следующем формате 15/04/1990
 * -c - добавляет человека с заданными параметрами в конец allPeople, выводит id (index) на экран
 * -r - выводит на экран информацию о человеке с id: name sex (м/ж) bd (формат 15-Apr-1990)
 * -u - обновляет данные человека с данным id
 * -d - производит логическое удаление человека с id, заменяет все его данные на null
 * <p>
 * id соответствует индексу в списке.
 * Все люди должны храниться в allPeople.
 * Используй Locale.ENGLISH в качестве второго параметра для SimpleDateFormat.
 * <p>
 * Пример параметров:
 * -c Миронов м 15/04/1990
 * <p>
 * Пример вывода для параметра -r:
 * Миронов м 15-Apr-1990
 * <p>
 * Если программа запущена с параметрами, то они попадают в массив args (аргумент метода main - String[] args).
 * Например, при запуске программы c параметрами:
 * -c name sex bd
 * получим в методе main
 * args[0] = "-c"
 * args[1] = "name"
 * args[2] = "sex"
 * args[3] = "bd"
 * Для запуска кода с параметрами в IDE IntellijIDEA нужно их прописать в поле Program arguments в меню
 * Run -> Edit Configurations.
 * <p>
 * Требования:
 * •	Класс Solution должен содержать public static поле allPeople типа List<Person>.
 * •	Класс Solution должен содержать статический блок, в котором добавляются два человека в список allPeople.
 * •	При запуске программы с параметром -с программа должна добавлять человека с заданными параметрами в конец
 * списка allPeople, и выводить id (index) на экран.
 * •	При запуске программы с параметром -r программа должна выводить на экран данные о человеке с заданным id по
 * формату указанному в задании.
 * •	При запуске программы с параметром -u программа должна обновлять данные человека с заданным id в списке allPeople.
 * •	При запуске программы с параметром -d программа должна логически удалять человека с заданным id в списке allPeople
 */
public class Solution {
    public static List<Person> allPeople = new ArrayList<Person>();

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }
    static SimpleDateFormat formatD = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
    static SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
    static Date bd;

    public static void main(String[] args) {
        //напишите тут ваш код
        if (args[0].equals("-c")) {
            create(args[1], args[2], args[3]);
            return;
        }
        int index = Integer.parseInt(args[1]);
        if (args[0].equals("-r")){
            read(index);
        }
        if (args[0].equals("-u")){
            update(index, args[2], args[3], args[4]);
        }
        if (args[0].equals("-d")){
            delete(index);
        }
//        if (args.length == 2) {
//            index = Integer.parseInt(args[1]);
//            if (args[0].equals("-r")) {//выводит на экран информацию о человеке с id: name sex (м/ж) bd (формат 15-Apr-1990)
//                //int index = Integer.parseInt(args[1]);
//                System.out.println(allPeople.get(index).getName() + " "
//                        + allPeople.get(index).getSex().getValue() + " "
//                        + formatD.format(allPeople.get(index).getBirthDate()));
//            }
//            if (args[0].equals("-d")) {//производит логическое удаление человека с id, заменяет все его данные на null
//                index = Integer.parseInt(args[1]);
//                Person up = allPeople.get(index);
//                up.setName(null);
//                up.setBirthDate(null);
//                up.setSex(null);
//                //System.out.println(allPeople.get(index).getName());
//            }
//        }
//        if (args.length == 4 && args[0].equals("-c")){//добавляет человека с заданными параметрами в конец allPeople,
//            // выводит id (index) на экран
//            try {
//                bd = formatter.parse(args[3]);
////                System.out.println(bd + "   TEST");
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            if (args[2].equals("m")) {
//                allPeople.add(Person.createMale(args[1], bd));
//            } else {
//                allPeople.add(Person.createFemale(args[1], bd));
//            }
//            int lastIndex = allPeople.size() - 1;
//            System.out.println(lastIndex);
//
//            System.out.println(allPeople.get(lastIndex).getName() + " "
//                    + allPeople.get(lastIndex).getSex().getValue() + " "
//                    + formatD.format(allPeople.get(lastIndex).getBirthDate()));
//        }
//        if (args.length == 5 && args[0].equals("-u")){//обновляет данные человека с данным id
//            try {
//                bd = formatter.parse(args[4]);
////                System.out.println(bd + "   TEST");
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            index = Integer.parseInt(args[1]);
//            Person up = allPeople.get(index);
//            up.setName(args[2]);
//            up.setBirthDate(bd);
//            up.setSex(Sex.getSexByString(args[3]));
//        }
    }
    public static void create(String name, String sex, String date){//добавляет человека с заданными параметрами в конец allPeople,
        // выводит id (index) на экран
        try {
            bd = formatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (sex.equals("m")) {
            allPeople.add(Person.createMale(name, bd));
        } else {
            allPeople.add(Person.createFemale(name, bd));
        }
        int lastIndex = allPeople.size() - 1;
        System.out.println(lastIndex);
    }
    public static void read(int index){//выводит на экран информацию о человеке с id: name sex (м/ж) bd (формат 15-Apr-1990)
        Person person = allPeople.get(index);
        System.out.println(person.getName() + " "
                + person.getSex().getValue() + " "
                + formatD.format(person.getBirthDate()));
    }
    public static void update(int index, String name, String sex, String date){//обновляет данные человека с данным id
        try {
            bd = formatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Person up = allPeople.get(index);
        up.setName(name);
        up.setBirthDate(bd);
        up.setSex(Sex.getSexByString(sex));

    }
    public static void delete(int index){//производит логическое удаление человека с id, заменяет все его данные на null
        Person up = allPeople.get(index);
        up.setName(null);
        up.setBirthDate(null);
        up.setSex(null);
        //update(index, null, null, null);
    }
}
