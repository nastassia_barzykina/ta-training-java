package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.wrapper.task1814;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Измени класс TxtInputStream так, чтобы он работал только с txt-файлами (*.txt).
 * Например, first.txt или name.1.part3.txt.
 * Если передан не txt-файл, например, file.txt.exe, то конструктор должен выбрасывать исключение UnsupportedFileNameException.
 * Подумай, что еще нужно сделать, в случае выброшенного исключения.
 *
 * Требования:
 * •	Класс TxtInputStream должен наследоваться от класса FileInputStream.
 * •	Если в конструктор передан txt-файл, TxtInputStream должен вести себя, как обычный FileInputStream.
 * •	Если в конструктор передан не txt-файл, должно быть выброшено исключение UnsupportedFileNameException.
 * •	В случае выброшенного исключения, так же должен быть вызван super.close().
 */

public class TxtInputStream extends FileInputStream {
    private FileInputStream inputStream;
    public TxtInputStream(String fileName) throws UnsupportedFileNameException, IOException {
        super(fileName);
        if (fileName.endsWith(".txt")){
            inputStream = new FileInputStream(fileName);
        } else {
            super.close();
            throw new UnsupportedFileNameException();
        }
    }

    @Override
    public int read() throws IOException {
        return inputStream.read();
    }

    @Override
    public int read(byte[] b) throws IOException {
        return inputStream.read(b);
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        return inputStream.read(b, off, len);
    }

    @Override
    public int available() throws IOException {
        return inputStream.available();
    }

    @Override
    public long skip(long n) throws IOException {
        return inputStream.skip(n);
    }

    @Override
    public void close() throws IOException {
        super.close();
        inputStream.close();
    }

    public static void main(String[] args) {
    }
}
