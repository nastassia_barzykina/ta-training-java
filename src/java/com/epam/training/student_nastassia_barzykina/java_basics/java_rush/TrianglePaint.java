package com.epam.training.student_nastassia_barzykina.java_basics.java_rush;

public class TrianglePaint {
    public static void main(String[] args) {
//        for (int i = 0; i < 10; i++){
//            int starCount = 10 - i;
//            for (int j = 0; j < starCount; j++){
//                System.out.print("*");
//            }
//            System.out.println();
//        }
        for (int i = 0; i < 10; i++){
            int count = 10 - i;
            for (int j = 10; j >= count; j--){
                System.out.print("8");
            }
            System.out.println();
        }
        for (int i = 0; i < 10; i++){// идентично предыдущему
            for (int j = 0; j <= i; j++){
                System.out.print("8");
            }
            System.out.println();
        }
    }
}
