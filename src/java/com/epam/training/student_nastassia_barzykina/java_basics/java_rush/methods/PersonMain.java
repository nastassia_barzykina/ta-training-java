package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods;

public class PersonMain {
    public static void main(String[] args) {
        Person person = new Person("Иван", "Иванов");
        System.out.println("Досье.");
        System.out.println("Имя: " + person.getFirstName());
        System.out.println("Фамилия: " + person.getLastName());
        System.out.println("Полное имя: " + person.getFullName());
        int x = (int) 3.14;
        System.out.println(x);
    }
}
