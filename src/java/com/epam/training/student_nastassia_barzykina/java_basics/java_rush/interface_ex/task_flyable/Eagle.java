package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex.task_flyable;

public class Eagle implements Flyable{
    @Override
    public int getMaxSpeed() {
        return 180;
    }
}
