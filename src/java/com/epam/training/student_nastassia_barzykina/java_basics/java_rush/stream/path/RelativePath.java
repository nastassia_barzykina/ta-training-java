package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.path;

import java.nio.file.Path;
import java.util.Scanner;

public class RelativePath {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str1 = scanner.nextLine();
        String str2 = scanner.nextLine();
        Path path1 = Path.of(str1);
        Path path2 = Path.of(str2);
//        if (
//                !str1.isEmpty()
//                        && !str2.isEmpty()
//                        && !path1.equals(path2)
//                        && (path1.isAbsolute() == path2.isAbsolute())
//                        && (!path1.isAbsolute() || path1.getRoot().equals(path2.getRoot()))
//        ) {
        /**
         * ПРАВИЛЬНОЕ РЕШЕНИЕ!!!
         */
        try {
            Path result = path1.relativize(path2);
            System.out.println(result);
        } catch (Exception e){
            //
        }
    }
}

