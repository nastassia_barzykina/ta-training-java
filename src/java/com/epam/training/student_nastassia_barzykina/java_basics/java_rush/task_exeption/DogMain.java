package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.task_exeption;

public class DogMain {
    public static void main(String[] args) {
        Dog dog = new Dog("Lucky");
        dog.putCollar();
        dog.putMuzzle();
        try {
            dog.walk();
        } catch (DogIsNotReadyException e) {
            System.out.println(e.getMessage() + "test");
            System.out.println("Проверяем снаряжение! Ошейник надет? " + dog.isCollarPutOn + "\r\n Поводок надет? "
                    + dog.isLeashPutOn + "\r\n Намордник надет? " + dog.isMuzzlePutOn);
        }
    }
}
