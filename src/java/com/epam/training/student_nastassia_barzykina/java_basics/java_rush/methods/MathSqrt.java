package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods;

/**
 * Исправь методы класса, используя класс Math:
 *
 * sqrt(double) — должен возвращать квадратный корень переданного аргумента.
 * cbrt(double) — должен возвращать кубический корень переданного аргумента.
 * pow(int, int) — должен возвращать значение первого аргумента, возведенного в степень второго аргумента.
 */
public class MathSqrt {
    public static double sqrt(double a) {
        return Math.sqrt(a);
    }

    public static double cbrt(double a) {
        return Math.cbrt(a);
    }

    public static double pow(int number, int power) {
//        if (power == 0) {
//            return 1;
//        }
        return Math.pow(number,power);

//        int modulus = power < 0 ? power * -1 : power;
//        int result = number;
//        for (int i = 1; i < modulus; i++) {
//            result *= number;
//        }
//        return power < 0 ? 1.0 / result : result;
    }

    public static void main(String[] args) {
        double a = 1;
        double b = 0;
        sqrt(a);
        cbrt(a);
        pow((int) a, (int) b);
        System.out.println(sqrt(a));
        System.out.println(cbrt(a));
        System.out.println(pow((int) a, (int) b));
    }
}
