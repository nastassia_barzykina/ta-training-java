package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.variable.nested_inner_class;

public class Outer {
    class Inner {
        Inner() {
            System.out.println("Создание объекта внутреннего класса");
        }
    }
    static class Nested {
        Nested() {
            System.out.println("Создание объекта вложенного класса");
        }
    }
}
