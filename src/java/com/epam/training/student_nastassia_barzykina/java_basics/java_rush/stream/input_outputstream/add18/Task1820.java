package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.add18;

import com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods.overloading.Test1526;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Считать с консоли 2 имени файла.
 * Первый файл содержит вещественные(дробные) числа, разделенные пробелом. Например, 3.1415.
 * Округлить числа до целых и записать через пробел во второй файл.
 * Закрыть потоки.
 *
 * Принцип округления:
 * 3.49 => 3
 * 3.50 => 4
 * 3.51 => 4
 * -3.49 => -3
 * -3.50 => -3
 * -3.51 => -4
 *
 * Требования:
 * •	Программа должна два раза считать имена файлов с консоли.
 * •	Для первого файла создай поток для чтения. Для второго - поток для записи.
 * •	Считать числа из первого файла, округлить их и записать через пробел во второй.
 * •	Должны соблюдаться принципы округления, указанные в задании.
 * •	Созданные для файлов потоки должны быть закрыты.
 */
public class Task1820 {
    public static void main(String[] args) throws Exception{
        Scanner scanner = new Scanner(System.in);
        String inputFile = scanner.nextLine();//"C:\\test\\num.txt";
        String outputFile = scanner.nextLine();//"C:\\test\\numInt.txt";//
//        try {
//            Scanner sc = new Scanner(new File(inputFile));
//            BufferedWriter writer = Files.newBufferedWriter(Paths.get(outputFile));
//            while (sc.hasNextDouble()){
//                double numDouble = sc.nextDouble();
//                int numInt = (int) Math.round(numDouble);
//                String res = String.valueOf(numInt);
//                writer.write(res);
//                writer.write(" ");
//            }
//            writer.close();
//            sc.close();
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
        try (BufferedReader reader = new BufferedReader(new FileReader(inputFile));
             BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile))) {

            String line;
            while ((line = reader.readLine()) != null) {
                String[] numbers = line.split(" ");
                for (String num : numbers) {
                    try {
                        double originalNumber = Double.parseDouble(num);
                        long roundedNumber = Math.round(originalNumber);
                        writer.write(roundedNumber + " ");
                    } catch (NumberFormatException e) {
                        // некорректные числа
                    }
                }
            }
        } catch (IOException e) {
            System.err.println("Ошибка при чтении/записи файла: " + e.getMessage());
        }
        // solution:
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = reader.readLine();
        String fileName2 = reader.readLine();

        try (BufferedReader bufferedFileReader = new BufferedReader(new FileReader(fileName1));
             PrintWriter printWriter = new PrintWriter(new FileWriter(fileName2))) {

            while (bufferedFileReader.ready()) {
                String[] splittedLine = bufferedFileReader.readLine().split(" ");
                for (String numberInString : splittedLine) {
                    double number = Double.parseDouble(numberInString);
                    long roundedNumber = Math.round(number);
                    printWriter.print(roundedNumber + " ");
                }
            }
        }
    }
}
