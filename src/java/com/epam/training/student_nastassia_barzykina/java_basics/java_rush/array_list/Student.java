package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.array_list;

public class Student {
    private String name;
    public Student(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
