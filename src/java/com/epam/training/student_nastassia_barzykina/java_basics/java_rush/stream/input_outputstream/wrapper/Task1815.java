package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.wrapper;

import java.util.List;

/**
 * Измени класс TableInterfaceWrapper так, чтобы он стал Wrapper-ом для TableInterface.
 * Метод setModel должен вывести в консоль количество элементов в списке перед обновлением модели (вызовом метода
 * setModel у объекта типа TableInterface).
 * Метод getHeaderText должен возвращать текст в верхнем регистре - используй метод toUpperCase().
 *
 * Требования:
 * •	Класс TableInterfaceWrapper должен реализовывать интерфейс TableInterface.
 * •	Класс TableInterfaceWrapper должен инициализировать в конструкторе поле типа TableInterface.
 * •	Метод setModel() должен вывести в консоль количество элементов в новом листе перед обновлением модели.
 * •	Метод getHeaderText() должен возвращать текст в верхнем регистре.
 * •	Метод setHeaderText() должен устанавливать текст для заголовка без изменений.
 */
public class Task1815 {
    public class TableInterfaceWrapper implements TableInterface{
        TableInterface component;
        TableInterfaceWrapper(TableInterface tableInterface){
            this.component = tableInterface;
        }

        @Override
        public void setModel(List rows) {
            System.out.println(rows.size());
            component.setModel(rows);
        }

        @Override
        public String getHeaderText() {
            return component.getHeaderText().toUpperCase();
        }

        @Override
        public void setHeaderText(String newHeaderText) {
            component.setHeaderText(newHeaderText);
        }
    }

    public interface TableInterface {
        void setModel(List rows);

        String getHeaderText();

        void setHeaderText(String newHeaderText);
    }

    public static void main(String[] args) {
    }
}
