package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex.task_compare;

public class Student {
    private String name;
    private int age;
    public Student (String name, int age){
        this.name = name;
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Студент - " + name +
                ", возраст - " + age +
                '.';
    }
}
