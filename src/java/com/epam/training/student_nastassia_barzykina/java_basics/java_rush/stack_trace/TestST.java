package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stack_trace;

public class TestST {
    public static void main(String[] args) {
        try{
            dangerousMethod();
        } catch(Exception e){
            //StackTraceElement[] methods = e.getStackTrace();
            e.printStackTrace();
        }
    }

    static void dangerousMethod() throws Exception {
        throw new Exception("Mu-ha-ha!");
    }
}
