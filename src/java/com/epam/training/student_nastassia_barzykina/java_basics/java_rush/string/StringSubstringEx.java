package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.string;

public class StringSubstringEx {
    public static void main(String[] args) {
        String filePath = "D:\\Movies\\Futurama.mp4";
        int lastFileSeparatorIndex = filePath.lastIndexOf('\\');
        String fileName = filePath.substring(lastFileSeparatorIndex + 1);
        System.out.println(fileName); //Futurama.mp4
        System.out.println(-5 / 0.0);
    }
}
