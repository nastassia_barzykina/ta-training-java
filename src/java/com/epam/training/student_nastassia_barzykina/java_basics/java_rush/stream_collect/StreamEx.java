package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream_collect;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Stream;

public class StreamEx {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        Collections.addAll(list, "ss", "ssa", "hhh2");
        String max = list.stream().max((s1, s2) -> s1.length() - s2.length()).get();
        System.out.println(max);
        int[] data = new int[]{1, 2, 3, 5, -2, -8, 0, 77, 5, 5};
        int maxInt = Arrays.stream(data).max().getAsInt();
        System.out.println(maxInt);

        // замена кода:
        ArrayList<Double> num = new ArrayList<>();
        Collections.addAll(num, 3.1,5.0,22.9,7.7,999.4, -12.0);
        Stream<Double> stream = num.stream();
        Optional<Double> optional = stream.max((s1, s2) -> s1.compareTo(s2));//Double::compareTo
        double maxNum = optional.get();
        System.out.println(maxNum);
    }
}
