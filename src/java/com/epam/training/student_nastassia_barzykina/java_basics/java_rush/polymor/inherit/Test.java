package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.inherit;

public class Test {
    public static void main(String[] args) {
        Cat parent = new Cat();

        Cat me = new Cat();
        me.setMyParent(parent);
        Cat myParent = me.getMyParent();
        Tiger parent1 = new Tiger();

        Tiger me1 = new Tiger();
        me1.setMyParent(parent1);
        Tiger myParent1 = me1.getMyParent();
    }
    static class Cat
    {
        public Cat parent;
        public Cat getMyParent()
        {
            return this.parent;
        }
        public void setMyParent(Cat cat)
        {
            this.parent = cat;
        }
    }
    static class Tiger extends Cat
    {
        public Tiger getMyParent()
        {
            return (Tiger) this.parent;
        }
    }
}
