package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.variable;

public class Test {
    public static int salary;

    public static void add(int increase) {
        //int salary = Test.salary;
        salary += increase;
    }
    public static void main(String[] args) {
        add(100500);
        System.out.println(salary);
    }
}
