package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.poly_abstract.ex_chess;

public abstract class ChessItem {
    public int x, y; // координаты
    private int value; // «ценность» фигуры
    public int getValue() // обычный метод, возвращает значение value
    {
        return value;
    }

    public abstract void draw(); // абстрактный метод. Реализация отсутствует.
}
