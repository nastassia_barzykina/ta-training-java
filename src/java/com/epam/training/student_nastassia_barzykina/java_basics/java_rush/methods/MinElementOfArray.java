package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods;

import java.util.Scanner;

/**
 * Считать 10 чисел с консоли и заполнить ими массив с помощью метода getArrayOfTenElements().
 * Найти минимальный элемент массива и вернуть этот элемент с помощью метода min(int[]).
 * В методе min(int[]) обязательно используй метод Math.min(int, int).
 */
public class MinElementOfArray {
    public static void main(String[] args) {
        int[] intArray = getArrayOfTenElements();
        System.out.println(min(intArray));
    }

    public static int min(int[] ints) {
        int min = ints[0];
//        for (int i = 1; i < ints.length; i++){
//           min = Math.min(min, ints[i]);
//        } с использованием цикла foreach:
        for (int number : ints) {
            min = Math.min(min, number);
        }
        return min;
    }

    public static int[] getArrayOfTenElements() {
        Scanner sc = new Scanner(System.in);
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++){
          array[i] = sc.nextInt();
        }
        return array;
    }

}
