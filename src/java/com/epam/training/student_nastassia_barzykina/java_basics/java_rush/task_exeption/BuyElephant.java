package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.task_exeption;

import java.util.Scanner;

/**
 * В блоке catch выведи на экран переменную answer и брось дальше пойманное исключение.
 */
public class BuyElephant {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        buyElephant(null, scanner);
    }

    static void buyElephant(String answer, Scanner scanner) {
        if (answer == null) {
            System.out.println("Купи слона");
        } else if (answer.toLowerCase().equals("ок")) {
            System.out.println("Так-то лучше :) Список твоих отговорок:");
            throw new SecurityException();// создается исключение
/**
 * смотри, у нас выполняется код в блоке трай. при некоторых условиях он выбрасывает нам исклбчение и начинает выполнять
 * альтернативный код в поле кетч.
 * а если нам нужно, чтобы программа завершилась с ошибкой всеравно? нам тогда и в блоке кетч нужно чтото запрещенное сделать,
 * на ноль поделить например и снова получить ошибку...
 * для этого есть возможность выбросить эту ошибку в ручную с помошью throw.
 * например, если у нас произошел конец масива в блоке трай, мы получим outOfBounds, и начнет ввполнятся блок кетч.
 * допустим мы хотим вывести все что попало в массив до ошибки, написать пользователю в чем проблема,  ввдать какие то ещё
 * данные и в конце концов выдать outOfBounds.
 * вот throw нужен именно для того чтобы вызвать виконце outOfBounds не совершая ошибку по насорящему.
 *
 * Чтобы передать это исключение наверх по иерархии. Например, в методах, вызвавших этот метод, может быть своя обработка исключений
 *
 * Обычно это делается для логирования ошибок или для счетчика сбоев в работе метода.
  */
        } else {
            System.out.println("Все говорят \"" + answer + "\", а ты купи слона");
        }

        answer = scanner.nextLine();

        try {
            buyElephant(answer, scanner);
        } catch (Exception e) {
            System.out.println(answer);
            throw e;// выбрасывается пойманное исключение, а не создается новое.
        }
    }
}
