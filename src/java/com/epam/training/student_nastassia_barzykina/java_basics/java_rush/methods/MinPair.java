package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods;

public class MinPair {
    public static void main(String[] args) throws Exception {
        int[] data = new int[]{-100, 1, 2, 3, 5, -2, -8, 0, 77, 5, 5};

        Pair<Integer, Integer> result = getMinimumAndIndex(data);

        System.out.println("The minimum is " + result.x);
        System.out.println("The index of the minimum element is " + result.y);
    }

    public static Pair<Integer, Integer> getMinimumAndIndex(int[] array) {
        if (array == null || array.length == 0) {
            return new Pair<Integer, Integer>(null, null);
        }
        int min = array[0];
        int y = 0;
        for (int i = 1; i < array.length; i++) {
            if (min > array[i]) {
                min = Math.min(min, array[i]);
                y = i;
            }
        }
        //int index = 0;
//        for (int i = 1; i < array.length; i++) {
//            if (array[i] < array[index]) {
//                index = i;
//            }
//
//        }
//
//        return new Pair<Integer, Integer>(array[index], index);

        return new Pair<Integer, Integer>(min, y);
    }

    public static class Pair<X, Y> {
        public X x;
        public Y y;

        public Pair(X x, Y y) {
            this.x = x;
            this.y = y;
        }
    }
}
