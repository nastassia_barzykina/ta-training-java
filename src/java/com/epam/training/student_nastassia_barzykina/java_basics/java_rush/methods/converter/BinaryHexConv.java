package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods.converter;

public class BinaryHexConv {
    public static void main(String[] args) {
        String binaryNumber = "100111010000";
        //String binaryNumber = "1011110";
        System.out.println("Двоичное число " + binaryNumber + " равно шестнадцатеричному числу " + toHex(binaryNumber));
        String hexNumber = "9d0";
        System.out.println("Шестнадцатеричное число " + hexNumber + " равно двоичному числу " + toBinary(hexNumber));
    }

    public static String toHex(String binaryNumber) {
        String hexNumber = "";
        if (binaryNumber == null || binaryNumber.isEmpty()) return hexNumber;
        //for (int i = 0; i < binaryNumber.length(); i++){
        if (!(binaryNumber.matches("[0-1]+"))) return hexNumber;
        //}
        if (binaryNumber.length() % 4 != 0) {
            for (int i = 0; i < binaryNumber.length() % 4; i++){
            binaryNumber = "0" + binaryNumber;
            }
        }
        int group = binaryNumber.length() / 4;
        String[] bit = new String[group];
        for (int i = 0; i < group; i++){
            bit[i] = "";
        }
        int step = 0;
        for (int i = 0; i < group; i++){
            for (int j = 0; j < 4; j++){
                bit[i] = bit[i] + binaryNumber.charAt(j + step);
            }
            step = step + 4;
        }
        for (int i = 0; i < group; i++){
            switch (bit[i]){
                case "0000" -> { hexNumber = hexNumber + "0"; }
                case "0001" -> { hexNumber = hexNumber + "1"; }
                case "0010" -> { hexNumber = hexNumber + "2"; }
                case "0011" -> { hexNumber = hexNumber + "3"; }
                case "0100" -> { hexNumber = hexNumber + "4"; }
                case "0101" -> { hexNumber = hexNumber + "5"; }
                case "0110" -> { hexNumber = hexNumber + "6"; }
                case "0111" -> { hexNumber = hexNumber + "7"; }
                case "1000" -> { hexNumber = hexNumber + "8"; }
                case "1001" -> { hexNumber = hexNumber + "9"; }
                case "1010" -> { hexNumber = hexNumber + "a"; }
                case "1011" -> { hexNumber = hexNumber + "b"; }
                case "1100" -> { hexNumber = hexNumber + "c"; }
                case "1101" -> { hexNumber = hexNumber + "d"; }
                case "1110" -> { hexNumber = hexNumber + "e"; }
                case "1111" -> { hexNumber = hexNumber + "f"; }
            }
        }
        return hexNumber;
    }

    public static String toBinary(String hexNumber) {
        String binaryNumber = "";
        if (hexNumber == null || hexNumber.isEmpty()) return binaryNumber;
        //for (int i = 0; i < hexNumber.length(); i++){
            if (!(hexNumber.matches("[a-f0-9]+"))) return binaryNumber;
       // }
        for (int i = 0; i < hexNumber.length(); i++){
            switch (hexNumber.charAt(i)){
                case '0' -> { binaryNumber = binaryNumber + "0000"; }
                case '1' -> { binaryNumber = binaryNumber + "0001"; }
                case '2' -> { binaryNumber = binaryNumber + "0010"; }
                case '3' -> { binaryNumber = binaryNumber + "0011"; }
                case '4' -> { binaryNumber = binaryNumber + "0100"; }
                case '5' -> { binaryNumber = binaryNumber + "0101"; }
                case '6' -> { binaryNumber = binaryNumber + "0110"; }
                case '7' -> { binaryNumber = binaryNumber + "0111"; }
                case '8' -> { binaryNumber = binaryNumber + "1000"; }
                case '9' -> { binaryNumber = binaryNumber + "1001"; }
                case 'a' -> { binaryNumber = binaryNumber + "1010"; }
                case 'b' -> { binaryNumber = binaryNumber + "1011"; }
                case 'c' -> { binaryNumber = binaryNumber + "1100"; }
                case 'd' -> { binaryNumber = binaryNumber + "1101"; }
                case 'e' -> { binaryNumber = binaryNumber + "1110"; }
                case 'f' -> { binaryNumber = binaryNumber + "1111"; }
            }
        }
        return binaryNumber;
    }
}
