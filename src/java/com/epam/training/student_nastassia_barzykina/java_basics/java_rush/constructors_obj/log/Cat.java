package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.constructors_obj.log;

/**
 * уборка мусора, метод finalize() - вызывается не всегда
 * В момент, когда сборщик мусора добрался до объекта, перед самым его уничтожением, у объекта вызывается специальный
 * метод — finalize().
 *
 * Его можно использовать, чтобы освободить какие-то дополнительные ресурсы, которые использовал объект.
 *
 * Метод finalize() принадлежит классу Object. То есть, наравне с equals(), hashCode() и toString(), он есть у любого объекта.
 *
 * Но перед уничтожением объекта он вызывается далеко не всегда.
 */
    public class Cat {

        private String name;
        public static int j;

        public Cat(String name) {
            this.name = name;
        }
        public Cat() {
        }

        public static void main(String[] args) throws Throwable {

            for (int i = 0 ; i < 1000000; i++) {

                Cat cat = new Cat();
                cat = null;//вот здесь первый объект становится доступен сборщику мусора
                //j++;
            }
        }

        @Override
        protected void finalize() throws Throwable {
            System.out.println("Объект Cat уничтожен!" + j++);
        }
    }
