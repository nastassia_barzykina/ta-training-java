package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.wrapper;

public class Decorator {
    public static void main(String[] args) {
        Printer printer = new Bracket(new SomeText(new Printer()));
        printer.print();
    }
    interface Print {
        void print();
    }
    static class Printer implements Print{

        @Override
        public void print() {
            System.out.print("Hello");
        }
    }
    static class Bracket extends Printer{
        private Printer component;
        Bracket(Printer text){
            this.component = text;
        }

        @Override
        public void print() {
            System.out.print("[ ");
            component.print();
            System.out.print(" ]");
        }
    }
    static class SomeText extends Printer{
        private Printer component;
        SomeText(Printer text){
            this.component = text;
        }

        @Override
        public void print() {
            component.print();
            System.out.println(" + some text");
        }
    }
}
