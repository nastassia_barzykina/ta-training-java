package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

/**
 * Напиши программу, которая считывает из консоли имя текстового файла, далее читает символы из этого файла (используй метод
 * readAllLines(Path) класса Files) и выводит на экран все, за исключением точки, запятой и пробела.
 */
public class ReadFileTask {
    static String src = "c:\\test\\chartest.txt";//                f
    public static void main(String[] args) {
//        String src = "c:\\test\\chartest.txt";
//        try (Scanner scanner = new Scanner(System.in)) {
//            List<String> lines = Files.readAllLines(Paths.get(scanner.nextLine()));
//            for (String el : lines) {
//                System.out.println(el.replaceAll("[, \\.]", ""));
//            }
//        } catch (Exception e) {
//            System.out.println("Something went wrong : " + e);
//        }
        /**
         * with BR:
         */
        System.out.println(getLine());

    }

    public static String getLine() {
        //String line="";
        String res = "";

        try (FileReader in = new FileReader(src);//             FR
             BufferedReader reader = new BufferedReader(in))//  BR
        {
            while (reader.ready()) {
                //line = reader.readLine();
                res += reader.readLine() + " ";
                //System.out.println(line.replaceAll("[ ,//.]", ""));
                //System.out.print(line + " ");
            }
        } catch (IOException e) {
            System.out.println("Exception!" + e);
        }
        return res;
}
}
