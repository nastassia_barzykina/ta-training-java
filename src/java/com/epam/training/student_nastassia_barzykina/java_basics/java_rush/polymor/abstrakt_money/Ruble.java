package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.abstrakt_money;

public class Ruble extends Money {
    public Ruble(double amount) {
        super(amount);
    }

    @Override
    public String getCurrencyName() {
        return "RUB";
    }
}
