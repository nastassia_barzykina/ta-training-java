package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread.synchron.task1706;

public class OurPresident {
    private static OurPresident president;

    static {
        synchronized (OurPresident.class) {
            if (president == null)
                president = new OurPresident();
        }
    }

    private OurPresident() {
    }

    public static OurPresident getOurPresident() {
        return president;
    }
}
