package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.data.class_calendar;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

public class BirthDay {
    static Calendar birthDate = new GregorianCalendar(1980, Calendar.APRIL, 13);

    public static void main(String[] args) {
        System.out.println(getDayOfWeek(birthDate));
    }

    static String getDayOfWeek(Calendar calendar) {
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE", new Locale("ru", "RU"));
        return formatter.format(calendar.getTime());
    }
}
