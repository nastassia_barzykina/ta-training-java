package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.string;

public class StringFormat {
    public static void main(String[] args) {
        System.out.println(format("Amigo", 5000));
    }
    public static String format(String name, int salary){
        String phrase = "Меня зовут %s. Я буду зарабатывать $%d в месяц.";
        return String.format(phrase, name, salary);
    }
}
