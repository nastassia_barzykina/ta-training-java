package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.network;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class URLEx {
    public static void main(String[] args) throws IOException {
        /**
         * Получение странички из интернета:
         * Создает объект URL с путем к странице
         * Получает InputStream у интернет-объекта
         * Читает все байты и возвращает массив байт
         * Преобразуем массив в строку
         * Выводим строку на экран - содержимое HTML
         */
        URL url = new URL("https://javarush.com");// не www!
        InputStream input = url.openStream();
        byte[] buffer = input.readAllBytes();
        String string = new String(buffer);
        System.out.println(string);

    }
}
