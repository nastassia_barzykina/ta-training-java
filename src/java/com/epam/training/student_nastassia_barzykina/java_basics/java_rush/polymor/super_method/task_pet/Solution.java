package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.super_method.task_pet;

public class Solution {
    public static void main(String[] args) {
        Cat cat = new Cat();
        cat.printInfo();
        Dog dog = new Dog();
        dog.printInfo();
    }
}
