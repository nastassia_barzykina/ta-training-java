package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stack_trace;

public class StackTraceMain {
    public static void main(String[] args)
    {
        test();
    }

    public static void test()
    {
        Thread current = Thread.currentThread();
        StackTraceElement[] methods = current.getStackTrace();

        for(var info: methods)
            System.out.println(info);
        System.out.println("More info:");
        for(StackTraceElement info: methods)
        {
            System.out.println(info.getClassName());
            System.out.println(info.getMethodName());

            System.out.println(info.getFileName());
            System.out.println(info.getLineNumber());

            System.out.println(info.getModuleName());
            System.out.println(info.getModuleVersion());
            System.out.println();
        }
    }

    /**
     * Метод Thread.getStackTrace() был вызван методом StackTraceMain.test() в строке 12 файла StackTraceMain.java
     * Метод StackTraceMain.test() был вызван методом StackTraceMain.main() в строке 6 файла StackTraceMain.java
     * Метод StackTraceMain.main() никто не вызывал — это первый метод в цепочке вызовов.
     */
}
