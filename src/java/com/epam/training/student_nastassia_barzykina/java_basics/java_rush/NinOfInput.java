package com.epam.training.student_nastassia_barzykina.java_basics.java_rush;

import java.util.Scanner;

public class NinOfInput {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
//        int MIN = Integer.MAX_VALUE;
//        while (console.hasNextInt())
//        {
//            int x = console.nextInt();
//            if (x < MIN)
//                MIN = x;
//        }
//        System.out.println(MIN);
        System.out.println("MAX:");
        int MAX = Integer.MIN_VALUE;
        while (console.hasNextInt())
        {
            int x = console.nextInt();
            if (x % 2 == 0 && x > MAX)
                MAX = x;
        }
        System.out.println(MAX);

    }
}
