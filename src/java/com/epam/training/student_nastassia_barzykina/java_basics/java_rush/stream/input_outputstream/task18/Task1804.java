package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.task18;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Task1804 {
    public static void main(String[] args) throws Exception{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String fileName = br.readLine();
        FileInputStream inputStream = new FileInputStream(fileName);
        HashMap<Integer, Integer> map = new HashMap<>();

        while (inputStream.available() > 0) {
            int data = inputStream.read();
            Integer count = map.get(data);
            if (count == null){
                map.put(data, 1);
            } else {
                map.put(data, ++count);
            }
        }
        inputStream.close();
        int min = Integer.MAX_VALUE;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (entry.getValue() < min){
                min = entry.getValue();
            }
        }
        for (Map.Entry<Integer, Integer> entry : map.entrySet()){
            if (entry.getValue() == min){
                System.out.print(entry.getKey() + " ");
            }
        }

    }

}
