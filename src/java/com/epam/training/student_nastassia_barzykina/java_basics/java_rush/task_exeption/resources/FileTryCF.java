package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.task_exeption.resources;

import java.io.FileOutputStream;
import java.io.IOException;

public class FileTryCF {
    public static void main(String[] args) throws IOException {
        FileOutputStream output = null;
        String path = "c:\\test\\log.txt";

        try
        {
            output = new FileOutputStream(path);
            output.write("Hello".getBytes());
        }
        finally
        {
            if (output != null)
                output.close();
        }
    }
}
