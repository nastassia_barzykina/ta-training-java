package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.string;

import java.util.StringTokenizer;

public class STokenizer {
    public static void main(String[] args) {
        String str = "Go,od new.s everyone!";
        String packagePath = "java.util.stream";
        StringTokenizer s = new StringTokenizer(str, " ");
        StringTokenizer st = new StringTokenizer(str);
        StringTokenizer st1 = new StringTokenizer(packagePath, ".");
        int tk = st.countTokens();
        int tk2 = st1.countTokens();
        System.out.println("st = " + tk);
        System.out.println("st1 =  " + tk2);
//        разделителем считается каждый символ строки!!! "n", "e"
        while (s.hasMoreTokens()){
            String token = s.nextToken();
            System.out.println(token);
        }
    }
}
