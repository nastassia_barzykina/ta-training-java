package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.files;

import java.io.*;
import java.util.Scanner;

/**
 * Считать с консоли путь к файлу.
 * Вывести в консоли (на экран) содержимое файла.
 * Освободить ресурсы. Закрыть поток чтения с файла и поток ввода с клавиатуры.
 * Требования:
 * •	Программа должна считывать c консоли путь к файлу.
 * •	Программа должна выводить на экран содержимое файла.
 * •	Поток чтения из файла (FileInputStream) должен быть закрыт.
 * •	BufferedReader также должен быть закрыт.
 */
public class ISRPath {
    public static void main(String[] args) throws IOException {
        //Scanner scanner = new Scanner(System.in);
//        FileReader reader = new FileReader(scanner.nextLine());
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream reader = new FileInputStream(bufferedReader.readLine());
        Scanner sc = new Scanner(reader);
        while (sc.hasNextLine()) {
            Object line = sc.nextLine();
            System.out.println(line);
        }
        sc.close();
        reader.close();
        bufferedReader.close();
    }
//    правильное решение:
//StringBuilder builder = new StringBuilder();
//    while (scanner.hasNextLine()) {
//        builder.append(scanner.nextLine()).append("\n");
//    }
//
//        System.out.print(builder.toString());
//
//        scanner.close();
//        reader.close();

}
