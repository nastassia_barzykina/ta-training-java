package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.task_exeption;

import java.util.ArrayList;
import java.util.List;

public class ExceptionsTask {
    public static List<Exception> exceptions = new ArrayList<Exception>();

    public static void main(String[] args) {
        initExceptions();

        for (Exception exception : exceptions) {
            System.out.println(exception);
        }
    }

    private static void initExceptions() {   //the first exception
        try {
            float i = 1 / 0;
        } catch (Exception e) {
            exceptions.add(e);
            exceptions.add(new IllegalArgumentException());
            exceptions.add(new ArrayIndexOutOfBoundsException());
            exceptions.add(new ClassCastException());
            exceptions.add(new NegativeArraySizeException());
            exceptions.add(new RuntimeException());
            exceptions.add(new IndexOutOfBoundsException());
            exceptions.add(new SecurityException());
            exceptions.add(new IllegalAccessException());
            exceptions.add(new CloneNotSupportedException());


        }
//        try {
//            int[] array = {1, 2};
//            for (int k = 0; k <= array.length; k++) {
//                array[k] = 1;
//            }
//            //напишите тут ваш код
//
//        } catch (Exception e) {
//            exceptions.add(e);
//        }
//        try {
//            String s = null;
//            String t = null;
//            if (s.equals(t)) s = t;
//
//        } catch (Exception e){
//            exceptions.add(e);
//        }

    }
}
