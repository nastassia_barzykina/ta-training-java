package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex.task_compare.ex;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class User implements Comparable<User>{ // добавляем возможность сравнивать объекты User
    private String name;
    private Integer age;
    private String email;

    public User(String name, int age, String email) {
        this.name = name;
        this.age = age;
        this.email = email;
    }
    @Override
    public int compareTo(User user) {
        //используем метод compareTo из класса String для сравнения имен
        int result = this.name.compareTo(user.name);

//если имена одинаковые - сравниваем возраст, используя метод compareTo из класса Integer

        if (result == 0) {
            result = this.age.compareTo(user.age);
        }
        return result;
    }
    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", email='" + email + '\'' +
                '}';
    }
    /**
     * Протестируем работу метода compareTo, реализованного в классе User, c помощью метода sort класса Collections
     */
    public static void main(String[] args) {
        User user = new User("Андрей", 19, "andryha@mail.ru");
        User user2 = new User("Олег", 25, "oleg@mail.ru");
        User user3 = new User("Андрей", 24,"opr@google.com");
        User user4 = new User("Игорь", 16, "igor@mail.ru");
        User user5 = new User("Андрей", 44,"stary@google.com");
        List<User> list = new ArrayList<>();

        list.add(user);
        list.add(user2);
        list.add(user3);
        list.add(user4);
        list.add(user5);

        System.out.println("-------до сортировки--------");
        for (User u : list) {
            System.out.println(u);
        }
        System.out.println("-------после сортировки-----");
        Collections.sort(list);
        for (User u : list) {
            System.out.println(u);
        }
    }
}