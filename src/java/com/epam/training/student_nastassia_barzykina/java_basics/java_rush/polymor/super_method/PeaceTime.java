package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.super_method;

class PeaceTime {
    public double getPi(){
        return 3.14;
    }
}
class WarTime extends PeaceTime{
    @Override
    public double getPi() {
        return super.getPi() * 2; // 3.14*2
    }
}
