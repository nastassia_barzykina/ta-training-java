package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.string.sb;

public class SBMethods {
    public static void main(String[] args) {
        char d = ' ';
        System.out.println(d);
        StringBuilder stringBuilder = new StringBuilder("Hello");
        /* append: Добавление к строке
        Преобразование к стандартной строке - toString
         */
        stringBuilder.append("!!!");
        stringBuilder.append(123);
        String s = stringBuilder.toString();
        System.out.println(s);
        /*Чтобы удалить символ в изменяемой строке, вам нужно воспользоваться методом deleteCharAt()
         */
        stringBuilder.deleteCharAt(0);
        System.out.println(stringBuilder);
        /*заменить часть строки на другую
         */
        stringBuilder.replace(0, 4, "Привет");
        System.out.println(stringBuilder);
        // реверс
        stringBuilder.reverse();
        System.out.println(stringBuilder);


    }
}
