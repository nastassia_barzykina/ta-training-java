package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * В методе main добавить в статический объект list 5 нитей.
 * Каждая нить должна быть новым объектом класса Thread, работающим со своим объектом класса SpecialThread.
 */
public class Task1603 {
    public static volatile List<Thread> list = new ArrayList<Thread>(5);

    public static void main(String[] args) {
//        SpecialThread[] st = new SpecialThread[5];
//        for (int i = 0; i < st.length; i++){
//            st[i] = new SpecialThread();
//        }
//        Thread[] tr = new Thread[5];
//        for (int i = 0; i < tr.length; i++){
//            tr[i] = new Thread(st[i]);
//        }
//        for (SpecialThread el : st){
//            Thread e1 = new Thread(el);
//            list.add(e1);
//            e1.start();
//        } - не прошло валидацию. Правильное решение:
        list.add(new Thread(new SpecialThread()));
        list.add(new Thread(new SpecialThread()));
        list.add(new Thread(new SpecialThread()));
        list.add(new Thread(new SpecialThread()));
        list.add(new Thread(new SpecialThread()));

//        SpecialThread s1 = new SpecialThread(); // прошло валидацию
//        SpecialThread s2 = new SpecialThread();
//        SpecialThread s3 = new SpecialThread();
//        SpecialThread s4 = new SpecialThread();
//        SpecialThread s5 = new SpecialThread();
//        Thread t1 = new Thread(s1);
//        Thread t2 = new Thread(s2);
//        Thread t3 = new Thread(s3);
//        Thread t4 = new Thread(s4);
//        Thread t5 = new Thread(s5);
//        list.add(t1);
//        list.add(t2);
//        list.add(t3);
//        list.add(t4);
//        list.add(t5);
//        t1.start();
//        t2.start();
//        t3.start();
//        t4.start();
//        t5.start();

        //напишите тут ваш код
    }

    public static class SpecialThread implements Runnable {
        public void run() {
            System.out.println("it's a run method inside SpecialThread");
        }
    }

}
