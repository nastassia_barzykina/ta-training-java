package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.array_list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;

public class PracticalExamples {
    static class Cat{
        private String name;
        Cat(String name){
            this.name = name;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("Cat{");
            sb.append("name='").append(name).append('\'');
            sb.append('}');
            return sb.toString();
        }
    }
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 1; i <= 20; i++){// добавляем в список четные числа
            if (i % 2 == 0){
                list.add(i);
            }
        }
        for (int i = 0; i < list.size(); i++){
            System.out.println(list.get(i));
        }
        for (int i = 0; i < list.size(); i++){ // удаляем числа, кратные 4
            if (list.get(i) % 4 == 0){
                list.remove(i);
                i--;// уменьшаем счетчик i, чтобы на следующем витке цикла опять попасть на тот же элемент
            }
        }
        System.out.println("после удаления:");
        for (int i = 0; i < list.size(); i++){
            System.out.println(list.get(i));
        }
        System.out.println();
/* Так нельзя, будет ошибка IndexOutOfBoundsException  на 31 строке, т.к. размер меняется динамически.
        int n = list.size();// =5
        list.remove(n - 3);
        list.remove(n - 2);
        list.remove(n - 1);
        Элементы нужно или удалять с конца, или просто в одном и том же месте, а после каждого удаления
        элементы будут сдвигаться на один.*/
//        int n = list.size();// =5
//        list.remove(n - 3);// или n-1
//        list.remove(n - 3);//     n-2
//        list. remove(n - 3);
        // или:
        list.remove(list.size() - 1);
        list.remove(list.size() - 1);
        list.remove(list.size() - 1);
        System.out.println("после удаления:");
        for (int i = 0; i < list.size(); i++){
            System.out.println(list.get(i));
        }
        ArrayList<Cat> cats = new ArrayList<>();
        Cat thomas = new Cat("Томас");
        Cat behemoth = new Cat("Бегемот");
        Cat philipp = new Cat("Филипп Маркович");
        Cat pushok = new Cat("Пушок");

        cats.add(thomas);
        cats.add(behemoth);
        cats.add(philipp);
        cats.add(pushok);
//        for(Cat cat: cats){
//            System.out.println(cat.name);
//        }
/**
 * Удаление объекта листа не работает через for-each.
 */
//        for (Cat cat: cats) {
//
//            if (cat.name.equals("Бегемот")) {
//                cats.remove(cat);
//            }
//        }
        cats.get(1);
        System.out.println(cats.get(1).name.equals("Бегемот"));
        for (int i = 0; i < cats.size(); i++){// работает
            if (cats.get(i).name.equals("Бегемот")) {
                cats.remove(i);
                System.out.println(i);
            }
        }
        System.out.println(cats.toString());
        System.out.println("before iterator");
        /**
         * для удаления элементов во время перебора нужно использовать специальный объект — итератор (класс Iterator).
         *
         * Класс Iterator отвечает за безопасный проход по списку элементов.
         *
         * Он достаточно прост, поскольку имеет всего 3 метода:
         * hasNext() — возвращает true или false в зависимости от того, есть ли в списке следующий элемент, или мы уже
         * дошли до последнего.
         * next() — возвращает следующий элемент списка
         * remove() — удаляет элемент из списка
         * Например, мы хотим проверить, есть ли в нашем списке следующий элемент, и если есть — вывести его в консоль:
         *
          */
          Iterator<Cat> catIterator = cats.iterator();//создаем итератор
          while(catIterator.hasNext()) {//до тех пор, пока в списке есть элементы

             Cat nextCat = catIterator.next();//получаем следующий элемент
             System.out.println(nextCat);//выводим его в консоль
              if (nextCat.name.equals("Филипп Маркович")) {
                  catIterator.remove();//удаляем кота с нужным именем/метод remove() удаляет последний элемент, который был возвращен итератором.
              }
          }
        System.out.println(cats);

        Cat[] catsArray = {thomas, behemoth, philipp, pushok};

        ArrayList<Cat> catsList = new ArrayList<>(Arrays.asList(catsArray));
        System.out.println(catsList);
        Cat[] catsAr = catsList.toArray(new Cat[0]);
        System.out.println("Array:");
        System.out.println(Arrays.toString(catsAr));



//        System.out.println(cats.contains(behemoth));
//        System.out.println();
//        for(Cat cat : cats) {
//            System.out.println(cat.name);
        }


    }

