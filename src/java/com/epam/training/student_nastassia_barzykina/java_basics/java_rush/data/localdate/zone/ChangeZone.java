package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.data.localdate.zone;

import java.time.*;

/**
 * Можно ли, зная время в одном часовом поясе, определить время в другом?
 * Реши эту задачу в методе changeZone. Его параметры:
 *
 * fromTime — известное время;
 * fromZone — временная зона, в которой известно время;
 * toZone — временная зона, в которой нужно определить время.
 * Требования:
 * •	Метод changeZone должен вернуть LocalDateTime во временной зоне toZone.
 */
public class ChangeZone {
    public static void main(String[] args) {
        ZoneId zone1 = ZoneId.of("Zulu");
        ZoneId zone2 = ZoneId.of("Etc/GMT+8");
        System.out.println(ZonedDateTime.now(zone1));
        System.out.println(ZonedDateTime.now(zone2));

        LocalDateTime time = changeZone(LocalDateTime.of(2020, 3, 19, 1, 40), zone1, zone2);
        System.out.println(time);
    }

    static LocalDateTime changeZone(LocalDateTime fromDateTime, ZoneId fromZone, ZoneId toZone) {
//        System.out.println("Method:");
        ZonedDateTime zoneFrom = fromDateTime.atZone(fromZone);
//        System.out.println(zoneFrom);
        ZonedDateTime toOtherZone = zoneFrom.withZoneSameInstant(toZone);//время в другой зоне
//        System.out.println(toOtherZone);
        return toOtherZone.toLocalDateTime();
    }
}
