package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class ExCopyFileBuffArray {
    public static void main(String[] args) throws Exception {
        //Создаем поток-чтения-байт-из-файла
        FileInputStream inputStream = new FileInputStream("C:\\test\\test.txt");
        // Создаем поток-записи-байт-в-файл (файл создается, если не было)
        FileOutputStream outputStream = new FileOutputStream("C:\\test\\result.txt");
        FileOutputStream outputStream1 = new FileOutputStream("C:\\test\\result1.txt");

        byte[] buffer = new byte[1000];
        int tmp = 0;
        while (inputStream.available() > 0) //пока есть еще непрочитанные байты
        {
            // прочитать очередной блок байт в переменную buffer и реальное количество в count
//            Метод read при чтении последнего блока вернет значение равное количеству реально прочитанных байт.
//            Для всех чтений – 1000, а для последнего блока – 328.

//            Поэтому при записи блока мы указываем, что нужно записать не все байты из буфера, а только 328
//            (т.е. значение, хранимое в переменной count).
//            int count =
            inputStream.read(buffer);
//            tmp += count;
//            System.out.println(count + " -- кол-во прочитанных байт");
            File file = new File("C:\\test\\test.txt");
            long fileSize = file.length(); // получение размера файла в байтах
            outputStream.write(buffer, 0, (int) (fileSize / 2)); //записать блок(часть блока) во второй поток
            outputStream1.write(buffer, (int) (fileSize / 2), (int) (fileSize - fileSize / 2)); //записать блок(часть блока) во второй поток
            System.out.println("File size: " + fileSize + " bytes");
        }
//        System.out.println(tmp + "  - общее кол-во байт в исходном файле");

        inputStream.close(); //закрываем оба потока. Они больше не нужны.
        outputStream.close();
        outputStream1.close();
        // размер файла:

    }
}
