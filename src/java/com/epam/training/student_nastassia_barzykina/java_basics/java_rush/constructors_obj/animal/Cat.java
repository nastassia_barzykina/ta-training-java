package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.constructors_obj.animal;

public class Cat extends Animal{
    String tail = "Изначальное значение tail в классе Cat";
    public  static int catsCount = 37;
    Cat(String heart, String brain, String tail){
        super(heart, brain);// отрабатывает до this.heart = heart;
        System.out.println("Конструктор класса Cat начал работу (конструктор Animal уже был выполнен)");
        System.out.println("Текущее значение статической переменной catsCount = " + catsCount);
        System.out.println("Текущее значение tail = " + this.tail);
       // this.heart = heart;
//        this.brain = brain;
        this.tail = tail;
        System.out.println("Текущее значение tail = " + this.tail);
    }

    public static void main(String[] args) {
        Cat cat = new Cat("Сердце", "Мозг", "Хвост");
    }
}
