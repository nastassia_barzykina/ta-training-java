package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.wrapper.cat;

/**
 * Паттерн проектирования «Обёртка» (Wrapper или Decorator)
 * подменяем каждый оригинальный объект на объект-обертку, в который уже передаем ссылку на оригинальный объект.
 * Все вызовы методов у обертки идут к оригинальному объекту, и все работает как часы.
 * Класс BufferedInputStream – классический представитель обертки-буфера. Он – класс-обертка над InputStream.
 * При чтении данных из него, он читает их из оригинального InputStream’а большими порциями в буфер, а потом отдает
 * из буфера потихоньку.
 */
public class Cat {
    private String name;
    Cat(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
