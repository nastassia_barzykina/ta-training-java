package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex.task_compare;

import java.util.ArrayList;
import java.util.Collections;

public class Solution {
    public static ArrayList<Student> students = new ArrayList<>();
    public static void main(String[] args) {
        Collections.addAll(students,
                new Student("Иванов", 22),
                new Student("Петров", 18),
                new Student("Сидоров", 19)
        );
        Collections.sort(students, new AgeComparator());
        for (Student person : students){
            System.out.println(person);
        }
    }
}
