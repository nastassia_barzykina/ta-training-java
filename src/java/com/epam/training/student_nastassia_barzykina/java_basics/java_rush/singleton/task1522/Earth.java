package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.singleton.task1522;

public class Earth implements Planet {
    private static Earth instance;

    private Earth() {

    }

    public static Earth getInstance() {
        if (instance == null) {
            instance = new Earth();
        }
        return instance;
    }
}
