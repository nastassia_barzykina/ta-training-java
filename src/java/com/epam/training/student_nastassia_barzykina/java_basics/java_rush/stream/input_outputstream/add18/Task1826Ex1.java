package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.add18;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
/**
 * Придумать механизм шифровки/дешифровки.
 * <p>
 * Программа запускается с одним из следующих наборов параметров:
 * -e fileName fileOutputName (-e C:\test\cipher.txt C:\test\cipherE.txt)
 * -d fileName fileOutputName (-d C:\test\cipherE.txt C:\test\cipherD.txt)
 * <p>
 * где:
 * fileName - имя файла, который необходимо зашифровать/расшифровать.
 * fileOutputName - имя файла, куда необходимо записать результат шифрования/дешифрования(содержимое файла fileName)
 * -e - ключ указывает, что необходимо зашифровать данные.
 * -d - ключ указывает, что необходимо расшифровать данные.
 * <p>
 * Требования:
 * •	Считывать с консоли ничего не нужно.
 * •	Создай поток для чтения из файла, который приходит вторым параметром ([fileName]).
 * •	Создай поток для записи в файл, который приходит третьим параметром ([fileOutputName]).
 * •	В режиме "-e" программа должна зашифровать [fileName] и записать в [fileOutputName].
 * •	В режиме "-d" программа должна расшифровать [fileName] и записать в [fileOutputName].
 * •	Созданные для файлов потоки должны быть закрыты.
 */

public class Task1826Ex1 {
    public static void main(String[] args) throws IOException {
        FileInputStream fin = new FileInputStream(args[1]);
        FileOutputStream fout = new FileOutputStream(args[2]);

        while (fin.available() > 0) {
            int b = fin.read();
            if (args[0].equals("-e"))  // encryption
                b = b + 1;
            if (args[0].equals("-d"))  // decryption
                b = b - 1;
            fout.write(b);
        }

        fin.close();
        fout.close();
        //
        String mode = args[0];
        if (!(mode.equals("-e") || mode.equals("-d"))) return;
        try (FileInputStream fileInputStream = new FileInputStream(args[1]);
             FileOutputStream fileOutputStream = new FileOutputStream(args[2])) {

            if (mode.equals("-e")) {
                while (fileInputStream.available() > 0) {
                    fileOutputStream.write(fileInputStream.read() + 32);
                }
            } else {
                while (fileInputStream.available() > 0) {
                    fileOutputStream.write(fileInputStream.read() - 32);
                }
            }
        }
    }
}
