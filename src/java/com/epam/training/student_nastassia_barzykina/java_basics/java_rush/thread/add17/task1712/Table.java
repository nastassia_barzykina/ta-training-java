package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.thread.add17.task1712;

public class Table {
    private static byte tableNumber;
    private byte number = ++tableNumber;

    public Order getOrder() {
        return new Order(number);
    }
}
