package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.linkedlist;


public class StringLinkedList {
    private Node first = new Node();
    private Node last = new Node();

    public void add(String value) {
        if (first.next == null) {
            Node node = new Node();
            node.value = value;
            first.next = node;
        }
        if (last.prev == null) {
            last.prev = first.next;
            return;
        }

        Node node = new Node();
        node.value = value;

        Node lastNode = last.prev;
        lastNode.next = node;
        node.prev = lastNode;
        last.prev = node;
    }

    /**
     * нужно реализовать метод get(int), который вернет строку под индексом, переданным в метод. Если строки с таким
     * индексом нет, нужно вернуть null.
     * Помни, что first и last не имеют значений, а ссылаются только на первый и последний элемент соответственно.
     *
     * Требования:
     * •	Метод get(int index) должен возвращать элемент, который находится под индексом index в списке.
     * @param index
     * @return
     */
    public String get(int index) {// возвращает значение элемента с индексом index
//        StringLinkedList.Node elem = first.next;
//        int count = 0;
//        while (elem != last.next && count < index) {
//        //while (elem != last.next && parseInt(elem.value) != index +1) {// не по условию
//                elem = elem.next;
//                count++;
//            }
//        if (elem == last.next){
//            return null;
//        } else {
//            return elem.value;
//        }
        int currentIndex = 0;
        Node currentElement = first.next;
        while ((currentElement) != null) {
            if (currentIndex == index) {
                return currentElement.value;
            }
            currentElement = currentElement.next;
            currentIndex++;
        }
        return null;
    }

    public static class Node {
        private Node prev;
        private String value;
        private Node next;
    }
    public void printAll() {
        StringLinkedList.Node currentElement = first.next;
        while ((currentElement) != null) {
            System.out.println(currentElement.value);
            currentElement = currentElement.next;
        }
    }
}
