package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.abstrakt_money;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Person ivan = new Person("Иван");
        for (Money money : ivan.getAllMoney()) {
            System.out.println(ivan.name + " имеет заначку в размере " + money.getAmount() + " " + money.getCurrencyName());
        }
    }

    static class Person {
        public String name;

        Person(String name) {
            this.name = name;
            this.allMoney = new ArrayList<Money>();
            allMoney.add(new Hryvnia(5));
            allMoney.add(new Ruble(15));
            allMoney.add(new USD(55));
            //напишите тут ваш код
        }

        private List<Money> allMoney;

        public List<Money> getAllMoney() {
            return allMoney;
        }
    }
}
