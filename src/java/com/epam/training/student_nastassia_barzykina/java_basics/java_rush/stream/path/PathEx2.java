package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.path;

import java.nio.file.Path;

public class PathEx2 {
    public static void main(String[] args) {
        //В пути вместо имени директории можно писать «..», и это будет означать вернуться на одну директорию назад.
        // Нормализация устраняет эти вещи
        String str = "c:\\windows\\..\\projects\\note.txt";
        Path pathE = Path.of(str).normalize();
        System.out.println(pathE);//c:\projects\note.txt
        // Отн путь
        String stringE = "for_path\\file_test.txt";
        boolean abs = Path.of(stringE).isAbsolute();
        System.out.println(abs);//false
        Path path11 = Path.of(stringE).toAbsolutePath();//C:\\test\\project\\for_path\\file_test.txt;
        System.out.println(path11);//C:\Users\rando\My_projects\ta-traning-java\for_path\file_test.txt, выстраивает
        // путь относительно рабочей директории
        /**
         * Метод relativize() позволяет вычислить «разницу путей»: один путь относительно другого
         * относительный путь из 2 абсолютных путей
         */
        Path path1 = Path.of("c:\\windows\\projects\\note.txt");
        Path path2 = Path.of("c:\\windows\\");
        Path result = path2.relativize(path1);
        System.out.println(result);//"projects\\note.txt"

        Path path12 = Path.of("\\windows\\projects\\note.txt");
        Path path22 = Path.of("\\windows\\projects\\note.txt");
        System.out.println("равны?");
        System.out.println(path22.equals(path12));
        Path result2 = path12.relativize(path22);
        System.out.println("1: " + result2);//"..\\.."
        result2 = path22.relativize(path12);//Если пути одинаковые, пусто...
        System.out.println("2: " + result2);//projects\note.txt.

        Path path13 = Path.of("c:\\aaa\\bbb\\1.txt");
        Path path23 = Path.of("d:\\zzz\\y.jpg");
        //Path result3 = path13.relativize(path23);
        //System.out.println(result3);
//        Ошибка IllegalArgumentException:
//        два пути имеют разный "корень" (разные диски)
        /**
         * Метод resolve() выполняет операцию, обратную relativize(): из абсолютного и относительного пути он строит
         * новый абсолютный путь.
         * Как в задаче: есть относительный путь (директория), и список файлов. Добавляя к директории имя файла,
         * выстраиваем новый абсолютный путь для копирования файлов в директорию
         */
        Path dir = Path.of("c:\\windows\\project");
        Path path14 = Path.of("project\\note.txt");
        System.out.println("Полный путь:");
        System.out.println(dir.resolve(path14.getFileName()));// c:\windows\project\note.txt
        System.out.println(dir.resolve(path14) + " - так не правильно!");// c:\windows\project\note.txt
        Path path24 = Path.of("c:\\windows\\");
        Path result4 = path14.resolve(path24);  //"c:\\windows"
        System.out.println("4: " + result4);

        Path path15 = Path.of("project\\note.txt");
        Path path25 = Path.of("c:\\windows\\");
        Path result5 = path25.resolve(path15); //"c:\\windows\\projects\\note.txt", 25+15
        System.out.println("5: " + result5);

    }
}
