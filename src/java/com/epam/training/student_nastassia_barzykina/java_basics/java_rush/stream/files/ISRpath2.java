package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.files;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * Считать с консоли путь к файлу.
 * Вывести в консоли (на экран) содержимое файла.
 * Освободить ресурсы. Закрыть поток чтения с файла и поток ввода с клавиатуры.
 * Требования:
 * •	Программа должна считывать c консоли путь к файлу.
 * •	Программа должна выводить на экран содержимое файла.
 * •	Поток чтения из файла (FileInputStream) должен быть закрыт.
 * •	BufferedReader также должен быть закрыт.
 */

public class ISRpath2 {
    public static void main(String[] args) throws IOException {
//        Scanner sc = new Scanner(System.in);//C:\test\project\for_path\file_test.txt
//        //String path = sc.nextLine();
//        //File file = new File(path);
//        FileInputStream fis = new FileInputStream(sc.nextLine());
//        InputStreamReader isr = new InputStreamReader(fis);
//        BufferedReader buff = new BufferedReader(isr);
//        while (buff.ready()) {
//            String line = buff.readLine();
//            System.out.println(line);
//
//        }
//        //buff.lines().forEach(x -> System.out.println(x));// через лямбду - вместо while
//        buff.close();
//        isr.close();
//        fis.close();
//    правильное решение:
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String sourceFileName = reader.readLine();
        FileInputStream fileInputStream = new FileInputStream(sourceFileName);

        Scanner scanner = new Scanner(fileInputStream);
        StringBuilder builder = new StringBuilder();
        while (scanner.hasNextLine()) {
            builder.append(scanner.nextLine()).append("\n");
        }

        System.out.print(builder.toString());

        scanner.close();
        reader.close();
    }
}
