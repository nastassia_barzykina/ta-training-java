package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor;

public class Test {
    public static void main(String[] args) {
        print(5);
        print(Integer.valueOf(10));
        //print(new Integer(1)); - не рекомендуется
        print("fff");
        Tiger tiger = new Tiger();// расширение типа
        Cat cat = new Tiger();//методы Cat, Animal, Object
        Animal animal = new Tiger();
        Animal animal1 = cat;// Ссылку на объект всегда можно сохранить в переменную любого его базового типа.
        Object o = cat;//только методы класса Object

        Object obj = new Tiger();// сужение типа
        if (obj instanceof Cat){
            Cat cat1 = (Cat) obj;
            cat1.doCatActions();
        }
        Animal an = (Animal) obj;
        Cat tom = (Cat) an;


    }
    public static void print(int a){
        System.out.println(a);
    }
    public static void print(Integer a){
        System.out.println(a + " - Integer");
    }

    public static void print(String s){
        System.out.println(s);
    }
    static class Animal {
        public void doAnimalActions(){}
    }
    static class  Cat extends Animal {
        public void doCatActions() {
        }
    }
    static class Tiger extends  Cat {
        public void doTigerActions() {
        }
    }
}
