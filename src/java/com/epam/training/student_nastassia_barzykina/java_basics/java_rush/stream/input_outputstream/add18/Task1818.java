package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.add18;

import java.io.*;

/**
 * Считать с консоли 3 имени файла.
 * Записать в первый файл содержимого второго файла, а потом дописать в первый файл содержимое третьего файла.
 * Закрыть потоки.
 *
 * Требования:
 * •	Программа должна три раза считать имена файлов с консоли.
 * •	Для первого файла создай поток для записи. Для двух других - потоки для чтения.
 * •	Содержимое второго файла нужно переписать в первый файл.
 * •	Содержимое третьего файла нужно дописать в первый файл (в который уже записан второй файл).
 * •	Созданные для файлов потоки должны быть закрыты.
 */
public class Task1818 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String file1 = /*br.readLine();*/"C:\\test\\testWrite.txt";//
        String file2 = /*br.readLine();*/"C:\\test\\file2.txt";//
        String file3 = /*br.readLine();*/"C:\\test\\file3.txt";//
        br.close();
        FileOutputStream fos = new FileOutputStream(file1, true);
        FileInputStream fis1 = new FileInputStream(file2);
        FileInputStream fis2 = new FileInputStream(file3);
        byte[] buffer1 = new byte[fis1.available()];
        byte[] buffer2 = new byte[fis2.available()];
        while (fis1.available() > 0){
            fis1.read(buffer1);
            fos.write(buffer1);
        }
        fis1.close();

        while (fis2.available() > 0){
            fis2.read(buffer2);
            fos.write(buffer2);
        }
        fis2.close();
        fos.close();
    }
}
