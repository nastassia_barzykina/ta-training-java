package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.files;

import java.io.*;


/**
 * Прочесть с консоли имя файла.
 * Считывать строки с консоли, пока пользователь не введет строку "exit".
 * Записать абсолютно все введенные в п.2 строки в файл: каждую строчку — с новой строки.
 * Требования:
 * •	Программа должна считывать c консоли имя файла.
 * •	Создай и используй объект типа BufferedWriter.
 * •	Программа не должна ничего считывать из файловой системы.
 * •	Программа должна считывать строки с консоли, пока пользователь не введет строку "exit".
 * •	Программа должна записать все введенные строки (включая "exit", но не включая имя файла) в файл:
 * каждую строчку — с новой строки.
 * •	Метод main должен закрывать объект типа BufferedWriter после использования.
 * •	Метод main не должен выводить данные на экран.
 */
public class BW {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fName = reader.readLine();
        BufferedWriter writer = new BufferedWriter(new FileWriter(fName));
        while (true) {
            String forWrite = reader.readLine();
            if (forWrite.equals("exit")) {
                writer.write(forWrite + '\n');
                break;
            }
            writer.write(forWrite + '\n');

        }
        writer.close();
        reader.close();
        /**
         * Правильное решение (через StringBuilder):
         * StringBuilder builder = new StringBuilder();
         *
         *         String s = "";
         *         while (!s.equals("exit")) {
         *             s = reader.readLine();
         *             builder.append(s).append("\n");
         *         }
         *
         *         writer.write(builder.toString());
         *         writer.close();
         */
    }
}
