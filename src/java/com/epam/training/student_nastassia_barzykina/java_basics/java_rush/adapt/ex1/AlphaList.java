package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.adapt.ex1;

/**
 * AlphaList – это один из интерфейсов, для взаимодействия кода фреймворка и кода, который будет использовать этот фреймворк.
 */
public interface AlphaList {
    void add(int value);
    void insert(int index, int value);
    int get(int index);
    void set(int index, int value);
    int count();
    void remove(int index);
}
