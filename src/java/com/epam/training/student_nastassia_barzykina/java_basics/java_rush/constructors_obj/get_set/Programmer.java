package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.constructors_obj.get_set;

/**
 * зарплату можно только повышать. Поэтому тебе нужно добавить проверку в сеттер: если значение аргумента больше
 * текущего значения, то поле salary изменяем, иначе — игнорируем.
 */
public class Programmer {
    private int salary = 1000;

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary > this.salary ? salary : this.salary;
    }
}
