package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.enums;

/**
 * из enum Month сделать class, а также добавить и реализовать методы ordinal() и values().
 *
 * Требования:
 * •	В отдельном файле должен быть класс Month.
 * •	В классе Month должен быть только один приватный конструктор с одним параметром типа int.
 * •	В классе Month должно быть 12 констант типа Month.
 * •	В классе Month метод values() должен возвращать массив со всеми константами.
 * •	В классе Month метод ordinal() должен возвращать порядковый номер элемента(константы).
 */
public class MonthClass {
    public static final MonthClass JANUARY = new MonthClass(1);
    public static final MonthClass FEBRUARY = new MonthClass(2);
    public static final MonthClass MARCH = new MonthClass(3);
    public static final MonthClass APRIL = new MonthClass(4);
    public static final MonthClass MAY = new MonthClass(5);
    public static final MonthClass JUNE = new MonthClass(6);
    public static final MonthClass JULY = new MonthClass(7);
    public static final MonthClass AUGUST = new MonthClass(8);
    public static final MonthClass SEPTEMBER = new MonthClass(9);
    public static final MonthClass OCTOBER = new MonthClass(10);
    public static final MonthClass NOVEMBER = new MonthClass(11);
    public static final MonthClass DECEMBER = new MonthClass(12);
    private static final MonthClass[] array ={JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY,
            AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER};

    private final int value;//Переменная со значением номера конкретного объекта Day

    private MonthClass(int value){
        this.value = value;
    }
    public int ordinal(){
        return this.value;
    }
    public static MonthClass[] values(){
        return array;
    }

    public static void main(String[] args) {
        System.out.println(MonthClass.JANUARY.ordinal());
        MonthClass[] months = MonthClass.values();
        System.out.println(months[2].value);

    }

}
