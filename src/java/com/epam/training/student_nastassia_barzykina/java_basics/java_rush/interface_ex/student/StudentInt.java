package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex.student;

public interface StudentInt {
    public String getName();
}
interface Test{
    default void test(){
        System.out.println("Test");
    }
}
class StudentImpl implements StudentInt, Test{
    private String name;
    public StudentImpl (String name){
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }
    private void setName(String name){
        this.name = name;
    }

    public static void main(String[] args) {
        StudentInt studentInt = new StudentImpl("Ali");
        System.out.println(studentInt.getName());
//        Test test = new Test(); создавать объекты интерфейса нельзя! Только через анонимный класс
    }

}
