package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.poly_abstract;

public interface Flyable {
    /*private*/ String species = new String();// нельзя сделать private
    int age = 10;
    public void fly();
}
