package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.array_list;

import java.util.ArrayList;

/**
 * исправить сигнатуру метода printStudentNames(ArrayList), чтобы он принимал список объектов только типа Student
 * (используй дженерик)
 */
public class StudentMain {
    public static void main(String[] args) {
        var students = new ArrayList<Student>();
        students.add(new Student("Акакий"));
        students.add(new Student("Любослав"));

        printStudentNames(students);
        var arrayList = new ArrayList<>();
        arrayList.add(15);
        arrayList.add("Hello");
        arrayList.add(154);
        arrayList.add("string");

        printAnything(arrayList);
        //Список списков:
// список приветствий
        ArrayList<String> listHello = new ArrayList<String>();
        listHello.add("Привет");
        listHello.add("Hi");

// список прощаний
        ArrayList<String> listBye = new ArrayList<String>();
        listBye.add("Пока");
        listBye.add("Good Bye");


// список списков
        ArrayList<ArrayList<String>> lists = new ArrayList<ArrayList<String>>();
        lists.add(listHello);
        lists.add(listBye);

    }

    /**
     * ArrayList<Student> - на вход только тип Student
     * ArrayList - объекты любого типа
     * @param students
     */
    public static void printStudentNames(ArrayList<Student> students) {
        for (int i = 0; i < students.size(); i++) {
            System.out.println(students.get(i).getName());
        }
    }
    public static void printAnything(ArrayList arrayList) {// объекты любого типа
        for (int i = 0; i < arrayList.size(); i++) {
            System.out.println(arrayList.get(i));
        }
    }


}
