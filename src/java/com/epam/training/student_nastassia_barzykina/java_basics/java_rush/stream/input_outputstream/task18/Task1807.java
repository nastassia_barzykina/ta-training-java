package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.task18;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * С консоли считать имя файла.
 * Посчитать в файле количество символов ',', количество вывести на консоль.
 * Закрыть потоки.
 *
 * Подсказка:
 * нужно сравнивать с ascii-кодом символа ','(44).
 */
public class Task1807 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String nameFile = br.readLine();
        FileInputStream inputStream = new FileInputStream(nameFile);
        int count = 0;
        while (inputStream.available() > 0){
            int data = inputStream.read();
            if (data == 44){
                count++;
            }
        }
        inputStream.close();
        System.out.println(count);
        // или:
//        int commaCount = 0;
//        try (FileInputStream fileInputStream = new FileInputStream(nameFile)) {
//            while (fileInputStream.available() > 0) {
//                if (fileInputStream.read() == 44) commaCount++;
//            }
//        }
//        System.out.println(commaCount);
    }
}
