package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.array_list;

import java.util.ArrayList;
import java.util.Arrays;

public class ProgrammingLanguages {
    public static ArrayList<String> programmingLanguages = new ArrayList<>(Arrays.asList("C", "Pascal", "C++", "Python", "JavaScript", "Ruby", "Java", "Pascal"));

    public static void main(String[] args) {
        System.out.println(Arrays.asList(programmingLanguages));
        //programmingLanguages.remove(programmingLanguages.indexOf("Pascal"));
        //programmingLanguages.remove("Pascal");// правильное решение:
        for (int i = 0; i < programmingLanguages.size(); i++) {
            if (programmingLanguages.get(i).equals("Pascal")) {
                programmingLanguages.remove(i);
                //break;// если только первый элемент
            }
        }
        System.out.println(Arrays.asList(programmingLanguages));
    }
}
