package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.network;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;

public class URLConnectionEx {
    public static void main(String[] args) throws IOException {
        URL url = new URL("https://javarush.ru/api/1.0/rest/projects");
        URLConnection connection = url.openConnection();
/*
// получили поток для отправки данных
        OutputStream output = connection.getOutputStream();
        output.write(1); // отправляем данные */

// получили поток для чтения данных
//        InputStream input = connection.getInputStream();
//        int data = input.read();// читаем данные
//        System.out.println(data);
//        IS и OS нужно обернуть:*/
//        try (OutputStream output = connection.getOutputStream();
//             PrintStream sender = new PrintStream(output)){
//            sender.println("Hi");
//        }
        try (InputStream input = connection.getInputStream();
             BufferedReader buffer = new BufferedReader(new InputStreamReader(input))){
            while (buffer.ready()){
                System.out.println(buffer.readLine());
            }
        }
    }
}
