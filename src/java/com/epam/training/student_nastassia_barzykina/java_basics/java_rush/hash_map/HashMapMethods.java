package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.hash_map;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class HashMapMethods {
    public static void main(String[] args) {
        HashMap<String, Integer> map = new HashMap<>();
        map.put("Сергей", 21);
        map.put("Анна", 25);
        map.put("Влад", 20);
        map.put("Игорь", null);
        map.put("Руслан", 35);
        //Set<String> keys = map.keySet();
        for (String key : map.keySet()){
            Integer value = map.get(key);
            System.out.println(key + " --> " + value);
        }
        //System.out.println(map);
        map.put("Игорь", 29);
        System.out.println(map);
        for (int i : map.values()){
            System.out.print(i + " ");
        }
        System.out.println();
        Set<Map.Entry<String,Integer>> entries = map.entrySet();
        for (var elem : entries){// вместо Map.Entry<String, Integer>
            String key = elem.getKey();
            Integer value = elem.getValue();
            System.out.println(key + "==>" + value);
        }

    }
}
