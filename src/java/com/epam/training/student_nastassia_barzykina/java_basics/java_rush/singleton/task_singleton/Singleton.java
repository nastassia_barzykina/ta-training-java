package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.singleton.task_singleton;

public class Singleton {
    private static Singleton instance;
    //private static Singleton instance = new Singleton();
    private int i;
//    private Singleton(){
//
//    }
    private Singleton(int i){
        this.i = i;
        System.out.println(i);
    }

//    public static Singleton getInstance() {
//        if (instance == null){
//            instance = new Singleton();
//        }
//        return instance;
//    }
    public static Singleton getInstance(int i) {
        if (instance == null){
            instance = new Singleton(i);
        }
        return instance;
    }
//    public static Singleton getInstance(){
//        return instance;
//    }
}
