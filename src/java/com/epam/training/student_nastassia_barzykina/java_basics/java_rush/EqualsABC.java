package com.epam.training.student_nastassia_barzykina.java_basics.java_rush;

import java.util.Scanner;

public class EqualsABC {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();
        boolean pAB = (a == b);
        boolean pBC = (b == c);
        boolean pCA = (c == a);
        boolean all = (pAB && pBC && pCA);
        if (all){
            System.out.println(a + " " + b + " " + c);
        } else if (pAB) {
            System.out.println(a + " " + b);

        } else if (pBC) {
            System.out.println(b + " " + c);
        } else if (pCA) {
            System.out.println(c + " " + a);
        }

       // Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        int y = sc.nextInt();
        boolean q1 = (x > 0 && y > 0);
        boolean q2 = (x < 0 && y > 0);
        boolean q3 = (x < 0 && y < 0);
        //boolean q4 = (x > 0 && y < 0);
        if (q1){
            System.out.println("1");
        } else if (q2) {
            System.out.println("2");
        } else if (q3) {
            System.out.println("3");
        } else {
            System.out.println("4");
        }
    }
}
