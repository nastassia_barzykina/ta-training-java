package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.network;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

public class ImageEx {
    public static void main(String[] args) throws IOException {
        String image = "https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png";
       URL url = new URL(image);
        InputStream input = url.openStream();

        Path path = Path.of("C:\\Users\\rando\\My_projects\\for_test\\GoogleLogo.png");
        Files.copy(input, path);
// С помощью трех первых строк мы получаем поток данных от интернет-ресурса — от картинки.
//
//В четвертой строке мы создаем имя файла, в который будем сохранять картинку. Имя может быть любым, однако расширение файла
// должно совпадать с расширением картинки в интернете. Так локальные программы просмотра картинок будут правильно ее открывать.
//
//Ну и наконец, последняя строка — это один из методов copy класса Files. У класса Files их несколько. Этот метод, который
// мы использовали, принимает в качестве первого параметра источник данных — байтовый поток (InputStream), а в качестве
// второго параметра — имя файла, куда нужно записывать данные


    }

}
