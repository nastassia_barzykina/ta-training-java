package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.enums;

import java.util.List;

public class SeasonMain {
    public static void main(String[] args) {
        System.out.println(Season.WINTER.toString);
        System.out.println(Season.SPRING);
        System.out.println(Season.SUMMER);
        System.out.println(Season.AUTUMN);
        Days[] days = Days.values();
        for (Days day : days){
            System.out.print(day + "  ");
        }
        System.out.println();
        System.out.println(days[2]);
        System.out.println(Day.WEDNESDAY.ordinal());
        System.out.println(Days.SATURDAY.toString());
        Days days1 = Days.values()[4];
        System.out.println(days1);
        Season season = Season.SPRING;
        int index = season.ordinal();
        System.out.println(index);
        System.out.println(Days.asList());
        List<Days> daysList = Days.asList();// переменную можно использовать в коде
        System.out.println(daysList);
    }
}
