package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.files;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

/**
 * Напиши программу, которая будет считывать с клавиатуры путь к директории, получать список файлов и директорий в
 * заданной директории и выводить в консоли информацию о них в виде:
 * "<путь к файлу> - это файл", если это файл,
 * "<путь к директории> - это директория", если это директория.
 * Треугольные скобки и кавычки выводить не нужно.
 * Используй соответствующие методы класса Files: newDirectoryStream(Path), isRegularFile(Path) и isDirectory(Path).
 * <p>
 * Ввод:
 * C:\javarush\
 * <p>
 * Вывод:
 * C:\javarush\test.txt - это файл
 * C:\javarush\tasks - это директория
 */
public class Directory {
    private static final String THIS_IS_FILE = " - это файл";
    private static final String THIS_IS_DIR = " - это директория";

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        Path directory = Path.of(scanner.nextLine());
        try (DirectoryStream<Path> files = Files.newDirectoryStream(directory)) {
            for (Path paths : files) {
                if (Files.isRegularFile(paths)) {
                    System.out.println(paths + THIS_IS_FILE);
                } else if (Files.isDirectory(paths)) {
                    System.out.println(paths + THIS_IS_DIR);
//                    } else {// - не нужен, из-за этого не проходило проверку.
//                        break;
                }
            }
        } catch (Exception e) {
            //
        }
    }
}

