package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.add18;

import java.io.*;

/**
 * Читайте с консоли имена файлов.
 * Если файла не существует (передано неправильное имя файла), то перехватить исключение FileNotFoundException,
 * вывести в консоль переданное неправильное имя файла и завершить работу программы.
 * Закрыть потоки.
 * Не используй System.exit();
 *
 * Требования:
 * •	Программа должна считывать имена файлов с консоли.
 * •	Для каждого файла нужно создавать поток для чтения.
 * •	Если файл не существует, программа должна перехватывать исключение FileNotFoundException.
 * •	После перехвата исключения, программа должна вывести имя файла в консоль и завершить работу.
 * •	Потоки для чтения из файла должны быть закрыты.
 */
public class Task1824 {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = null;
        while (true){
            try {
                s = reader.readLine();
                FileReader fr = new FileReader(s);
                fr.close();
            } catch (FileNotFoundException e) {
                System.out.println(s);
                return;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

}
