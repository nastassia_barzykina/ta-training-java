package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.string;

import java.util.Scanner;

/**
 * у нас есть url типа такого: «https://domen.com/about/reviews», и мы хотим заменить имя домена на javarush.com.
 * Домены в урлах могут быть разными, но мы знаем, что:
 *
 * Перед именем домена идут два слеша //
 * После имени домена идет одинарный слеш /
 */
public class ReplaceDomain {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String path = sc.nextLine();
        int index = path.indexOf("//");// -Получаем индекс первого вхождения строки //
        int index2 = path.indexOf("/", index + 2);//Получаем индекс первого вхождения строки /, но ищем только после символов //.
        String first = path.substring(0, index + 2); // Получаем строку от начала и заканчивая символами //
        String second = path.substring(index2);
        String result = first + "javarush.com" + second;
        System.out.println(result);

    }
}
