package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods.statics.task1530;

public abstract class DrinkMaker {
    abstract void getRightCup();

    abstract void putIngredient();

    abstract void pour();
    void makeDrink(){
        getRightCup();
        putIngredient();
        pour();
    }

}
