package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.interface_ex.time_and_task;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public interface Runnable {
    void run();
}
class Timer implements Runnable{

    @Override
    public void run() {
        System.out.println(LocalTime.now());
    }
}
class Calendar implements Runnable{

    @Override
    public void run() {
        var date = LocalDate.now();
        System.out.println("Today is " + date.getDayOfWeek());
    }
}
class Main{
    public static void main(String[] args) throws InterruptedException {
        Timer timer = new Timer();
        timer.run();//Будет вызван метод run() класса Timer
        TimeUnit.SECONDS.sleep(5);

        Runnable timer2 = new Timer();
        timer2.run();//Будет вызван метод run() класса Timer

        Runnable timer3 = new Calendar();
        timer3.run();//Будет вызван метод run() класса Calendar

        ArrayList<Runnable> list = new ArrayList<>();
        list.add(new Timer());
        list.add(new Timer());
        list.add(new Calendar());

        for (Runnable el : list)
            el.run();
    }
}
