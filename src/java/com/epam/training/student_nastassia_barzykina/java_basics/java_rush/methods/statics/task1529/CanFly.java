package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods.statics.task1529;

public interface CanFly {
    void fly();
}
