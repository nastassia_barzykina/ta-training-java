package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream;

import java.io.FileInputStream;
import java.io.FileOutputStream;

public class ExCopyFile {
    /**
     * Копируем файл на диске
     */
    public static void main(String[] args) throws Exception{
        //Создаем поток-чтения-байт-из-файла
        FileInputStream inputStream = new FileInputStream("C:\\test\\chartest.txt");
        // Создаем поток-записи-байт-в-файл (если файла нет - создается)
        FileOutputStream outputStream = new FileOutputStream("C:\\test\\result1.txt");

        while (inputStream.available() > 0) //пока есть еще непрочитанные байты
        {
            int data = inputStream.read(); // прочитать очередной байт в переменную data
            outputStream.write(data); // и записать его во второй поток
        }

        inputStream.close(); //закрываем оба потока. Они больше не нужны.
        outputStream.close();

    }
}
