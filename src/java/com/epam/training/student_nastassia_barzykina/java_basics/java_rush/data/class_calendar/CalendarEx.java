package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.data.class_calendar;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class CalendarEx {
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance(); // текущая дата
        Calendar myBirt = new GregorianCalendar(1980, 3, 13);// произвольная дата
        Date date = calendar.getTime();// преобразование в тип Date
        Date dateOfBirt = myBirt.getTime();
        System.out.println(date + " - now");
        System.out.println(myBirt.getTime() + " - my birthday");
    }
}
