package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.hash_set;

import java.util.HashSet;

import static java.util.Arrays.asList;

/**
 * метод checkWords(String), который должен проверять наличие переданного слова в множестве words.
 * Если слово есть, то выводим в консоль:
 * Слово [переданное слово] есть в множестве
 * Если нет:
 * Слова [переданное слово] нет в множестве
 */
public class CheckWords {
    public static HashSet<String> words = new HashSet<>(asList("Если бы меня попросили выбрать язык на замену Java я бы не выбирал".split(" ")));

    /**
     * "Если бы меня попросили выбрать язык на замену Java я бы не выбирал".split(" "): Эта часть кода разделяет строку
     * на слова, используя пробел как разделитель. Результатом является массив строк, где каждый элемент массива представляет
     * собой одно слово из исходной строки.
     *
     * asList(...): Это статический метод из класса Arrays, который преобразует массив в список. В данном случае, он преобразует
     * массив строк (слов) в список строк.
     *
     * new HashSet<>(...): Это создание экземпляра класса HashSet и инициализация его содержимым. HashSet - это коллекция,
     * которая хранит только уникальные элементы. При создании нового объекта HashSet, вы передаете ему список, который затем
     * используется для заполнения нового множества.
     *
     * public static HashSet<String> words = ...: Это объявление и инициализация статической переменной words. Теперь у вас
     * есть статическое множество (HashSet) с уникальными словами из исходной строки.
     *
     * Таким образом, в результате выполнения этой строки кода у вас есть статическое множество (HashSet) с уникальными словами,
     * выделенными из исходной строки.
     * @param word
     */
    public static void checkWords(String word) {
        if (words.contains(word)){
            System.out.println(String.format("Слово %s есть в множестве", word));
        } else if (!(words.contains(word))){
            System.out.println(String.format("Слова %s нет в множестве", word));
        }
        //напишите тут ваш код
    }

    public static void main(String[] args) {
        checkWords("JavaScript");
        checkWords("Java");
        checkWords("Если");
    }
    ;
}
