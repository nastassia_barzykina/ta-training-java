package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.task_exeption.resources;

import java.util.Scanner;

public class TaskTryCF {
    public static void main(String[] args) {
        Scanner scanner = null;
        try {
            scanner = new Scanner(System.in);
            String line = scanner.nextLine();
            System.out.println(line.toUpperCase());
        } catch (Exception e) {
            System.out.println("Something went wrong : " + e);
        }
        finally {
            if (scanner != null)
                scanner.close();
        }
    }
}
