package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.add18;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class SolutionEx {
    private static final String KEY = "key";

    public static void main(String[] args) throws IOException {
        if ("-e".equals(args[0])) {
            try {
                FileInputStream fis = new FileInputStream(args[1]);
                FileOutputStream fos = new FileOutputStream(args[2]);
                BufferedInputStream bufR = new BufferedInputStream(fis);
                BufferedOutputStream bufO = new BufferedOutputStream(fos);
                while (bufR.available() > 0) {
                    byte[] buf = new byte[bufR.available()];
                    bufR.read(buf);
                    bufO.write(encrypt(buf, KEY));
                }
                bufO.close();
                bufR.close();
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }

        }
        if ("-d".equals(args[0])) {
            try {
                FileInputStream fis = new FileInputStream(args[1]);
                FileOutputStream fos = new FileOutputStream(args[2]);
                BufferedInputStream bufR = new BufferedInputStream(fis);
                BufferedOutputStream bufO = new BufferedOutputStream(fos);
                while (bufR.available() > 0) {
                    byte[] buf = new byte[bufR.available()];
                    int n = bufR.read(buf);
                    bufO.write(decrypt(buf, KEY));
                }
                bufO.close();
                bufR.close();
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static byte[] encrypt(byte[] textBytes, String key) {
        //byte[] textByte
        // s = text.getBytes(StandardCharsets.UTF_8);
        byte[] keyBytes = key.getBytes(StandardCharsets.UTF_8);
        byte[] result = new byte[textBytes.length];
        for (int i = 0; i < textBytes.length; i++) {
            result[i] = (byte) (textBytes[i] ^ keyBytes[i % keyBytes.length]);
        }
        return result;
    }

    public static byte[] decrypt(byte[] encryptedBytes, String key) {
        byte[] result = new byte[encryptedBytes.length];
        byte[] keyBytes = key.getBytes(StandardCharsets.UTF_8);
        for (int i = 0; i < encryptedBytes.length; i++) {
            result[i] = (byte) (encryptedBytes[i] ^ keyBytes[i % keyBytes.length]);
        }
        return  result;
    }
}
