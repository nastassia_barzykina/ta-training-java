package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream_collect.ex_pets;

public class Snake extends Animal{
    public Snake(String name, Color color, int age) {
        super(name, color, age);
    }
}
