package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

public class ConsoleRead {
    public static void main(String[] args) throws IOException {
        InputStream stream = System.in;
//        через Scanner:
        Scanner console = new Scanner(stream);
        String line = console.nextLine();
//      через ISR:
        InputStreamReader reader = new InputStreamReader(stream);
        BufferedReader buffer = new BufferedReader(reader);
        String lineBR = buffer.readLine();
    }
}
