package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.task18;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Считать с консоли 2 имени файла: файл1, файл2.
 * Записать в файл2 все байты из файл1, но в обратном порядке.
 * Закрыть потоки.
 */
public class Task1809 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String file1 = "C:\\test\\test.txt";//br.readLine();
        String file2 = "C:\\test\\revers.txt";//br.readLine();
        FileInputStream input = new FileInputStream(file1);
        FileOutputStream output = new FileOutputStream(file2);
        List<Byte> result = new ArrayList<>();
        while (input.available() > 0) {
            int data = input.read();
            result.add((byte) data);
        }
        Collections.reverse(result);
        byte[] reverse = new byte[result.size()];
        for (int i = 0; i < result.size(); i++) {
            reverse[i] = result.get(i);
        }
        /**
         * List<Integer> inputBytes = new ArrayList<>();
         *             while (fileInputStream.available() > 0) {
         *                 inputBytes.add(fileInputStream.read());
         *             }
         *             Collections.reverse(inputBytes);
         *             for (Integer aByte : inputBytes) {
         *                 fileOutputStream.write(aByte);
         *             }
         */
        output.write(reverse);
        input.close();
        output.close();
    }
}
