package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.adapt.ex;

public class Ex implements InterfaceTest2 {
    private InterfaceTest1 obj;
    Ex(InterfaceTest1 object){
        this.obj = object;
    }
    // тут располагаются методы Interface2,
    // которые вызывают методы Interface1
    /**
     * Это схематическое описание «паттерна проектирования адаптер».
     * Суть его в том, что класс Ex является преобразователем (адаптером) одного интерфейса к другому.
     */

}
