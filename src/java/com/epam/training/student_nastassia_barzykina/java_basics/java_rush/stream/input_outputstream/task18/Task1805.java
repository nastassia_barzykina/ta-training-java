package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.task18;

/**
 * Ввести с консоли имя файла.
 * Считать все байты из файла.
 * Не учитывая повторений - отсортировать их по байт-коду в возрастающем порядке.
 * Вывести на экран.
 * Закрыть поток ввода-вывода.
 *
 * Пример байт входного файла:
 * 44 83 44
 *
 * Пример вывода:
 * 44 83
 *
 * Требования:
 * •	Программа должна считывать имя файла с консоли.
 * •	Для чтения из файла используй поток FileInputStream.
 * •	В консоль через пробел должны выводиться все уникальные байты из файла в порядке возрастания.
 * •	Данные в консоль должны выводится в одну строку.
 * •	Поток чтения из файла должен быть закрыт.
 */
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.TreeSet;

public class Task1805 {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String fileName = br.readLine();
        FileInputStream inputStream = new FileInputStream(fileName);
        Set<Integer> dataSet = new TreeSet<>();

        while (inputStream.available() > 0){
            int data = inputStream.read();
            dataSet.add(data);
        }
        inputStream.close();
        for (Integer elem : dataSet){
            System.out.print(elem + " ");
        }
    }
}
