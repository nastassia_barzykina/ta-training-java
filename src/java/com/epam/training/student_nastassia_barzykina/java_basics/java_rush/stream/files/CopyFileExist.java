package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.files;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class CopyFileExist {
    public static void main(String[] args) {
        Path sourcePath = Paths.get("c:\\test\\chartest.txt");
        Path destPath = Paths.get("c:\\test\\dest.txt");

        try {
            Files.copy(sourcePath, destPath, REPLACE_EXISTING);// перенакрывает сущ. файл
            System.out.println("File is copied successful!");
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
