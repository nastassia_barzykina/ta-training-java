package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stack_trace;

import java.util.Stack;

public class StackExample {
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        System.out.println(stack);
        int x = stack.pop();// 1, 2
        stack.push(4);// 1, 2, 4
        int y = stack.peek();
        stack.pop();//1 , 2
        stack.pop();// 1
        boolean isEmpty = stack.empty();
        System.out.println("x = " + x + " y = " + y + " isEmpty = " + isEmpty);
    }


}
