package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.polymor.hen_task;

public abstract class Hen  {
    public abstract int getCountOfEggsPerMonth();
    public String getDescription(){
        return "Я - курица.";
    }
}
