package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream_collect.ex_pets;

public abstract class Animal {
    private String name;
    private Color color;
    private int age;
    public Animal(String name, Color color, int age){
        this.name = name;
        this.color = color;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public Color getColor() {
        return color;
    }

    public int getAge() {
        return age;
    }
}
