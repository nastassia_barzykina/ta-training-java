package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.methods.new_swap;

public class Pair {
    private int x;
    private int y;

    public Pair(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return String.format("x=%d, y=%d", x, y);
    }

    public void swap() {
        x ^= y;
        y ^= x;
        x ^= y;
    }

}
