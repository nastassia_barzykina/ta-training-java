package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.stream.input_outputstream.task18;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class Task1802 {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String fileName = br.readLine();
        FileInputStream inputStream = new FileInputStream(fileName);
        int min = Integer.MAX_VALUE;// = 255

        while (inputStream.available() > 0)
        {
            int data = inputStream.read();
            if (data < min) {
                min = data;
            }
        }
        inputStream.close();

        System.out.println(min);
    }
}
