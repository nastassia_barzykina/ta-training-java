package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.constructors_obj.task;

import java.util.Arrays;

/**
 * Метод executeDefragmentation(String[]), принимающий массив строк, выполняет его "дефрагментацию", то есть перемещает
 * все объекты в начало массива в таком же порядке, передвинув все "дыры" (элементы равные null) в конец массива.
 * я не знаю, как я это сделала... Подсмотрено в StringArray.java.
 */
public class Memory {
    public static void main(String[] args) {
        String[] memory = {null, "object15", null, null, "object2", "object21", null, null, null, "object32", null, "object4", null};
        executeDefragmentation(memory);
        System.out.println(Arrays.toString(memory));
    }

    public static void executeDefragmentation(String[] array) {
//        String tmp;
//        for (int i = 0; i < array.length; i++){
//            for (int j = i + 1; j <= array.length - 1; j++) {
//                if (array[i] == null) {
//                    if (array[j] != null) {
//                        tmp = array[i];
//                        array[i] = array[j];
//                        array[j] = tmp;
//                        break;
//                    }
//                }
//            }
//        }// решение:

        String[] temp = new String[array.length];
        int j = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != null) {
                temp[j] = array[i];
                j++;
            }
        }
        for (int i = 0; i < array.length; i++) {
            array[i] = temp[i];
        }
    }
}
