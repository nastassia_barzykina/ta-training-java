package com.epam.training.student_nastassia_barzykina.java_basics.java_rush.singleton.task1522;

public interface Planet {
    static String SUN = "sun";
    static String MOON = "moon";
    static String EARTH = "earth";
}
