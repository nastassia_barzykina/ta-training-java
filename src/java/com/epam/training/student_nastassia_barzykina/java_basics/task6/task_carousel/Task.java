package com.epam.training.student_nastassia_barzykina.java_basics.task6.task_carousel;

public interface Task {
    void execute();
    boolean isFinished();
}
