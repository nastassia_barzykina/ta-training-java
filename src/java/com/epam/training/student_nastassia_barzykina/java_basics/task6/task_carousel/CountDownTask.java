package com.epam.training.student_nastassia_barzykina.java_basics.task6.task_carousel;

/**
 * Конструктор CountDownTask принимает в качестве параметра одно целое значение. Это начальное значение обратного отсчета.
 * Входное значение не должно быть отрицательным. Если оно отрицательно, установите нулевое значение.
 * Каждый раз, когда вызывается метод execute, это значение уменьшается на единицу, пока не достигнет нуля. После этого
 * метод execute больше не уменьшает значение, и задача считается завершенной.
 * Если задача инициализирована с нулевым значением, считайте ее завершенной сразу после создания.
 * Значение задачи доступно через метод геттер.
 */
public class CountDownTask implements Task{
    private int value;// значение обратного отсчета

    public CountDownTask(int value) {
        if (value < 0) value = 0;// установка 0 значения
        this.value = value;
    }
    public int getValue() {
      return value;
    }
    @Override
    public void execute() {// пока значение обратного отсчета >0, уменьшаем
        if (value > 0) {
            value--;
        }
    }

    @Override
    public boolean isFinished() {//если =0, задача завершена
        return value == 0;

    }
}
