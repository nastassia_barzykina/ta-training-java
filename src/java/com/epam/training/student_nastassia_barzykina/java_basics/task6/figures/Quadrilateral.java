package com.epam.training.student_nastassia_barzykina.java_basics.task6.figures;

class Quadrilateral extends Figure{
    Point a;
    Point b;
    Point c;
    Point d;
    Quadrilateral (Point a, Point b, Point c, Point d){
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    @Override
    public double area() {
        double abcS = new Triangle(this.a, this.b, this.c).area();
        double acdS = new Triangle(this.a, this.c, this.d).area();
        return abcS + acdS;
    }

    @Override
    public String pointsToString() {
        return a.toString() + b.toString() + c.toString() + d.toString();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public Point leftmostPoint() {
        Point[] points = {a, b, c, d};
        return left(points);
    }
}
