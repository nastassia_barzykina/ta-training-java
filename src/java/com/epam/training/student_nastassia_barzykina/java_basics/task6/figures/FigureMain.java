package com.epam.training.student_nastassia_barzykina.java_basics.task6.figures;

public class FigureMain {
    public static void main(String[] args) {
        Figure t = new Triangle(new Point(0,0), new Point(3, 0), new Point(0, 4));
        System.out.println(t.pointsToString());
        System.out.println(t.area());
        System.out.println(t.leftmostPoint());
        System.out.println(t);
        Figure c = new Circle(new Point(1,1), 3);
        System.out.println(c.pointsToString());
        double area = new Circle(new Point(1,1), 3).area();//28.274333882308138
        System.out.println(area);
        System.out.println(c.leftmostPoint());
        System.out.println(c);
        System.out.println("Quadrilateral:");
        Figure q = new Quadrilateral(new Point(1,0), new Point(2, 1), new Point(1, 2), new Point(0, 1));
        System.out.println(q.pointsToString());
        double areaQ = new Quadrilateral(new Point(1,0), new Point(2, 1), new Point(1, 2), new Point(0, 1)).area();
        System.out.println(areaQ);
        System.out.println(q.leftmostPoint());
        System.out.println(q);
    }
}
