package com.epam.training.student_nastassia_barzykina.java_basics.task6.figures_challenge;

class Circle extends Figure {
    Point center;
    double r;
    Circle(Point center, double r){
        if (r <= 0 || center == null){
            throw new IllegalArgumentException();
        }
        this.center = center;
        this.r = r;
    }
    @Override
    public double area() {
        return Math.PI * r * r;
    }

    @Override
    public Point centroid() {
        return center;
    }

    @Override
    public boolean isTheSame(Figure figure) {
        if (figure instanceof Circle) {
            Circle other = (Circle)figure;
            return Utils.isEqual(other.r, r) // сравнение радиусов с заданной точностью через метод утилиты для переменных типа double
                    // т.к. sqrt(2)*sqrt(2)!= 2. Сравнивается радиус объекта other и радиус, инициализированный к-ром
                    && other.center.equals(center);
        }
        return false;
    }

    @Override
        public String pointsToString() {
            return center.toString();
        }

    @Override
    public Point leftmostPoint() {
        return new Point(center.getX() - r, center.getY());
    }

}
