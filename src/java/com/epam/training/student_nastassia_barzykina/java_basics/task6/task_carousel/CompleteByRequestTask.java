package com.epam.training.student_nastassia_barzykina.java_basics.task6.task_carousel;

/**
 * Конструктор CompleteByRequestTask не принимает параметров.
 * Вызов метода execute для задачи не завершает ее до тех пор, пока не будет вызван метод complete.
 * После вызова полного метода complete следующий вызов метода execute завершает задачу. Обратите внимание, что задача
 * не завершается сразу после вызова метода complete. Задача завершается только тогда, когда происходит последующий вызов
 * для выполнения.
 */
public class CompleteByRequestTask implements Task {
    private boolean flagEx;// флаг для execute()
    private boolean flagComp;// флаг для complete()

    @Override
    public void execute() {
        if (flagComp) {//если прежде был вызван complete(),меняем значение флага
            flagEx = true;
        }
    }

    @Override
    public boolean isFinished() {
        return flagEx;
    }

    public void complete() {//если был вызван complete(), меняется значение флага
        flagComp = true;
    }
}
