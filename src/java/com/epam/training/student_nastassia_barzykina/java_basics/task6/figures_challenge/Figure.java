package com.epam.training.student_nastassia_barzykina.java_basics.task6.figures_challenge;

abstract class Figure{

    public abstract double area();
    public abstract Point centroid();
    public abstract boolean isTheSame(Figure figure);

    public abstract String pointsToString();

    public String toString() {
        return this.getClass().getSimpleName() + "[" + pointsToString() + "]";
    }

    public abstract Point leftmostPoint();
    protected Point left(Point[] points) {
        Point result = points[0];
        for (int i = 1; i < points.length; i++){
            if (points[i].getX() < result.getX()) {
                result = points[i];
            }
        }
        return result;
    }
}
