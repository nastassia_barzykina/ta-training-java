package com.epam.training.student_nastassia_barzykina.java_basics.task6.figures_challenge;

import static java.lang.Math.abs;
import static java.lang.Math.sqrt;
import static java.lang.StrictMath.pow;

class Segment {// не понадобился в этой задаче
    Point p1;
    Point p2;
    public Segment(Point p1, Point p2) {
        if (p1.equals(p2)) {
            throw new IllegalArgumentException();
        }
        this.p1 = p1;
        this.p2 = p2;
    }

    public Point getP1() {
        return p1;
    }

    public Point getP2() {
        return p2;
    }


    double length() {
        if (p1.equals(p2)) {
            throw new IllegalArgumentException();
        }
        double l = sqrt(abs(pow(getP2().getX() - getP1().getX(), 2) + pow(getP2().getY() - getP1().getY(), 2)));
        return l;

    }
}
