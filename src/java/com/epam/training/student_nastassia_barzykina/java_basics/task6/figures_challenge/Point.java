package com.epam.training.student_nastassia_barzykina.java_basics.task6.figures_challenge;

class Point {
    private double x;
    private double y;

    public Point(final double x, final double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    @Override
    public String toString() {
        return "(" +
                x +
                "," + y + ")";

    }

    @Override
    public boolean equals(Object o) {// переопределение метода
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Point point = (Point) o;

        if (!Utils.isEqual(x, point.x)) return false;// если координаты по X не равны, false. Иначе сравниваем координаты по Y
        return Utils.isEqual(y, point.y);// и возвращаем результат сравнения
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(x);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
