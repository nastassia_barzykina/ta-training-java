package com.epam.training.student_nastassia_barzykina.java_basics.task6.figures;
import static java.lang.Math.abs;
class Triangle extends Figure {
    Point a;
    Point b;
    Point c;
    Triangle() {

    }

    Triangle (Point a, Point b, Point c){
        this.a = a;
        this.b = b;
        this.c = c;

    }

    @Override
    public double area() {
        return abs(a.getX() * (b.getY() - c.getY()) + b.getX() * (c.getY() - a.getY())
                + c.getX() * (a.getY() - b.getY())) / 2;
    }

    @Override
    public String pointsToString() {
        return a.toString() + b.toString() + c.toString();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public Point leftmostPoint() {
        Point[] points = {a, b, c};
        return left(points);
    }


}
