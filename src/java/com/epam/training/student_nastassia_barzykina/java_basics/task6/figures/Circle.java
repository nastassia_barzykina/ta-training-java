package com.epam.training.student_nastassia_barzykina.java_basics.task6.figures;

class Circle extends Figure{
    Point center;
    double r;
    Circle(Point center, double r){
        this.center = center;
        this.r = r;
    }
    @Override
    public double area() {
        return Math.PI * r * r;
    }

    @Override
    public String pointsToString() {
        return center.toString();
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "[" + pointsToString() + r + "]";
    }

    @Override
    public Point leftmostPoint() {
        return new Point(center.getX() - r, center.getY());
    }
}
