package com.epam.training.student_nastassia_barzykina.java_basics.task6.figures_challenge;

import static java.lang.Math.sqrt;

public class FigureMain {
    public static void main(String[] args) {
        System.out.println("Triangle:");
        Figure t = new Triangle(new Point(0,0), new Point(3, 0), new Point(0, 4));
//        System.out.println(t.isTheSame(t));
        System.out.println(t.centroid());
        Figure t1 = new Triangle(new Point(3,0), new Point(0, 4), new Point(0, 0));
        System.out.println(t1.isTheSame(t));
//        System.out.println(t1.centroid());
//        Figure c = new Circle(new Point(0, 00000.1), 3);
//        Figure c1 = new Circle(new Point(0,0), 3);
//        Figure c2 = new Circle(new Point(1,1), 3);
//        boolean c3 = new Circle(new Point(sqrt(2) * sqrt(2), 4 - sqrt(2) * sqrt(2)), sqrt(3) * sqrt(3)).isTheSame(new Circle(new Point(2, 2), 3));
//        System.out.println(c.isTheSame(c2));
//        System.out.println(c.centroid());
//        System.out.println(c1.centroid());
//        System.out.println(c3);
        System.out.println("Quadrilateral:");
        Figure q = new Quadrilateral(new Point(1,0), new Point(2, 1), new Point(1, 2), new Point(0, 1));
        Figure q1 = new Quadrilateral(new Point(2,1), new Point(1, 2), new Point(0, 1), new Point(1, 0));
        System.out.println(q.isTheSame(q1));
        System.out.println(q.centroid());
        System.out.println(q1.centroid());
        System.out.println(q1.area());
        Figure q2 = new Quadrilateral(new Point(1,1), new Point(1, -1), new Point(-1, -1), new Point(-2, 2));
        System.out.println(q2.centroid());
//        Figure q3 = new Quadrilateral(new Point(1,1), new Point(1, 1), new Point(1, 1), new Point(-2, 2));// exception


    }
}
