package com.epam.training.student_nastassia_barzykina.java_basics.task6.figures_challenge;

import static java.lang.Math.abs;
class Triangle extends Figure {
    Point a;
    Point b;
    Point c;

    Triangle (Point a, Point b, Point c){
        if (a == null || b == null || c == null || areaCheck(a, b, c) == 0) {
            throw new IllegalArgumentException();
        }
        this.a = a;
        this.b = b;
        this.c = c;

    }
    private static double areaCheck(Point a, Point b, Point c){
        return abs(a.getX() * (b.getY() - c.getY()) + b.getX() * (c.getY() - a.getY())
                + c.getX() * (a.getY() - b.getY())) / 2;
    }

    @Override
    public double area() {
        return areaCheck(a, b, c);
    }

    @Override
    public Point centroid() {
        double xO = (getA().getX() + getB().getX() + getC().getX()) / 3;
        double yO = (getA().getY() + getB().getY() + getC().getY()) / 3;
        return new Point(xO, yO);
    }
    public Point getA() {
        return a;
    }
    public Point getB() {
        return b;
    }
    public Point getC(){
        return c;
    }

    @Override
    public boolean isTheSame(Figure figure) {//
        int flag = 0;
        if (figure instanceof Triangle) {
            Triangle other = (Triangle) figure;
            Point[] otherP = {other.a, other.b, other.c};
            Point[] points = {a, b, c};
            for (int i = 0; i <= points.length - 1; i++) {
                for (int j = 0; j <= otherP.length - 1; j++) {
                    if (points[i].equals(otherP[j])) {
                        flag++;
                    }
                }
            }
            if (flag == 3) return true;// если все 3 точки совпадают
        }
        return false;

//            return other.a.equals(a) && other.b.equals(b) && other.c.equals(c);// не будет работать для случая (abc -- bca)
//            по идее, нужно реализовать аналогично четырехугольнику через массивы
//        }
//        return false;
    }

    @Override
    public String pointsToString() {
        return a.toString() + b.toString() + c.toString();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public Point leftmostPoint() {
        Point[] points = {a, b, c};
        return left(points);
    }


}
