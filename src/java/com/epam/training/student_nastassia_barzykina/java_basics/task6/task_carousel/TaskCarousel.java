package com.epam.training.student_nastassia_barzykina.java_basics.task6.task_carousel;

/**
 * TaskCarousel имеет емкость, указанную в качестве параметра конструктора.
 * TaskCarousel имеет метод isEmpty, который возвращает true, если в карусели нет задачи для выполнения.
 * В противном случае возвращает false.
 * TaskCarousel имеет метод isFull, который возвращает true, если в карусели больше нет места для добавления другой задачи.
 * В противном случае возвращает false.
 * Вы можете добавлять задачи в карусель с помощью метода addTask. Он возвращает true, если задача принята, и false
 * в противном случае.
 * Задание может быть не принято по следующим причинам:
 * Аргумент задачи равен нулю
 * Задача уже завершена
 * Карусель заполнена
 * Вы можете выполнять задачи в карусели с помощью метода execute:
 * Каждый раз при вызове этого метода, карусель должна переключаться на следующую задачу внутри и выполнять ее.
 * Итерация круговая. Если внутри карусели 4 задачи, то если мы вызовем метод execute на карусели 4 раза подряд,
 * каждая задача должна быть выполнена один раз.
 * Если задача завершена после выполнения, удалите ее из карусели.
 * Метод возвращает true, если какая-либо задача была выполнена. В противном случае возвращает false.
 */
public class TaskCarousel {
    private final Task tasks[];//объявление массива задач внутри карусели
    private int pointer;//указатель текущей позиции

    public TaskCarousel(int capacity) {
        this.tasks = new Task[capacity];// иниц-я массива заданной емкости
    }

    public boolean addTask(Task task) {
        if (( task == null) || (isFull()) || (task.isFinished())) {//если задача не пустая (не удалена), если в карусели есть место и если задача не завершена
            return false;
        }
        int tmpPointer = pointer;// для возврата на текущую позицию после анализа ячеек карусели
        while (tasks[pointer] != null) {// пока i-ая задача не удалена/ не пустая
            if ((pointer < tasks.length - 1)) {// и если не конец массива,
                pointer++;// увеличение счетчика

            } else {
                pointer = 0;// сброс счетчика на 0 для последующего круга итераций
            }
        }
        tasks[pointer] = task;// добавление задачи в i-ую ячейку карусели
        pointer = tmpPointer;

        return true;// успешное выполнение задачи
    }

    public boolean execute() {
        if (isEmpty()) {// если карусель пустая - выход
            return false;
        }

        if (tasks[pointer].isFinished()) {// если i-ая задача завершена
            return false;
        }

        tasks[pointer].execute();// вызов имплементирующего метода в зависимости от типа задачи

        if (tasks[pointer].isFinished()) {// если задача завершена, удаляем (обнуляем) ее
            tasks[pointer] = null;
        }
        incPointer();// вызов метода обработки счетчика

        return true;
    }

    private void incPointer() {// метод обработки счетчика текущей позиции
        if (isEmpty()) {// если карусель пустая - выход
            return;
        }
        do {
            if ((pointer < tasks.length - 1)) {
                pointer++;
            } else {
                pointer = 0;
            }
        } while (tasks[pointer] == null);// увеличение счетчика, пока задача пустая (ищем ячейку с задачей для выполнения)
    }

    public boolean isFull() {// проверка на заполненность карусели
        for (Task task : tasks) {
            if (task == null) {return false;}// если хоть 1 ячейка свободна
        }
        return true;
    }

    public boolean isEmpty() {// проверка на пустоту
        for (Task task : tasks) {
            if (task != null) {return false;}// если хоть 1 ячейка занята
        }
        return true;
    }
}
