package com.epam.training.student_nastassia_barzykina.java_basics.task6.figures_challenge;

import static java.lang.Math.abs;

class Quadrilateral extends Figure {
    Point a;
    Point b;
    Point c;
    Point d;
    Quadrilateral (Point a, Point b, Point c, Point d){
        Point[] points = {a, b, c, d};
        if (a == null || b == null || c == null || d == null || areaCheck(a, b, c, d) == 0) {// точки есть и S != 0
            throw new IllegalArgumentException();
        }
        if (checkOnCoincidences(points) || checkOfConvex(a, b, c, d)) {
            throw new IllegalArgumentException();
        }

        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    @Override
    public double area() {
        return areaCheck(a, b, c, d);
    }
    private static double areaCheck(Point a, Point b, Point c, Point d){
        return new Triangle(a, b, c).area() + new Triangle(a, c, d).area();
    }


    private static boolean checkOnCoincidences(Point[] points) {// проверка на вырожденность четырехугольника
        for (int i = 0; i <= points.length - 2; i++){
            for (int j = i + 1; j <= points.length - 1; j++){
                if (points[i].equals(points[j])) {
                    return true;// хотя бы 2 точки совпадают
                }
            }
        }
        return false;
    }
    private static boolean checkOfConvex(Point a, Point b, Point c, Point d){// проверка на выпуклость четырехугольника
        double sABC = a.getX() * (b.getY() - c.getY()) + b.getX() * (c.getY() - a.getY()) + c.getX() * (a.getY() - b.getY());
        double sCDA = c.getX() * (d.getY() - a.getY()) + d.getX() * (a.getY() - c.getY()) + a.getX() * (c.getY() - d.getY());
        double sBCD = b.getX() * (c.getY() - d.getY()) + c.getX() * (d.getY() - b.getY()) + d.getX() * (b.getY() - c.getY());
        double sDAB = d.getX() * (a.getY() - b.getY()) + a.getX() * (b.getY() - d.getY()) + b.getX() * (d.getY() - a.getY());
        if (sABC * sCDA > 0 && sBCD * sDAB > 0) {// проверка удвоенных площадей с учетом знаков
            return false;//выпуклый
        }
        return true;
    }

    @Override
    public Point centroid() {//центроид (не барицентр!!!) четырехугольника (area centroid) через разбиение - триангуляцию, через площади треугольников и их центроиды
        Point cABC = new Triangle(a, b, c).centroid(); //центроид треугольника
        Point cACD = new Triangle(a, c, d).centroid();
//        double lengthOfCenters = new Segment(cABC,cACD).length();//расстояние между центрами масс треугольников
        double sABC = new Triangle(a, b, c).area();// площади треугольников
        double sACD = new Triangle(a, c, d).area();

//        double lOfABC = lengthOfCenters / abcS;
//        Point[] points = {a, b, c, d};
        double rezX = (sABC * cABC.getX() + sACD * cACD.getX()) / (sABC + sACD);// (s1*x1 +s2*x2)/(s1+s2)
        double rezY = (sABC * cABC.getY() + sACD * cACD.getY()) / (sABC + sACD);
//        double s = area();// для барицентра четырехугольника:
//                s = ((a.getX() * b.getY() - b.getX() * a.getY())
//                        + (b.getX() * c.getY() - c.getX() * b.getY())
//                        + (c.getX() * d.getY() - d.getX() * c.getY())
//                        + (d.getX() * a.getY() - a.getX() * d.getY())
//                ) / 2;
//                rezX = ((a.getX() + b.getX()) * (a.getX() * b.getY() - b.getX() * a.getY())
//                        + (b.getX() + c.getX()) * (b.getX() * c.getY() - c.getX() * b.getY())
//                        + (c.getX() + d.getX()) * (c.getX() * d.getY() - d.getX() * c.getY())
//                        + (d.getX() + a.getX()) * (d.getX() * a.getY() - a.getX() * d.getY())
//                ) / (6 * s);
//                rezY = ((a.getY() + b.getY()) * (a.getX() * b.getY() - b.getX() * a.getY())
//                        + (b.getY() + c.getY()) * (a.getX() * b.getY() - b.getX() * a.getY())
//                        + (c.getY() + d.getY()) * (a.getX() * b.getY() - b.getX() * a.getY())
//                        + (d.getY() + a.getY()) * (a.getX() * b.getY() - b.getX() * a.getY())
//    ) / (6 * s);
//            }

        return new Point(rezX, rezY);
    }

    @Override
    public boolean isTheSame(Figure figure) {// проверка на совпадение (например, abcd и bcda с одинаковыми координатами, но другим порядком)
        int flag = 0;
        if (figure instanceof Quadrilateral) {
            Quadrilateral other = (Quadrilateral) figure;
            Point[] otherP = {other.a, other.b, other.c, other.d};
            Point[] points = {a, b, c, d};
            for (int i = 0; i <= points.length - 1; i++) {
                for (int j = 0; j <= otherP.length - 1; j++) {
                    if (points[i].equals(otherP[j])) {
                       flag++;
                    }
                    //return other.a.equals(a) && other.b.equals(b) && other.c.equals(c) && other.d.equals(d);
                }
            }
            if (flag == 4) return true;// если все 4 точки совпадают
        }
        return false;
    }

    @Override
    public String pointsToString() {
        return a.toString() + b.toString() + c.toString() + d.toString();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public Point leftmostPoint() {
        Point[] points = {a, b, c, d};
        return left(points);
    }
}
