package com.epam.training.student_nastassia_barzykina.java_basics.task6.figures_challenge;

public class Utils {
    private final static double DELTA = 0.00001;// задается точность для сравнения переменных типа double
    public static boolean isEqual(double d1, double d2) {
        return d1 == d2 || isRelativelyEqual(d1,d2);
    }// true, если либо сами переменные равны, либо результат сравнения через метод меньше DELTA - заданной точности

    private static boolean isRelativelyEqual(double d1, double d2) {
        return DELTA > Math.abs(d1- d2);
    }
}
