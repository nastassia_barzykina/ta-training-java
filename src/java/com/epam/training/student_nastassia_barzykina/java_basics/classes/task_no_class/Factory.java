package com.epam.training.student_nastassia_barzykina.java_basics.classes.task_no_class;

class Factory {
    private int numOfCarsOfModel;
    Factory() {

    }

    public int getNumOfCarsOfModel() {
        return numOfCarsOfModel;
    }

    Car createCar (String name, int maxSpeed){
        Car car = new Car(name, maxSpeed);
        numOfCarsOfModel++;
        return car;
    }
    Car createLuxCar (String name, int maxSpeed, String type) {
        Car luxCar = new Car(name, maxSpeed, type);
        numOfCarsOfModel++;
        return luxCar;
    }
}
