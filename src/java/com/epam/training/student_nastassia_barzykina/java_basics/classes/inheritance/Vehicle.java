package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance;

public class Vehicle {
    protected int maxSpeed = 230;
    protected void showSpeed() {
        System.out.println(maxSpeed + "from Vehicle");
    }
}
