package com.epam.training.student_nastassia_barzykina.java_basics.classes.task_no_class.ForTest;

class SuperMan extends Person{
    String skills;
    SuperMan (String name, String skills){
//        super(name);// без этого не получим имя из суперкласса
        this.skills = skills;
    }
    public String showInfo (){
        return name + " has " + skills;
    }
}
