package com.epam.training.student_nastassia_barzykina.java_basics.classes.task_no_class.ForTest;

class MainTest {
    public static void main(String[] args) {
        Person person = new Student();
        Student student = new Student();
        System.out.println(person.version);
        System.out.println(student.version);
        SuperMan superMan = new SuperMan("Clarc", "supervision");
        System.out.println(superMan.showInfo());
    }
}
