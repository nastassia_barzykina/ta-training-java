package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.interface1;

public interface Shape {
    double PI = Math.PI;
    double getSquare();// абстрактный метод
}
