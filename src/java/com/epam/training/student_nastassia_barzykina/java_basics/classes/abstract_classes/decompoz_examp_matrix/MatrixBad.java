package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.decompoz_examp_matrix;

/**
 * Пусть требуется решить следующую задачу: создать систему, позволяющую умножать целочисленные матрицы друг на друга.
 *
 * Создан только один класс, этого мало, но и задача невелика.
 * Класс обладает лишними полями, значения которых зависят от значений других полей.
 * В классе объявлены два конструктора, оба выделяют память под матрицу и заполняют ее переданными или сгенерированными
 * элементами. Оба конструктора решают похожие задачи и не проверяют на корректность входные значения, так как решают
 * слишком обширные задачи.
 * Определен метод show() для вывода матрицы на консоль, что ограничивает способы общения класса с внешними для него классами.
 * Задача умножения решается в методе main(), и класс является одноразовым, то есть для умножения двух других матриц
 * придется копировать код умножения в другое место.
 * Реализован только основной положительный сценарий, например, не выполняется проверка размерности при умножении, и,
 * как следствие, отсутствует реакция приложения на некорректные данные.
 */
public class MatrixBad {
    private int[][] a;
    private int n;
    private int m;
    public MatrixBad(int nn, int mm) {
        n = nn;
        m = mm;
        // creation and filling with random values
        a = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                a[i][j] = (int)(Math.random() * 10);
            }
        }
        show();
    }
    public MatrixBad(int nn, int mm, int k) {
        n = nn;
        m = mm;
        a = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                a[i][j] = k;
            }
        }
        if(k != 0) {
            show();
        }
    }
    public void show() {
        System.out.println("matrix : " + a.length + " by " + a[0].length);
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }
    public static void main(String[] args) {
        int n = 2, m = 3, l = 4;
        MatrixBad p = new MatrixBad(n, m);
        MatrixBad q = new MatrixBad(m, l);
        MatrixBad r = new MatrixBad(n, l, 0);
        for (int i = 0; i < p.a.length; i++) {
            for (int j = 0; j < q.a[0].length; j++) {
                for (int k = 0; k < p.a[0].length; k++) {
                    r.a[i][j] += p.a[i][k] * q.a[k][j];
                }
            }
        }
        System.out.println("matrix multiplication result: ");
        r.show();
    }
}
