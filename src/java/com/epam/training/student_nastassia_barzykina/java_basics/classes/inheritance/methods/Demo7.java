package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance.methods;
class Point{
    protected int x;
    protected int y;
    public Point(int x, int y){
        this.x = x;
        this.y = y;
    }
}
public class Demo7 {
    public static void main(String[] arg) {
        Point point1 = new Point(5, -5);
        Point point2 = point1;// копируется ссылка
        Point point3 = new Point(5, -5);
        Point point4 = new Point(5, 5);
        System.out.println(point1.equals(point2));// проверяется эквивалентность ссылок, но не значений, т.к. метод не переопределен
        System.out.println(point1.equals(point3));
        System.out.println(point1.equals(point4));
        System.out.println(point1.hashCode());
        System.out.println(point2.hashCode());
        System.out.println(point3.hashCode());
        System.out.println(point4.hashCode());
        /**
         * Если Java-компилятор в вашем коде (именно в коде) найдет несколько одинаковых строк, для экономии памяти он
         * создаст для них только один объект.
         * String text = "Это очень важное сообщение";
         * String message = "Это очень важное сообщение";
         * text == message даст true.
         * А если вам вдруг очень нужно, чтобы ссылки были разные, вы можете написать так:
         *
         * String text = "Это очень важное сообщение";
         * String message = new String ("Это очень важное сообщение");
         * Или так:
         *
         * String text = "Это очень важное сообщение";
         * String message = new String (text);
         * В обоих этих случаях переменные text и message указывают на разные объекты, содержащие одинаковый текст.
         */
    }
}
