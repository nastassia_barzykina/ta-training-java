package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance.methods;

public class Student {
    private final String name;
    private final long phone;
    private final int age;

    public Student(String name, long phone, int age) {
        this.name = name;
        this.phone = phone;
        this.age = age;
    }

    @Override
    public int hashCode() { // переопределение метода
        int result = 17;
        result = 31 * result + name.hashCode();
        result = 31 * result + (int) (phone ^ (phone >>> 32));
        result = 31 * result + age;
        return result;
    }
}
