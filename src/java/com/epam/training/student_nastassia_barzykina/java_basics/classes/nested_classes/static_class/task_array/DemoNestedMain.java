package com.epam.training.student_nastassia_barzykina.java_basics.classes.nested_classes.static_class.task_array;

public class DemoNestedMain {
    public static void main(String[] args) {
        double[] array = new double[20];
        for (int i = 0; i < array.length; i++) {
            array[i] = 10 * Math.random();// заполняем массив случайными значениями

        }
        ArrayOperation.Pair pair = ArrayOperation.searchMinMax(array);
        System.out.println("Min = " + pair.getMin());
        System.out.println("Max = " + pair.getMax());
    }
}
