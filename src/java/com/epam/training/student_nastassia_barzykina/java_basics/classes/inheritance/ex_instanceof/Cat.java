package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance.ex_instanceof;

/**
 * В классе Main создадим ссылку cat типа Cat и инициализируем ее объектом класса BritishCat.
 * Если затем ссылку cat привести к типу BritishCat, то все пройдет корректно, т.к. объект тот же,
 * но теперь мы смотрим на него не просто как на кошку, а именно кошку британской породы. Однако если ссылку cat
 * привести к типу PersianCat, то получим ошибку во время исполнения – ClassCastException. Это происходит потому,
 * что типы BritishCat и PersianCat находятся в разных ветках наследования.
 */
public class Cat {
    public void move() { System.out.println("Cat move"); }
}
class BritishCat extends Cat {
    @Override
    public void move() { System.out.println("British cat move"); }
}
class PersianCat extends Cat {
    @Override
    public void move() { System.out.println("Persian cat move"); }
}
class Main {
    public static void main(String[] arg) {
        Cat cat = new BritishCat();
        BritishCat cat2 = (BritishCat)cat;// ссылку cat привести к типу BritishCat
        //PersianCat cat3 = (PersianCat)cat;// - нельзя, тк разные ветки наследования
        cat.move();
        cat2.move();
        if (cat  instanceof  PersianCat) {
            System.out.println("Persian cat!");
            PersianCat cat3 = (PersianCat) cat;
            cat3.move();
        } else {
            System.out.println("Not Persian cat!");
        }
        Cat cat4 = new PersianCat();
        if (cat4 instanceof PersianCat){
            System.out.println("Persian cat!");
            PersianCat cat5 = (PersianCat) cat4;
            cat5.move();
        } else {
            System.out.println("Not Persian cat!");
        }
        //PersianCat cat5 = (PersianCat)cat4;
        cat4.move();
        //cat5.move();

    }
}
