package com.epam.training.student_nastassia_barzykina.java_basics.classes.nested_classes.anonim;

import java.util.Arrays;
import java.util.Comparator;

public class Ship1 {
    private int x = 20;
    void doJob(){
        final int y = 40;
        CustomTest tst = new CustomTest() {
            private int z = 60;{// собственное поле анонимного класса

                System.out.println("Блок инициализации");// секция инициализации
            }
            @Override
            public void test() {
                System.out.println(x + " " + y + " " + z);
            }
        };
        tst.test();
    }

    public static void main(String[] args) {
        Ship1 ship1 = new Ship1();
        ship1.doJob();
        /**
         * Для реализации интерфейса Comparator удобно использовать анонимный класс:
         *
         * реализации подлежит только один метод
         * требуется только один объект этого типа
         * код реализации будет размещаться рядом с его использованием.
         * Рассмотрим пример. Описывается массив строк, который затем упорядочивается в порядке, обратном алфавитному,
         * статическим методом sort() класса Arrays, и отображается в консоль. Этому методу нужен объект типа Comparator,
         * который создается как второй аргумент метода и реализуется анонимным классом. Здесь даже не нужно создавать ссылку
         * типа Comparator, поскольку она бы использовалась только для указания в качестве аргумента метода sort() и больше нигде.
         */
        String[] arr = {"java", "scala", "fortran", "ada", "modula"};
        Arrays.sort(arr, new Comparator() {// анонимный класс и интерфейс Comporator, объявление и инициализация внутри аргумента метода
            @Override
            public int compare(Object o1, Object o2) {
                String str1 = (String) o1;
                String str2 = (String) o2;
                return str2.compareTo(str1);// сортировка в порядке, обратном алфавитному
            }
        });
        System.out.println(Arrays.toString(arr));
    }
}
