package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.cloneable1;

import java.util.Date;
public class Student implements Cloneable {
    private Date yearSet;
    private int group;
    public Student(Date year, int group) {
        this.yearSet = year;
        this.group = group;
    }
    public String toString() {
        return "year = " + yearSet + ", group = " + group;
    }
    public Object clone() throws CloneNotSupportedException {// для использования метода - переопределение с доступом public
        /**
         * пример глубокого клонирования на том же классе Student, изменив только реализацию метода clone():
         *
         * Сначала выполним поверхностное клонирование, чтобы получить новый объект.
         * Затем выполним клонирование ссылочного поля yearSet и перезапишем полученную копию в поле yearSet нового объекта
         */
        Student stud = (Student) super.clone();// используем поверхностное копирование
        stud.yearSet = (Date) this.yearSet.clone();//копии для всех ссылочных полей
        return stud;
        //      return super.clone();// поверхностное клонирование
    }
}
