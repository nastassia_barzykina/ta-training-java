package com.epam.training.student_nastassia_barzykina.java_basics.classes.nested_classes.inner_classes;

public class OuterArr {
    private int[] nums;// массив для анализа

    OuterArr(int[] n) {
        nums = n;
    }
    public void analyze(){
        Inner inObj = new Inner();
        System.out.println("Min: " + inObj.getMin());
        System.out.println("Max: " + inObj.getMax());
        System.out.println("Avg: " + inObj.getAvg());

    }
    class Inner{
        public int getMin(){
            int min = nums[0];
            for (int i = 1; i < nums.length; i++) {
                if (nums[i] < min)
                    min = nums[i];
            }
            return min;
        }
        public int getMax(){
            int max = nums[0];
            for (int i = 1; i < nums.length; i++) {
                if (nums[i] > max)
                    max = nums[i];
            }
            return max;
        }
        public double getAvg(){
            double avg = 0.0;
            for (int element: nums) {
                avg += element;
            }
            return avg / nums.length;
        }
    }
    public static void main(String[] args) {
        int[] x = {1, 2, 3, 5, 6, 7, 8, 9};
        OuterArr outOb = new OuterArr(x);
        outOb.analyze();
    }
}
