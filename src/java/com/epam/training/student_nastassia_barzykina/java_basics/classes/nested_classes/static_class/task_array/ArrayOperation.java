package com.epam.training.student_nastassia_barzykina.java_basics.classes.nested_classes.static_class.task_array;

public class ArrayOperation {
    public  static class Pair {// стат класс для стат метода
        private double min;
        private double max;

        Pair(double f, double s) {
            min = f;
            max = s;
        }

        public double getMin() {
            return min;
        }

        public double getMax() {
            return max;
        }
    }
        public static Pair searchMinMax(double[] values){
            double min = values[0];
            double max = values[0];
            for (double v: values){
               if (v < min){
                  min = v;
               }
               if (v > max){
                   max = v;
               }
            }

            return new Pair(min, max);
        }
    }

