package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.decompoz_examp_matrix;

public class MatrixCreator {
    public void fillRandomized(Matrix matrix, int minValue, int maxValue) {
        int v = matrix.getVerticalSize();
        int h = matrix.getHorizontalSize();
        for (int i = 0; i < v; i++) {
            for (int j = 0; j < h; j++) {
                try {
                    int value = (int) ((Math.random() * (maxValue - minValue)) + minValue);
                    matrix.setElement(i, j, value);
                } catch (MatrixException e) {
                    // log: exception impossible
                }
            }
        }
    }
    /**
     * Инициализация элементов матрицы различными способами вынесена в отдельный класс. Методы класса могут в
     * зависимости от условий извлекать значения для инициализации элементов из различных источников.
     */
// public int[][] createArray(int n, int m, int minValue, int maxValue) {/*code*/
// public void createFromFile(Matrix matrix, File f) { /* code */ }
// public void createFromStream(Matrix matrix, Stream stream) { /* code */ }
}
