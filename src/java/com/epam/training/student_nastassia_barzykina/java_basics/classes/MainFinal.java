package com.epam.training.student_nastassia_barzykina.java_basics.classes;

/**
 * Поскольку метод addFriend() не меняет ссылку, а только изменяет состояние коллекции, то ошибки не происходит.
 */
public class MainFinal {
    public static void main(String[] args) {
        PersonFinal man1 = new PersonFinal();
        PersonFinal man2 = new PersonFinal();
        PersonFinal man3 = new PersonFinal();
        man3.addFriend(man1);
        man3.addFriend(man2);
        System.out.println(man3.getFriends());
    }
}
