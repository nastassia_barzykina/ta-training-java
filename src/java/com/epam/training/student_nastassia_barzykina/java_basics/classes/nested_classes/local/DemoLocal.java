package com.epam.training.student_nastassia_barzykina.java_basics.classes.nested_classes.local;

public class DemoLocal{
    private int y = 33;
    public Read dest(final String string){// QQQ
        class PQ implements Read{// only protected-private!
            private String label;//проинициализирован значением qqq
            PQ(String st){
                label = st;
            }

            public String readLabel() {
                return label + y + string;
            }// qqq+33+QQQ
        }
        return new PQ("qqq");// возвращает экземпляр внутреннего класса через ссылку на интерфейс
    }

    public static void main(String[] args) {
        DemoLocal demoLocal = new DemoLocal();// переменная внешнего класса
        Read read = demoLocal.dest("QQQQ");// переменная внутреннего класса
        System.out.println(read.readLabel());
    }

}
