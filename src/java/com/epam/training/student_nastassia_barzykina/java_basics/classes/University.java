package com.epam.training.student_nastassia_barzykina.java_basics.classes;

public class University {
    public static void main(String[] args) {
        JavaCourse courses[] = {new JavaCourse(), new JavaCourse(), new JavaCourse()};
        courses[0].courseName = "MegaCourse";
        courses[1].courseName = "MegaCourseТ";
        for (JavaCourse c : courses) {
            c = new JavaCourse();
        }
        for (JavaCourse c : courses) {
            System.out.println(c.courseName);
        }
        fooMaxChange();

    }
    static void fooMaxChange(){
        //JavaCourse.FOO_MAX *= 2;
        System.out.print(JavaCourse.FOO_MAX);
    }
}
