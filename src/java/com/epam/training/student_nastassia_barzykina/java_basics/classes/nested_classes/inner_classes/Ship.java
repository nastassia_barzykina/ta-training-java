package com.epam.training.student_nastassia_barzykina.java_basics.classes.nested_classes.inner_classes;

class Ship {
    private int x = 10;


    protected class Engine{
        boolean isRunning;
        public void test(){
            x = 20;// обращение к полю внешнего класса напрямую

        }
        public void start(){
            isRunning = true;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("Engine{");
            sb.append('}');
            return sb.toString();
        }
    }
    public void testing(){
        Engine eng = new Engine();
        eng.test();// обращение к методу внутреннего класса через объект при создании обьекта внешнего класса (метод testing()) - не статический
        eng.start();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Ship{");
        sb.append("x=").append(x);
        sb.append('}');
        return sb.toString();
    }


}
