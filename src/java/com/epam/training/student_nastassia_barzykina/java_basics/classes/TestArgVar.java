package com.epam.training.student_nastassia_barzykina.java_basics.classes;

/**
 * Методы переменной арности используются в случае, когда одно и тоже действие требуется выполнить над различным
 * количеством значений одного типа.
 * Внутри тела метода параметр переменной арности (varargs) рассматривается как массив. На вход могут подаваться как параметры,
 * так и массив (arr)
 */

class VarArg {
    public int calcSum(int... values) {
        int res = 0;
        for (int x : values) {
            res += x;
        }
        return res;
    }
}
public class TestArgVar {
    public static void main(String[] arg) {
        VarArg tstvarg = new VarArg();
        int[] arr = {4, 5, 1, 100};
        //int[] arrRes = new int[]{tstvarg.calcSum(arr)};
        //System.out.println(Arrays.toString(arrRes));
        System.out.println(tstvarg.calcSum(arr));
        System.out.println(tstvarg.calcSum());
        System.out.println(tstvarg.calcSum(3));
        System.out.println(tstvarg.calcSum(55, 66));
        System.out.println(tstvarg.calcSum(77, 55, 33, 11, 99));
    }
}
