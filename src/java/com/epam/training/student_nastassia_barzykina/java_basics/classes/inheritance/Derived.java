package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance;

public class Derived extends Base{
    @Override
    public void show() {// переопределение метода
        //super.show(); //- дополнение поведения при переопределении метода
        /*
        НО:
        если в цепочке наследования находятся более двух классов, то обращение через ключевое слово super всегда относится
        к ближайшему/непосредственному суперклассу.
         */
        System.out.println("Derived");
    }
}
