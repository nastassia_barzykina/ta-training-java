package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance.ex_instanceof;

class ParentClass {
    void parentMethod(int i) {
        System.out.println("parentMethod ParentClass" + i);
    }
    public void childMethod(int i) {
        System.out.println("childMethod ParentClass" + i);
    }
}
class ChildClass extends ParentClass{
    public void parentMethod(int i) {
        System.out.println("parentMethod ChildClass" + i);
    }
    public void childMethod(int i) {
        System.out.println("childMethod ChildClass" + i);
    }
    public static void main(String args[]) {
        ParentClass quest = new ChildClass();   // 1
        quest.parentMethod(1);   // 2 механизм позднего связывания, метод по типу объекта
        quest.childMethod(1);   // 3 ош компиляции, т.к. через ссылку от суперкласса метод подкласса не виден.
        // но если метод будет в суперклассе, то аналогично (2)
    }
}
