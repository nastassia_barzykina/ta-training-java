package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance.methods;

public class Demo9 {
    public static void main(String[] args) {
        Student stud1 = new Student("Peter", 5558956L, 20);
        Student stud2 = new Student("Ivan", 9876543L, 18);
        Student stud3 = new Student("Dasha", 5558956L, 20);
        Student stud4 = new Student("Ivan", 9876543L, 18);
        Student stud5 = stud1;
        System.out.println(stud1.hashCode() + " 1");
        System.out.println(stud2.hashCode());
        System.out.println(stud3.hashCode());
        System.out.println(stud4.hashCode());
        System.out.println(stud5.hashCode() + " 5");
        StudentEx s1 = new StudentEx("Jan", 2, 3);
        StudentEx s2 = new StudentEx("Janny", 3, 2);
        StudentEx s3 = new StudentEx("Jan", 2, 3);
        System.out.println(s1.hashCode());
        System.out.println(s2.hashCode());
        System.out.println(s3.hashCode());
    }
}
