package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance;

public class BritishCat extends Cat{
    BritishCat(String name) {
        //super();
        //super(name); //- если не указывать, создается пустой super();
        //если в классе Cat нет конструктора по умолчанию Cat() {}, будет ошибка компиляции
        System.out.println("British constructor");
    }
}
