package com.epam.training.student_nastassia_barzykina.java_basics.classes;

public class InitDemo7 {
    private final int XX = 50;
    private final int ZZ;
    private final int YY;
    {
        ZZ = 20;
        System.out.println("Dynamic section" + ZZ);
    }
    public InitDemo7() {
        YY = 30;
        System.out.println("Constructor" + YY);
    }
    public static void main(String[] arg) {
        System.out.println("Main");
        InitDemo7 obj = new InitDemo7();
        System.out.println(obj.XX + obj.YY + obj.ZZ);
    }
}
