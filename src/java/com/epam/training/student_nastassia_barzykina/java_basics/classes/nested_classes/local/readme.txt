Локальный класс (local class)
определяется в блоке Java кода. На практике чаще всего объявление происходит в методе некоторого другого класса.
Как и member классы, локальные классы ассоциируются с экземпляром внешнего класса и имеют доступ к его полям и методам.

Локальный класс может обращаться к локальным переменным и параметрам метода, если они объявлены с модификатором final или
являются effective final(начиная с Java 8).
Effective final переменная - это переменная, которая не объявлена явно как final, но ее значение не меняется.

Экземпляр класса может быть создан внутри того же метода, что и класс, но ниже объявления класса.

Локальные классы не могут быть объявлены как private, public, protected или static.

Они не могут иметь внутри себя статических объявлений (полей, методов, классов). Исключением являются константы (static final).

Локальные классы могут быть объявлены как abstract или final.
Если локальный класс объявлен внутри статического метода, он имеет доступ только к статическим переменным класса

В этом примере описан внешний класс Ship с закрытым полем x и методом work(). Этот метод содержит описание локальной переменной y,
которая только инициализируется и не меняет своего значения. Поэтому такая переменная интерпретируется Java как effectively final.
Метод work() также содержит описание локального класса LocalClass с методом test(). В этом методе демонстрируется, что
локальный класс имеет прямой доступ и к закрытому полю внешнего класса x и к локальной переменной y, несмотря на то, что область
использования класса ограничивается только методом work().

Пример сокрытия реализации

Описан интерфейс Read с одним абстрактным методом readLabel().
Описан класс DemoLocal, который содержит:
поле y типа int
метод dest() с неизменяемым параметром s. Все, что делает метод dest(), это создает объект локального класса PD, описанного в его
теле.
Локальный класс PD реализует интерфейс Read и переопределяет его метод readLabel() следующим образом: к значению своего поля label,
которое инициализируется конструктором при создании объекта, добавляет значение поля y внешнего класса и добавляет значение
параметра s метода dest().

В методе main() создается объект внешнего класса DemoLocal, на котором вызывается метод dest() с аргументом "QQQQQ". Этот метод
возвращает объект внутреннего класса PD через ссылку на интерфейс Read. Затем на этой ссылке вызывается метод readLabel(), и
результат отображается в консоль.

Таким образом, во внутреннем классе было скрыто, как формируется некоторая метка, которую потом можно получить через вызов метода
интерфейса.