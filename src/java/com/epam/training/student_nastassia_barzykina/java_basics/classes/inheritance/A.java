package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance;

public class A extends Object {
    public void job() {
        System.out.println("Class A");
    }
}
