package com.epam.training.student_nastassia_barzykina.java_basics.classes;

public class MainCustomMath {
    public static void main(String[] args) {
        CustomMath.percent = 10;
        int numberAdd = CustomMath.add(2, 7);
        System.out.println("numberAdd = " + numberAdd);
        int numberMultiply = CustomMath.multiply(2, 7);
        System.out.println("number multiply = " + numberMultiply);
    }
}
