package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance.example_early;

/**
 * Раннее (статическое) связывание
 */
public class Insurance {
    public static final int LOW = 100;
    public  int getPremium() {
        return LOW;
    }
    public static String getCategory() {
        return "Insurance";
    }
}
