package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.interface1;
public class Circle implements Shape {
    double radius;
    Circle(double x){
        radius = x;
    }

    @Override
    public double getSquare() {/* реализация метода */
        return PI * radius * radius;
    }//реализация(переопределение) абстрактного метода интерфейса
}
