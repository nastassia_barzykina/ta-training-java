package com.epam.training.student_nastassia_barzykina.java_basics.classes.nested_classes.anonim;

public class Ship {
    private int t = 77;
    void doJob(){
        int a = 5;
        CustomTest tst = new CustomTest() {// блок анонимного класса
            @Override
            public void test() {
                System.out.println(a);
                System.out.println("TEST" + t);
            }
        };//
        tst.test();
    }

    public static void main(String[] args) {
        Ship ship = new Ship();
        ship.doJob();
    }

}
