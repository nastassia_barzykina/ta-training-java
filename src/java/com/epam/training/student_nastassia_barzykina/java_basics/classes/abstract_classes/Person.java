package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes;

public abstract class Person {
    private int personId;
    private String lastname;

    public Person(int personId, String lastname) {
        this.personId = personId;
        this.lastname = lastname;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("personId=").append(personId);
        sb.append(", lastname='").append(lastname).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
