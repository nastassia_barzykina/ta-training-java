package com.epam.training.student_nastassia_barzykina.java_basics.classes.nested_classes.local;

public interface Read {
    String readLabel();
}
