package com.epam.training.student_nastassia_barzykina.java_basics.classes.task_no_class;

class Start {
    public static void main(String[] args) {
        Factory bmw = new Factory();
        Factory lada = new Factory();
        Car bmwRed = bmw.createCar("BMW", 120);
        Car bmwBlue = bmw.createCar("BMW", 100);
        Car bmwLux = bmw.createLuxCar("BMW", 160, "LUX");
        Car ladaRed = lada.createCar("Lada", 70);
        Car ladaBlue = lada.createCar("Lada", 80);
        Car ladaWhite = lada.createCar("Lada", 85);
        bmwLux.sound();
        bmwBlue.sound();
        System.out.println(bmwRed);
        System.out.println(bmwBlue);
        System.out.println(bmwLux);
        System.out.println(ladaRed);
        System.out.println(ladaBlue);
        System.out.println(ladaWhite);
        System.out.println("Всего выпущено автомобилей: " + Car.getNumOfCars());
        System.out.println("Выпущено автомобилей BMW: " + bmw.getNumOfCarsOfModel());
        System.out.println("Выпущено автомобилей Lada: " + lada.getNumOfCarsOfModel());
    }
}
