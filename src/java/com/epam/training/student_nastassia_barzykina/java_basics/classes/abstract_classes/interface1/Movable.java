package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.interface1;

public interface Movable {
    default void print(String str) {
        if ( !isNull(str) ) {
            System.out.println("Data: " + str);
        }
    }
    static boolean isNull(String str) {
        return str == null ? true : "".equals(str.trim()) ? true : false;
    }
}