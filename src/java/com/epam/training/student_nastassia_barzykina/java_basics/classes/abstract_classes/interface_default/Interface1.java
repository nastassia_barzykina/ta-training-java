package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.interface_default;
@FunctionalInterface
public interface Interface1 {
    void m1(String str);
    default void log(String str){
        System.out.println("default. Log: " + str);
    }
}
