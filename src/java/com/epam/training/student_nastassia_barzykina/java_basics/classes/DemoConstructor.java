package com.epam.training.student_nastassia_barzykina.java_basics.classes;

public class DemoConstructor {
    public static void main(String[] arg) {
        Car сar1 = new Car("Audi");
        Car сar2 = new Car("BMW");
        Car сar3 = new Car("Bentley");
        //System.out.println(сar1.getKm());
        System.out.println(сar2.getCarModel());
        System.out.println(сar3.getCarModel());
        System.out.println("test");
    }
        }
