package com.epam.training.student_nastassia_barzykina.java_basics.classes.nested_classes.inner_classes;

public class Main {
        public static void main(String[] args) {
            Ship ship = new Ship();
            System.out.println(ship);
            ship.testing();
            System.out.println(ship);
            Ship ship1 = new Ship();
            Ship.Engine engine = ship1.new Engine();// или:
            Ship.Engine engine1 = new Ship().new Engine();
            System.out.println(engine.isRunning);
            engine.start();
            System.out.println(engine.isRunning);
            System.out.println("ship1 = " + ship1);
            System.out.println("engine = " + engine);
            System.out.println("engine1 = " + engine1);
        }

}
