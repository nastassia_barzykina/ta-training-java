package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance.example;

public class Triangle extends Figure{
    public Triangle(double dim1, double dim2) {
        super(dim1, dim2);
    }

    public double area() {
        System.out.print("Площадь треугольника ");
        return dim1 * dim2 / 2;
    }
}
