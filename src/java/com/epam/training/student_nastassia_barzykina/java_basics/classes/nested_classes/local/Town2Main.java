package com.epam.training.student_nastassia_barzykina.java_basics.classes.nested_classes.local;

public class Town2Main {
    public static void main(String[] args) {
        Town2 town = new Town2();
        town.createAddress();
        Town3.createAddress();//локальный класс объявлен внутри статического метода, он имеет доступ только к статическим переменным класса
        Ship ship = new Ship();
        ship.work();
    }
}
