package com.epam.training.student_nastassia_barzykina.java_basics.classes.nested_classes.nested_town;

public class Town {
    private String postCode = "0100";

    public class Street{// обычный внутренний класс
        private int house;
        public void printAddress(){
            System.out.println("Город: " + Town.this);
            System.out.println("Индекс :" + postCode);
            System.out.println("Улица: " + this);
            System.out.println("Дом :" + house);
        }

    }
    public void createStreet() {
        Street street = new Street();// создание объекта внутри вложенного класса
        street.house = 78;
        street.printAddress();
    }
}
