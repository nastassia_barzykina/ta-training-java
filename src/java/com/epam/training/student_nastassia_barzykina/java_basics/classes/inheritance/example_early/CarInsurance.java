package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance.example_early;

public class CarInsurance extends Insurance{
    public static final int HIGH = 200;
    //@Override
    public  int getPremium() {
        return HIGH;
    }
    public static String getCategory() {
        return "CarInsurance";
    }
}
