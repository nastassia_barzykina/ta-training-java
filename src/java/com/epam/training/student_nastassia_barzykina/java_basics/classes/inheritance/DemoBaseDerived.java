package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance;

/**
 * Полное замещение поведения:
 * метод show() описан в суперклассе Base и наследуется подклассом Derived. Подкласс Derived заменяет тело этого метода,
 * подстраивая его под свои нужды. В результате, когда вызывается метод show() на объектах этих классов, вариант его
 * исполнения определяется типом объекта.
 */
public class DemoBaseDerived {
    public static void main(String[] args) {
        Base base = new Base();
        Derived obj = new Derived();
        base.show();
        obj.show();// для каждого объекта - свой вариант метода
    }
}
