package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.interface_default;

public class Class1 implements Interface1, Interface2, Cloneable {
    @Override
    public void m1(String str) {
        System.out.println("m1 = " + str);
    }

    @Override
    public void m2() {
        System.out.println("m2");
    }

    @Override
    public void log(String str) {
        System.out.println("Class1. Log: " + str); // общая реализация метода интерфейсов
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
