package com.epam.training.student_nastassia_barzykina.java_basics.classes;

/**
 * Инициализация полей значениями по умолчанию
 * Если статические поля только объявить, то они будут проинициализированы значениями по умолчанию своего типа и их
 * можно будет использовать.
 *
 * В выражении инициализации поля класса можно использовать обращение к статическому методу. Преимущество – повторное
 * использование, если есть необходимость в использовании кода метода неоднократно.
 */
public class InitDemo1 {
    private static char ch;
    private static boolean bb;
    private  static byte by;
    private static int ii = initSt();// вызов метода инициализации
    private static float ff;
    private static String str;
    private static int[] array;
    private static int initSt() {// метод инициализации
        System.out.println("Init ii value");// если инициализируется класс с точкой входа в Java-программу, то метод
        // main() выполняется после инициализации полей класса.
        return 1000;
    }
    public static void main(String[] arg) {
        System.out.println("char: " + ch);
        System.out.println("boolean: " + bb);
        System.out.println("byte: " + by);
        System.out.println("String: " + str);
        System.out.println("int: " + ii);
        System.out.println(array);
    }
}
