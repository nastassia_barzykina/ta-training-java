package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance;

public class Main {
    public static void main(String[] arg) {
        BritishCat cat = new BritishCat("Mulberry");// Mulberry вызывается к-р суперкласса, потом подкласса
        System.out.println();
        Shape myShape = new Square();// создаем ссылку на суперкласс, а иниц-ем ее объектом
        myShape.draw();// вариант использования метода зависит от типа объекта (Square), а не ссылки
        System.out.println();
        Shape[] shapes = { new Square(), new Circle(), new Triangle(), new Triangle() };// массив фигур
        for (Shape  shape : shapes) {
            shape.draw();
        }
    }
}
