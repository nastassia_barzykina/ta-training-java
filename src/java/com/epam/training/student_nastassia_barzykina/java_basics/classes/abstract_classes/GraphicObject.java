package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes;

/**
 * абстрактный суперкласс GraphicObject с двумя полями x и y, абстрактным методом рисования фигуры draw() и методом
 * перемещения фигуры с реализацией moveTo(). Далее описан его подкласс Circle, в котором приведена пустая реализация
 * метода draw(). А значит, класс не абстрактный и можно создавать его объекты. В классе Runner создается ссылка
 * типа GraphicObject, которая инициализируется объектом типа Circle. На этом объекте вызывается метод draw().
 * Все работает корректно
 */
public abstract class GraphicObject {
    public abstract void draw();
    public void moveTo(int x, int y) {    }
}

class Circle extends GraphicObject {
    @Override
    public void draw() {
        // implementation drawing a circle
    }
}
class Runner {
    public static void main(String[] args) {
        GraphicObject mng;
        // mng = new GraphicObject();
        // нельзя создать объект!
        mng = new Circle();
        mng.draw();
        mng.moveTo(10, 10);
    }
}
