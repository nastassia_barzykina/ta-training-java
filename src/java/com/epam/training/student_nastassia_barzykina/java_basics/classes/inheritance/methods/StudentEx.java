package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance.methods;

import java.util.Objects;

public class StudentEx {
    private String name;
    private int id;
    private int yearOfStudy;

    public StudentEx(String name, int id, int yearOfStudy) {
        this.name = name;
        this.id = id;
        this.yearOfStudy = yearOfStudy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StudentEx studentEx = (StudentEx) o;

        if (id != studentEx.id) return false;
        if (yearOfStudy != studentEx.yearOfStudy) return false;
        return Objects.equals(name, studentEx.name);
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + id;
        result = 31 * result + yearOfStudy;
        return result;
    }
}
