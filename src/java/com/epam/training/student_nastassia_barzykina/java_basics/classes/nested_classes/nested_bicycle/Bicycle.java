package com.epam.training.student_nastassia_barzykina.java_basics.classes.nested_classes.nested_bicycle;

public class Bicycle {
    private String model;
    private int weight;
    private int seatPostDiameter;
    private static int value;
    public Bicycle (String model, int weight, int seatPostDiameter){
        this.model = model;
        this.weight = weight;
        this.seatPostDiameter = seatPostDiameter;
    }
    public void start(){
        System.out.println("Поехали!");
    }
    private static int getValue() { return value; }
    public class Handlebar {// внутренний класс
        public void right(){
            System.out.println("Вправо!");
        }
        public void left(){
            System.out.println("Влево!");
        }
    }
    public class Seat{// внутренний класс
        public void up(){
            System.out.println("Сиденье поднято!");
            start();
        }
        public void down(){
            System.out.println("Сиденье опущено!");
            start();
        }
        public void getSeatParam(){
            System.out.println("Диаметр подсидельного штыря = " + Bicycle.this.seatPostDiameter);// доступ к переменной внешнего класса через this
        }
//        public static createSeat(){ - не может быть, т.к. может не быть объекта. А без объекта «внешнего» класса доступа к внутреннему классу не будет.
//            return new Seat();

//        }
        public static class Class {// доступ только к стат элементам объемлющего класса Bicycle

        }
    }

}
