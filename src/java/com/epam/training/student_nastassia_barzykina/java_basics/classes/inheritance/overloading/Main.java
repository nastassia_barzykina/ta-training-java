package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance.overloading;

public class Main {
    static void doJob(Byte b) {  System.out.println("Byte");  }
    static void doJob(int i) {  System.out.println("int");  }
    static void doJob(double d) {  System.out.println("double");  }

    static void doJob(String s) {  System.out.println("String");  }
    static void doJob(Object o) {  System.out.println("Object");  }

    static void doJob(byte b) {  System.out.println("byte");  }
    public static void main(String[] args) {
        /**
         * разрешения перегрузки на первом шаге:
         * Когда методы вызываются, то аргументы тоже имеют тип byte и Byte. Поэтому в этом случае получается
         * точное соответствие типов аргументов и параметров.
         * @param b
         */
        byte b = 5;
        Byte bb = b;
        System.out.println("First step:");
        doJob(b);
        doJob(bb);
        /**
         * на втором шаге:
         * три метода с именем doJob(), с параметрами примитивных типов byte, int, double. Когда методы вызываются,
         * то аргументы имеют тип short, long и double. Поэтому в этом случае два типа аргумента не соответствуют типам
         * параметров. Поскольку аргументы относятся к примитивным типам данных, то компилятор сначала выполнит
         * расширяющее приведение и проверит результат на совпадение с параметрами. Ближайший существующий тип для
         * short → int, для long → double.
          */
        short s = 10;
        long x = s;
        double dd = s;
        System.out.println("Second step:");
        doJob(s);
        doJob(x);
        doJob(dd);
        /**
         * на первом шаге для ссылочных типов:
         * два метода с именем doJob(), со ссылочными параметрами типа String и Object. Когда методы вызываются,
         * то им передается ссылка на строку, которая определяется типами String и Object. Поэтому в этом случае
         * получается точное соответствие типов аргументов и параметров.
         */
        String ss = "abcd";
        Object obj = ss;
        System.out.println("First-obj step:");
        doJob(ss);
        doJob(obj);
        Employee e = new Employee(5);

    }
}
