package com.epam.training.student_nastassia_barzykina.java_basics.classes.decompoz_example;

import java.util.Scanner;
public class RationalNumbersBadEx {

        public static void main(String[] args) {
            final Scanner scanner = new Scanner(System.in);

            System.out.println("Entering the first number... ");
            System.out.print("Numerator: ");
            int firstNumerator = scanner.nextInt();
            System.out.print("Denominator: ");
            int firstDenominator = scanner.nextInt();

            System.out.println("Entering the second number... ");
            System.out.print("Numerator: ");
            int secondNumerator = scanner.nextInt();
            System.out.print("Denominator: ");
            int secondDenominator = scanner.nextInt();

            System.out.println("What operation to do (SUM/MULT)?");

            while (true) {
                final String operation = scanner.next();

                if (operation.equalsIgnoreCase("SUM")) {

                    int sumNumerator = firstNumerator * secondDenominator
                            + secondNumerator * firstDenominator;

                    int sumDenominator = firstDenominator * secondDenominator;

                    int gcd = gcd(sumNumerator, sumDenominator);

                    System.out.println("Sum is: " + sumNumerator / gcd + "/" + sumDenominator / gcd);
                    break;
                }
                if (operation.equalsIgnoreCase("MULT")) {

                    int multNumerator = firstNumerator * secondNumerator;
                    int multDenominator = firstDenominator * secondDenominator;

                    int gcd = gcd(multNumerator, multDenominator);

                    System.out.println("Product is: " + multNumerator / gcd + "/" + multDenominator / gcd);
                    break;
                }
                System.out.println("Cannot recognize the command. Please, enter \"SUM\" or \"MULT\"");
            }
        }

        public static int gcd(int a, int b) {
            if (a < 0) a = -a;
            if (b < 0) b = -b;
            if (0 == b) return a;
            else return gcd(b, a % b);
        }
    }

