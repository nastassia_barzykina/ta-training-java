package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance.example;
/**
 * Пример позднего (динамического) связывания.
 * В примере описываются три класса Figure (как суперкласс), Rectangle и Triangle (как подклассы).
 * В классе Figure описаны два поля для хранения размеров фигур (dim1 и dim2), а также определен метод
 * вычисления площади фигуры area().
 * Для класса Figure неизвестна форма фигуры, поэтому этот метод возвращает 0.
 * Подклассы Rectangle и Triangle переопределяют метод area() для вычисления свой площади.
 * Конструкторы этих классов передают свои размерности для хранения в поля dim1 и dim2:
 * Rectangle — ширину и высоту, Triangle – длину основания и высоту.
 * Класс FindAreas создает три объекта. По одному для классов Figure, Rectangle и Triangle.
 * А также создает ссылку типа Figure – figref. Эта ссылка поочередно связывается с созданными объектами, и на ней
 * вызывается метод area().
 */

public class FindAreas {
    public static void main(String[] args) {
        Figure f = new Figure(10.0, 5.0);
//        Rectangle r = new Rectangle(9.0, 5.0);
//        Triangle t = new Triangle(10.0, 8.0);
//        Figure figref;
//        figref = r;
//        System.out.println( figref.area() );
//        figref = t;
//        System.out.println( figref.area() );
//        figref = f;
//        System.out.println( figref.area() ); // более универсально
        System.out.println(f.area());
        Figure r = new Rectangle(9.0, 5.0);
        System.out.println(r.area());
        Figure t = new Triangle(15.0, 4.2);
        System.out.println(t.area());
    }
}
