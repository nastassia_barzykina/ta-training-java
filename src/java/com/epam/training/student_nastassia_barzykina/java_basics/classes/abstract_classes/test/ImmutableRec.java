package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.test;

public record ImmutableRec (String name, int id) {// класс с неизменяемыми экземплярами
    void method() {}

}
