package com.epam.training.student_nastassia_barzykina.java_basics.classes.nested_classes.anonim;

public class FoodPotato {
    public static void main(String[] args) {
        Potato potato = new Potato(){// анонимный класс
            @Override
            public void peel(){
                System.out.println("Чистим картошку в анонимном классе");
            }
        };
        potato.peel();
    }

}
