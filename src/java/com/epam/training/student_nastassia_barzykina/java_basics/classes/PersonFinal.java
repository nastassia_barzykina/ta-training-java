package com.epam.training.student_nastassia_barzykina.java_basics.classes;

import java.util.ArrayList;

/**
 * определено поле friends как неизменяемое (ссылка на коллекцию друзей, изначально пустую).
 * Так же определены методы получения и добавления в коллекцию друзей.
 */
public class PersonFinal {
    private final ArrayList friends = new ArrayList<>();
    public ArrayList getFriends() {
        return friends;
    }
    public void addFriend(PersonFinal friend) { //добавление обьектов в лист
        friends.add(friend);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PersonFinal{");
        sb.append("friends=").append(friends);
        sb.append('}');
        return sb.toString();
    }
}
