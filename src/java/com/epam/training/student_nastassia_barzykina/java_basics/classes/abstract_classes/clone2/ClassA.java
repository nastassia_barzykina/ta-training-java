package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.clone2;

/**
 * Клонирование объекта конструктором копирования означает, что объект создается на базе уже существующего.
 *
 * Опишем класс ClassA, в котором определим два поля: одно примитивного типа, другое ссылочного.
 * Также определим геттеры и два конструктора – по умолчанию и с параметром.
 * Конструктор с параметром принимает объекты своего типа. В теле этого конструктора мы инициализируем все поля
 * создаваемого объекта копиями полей переданного. Если поле примитивного типа, то просто копируем его значение.
 * Если поле ссылочного типа, то сначала делаем его копию.
 * В данном случае используем конструктор копирования типа String.
 * Опишем класс Main, в котором создадим объект типа ClassA, а потом сделаем его копию.
 */
public class ClassA {
    public int field1 = 100;
    public String field2 = "Hello";
    public boolean y;

    public boolean isY() {
        return y;
    }

    public int getField1() {
        return field1;
    }

    public String getField2() {
        return field2;
    }
    public ClassA(){

    }

    public ClassA(int field1, String field2, boolean c) {
        this.field1 = field1;
        this.field2 = field2;
        this.y = c;
    }

    public ClassA(ClassA other) {
        this.field1 = other.getField1();//получить значение поля переданного объекта и копировать его в текущий
        this.field2 = new String(other.getField2());//к-р копирования для ссылочного поля
        this.y = other.isY();
    }

    public static ClassA copy(ClassA other) {//фабричный метод - через статический метод с вызовом к-ра копирования ClassA
        return new ClassA(other);
    }

}
