package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.clone2;

public class Main1 {
    public static void main(String[] args) {
        ClassA ob1 = new ClassA(5, "ee", true);//варианты инициализации полей объекта и дальнейшего клонирования
        ClassA ob2 = new ClassA(ob1.getField1(), ob1.getField2(), ob1.isY());
        ClassA ob3 = new ClassA(ob1);
        System.out.println(ob1.field1 + ob1.field2 + ob1.isY());
        System.out.println(ob1);
        System.out.println(ob2.field1 + ob2.field2 + ob2.isY());
        System.out.println(ob2);
        System.out.println(ob3.field1 + ob3.field2 + ob3.isY());
        System.out.println(ob3);
    }
}
