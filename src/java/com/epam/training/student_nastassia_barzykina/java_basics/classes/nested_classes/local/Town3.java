package com.epam.training.student_nastassia_barzykina.java_basics.classes.nested_classes.local;

public class Town3 {
    private  String postCode = "33333";

    public static void createAddress() {
        final int houseNumber = 34;
        class Street {// локальный класс, объявлен внутри стат метода, имеет доступ только к стат переменным класса
            public void printAddress() {
                //System.out.println("PostCode is " + postCode); - ошибка, т.к. поле не статич!
                System.out.println("House Number is " + houseNumber);
            }
        }
        Street street = new Street();
        street.printAddress();
    }
}
