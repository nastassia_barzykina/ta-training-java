package com.epam.training.student_nastassia_barzykina.java_basics.classes.decompoz_example;

import java.util.Scanner;
import java.util.StringJoiner;

public class Conversation {
    private final Command[] commands;// массив команд
    public Conversation(final Command...commands){
        this.commands = commands;
    }// к-р переменной арности
    public void run() {//метод запуска
        printGreeting();// метод инициирующего сообщения
        final Scanner scanner = new Scanner(System.in);
        conversation://метка для выхода
        while (true) {// цикл беседы
            final String input = scanner.next();// чтение польз. ввода
            for (Command command : commands) {// поиск подходящей команды
                if (command.hasTag(input)) {//если команда существует
                    command.execute();// выполняем ее
                    break conversation;// и завершаем; выход из блока, отмеченного меткой (т.е. из метода вообще)
                }
            }
            printRetryMessage();// иначе печатаем сообщение повтора и переходим к другой итерации while
        }
    }
        private void printGreeting(){
            StringJoiner commandTagsJoiner = new StringJoiner("/");//класс для склейки строки с указанием разделителя (.../.../...)
            for (Command command : commands) {
                commandTagsJoiner.add(command.getTag());// в переменную через разделитель склеиваются теги (команды)
            }
            System.out.printf("What operation to perform (%s)?", commandTagsJoiner);// переменная comTJ подставляется вместо (%s)
            System.out.println();
        }
        private void printRetryMessage(){
            System.out.println("Cannot recognize the command. Please, enter one of the following:");
            for (Command command : commands) {//печать списка командных тегов
                System.out.println(command.getTag());
            }

        }
    }