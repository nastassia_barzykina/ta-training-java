package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.interface2;

/**
 *интерфейс Call с полем NUM и абстрактным методом сall(). Опишем класс Base как суперкласс, в котором есть свой метод сall()
 * с реализаций. Опишем его подкласс Client, который еще реализует интерфейс Call и переопределяет метод сall(). Если
 * необходимо использовать как есть реализацию унаследованного метода сall() от класса Base, то это можно сделать, используя
 * ключевое слово super. В классе Main создадим ссылку на интерфейс Call, проинициализируем ее объектом класса Client и
 * вызовем на ней метод сall().
 *
 * Что получаем в результате: вызывается переопределенный метод сall() класса Client, который в свою очередь вызывает
 * унаследованный метод сall() класса Base
 */
public class Main {
    public static void main(String[] args) {
        Call obj = new Client();
        //Client obj1 = new Client();
        obj.call();
        obj.calls();
    }
}
