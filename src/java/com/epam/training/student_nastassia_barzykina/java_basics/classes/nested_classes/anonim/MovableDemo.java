package com.epam.training.student_nastassia_barzykina.java_basics.classes.nested_classes.anonim;

interface Movable{
    void moveRight();
    void moveLeft();
}
public class MovableDemo {
    public static void main(String[] args) {
        Movable movable = new Movable() {// анонимный класс, расширяющий интерфейс
            @Override
            public void moveRight() {
                System.out.println("Right!");
            }

            @Override
            public void moveLeft() {
                System.out.println("Left!");

            }
        };
        movable.moveLeft();
        movable.moveRight();
    }
}
