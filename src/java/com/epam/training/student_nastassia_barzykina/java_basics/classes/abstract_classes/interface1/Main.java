package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.interface1;

/**
 * интерфейс Shape с одним абстрактным методом getSquare() и полем PI. Этот интерфейс реализуется классом Circle,
 * в котором метод getSquare() переопределяется – получает тело. В классе Main создается ссылка на интерфейс Shape,
 * а инициализируется она объектом класса Circle (это полиморфное использование интерфейсов), на котором вызывается
 * метод getSquare()
 */
public class Main {
    public static void main(String[] args) {
        Shape object = new Circle(7.1);
        System.out.println("Square = " + object.getSquare());
    }
}
