package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance.example_early;

/**
 * Пример раннего связывания.
 * В примере описывается суперкласс Insurance, в котором определено:
 *
 * статическое поле LOW
 * статический метод getCategory(), который возвращает имя суперкласса
 * метод экземпляра getPremium(), который возвращает значение поля LOW.
 * Класс CarInsurance – является подклассом Insurance, в котором:
 *
 * определено свое статическое поле HIGH
 * переопределен статический метод метод getCategory(), который возвращает имя подкласса
 * переопределен метод экземпляра getPremium(), который возвращает значение поля HIGH.
 * В классе Main создается ссылка на суперкласс current, а инициализируется объектом подкласса.
 * Затем через ссылку на объект и через имя подкласса вызывается статический метод getCategory().
 */
public class Main {
    public static void main(String[] args) {
        Insurance current = new CarInsurance();
        System.out.println("category: " + Insurance.getCategory() );// или current.getCategory()
        System.out.println(current.getPremium());
        //CarInsurance current2 = (CarInsurance) current;
        System.out.println("category: " + CarInsurance.getCategory() );
        //System.out.println(current2.getPremium());
    }
}
