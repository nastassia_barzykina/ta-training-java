package com.epam.training.student_nastassia_barzykina.java_basics.classes.nested_classes.anonim;

interface Cookable {
    public void cook();
}
public class Food1 {
    Cookable c = new Cookable() {
        public void cook() {
            System.out.println("anonymous cookable implementer");
        }
    };
    public static void main(String[] args) {
        Food1 food = new Food1();
        food.c.cook();
    }


}
