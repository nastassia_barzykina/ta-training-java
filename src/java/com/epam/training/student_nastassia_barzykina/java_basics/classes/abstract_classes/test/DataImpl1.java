package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.test;

/**
 * Опишем интерфейс Data со статическим методом isNull() и методом по умолчанию print().
 * Опишем класс DataImpl, который реализует интерфейс Data, и определим в нем свой собственный метод isNull().
 * В методах isNull() и класса и интерфейса присутствует код вывода сообщений, но разный – это сделано для того,
 * чтобы отследить, в каком случае какой метод исполняется.
 * В методе main() создадим экземпляр класса DataImpl и вызовем на нем методы print() и isNull().
 *
 * В результате: когда исполняется метод по умолчанию print(), то мы наблюдаем, что вызывается статический метод интерфейса
 * Data, а не метод isNull() класса DataImpl. Только по обращению к методу isNull() через ссылку на объект уже исполняется
 * собственный метод класса DataImpl.
 */
public class DataImpl1 implements Data {
    public boolean isNull (String str){
        System.out.println("Null check " + str);
        return str == null;
    }

    public static void main(String[] args) {
        DataImpl1 obj = new DataImpl1();
        obj.print("");// вызываются методы интерфейса
        obj.print("test");//вызываются методы интерфейса
        obj.isNull("abc");//вызывается метод класса
        Data.isNull("DDD");//вызов статического метода интерфейса
    }
}
