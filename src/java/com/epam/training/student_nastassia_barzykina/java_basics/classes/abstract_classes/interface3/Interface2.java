package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.interface3;

public interface Interface2 {
    double someField = 200.5;
    String someMethod();
}
