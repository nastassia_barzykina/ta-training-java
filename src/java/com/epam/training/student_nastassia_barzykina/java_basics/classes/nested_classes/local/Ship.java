package com.epam.training.student_nastassia_barzykina.java_basics.classes.nested_classes.local;

public class Ship {
    private int x = 10;
    void work(){
        int y = 10;// effectively final, т.к. ее значение не меняется
        class LocalClass{
            public void test(){
                x = 20;// доступ к внешней переменной
                System.out.println(x + " " + y);// доступ и к Х, и к У
            }
        }
        LocalClass lC = new LocalClass();
        lC.test();
        //y++;// - ошибка компиляции, т.к. уже не будет final

    }

}
