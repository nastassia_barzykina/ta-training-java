package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.cloneable1;
/**
 * пример поверхностного клонирования/копирования:
 *
 * Опишем класс Student с ссылочным полем типа Date (из пакета java.util) год набора yearSet и полем примитивного типа
 * int группа group.
 * Переопределим метод toString() для получения описания объекта в виде текста.
 * Переопределим метод clone(), унаследованный от класса Object, просто вызвав метод clone() суперкласса.
 * В классе Main создадим объект типа Date: для этого воспользуемся статическим методом currentTimeMillis() класса System.
 * Затем создадим объект stud1 класса Student и отобразим его.
 * Создадим клон объекта stud1 и отобразим его
 */

import java.util.Date;

public class Main {
    public static void main(String[] args) throws Exception {
        Date dd = new Date(System.currentTimeMillis());
        Student stdu1 = new Student(dd, 201);
        System.out.println(stdu1);
        Student stdu2 = (Student) stdu1.clone();// приведение ссылки к типу Student, т.к.клон возвращается через ссылку Object
        System.out.println(stdu2);
        /**
         * получили ошибку CloneNotSupportedException(если не указывать implements Cloneable). Это произошло, потому что
         * недостаточно переопределить метод clone().
         * Нужно еще указать, что такой процесс возможен, т.е. класс должен еще и реализовать интерфейс Cloneable.
         */
    }
}
