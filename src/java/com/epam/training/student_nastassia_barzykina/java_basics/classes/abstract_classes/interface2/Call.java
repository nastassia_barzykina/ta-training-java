package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.interface2;

public interface Call {
    int NUM = 10;
    void call();
    default void calls(){
        System.out.println("Default calls()");
    }
}
