package com.epam.training.student_nastassia_barzykina.java_basics.classes.nested_classes.nested_bicycle;

public class BicycleMain {
    public static void main(String[] args) {
        Bicycle junior = new Bicycle("Junior", 60, 40);//создание и инициализация объекта
        Bicycle.Handlebar handlebar = junior.new Handlebar();// создание объекта объекта (подобъекта, сущности)
        Bicycle.Seat seat = junior.new Seat();
        //Handlebar handlebar1 = new Handlebar(); - ош! код не скомпилируется
        // Объект внутреннего класса не может существовать без объекта «внешнего» класса!
        seat.getSeatParam();
        seat.up();// метод сущности
        junior.start();// метод объекта
        handlebar.left();
        handlebar.right();

    }
}
