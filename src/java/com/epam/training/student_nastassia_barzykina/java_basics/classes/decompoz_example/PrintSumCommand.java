package com.epam.training.student_nastassia_barzykina.java_basics.classes.decompoz_example;

public class PrintSumCommand extends Command{
    private final RationalNumber first;
    private final RationalNumber second;

    public PrintSumCommand(final String tag,final RationalNumber first,final RationalNumber second) {
        super(tag);
        this.first = first;
        this.second = second;
    }
    @Override
    public void execute(){
        System.out.println("SUM is: " + first.add(second));
    }
}
