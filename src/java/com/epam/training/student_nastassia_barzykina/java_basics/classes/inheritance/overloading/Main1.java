package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance.overloading;

public class Main1 {
    static void doJob(int i) {  System.out.println("int");  }
    static void doJob(Double d) {  System.out.println("Double");  }
    static void doJob(String s) {
        System.out.println("String");
    }
    //static void doJob(String... ss) {
    //    System.out.println("String...");
    //}
    static void doJob(String s1, String s2) {
        System.out.println("String, String");
    }
    static void doJob(String s1, String... str) {
        System.out.println("String, String...");
    }
    static void doJob(Object obj1, Object obj2) {
        System.out.println("Object, Object ");
    }
    static void doJob(String str, Object... oo) {
        System.out.println("String, Object...");
    }
    public static void main(String[] args) {
        byte b = 5;
        Byte bb = b;
        /**
         * на втором и третьем шагах:
         *Когда методы вызываются, то аргументы имеют тип byte и Byte. Поэтому в этом случае для первого вызова
         * разрешение происходит на втором шаге, приведением byte → int. Для второго вызова – при приведении ссылки
         * к супертипу не нашлось нужного метода. Тогда компилятор выполняет распаковку аргумента типа Byte
         * в примитивный тип byte, а затем расширяющим приведением находит метод с типом параметра int.
         */
        System.out.println("Second&third step:");
        doJob(b);
        doJob(bb);
        /** на четвертом шаге:
         * три метода с именем doJob(), с одним и двумя параметрами ссылочного типа String, а также с параметром
         * переменной арности того же типа String. Поскольку методы переменной арности, как подходящие, проверяются
         * самыми последними, то этот метод будет исполняться только в случае третьего вызова, когда у метода указаны
         * три аргумента.
         */

        System.out.println("4 step:");
        doJob("hi");
        doJob("hi", "hi");//1
        doJob("hi", "hi", "hi");//2

        /**
         * на различных шагах:
         * При вызове метода с двумя аргументами типа String выполнится метод с двумя параметрами типа Object
         * (разрешение на 2-м шаге). При вызове метода с аргументами типа Object и String выполнится метод с двумя
         * параметрами типа Object (разрешение на 2-м шаге). И только при вызове метода с тремя аргументами типа String
         * выполнится метод с параметром переменной арности типа Object (разрешение на 4-м шаге).
         * static void doJob(Object obj1, Object obj2) {
         *         System.out.println("Object, Object ");
         *     }
         *     static void doJob(String str, Object... oo) {
         *         System.out.println("String, Object...");
         *     }
         */
        System.out.println("dif steps:");
        doJob(new Object(), "hi");// +1+2

    }

}
