package com.epam.training.student_nastassia_barzykina.java_basics.classes;

public class Car { // пример класса
    //        private String model;
//        private int maxSpeed;
//        private int year;
//        public Car (String model, int year, int maxSpeed) {
//            this.model = model;
//            this.year = year;
//            this.maxSpeed = maxSpeed;
//        }
//        public int getMaxSpeed() {
//            return maxSpeed;
//
//        }
    private String carModel;
//
//
    public Car(String carModel) { // конструктор для инициализации модели авто
       this.carModel = carModel;
    }
//
    public String getCarModel() {
        return carModel;
    }
    private int numOfCars;

    //другие поля
    public Car() {
        numOfCars++;
    }

    //другие методы
    public int getNumOfCars() {
        return numOfCars;
    }
}

