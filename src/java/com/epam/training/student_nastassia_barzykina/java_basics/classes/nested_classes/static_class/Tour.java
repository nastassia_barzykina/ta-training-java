package com.epam.training.student_nastassia_barzykina.java_basics.classes.nested_classes.static_class;

public class Tour {
    public static void main(String[] args) {
        Cathedral.Sanctum s = new Cathedral.Sanctum();

        s.go();
    }
}
