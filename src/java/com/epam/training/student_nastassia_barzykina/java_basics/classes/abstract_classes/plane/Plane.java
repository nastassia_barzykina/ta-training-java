package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.plane;

public class Plane implements FlyingMachine {
    @Override
    public void blastOff() { //реализация абстрактных методов
        System.out.println("The plane is blasting off");
    }
    @Override
    public void landing() {
        System.out.println("The plane is landing");
    }

}
