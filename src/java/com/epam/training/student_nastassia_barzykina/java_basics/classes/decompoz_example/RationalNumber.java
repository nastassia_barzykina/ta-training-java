package com.epam.training.student_nastassia_barzykina.java_basics.classes.decompoz_example;

/**
 * класс для RN и всех связанных операций
 */
public class RationalNumber {
    private final int numerator;//числитель
    private final int denominator;// знаменатель

    public RationalNumber(final int numerator, final int denominator) {// final - т.к. объект будет неизменяемым
        if (denominator == 0){
            throw new IllegalArgumentException("Denominator for a RationalNumber must not be zero");
        }//проверка на 0
        int gcd = gcd(numerator, denominator);//НОД для приведения дроби в каноничную форму, вынесен в к-р, т.к. др. методы используют канонич.ф.
        this.numerator = numerator / gcd;
        this.denominator = denominator / gcd;
    }

    @Override
    public String toString() {//если числитель =0, возвращаем 0
        return numerator == 0 ? "0" :numerator + "/" + denominator;
    }
    public RationalNumber add(RationalNumber another) {// метод сложения дробей, на вход подается другая дробь (объект класса)
        int sumNumerator = this.numerator * another.denominator + another.numerator * this.denominator;
        int sumDenominator = this.denominator * another.denominator;
        //int gcd = gcd(sumNumerator, sumDenominator); - вынесено в к-р

        return  new RationalNumber(sumNumerator, sumDenominator);
    }

    public RationalNumber multiply(RationalNumber another) {// метод умножения дробей
        int multNumerator = this.numerator * another.numerator;
        int multDenominator = this.denominator * another.denominator;

        //int gcd = gcd(multNumerator, multDenominator); - вынесли в конструктор

        return new RationalNumber(multNumerator, multDenominator);
    }

    public RationalNumber divideBy(RationalNumber another) {
//        int divNumerator = this.numerator * another.denominator;
//        int divDenominator = this.denominator * another.numerator;
//        return new RationalNumber(divNumerator, divDenominator);// упростили через вызов метода multiply
        return multiply(new RationalNumber(another.denominator, another.numerator));
    }

    private static int gcd(int a, int b) {// статический метод, возвращает НОД для дроби, чтобы потом упростить дробь до канон. ф.
        if (a < 0) a = -a;//для случая отрицательного числителя
        if (b < 0) b = -b;// ... знаменателя
        if (0 == b) return a;//проверка на 0 - если остаток от деления = 0, возвращает а=НОД
        else return gcd(b, a % b);// рекурсия. Для дроби 4/8 дважды вызывается метод, на третий раз остаток от деления будет 0, т.е. b=0 и метод вернет а=4.
    }
}

