package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.interface_default;
@FunctionalInterface
public interface Interface2 {
    void m2();
    default void log(String str){
        System.out.println("default. Log: " + str);
    }
}
