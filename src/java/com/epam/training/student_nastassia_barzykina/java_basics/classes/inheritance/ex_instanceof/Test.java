package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance.ex_instanceof;

public class Test {
    void parentMethod(int i) {
        System.out.println("parentMethod ParentClass" + i);
    }
}
class Child extends Test{
    public void parentMethod(int i) {
        System.out.println("parentMethod ChildClass" + i);
    }
    public void childMethod(int i) {
        System.out.println("childMethod ChildClass" + i);
    }
    public static void main(String args[]) {
        Test quest = new Child();   // 1
        quest.parentMethod(1);   // 2 позднее связывание
        if (quest instanceof Child) {
            ((Child)quest).childMethod(1);   // 3 приведение типа, чтобы был доступен метод подкласса и не было ошибки компиляции
        }
    }
}
