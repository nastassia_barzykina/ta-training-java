package com.epam.training.student_nastassia_barzykina.java_basics.classes.nested_classes.local;

public class Town2 {
        private String postCode = "33333";

        public void createAddress() {
            final int houseNumber = 34;
            class Street {// локальный класс
                public void printAddress() {
                    System.out.println("PostCode is " + postCode);
                    System.out.println("House Number is " + houseNumber);
                }
            }
            Street street = new Street();
            street.printAddress();
        }
    }
