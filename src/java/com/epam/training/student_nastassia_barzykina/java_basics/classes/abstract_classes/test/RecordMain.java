package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.test;

public class RecordMain {
    public static void main(String[] args) {
        ImmutableRec object = new ImmutableRec("Jan", 777);
        System.out.println(object.id());
        System.out.println(object.name());
        System.out.println(object);
        ImmutableRec object2 = new ImmutableRec("Jan", 777);
        System.out.println((object == object2) + " (сравнение ссылок и хэшкодов) " + (object.hashCode() == object2.hashCode()));//ссылки разные, хэши одинаковые, т.к. содержимое идентично
        System.out.println(object.equals(object2));
    }
}
