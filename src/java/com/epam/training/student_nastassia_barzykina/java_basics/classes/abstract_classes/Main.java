package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes;
/**
 класс Animal описывается как абстрактный суперкласс с абстрактным методом move().
 Затем описывается его подкласс Reptiles так же, как абстрактный класс, без какого-либо кода.
 И описывается наследник класса Reptiles – подкласс Boa (как обычный класс, в котором реализуется метод move() ).
 В классе Main создаем ссылку типа Animal, а инициализируем ее объектом подкласса Boa, на котором потом вызываем
 метод move(). Все работает корректно – динамический полиморфизм.
 */

abstract class Animal {
    public abstract void move();// абстрактный метод
}

abstract class Reptiles extends Animal {   }// нет реализации метода, поэтому абстрактный

class Boa extends Reptiles {
    @Override
    public void move() {//реализация метода
        System.out.println("Boa move");
    }
}

public class Main {
    public static void main(String[] arg) {
        Animal animal = new Boa();
        animal.move();
    }
}
