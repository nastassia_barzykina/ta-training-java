package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance;

public class Cat {
    Cat() {
        System.out.println("Cat default constructor");
    }
    Cat(String name) {
        System.out.println("Cat constructor - name: " + name);
    }
}
