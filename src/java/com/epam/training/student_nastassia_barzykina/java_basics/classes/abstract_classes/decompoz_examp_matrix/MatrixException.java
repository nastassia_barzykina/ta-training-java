package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.decompoz_examp_matrix;

public class MatrixException extends Exception {
    public MatrixException() {
    }
    public MatrixException(String message) {
        super(message);
    }
    public MatrixException(String message, Throwable cause) {
        super(message, cause);
    }
    public MatrixException(Throwable cause) {
        super(cause);
    }
}
