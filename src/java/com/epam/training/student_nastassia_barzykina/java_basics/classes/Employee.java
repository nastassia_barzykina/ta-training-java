package com.epam.training.student_nastassia_barzykina.java_basics.classes;

public class Employee  {
    String name;
    int age;
    // public Employee()  {}
    public Employee(String n, int a) {
        name = n;
        age = a;
    }
}