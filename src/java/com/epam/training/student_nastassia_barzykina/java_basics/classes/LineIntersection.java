package com.epam.training.student_nastassia_barzykina.java_basics.classes;

import java.util.Scanner;

public class LineIntersection {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int k1 = sc.nextInt();
        int b1 = sc.nextInt();
        int k2 = sc.nextInt();
        int b2 = sc.nextInt();
        Line l1 = new Line(k1, b1);
        Line l2 = new Line(k2, b2);
//        Point r = intersection(l1, l2);
        Point r = l1.intersection(l2);
        if (r == null) {
            System.out.println("прямые параллельны или совпадают");
        } else {
            System.out.println(r);
        }

    }
    public static Point intersection(Line ln1, Line ln2){
        int kn1 = ln1.getK();
        int kn2 = ln2.getK();
        int bn1 = ln1.getB();
        int bn2 = ln2.getB();
        if (kn1 == kn2) {
            return null;
        } else {
            int rezX = (bn2 - bn1) / (kn1 - kn2);
            int rezY = kn1 * (bn2 - bn1) / (kn1 - kn2) + bn1;
            Point result = new Point(rezX, rezY);
            return result;
        }

    }
}
