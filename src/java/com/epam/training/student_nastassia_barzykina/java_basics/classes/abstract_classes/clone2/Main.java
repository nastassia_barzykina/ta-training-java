package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.clone2;

public class Main {
    public static void main(String[] args) {
        ClassA obj1 = new ClassA();
        ClassA obj2 = new ClassA(obj1);//использование к-ра копирования
        System.out.println(obj1.getField1() + " " + obj1.getField2());
        System.out.println(obj2.getField1());
        System.out.println(obj1);
        System.out.println(ClassA.copy(obj1));//использование фабричного метода


    }
}
