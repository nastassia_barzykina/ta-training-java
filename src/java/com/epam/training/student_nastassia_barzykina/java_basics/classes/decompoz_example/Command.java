package com.epam.training.student_nastassia_barzykina.java_basics.classes.decompoz_example;

public abstract class Command {
    private final String tag;// тег команды операции
    protected Command(final String tag){
        this.tag = tag;
    }// к-р установки значения тега
    public boolean hasTag(String s){
        return tag.equalsIgnoreCase(s);
    }// чекер для проверки совпадения вх. значения с тегом
    public String getTag(){
        return tag;
    }// геттер тегов команд
    public abstract void execute();// абстрактный метод для вызова при активации
}
