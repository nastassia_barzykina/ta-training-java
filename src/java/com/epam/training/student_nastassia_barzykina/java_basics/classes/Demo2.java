package com.epam.training.student_nastassia_barzykina.java_basics.classes;

public class Demo2 {
    public static void main(String[] arg) {
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        String name = car1.getCarModel();
        System.out.println(name);
        System.out.println(car1.getNumOfCars());
        System.out.println(car2.getNumOfCars());
        System.out.println(car3.getNumOfCars());
    }// вместо ожидаемого значения 3, получили 1. Это произошло потому, что поле numOfCars является полем экземпляра.
    // Каждый объект имеет свою копию этого поля с начальным значением 0, которое принимает значение 1 в процессе
    // создания объекта. В этом примере было бы более правильным сделать поле numOfCars статическим.
    // Тогда оно было бы общим для всех объектов и было бы = 3.
}
