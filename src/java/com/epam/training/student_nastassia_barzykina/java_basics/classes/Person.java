package com.epam.training.student_nastassia_barzykina.java_basics.classes;

public class Person { //принцип инкапсуляции и сокрытия данных. Было:
    //  public String name;
   //   public int age;
    //  Стало:
        private String name;
        private int age;

        public Person(String name, int age) {// к-р с методами проверки name и age
            checkName(name);
            checkAge(age);

            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public int getAge() {
            return age;
        }

        public void setName(String name) {
            checkName(name);
            this.name = name;
        }

        public void incrementAge() {
            this.age++;
        }// политика изменения возраста строго определена внутри класса и не мб изменена снаружи

        private void checkAge(final int age) {
            if (age < 0){
                throw new IllegalArgumentException("Age is negative");
            }
        }

        private void checkName(final String name) {
            if (name == null || name.isBlank()){
                throw new IllegalArgumentException("Name is null or empty");
            }
        }
    }
