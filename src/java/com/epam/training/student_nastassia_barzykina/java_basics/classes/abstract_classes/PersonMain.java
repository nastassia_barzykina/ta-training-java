package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes;

public class PersonMain {
    public static void main(String[] args) {
        Person person = new Student(777, "Jan", 9.41);
        Person person1 = new Teacher(12, "Smith", "doctor");
        Teacher teacher = new Teacher(23, "Boo", "prof.");
        System.out.println(person);
        System.out.println(person1);
        System.out.println(teacher);
    }
}
