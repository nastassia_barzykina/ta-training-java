package com.epam.training.student_nastassia_barzykina.java_basics.classes;

import java.util.Objects;

/**
 * Пример класса - информации (хранение и предоставление информации о группе сущностей)
 */

public class Subject {
    private long subjectId;
    private String name;

    public Subject(long subjectId, String name) {
        this.subjectId = subjectId;
        this.name = name;

    }
    public long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(long subjectId) {
        this.subjectId = subjectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Subject subject = (Subject) o;
        return subjectId == subject.subjectId && Objects.equals(name, subject.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subjectId, name);
    }

    @Override
    public String toString() {
        return "Subject{" +
                "subjectId=" + subjectId +
                ", name='" + name + '\'' +
                '}';
    }
}
