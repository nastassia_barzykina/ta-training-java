package com.epam.training.student_nastassia_barzykina.java_basics.classes;

/**
 * В классе CustomMath нет полей экземпляра. Методы получают данные для работы через свои параметры, то есть класс
 * не имеет состояния. В этом случае методы лучше описать как статические.
 */
public class CustomMath {
        public static int percent;
        public static int add(int x, int y) {
            return x + y + percent;
        }
        public static int multiply(int x, int y) {
            return x * y;
        }
    }
