package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.test;

public interface Data {
    default void print (String str){
        if (!isNull(str)){
            System.out.println("Data. Prints lines: " + str );
        }
    }
    static boolean isNull(String str){
        System.out.println("Static method null checking ");
        return str == null ? true : "".equals(str.trim()) ? true : false;
    }
}
