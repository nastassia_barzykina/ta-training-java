package com.epam.training.student_nastassia_barzykina.java_basics.classes.decompoz_example;

public class PrintProductCommand extends Command{
    private final RationalNumber first;
    private final RationalNumber second;

    public PrintProductCommand(final String tag,final RationalNumber first,final RationalNumber second) {
        super(tag);
        this.first = first;
        this.second = second;
    }
    @Override
    public void execute(){
        System.out.println("Product is: " + first.multiply(second));
    }
}
