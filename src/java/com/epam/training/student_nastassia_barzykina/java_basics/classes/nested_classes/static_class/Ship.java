package com.epam.training.student_nastassia_barzykina.java_basics.classes.nested_classes.static_class;
interface Moving{
    default void move(){
        System.out.println("Move");
    }
}
public class Ship {
    public int x = 10;
    private static int y = 15;
    public static class Boat{
        public void test(){
            //x = 20; - ошибка, хоть поле и открытое, но нестатич!
            Ship sh = new Ship();
            sh.x = 20;// доступ к нестатич полю - через объект
            y = 20;// прямой доступ к статич полю внеш класса
            System.out.println(sh.x + " " + y + " - in Boat");
        }

    }
    //public static class Boat1 extends Boat2 implements Moving{ - СТАТ класс не может наследоваться от внутреннего класса



    private int x1 = 7;
    static class CustomStatic {
        private int x1 = 8;
        public void seeOuter() {
            System.out.println(" x is " + x1);
        }
    }

    public static void main(String[] args) {
        Ship.Boat boat = new Ship.Boat();
        boat.test();
        Boat boat1 = new Boat();
        boat1.test();
        System.out.println(boat1);
        System.out.println(new Ship().x + " - in Ship");
        CustomStatic obStatic = new CustomStatic();// т.к. внутри обрамляющего класса. Если снаружи - то вызов как в строках 33 и 35
        obStatic.seeOuter();
        System.out.println(obStatic);

    }
}
