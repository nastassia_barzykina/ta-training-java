package com.epam.training.student_nastassia_barzykina.java_basics.classes.nested_classes.nested_town;

public class TownMain {
    public static void main(String[] args) {
        Town town = new Town();
        town.createStreet();
        Town.Street street1 = town.new Street();
        Town.Street street2 = new Town().new Street();
        street1.printAddress();
        street2.printAddress();

    }
}
