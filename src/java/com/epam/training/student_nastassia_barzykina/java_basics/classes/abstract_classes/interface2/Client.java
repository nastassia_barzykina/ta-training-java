package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.interface2;

public class Client extends Base implements Call {
    @Override
    public void call() {//реализация метода интерфейса
        System.out.println("сall() of class Client: NUM = " + NUM);
        super.call();//вызов метода суперкласса Base
    }
//    public void calls() {
//        System.out.println("calls() of Client from Base = " + i*i);
//        super.calls();
//    }
        }
