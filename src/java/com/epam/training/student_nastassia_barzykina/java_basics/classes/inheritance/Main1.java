package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance;

class Person {
    String name = "Teddy";
    //String version = "Person";
}
//class Student extends Person {
//    String version = "Student";
//}
class SuperMan extends Person {
    String skills;
    SuperMan(String skills, String name) {
        super();
        this.skills = skills;
    }
    public String showInfo() {
        return name + " has " + skills;
    }
}
public class Main1 {
    public static void main(String[] args) {
        //Person person = new Student ();
        //System.out.println(person.version);
        SuperMan superMan = new SuperMan("super vision", "Clark Kent");
        System.out.print(superMan.showInfo());
    }
}
