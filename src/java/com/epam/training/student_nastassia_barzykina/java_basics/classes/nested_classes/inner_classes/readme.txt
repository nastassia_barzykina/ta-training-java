Обращение к элементам внутреннего и внешнего классов

В  этом примере описывается внешний класс Ship, в котором:

  описано закрытое поле экземпляра x
  метод экземпляра testing()
  внутренний класс Engine с доступом protected.
  Внутренний класс содержит описание одного метода test(), в коде которого происходит обращение к полю x внешнего класса напрямую.

  Внешний же класс, чтобы обратится к методу внутреннего класса, сначала создает его объект: тело метода testing().


  Создание объекта внутреннего класса вне внешнего

  Для получения экземпляра внутреннего класса вне тела внешнего класса, нужно:

  сначала создать экземпляр внешнего класса
  затем создать экземпляр внутреннего класса в пределах объекта внешнего по следующему синтаксису:
  <имя внеш.класса>  <имя объекта внеш.кл.> =  new <конструктор внеш.класса>;

  <имя внеш.класса>.<имя внутр.класса>   <имя объекта внутр.кл.> =
      <имя объекта внеш.кл.>.new  <конструктор внутр.класса>;
  Например:

  Ship ship = new Ship();
  Ship.Engine engine = ship.new Engine();

  Получение и использование объекта внутреннего класса вне тела внешнего класса возможно только если внутренний класс имеет
  соответствующую видимость, то есть не является скрытым в описании внешнего класса.

  Пример применения внутреннего класса для предоставления услуг внешнему классу, в котором он содержится. Другими словами,
  это пример перенесения или сокрытия реализации операций с объекта внешнего класса на внутренний класс.

  В примере:

  Описан внешний класс OuterArr, содержащий массив целых чисел nums, который инициализируется конструктором этого класса,
  а также метод analyze().
  Метод analyze(), используя объект внутреннего класса Inner, отображает в консоль информацию о наименьшем и наибольшем
  элементах массива, а также о среднеарифметическом значении его элементов.
  Операции по нахождению информации описаны в методах внутреннего класса.