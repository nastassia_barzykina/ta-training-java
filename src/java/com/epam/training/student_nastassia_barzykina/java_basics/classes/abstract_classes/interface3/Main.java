package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.interface3;

/**
 * два интерфейса Interface1 и Interface2, у каждого из которых есть поле someField (разного типа) и абстрактный метод
 * someMethod(). Опишем класс SomeClass, реализующий оба эти интерфейса. В этом классе определяется реализация метода
 * someMethod(), общая для обоих интерфейсов. Опишем класс Main, в котором рассмотрим различные способы обращения к
 * полям и методу.
 */
public class Main {
    public static void main(String[] args) {
        SomeClass obj = new SomeClass();
        System.out.println(obj.someMethod()); //12 и 13 -- одна и та же реализация
        System.out.println(((Interface2)obj).someMethod());//
        System.out.println(((Interface1)obj).someField);//14 и 15 -- разные обращения к одному и тому же полю
        System.out.println((Interface1.someField));//
    }
}
