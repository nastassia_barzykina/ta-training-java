package com.epam.training.student_nastassia_barzykina.java_basics.classes;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ForTest {
    //    static int i;
//
//    {
//        i = 5;
//    }
//
//    public static void main(String[] arg) {
//        System.out.println("Main");
//        ForTest obj = new ForTest();
//        System.out.println(obj.i);
//    }
    final static public int MIN_AGE = 20;
    static final int MAX_AGE = 70;
    final static int AVR_AGE = 70;
    static int i;

    public static void main(String[] args) throws Exception {
        //System.out.println(i);
        //int nod = gcd(28851538, 1183019);
        //System.out.println(nod);
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());
        System.out.println(gcd(a, b));
    }

    public static int gcd(int a, int b) {
        if (a < 0) a = -a;
        if (b < 0) b = -b;
        if (0 == b) return a;
        else return gcd(b, a % b);
//        while (a > 0 && b > 0) {
//            if (a >= b) {
//                a = a % b;
//            } else {
//                b = b % a;
//            }
//        }
//        return Math.max(a, b);
    }
}

