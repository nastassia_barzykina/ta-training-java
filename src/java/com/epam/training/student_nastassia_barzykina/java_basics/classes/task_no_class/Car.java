package com.epam.training.student_nastassia_barzykina.java_basics.classes.task_no_class;

class Car {
    private String name;
    private int maxSpeed;
    private String type;
    private static int numOfCars;

    Car() {
    }

    Car(String name, int maxSpeed) {
        this.name = name;
        this.maxSpeed = maxSpeed;
        numOfCars++;
    }

    Car(String name, int maxSpeed, String type) {
        this(name, maxSpeed);
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public static int getNumOfCars() {
        return numOfCars;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Car{");
        sb.append("name='").append(name).append('\'');
        sb.append(", maxSpeed=").append(maxSpeed);
        sb.append(", type='").append(type).append('\'');
        sb.append('}');
        return sb.toString();
    }

    void sound() {
        switch (type) {
            case "LUX" -> System.out.println("La-la");
            case "TRUCK" -> System.out.println("Fa-Fa");
            default -> System.out.println("Bi-Bi");
        }
    }
}
