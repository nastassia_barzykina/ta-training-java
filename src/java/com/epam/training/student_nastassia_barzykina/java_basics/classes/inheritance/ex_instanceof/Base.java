package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance.ex_instanceof;

public class Base {
    public void print() {
        System.out.println("Base");
    }
}
class SubClass extends Base {
    public void print() {
        System.out.println("SubClass");
    }
}
class A {
    String version = "1.0 A";
    String testMethod() {
        return "A";
    }
}
class B extends A {
    String version = "1.0 B";
    String testMethod() {
        return "B";
    }
}
class Main1 {
    public static void main(String[] args) {
        Base object = new SubClass();
        object.print();
        A a = new B();
        System.out.println(a.version + a.testMethod());
    }
}

