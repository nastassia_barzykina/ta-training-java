package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance;

public class B extends A {
        public void job(int i) {
        System.out.println("Class B");
    }

    public static void main(String[] args) {
        A b = new B(); // если B b = new B() и b.job(1) -- Class B
        b.job();  // Class A, т.е. вызовется метод без параметров класса А
        // если public void job() -- Class B
    }
}
