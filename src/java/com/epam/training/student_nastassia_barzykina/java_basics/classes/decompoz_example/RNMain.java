package com.epam.training.student_nastassia_barzykina.java_basics.classes.decompoz_example;

public class RNMain {
    public static void main(String[] args) {
        //final Scanner scanner = new Scanner(System.in);
        RationalNumber first = new RationalNumberInteractiveReader("first").read();//создаем объект и инициализируем его именем переменной, вызывая метод чтения, который возвращает RN
        RationalNumber second = new RationalNumberInteractiveReader("second").read();
        final Conversation conversation = new Conversation(
              new PrintSumCommand("SUM", first, second),//предоставление печати суммы (произведения, деления)
              new PrintProductCommand("MULT", first, second),
              new PrintQuotientCommand("DIV", first, second)
        );// инициализация диалога
        conversation.run();// запуск метода на переменной conversation

        /*System.out.println("Entering the first number... ");
        System.out.print("Numerator: ");
        int firstNumerator = scanner.nextInt();
        System.out.print("Denominator: ");
        int firstDenominator = scanner.nextInt();
        RationalNumber first = new RationalNumber(firstNumerator, firstDenominator);

        System.out.println("Entering the second number... ");
        System.out.print("Numerator: ");
        int secondNumerator = scanner.nextInt();
        System.out.print("Denominator: ");
        int secondDenominator = scanner.nextInt();
        RationalNumber second = new RationalNumber(secondNumerator, secondDenominator);*/

        //System.out.println("What operation to do (SUM/MULT)?");

        //while (true) {
          //  final String operation = scanner.next();

            //if (operation.equalsIgnoreCase("SUM")) {

                /*int sumNumerator = firstNumerator * secondDenominator
                        + secondNumerator * firstDenominator;

                int sumDenominator = firstDenominator * secondDenominator;

                int gcd = gcd(sumNumerator, sumDenominator);
*/
                //System.out.println("Sum is: " + first.add(second));
                //break;
            //}
            //if (operation.equalsIgnoreCase("MULT")) {

                /*int multNumerator = firstNumerator * secondNumerator;
                int multDenominator = firstDenominator * secondDenominator;

                int gcd = gcd(multNumerator, multDenominator);
*/
                //System.out.println("Product is: " + first.multiply(second));
                //break;
            //}
            //System.out.println("Cannot recognize the command. Please, enter \"SUM\" or \"MULT\"");
       // }
    }
}
