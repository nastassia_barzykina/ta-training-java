package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance;

public class Car extends Vehicle{
    protected int maxSpeed = 300;
    public void showSpeed() {
        System.out.println(super.maxSpeed + " from superclass");// обращение к затененному полю суперкласса
        System.out.println(maxSpeed + " from Car");
    }
}
