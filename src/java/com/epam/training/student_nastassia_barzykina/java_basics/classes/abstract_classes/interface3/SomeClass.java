package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.interface3;

public class SomeClass implements Interface1, Interface2 {
    @Override
    public String someMethod() {
        System.out.println("In interface2 = " + Interface2.someField);//доступ к полю именно интерфейса 2
        System.out.println("In interface1 = " + Interface1.someField);
        return "It works";
    }
}
