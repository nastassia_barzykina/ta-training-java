package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.interface_default;

public class MainClass {
    public static void main(String[] args) throws Exception {
        Class1 obj = new Class1();
        obj.log("TEST");
        obj.m1("NNN");
        obj.m2();
        Class1 obj2 = (Class1) obj.clone();
        System.out.println(obj2);
    }
}
