package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance;

/**
 * Затенение полей.
 *В классе Vehicle описано поле maxSpeed. Область видимости данного поля перейдет в подкласс Car, потому что доступ к
 * нему определен как protected. Однако класс Car объявляет свое собственное поле с именем maxSpeed.
 * Происходит затенение видимости унаследованного поля. Для разрешения этой ситуации в методе showSpeed() подкласса Car
 * используется ключевое слово super.
 * Но:
 * в этом случае в классе Car имеется два поля, сохраняющих значение максимальной скорости с разными величинами.
 * (крайне нежелательно)
 */
public class DemoCarVehicle {
    public static void main(String[] args) {
        Car car = new Car();
        car.showSpeed();
    }
}
