package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.plane;

/**
 * класс самолета Plane, который реализует интерфейс FlyingMachine, для работы с которым достаточно переопределить
 * только абстрактные методы интерфейса, и класс можно использовать. В классе Demo создадим ссылку типа интерфейса
 * FlyingMachine и инициализируем ее объектом класса Plane, затем вызовем метод makeTravel().
 */
public class Demo {
    public static void main(String[] args) {
        FlyingMachine plane = new Plane();
        plane.makeTravel();
    }
}
