package com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance.methods;
class Point1{
    protected int x;
    protected int y;

    public Point1(int x, int y) {
        this.x = x;
        this.y = y;
    }
   @Override
    public boolean equals(Object obj) { // переопределение метода суперкласса Object
        if (this == obj) return true;// проверка ссылки
        if (obj == null) return false;
        if (this.getClass() != obj.getClass()) return false;// если классы не совпадают, они неэквив. ИЛИ:
        // if (!(obj instanceof Point1)) return false;// если классы не совпадают, они неэквив.
        Point1 other = (Point1) obj;// явное приведение типа, для возможности использования полей и методов класса Point1
   // public boolean equals(Point1 other){
        return this.x == other.x && this.y == other.y;// хранятся ли в полях объекта идентичные значения
    }
}
public class Demo8 {
    public static void main(String[] args) {
        Point1 point1 = new Point1(5, -5);
        Point1 point2 = point1;
        Point1 point3 = new Point1(5, -5);
        Point1 point4 = new Point1(5, 5);
        System.out.println(point1.equals(point2));
        System.out.println(point1.equals(point3));// через приведение типа и проверку значений (эквивалентность не ссылки, но значений)
        System.out.println(point1.equals(point4));
    }
}
