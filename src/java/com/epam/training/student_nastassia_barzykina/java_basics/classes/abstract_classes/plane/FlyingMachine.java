package com.epam.training.student_nastassia_barzykina.java_basics.classes.abstract_classes.plane;

public interface FlyingMachine {
    void blastOff();
    void landing();
    default void makeTravel() { //не зависит от типа самолета, поэтому можно описать в интерфейсе
        blastOff();
        fly();
        landing();
    }
    default void fly() {// полет на автопилоте
        System.out.println("Machine is flying!");
    }
}
