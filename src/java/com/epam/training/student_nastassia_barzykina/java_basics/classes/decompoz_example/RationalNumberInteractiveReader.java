package com.epam.training.student_nastassia_barzykina.java_basics.classes.decompoz_example;

import java.util.Scanner;

/**
 * класс - диалог ввода RN
 */
public class RationalNumberInteractiveReader {
    private final String variableName;

    public RationalNumberInteractiveReader(final String variableName) {// к-р для установки имени переменной
        this.variableName = variableName;
    }
    public RationalNumber read(){// метод вз-я с пользователем и чтения RN
        final Scanner scanner = new Scanner(System.in);
        System.out.printf("Entering the %s...", variableName);
        System.out.print("Numerator: ");
        int numerator = scanner.nextInt();
        System.out.print("Denominator: ");
        int denominator = scanner.nextInt();
        return new RationalNumber(numerator,denominator);
    }
}
