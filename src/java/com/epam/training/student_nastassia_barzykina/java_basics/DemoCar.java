package com.epam.training.student_nastassia_barzykina.java_basics;

import com.epam.training.student_nastassia_barzykina.java_basics.array.Bicycle;
import com.epam.training.student_nastassia_barzykina.java_basics.classes.inheritance.Vehicle;

public class DemoCar {
    public static void main(String[] args) {
        Vehicle vehicle = new Vehicle();
        Bicycle bicycle = new Bicycle();
        //System.out.println(vehicle.maxSpeed); ош компиляции, т.к. поле protected
        //System.out.println(bicycle.maxSpeed);
        //vehicle.showSpeed();
        bicycle.showSpeed();
    }
}
