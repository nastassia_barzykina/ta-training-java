package com.epam.training.student_nastassia_barzykina.java_basics;

import java.util.Scanner;

public class HelloStrangers {
    /*
    запрашивает количество незнакомцев, с которыми нужно встретиться
построчно читает имена незнакомцев
построчно выводит: "Hello, (имя незнакомца)" для каждого незнакомца.
Гарантируется, что введенное количество незнакомцев — целое число.

Частные случаи:

Если количество незнакомцев равно нулю, программа должна вывести: "Oh, it looks like there is no one here".
Если количество незнакомцев отрицательное, программа должна вывести: "Seriously? Why so negative?".
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int input = scan.nextInt();
        if (input > 0) {
            String [] name = new String[input];//объявление и инициализация массива имен
            for (int i = 0; i < input; i++) {
                name[i] = scan.next();
            }
            for (int i = 0; i < input; i++) {
                System.out.println("Hello, " + name[i]);

            }


        } else if (input == 0) {
            System.out.println("Oh, it looks like there is no one here");

        } else {
            System.out.println("Seriously? Why so negative?");
        }
    }
}
