package com.epam.training.student_nastassia_barzykina.java_basics;

public class Cat {
    private boolean collarStatus = false;
    public boolean getCollarStatus() {
        return collarStatus;
    }
    public void setCollarStatus(boolean status) {
        collarStatus = status;
    }
}
