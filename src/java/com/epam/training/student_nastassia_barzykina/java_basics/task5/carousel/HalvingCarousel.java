package com.epam.training.student_nastassia_barzykina.java_basics.task5.carousel;

/**
 * необходимо расширить класс DecrementingCarousel. Вам нужно реализовать класс HalvingCarousel.
 * Этот подкласс должен делить элементы пополам, а не уменьшать их на единицу. Обратите внимание, что вам нужно применить
 * обычное целочисленное деление, отбрасывая остаток. Например, 5 / 2 = 2.
 */
public class HalvingCarousel extends DecrementingCarousel {
    public HalvingCarousel(final int capacity){
        super(capacity);

    }

    @Override
    public int getElement () {
        cycleIncrementK();
        if (array[k] > 0) {
            var elem = array[k];
            array[k] = elem / 2;
            return elem;
        } else {
            return getElement();
        }
    }

}
