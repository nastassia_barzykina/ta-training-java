package com.epam.training.student_nastassia_barzykina.java_basics.task5.triangle;

import static java.lang.Math.abs;
import static java.lang.Math.sqrt;
import static java.lang.StrictMath.pow;

public class Segment {
    public Point p1;
    public Point p2;

    public Segment() {

    }

    public Segment(Point p1, Point p2) {
        if (p1.equals(p2)) {
            throw new IllegalArgumentException();
        }
        this.p1 = p1;
        this.p2 = p2;
    }

    public Point getP1() {
        return p1;
    }

    public Point getP2() {
        return p2;
    }


    double length() {
        if (p1.equals(p2)) {
            throw new IllegalArgumentException();
        }
        double l = sqrt(abs(pow(getP2().getX() - getP1().getX(), 2) + pow(getP2().getY() - getP1().getY(), 2)));
        return l;

    }

    Point middle() {
        if (p1.equals(p2)) {
            throw new IllegalArgumentException();
        }
        double xS1 = getP1().getX();
        double yS1 = getP1().getY();
        double xS2 = getP2().getX();
        double yS2 = getP2().getY();
        double res1 = (xS1 + xS2) / 2;
        double res2 = (yS1 + yS2) / 2;
        return new Point(res1, res2);

    }

    Point intersection(Segment another) {
        if (p1.equals(p2) || another.getP1().equals(another.getP2())) {
            throw new IllegalArgumentException();
        }
        if ((this.getP1().getX() == this.getP2().getX())
                && another.getP1().getX() == another.getP2().getX()) {
            return null; // проверка на вертикальность и параллельность
        } else if (((this.getP1().getX() == this.getP2().getX())
                && !(another.getP1().getX() == another.getP2().getX()))) { // проверка на прараллельность одного из отрезков
            if (this.getP1().getX() < another.getP1().getX()
                    || this.getP1().getX() > another.getP2().getX()) {
                return null;
            } else {
                var point = resCase(this.getP1().getX(), another); //метод частного случая | /
                return getPointIfExists(another, point.getX(), point.getY());
            }
        } else if ((!(this.getP1().getX() == this.getP2().getX())
                && (another.getP1().getX() == another.getP2().getX()))) { // проверка на прараллельность одного из отрезков
            if (another.getP1().getX() > this.getP2().getX()
                    || another.getP1().getX() < this.getP1().getX()) {
                return null;
            } else {
                var point = resCase(another.getP1().getX(), this); //метод частного случая | /
                return getPointIfExists(another, point.getX(), point.getY());//метод частного случая / |
            }
        }
        double k1 = resK(this);
        double k2 = resK(another);
        if (k1 == k2) {
            return null;
        }
        double b1 = resB(this, k1);
        double b2 = resB(another, k2);
        double xIs = (b1 - b2) / (k2 - k1);
        double yIs = k1 * xIs + b1;
        return getPointIfExists(another, xIs, yIs);
    }

    private Point getPointIfExists(Segment another, double xIs, double yIs) {
        Point iP = new Point(xIs, yIs);
        boolean checkS1 = check(this, iP);
        boolean checkS2 = check(another, iP);
        if (checkS1 && checkS2) {
            return iP;
        } else {
            return null;
        }
    }

    private boolean check(Segment segment, Point iP) {
        var x1 = segment.getP1().getX();
        var x2 = segment.getP2().getX();
        if (x1 > x2) {
            double temp = x1;
            x1 = x2;
            x2 = temp;
        }
        var y1 = segment.getP1().getY();
        var y2 = segment.getP2().getY();
        if (y1 > y2) {
            double temp = y1;
            y1 = y2;
            y2 = temp;
        }
        return iP.getX() >= x1
                && iP.getX() <= x2
                && iP.getY() >= y1
                && iP.getY() <= y2;
    }

    static double resK(Segment a) {
        double k = (a.getP2().getY() - a.getP1().getY()) / (a.getP2().getX() - a.getP1().getX());
        return k;
    }

    static double resB(Segment a, double k) {
        double b = (a.getP1().getY() - k * a.getP1().getX());
        return b;
    }

    private static Point resCase(double x,Segment a) {
        double k = resK(a);
        double b = resB(a, k);
        double yIs = k * x + b;
        return new Point(x, yIs);
    }

}
