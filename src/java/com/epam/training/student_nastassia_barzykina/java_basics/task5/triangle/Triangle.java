package com.epam.training.student_nastassia_barzykina.java_basics.task5.triangle;
import static java.lang.Math.abs;

class Triangle {
    public Point a;
    public Point b;
    public Point c;

    public Triangle() {

    }

    public Triangle(Point a, Point b, Point c) {
        if (areaCheck(a, b, c) == 0) {
            throw new IllegalArgumentException();
        }
        this.a = a;
        this.b = b;
        this.c = c;

    }
    private static double areaCheck(Point a, Point b, Point c){
        return abs(a.getX() * (b.getY() - c.getY()) + b.getX() * (c.getY() - a.getY())
                + c.getX() * (a.getY() - b.getY())) / 2;
    }

    public double area() {
        return areaCheck(a, b, c);
    }

    public Point centroid(){
        double xO = (getA().getX() + getB().getX() + getC().getX()) / 3;
        double yO = (getA().getY() + getB().getY() + getC().getY()) / 3;
        return new Point(xO, yO);
    }

    public Point getA() {
        return a;
    }

    public Point getB() {
        return b;
    }

    public Point getC() {
        return c;
    }
}
