package com.epam.training.student_nastassia_barzykina.java_basics.task5.carousel;

public class Main {
    public static void main(String[] args) {
        /**
         * пустой случай:
         */
        CarouselRun run = new DecrementingCarousel(7).run();
        System.out.println(run.isFinished()); //true
        System.out.println(run.next()); //-1
        /**
         * обычный случай:
         */
        DecrementingCarousel carousel = new DecrementingCarousel(7);//

        carousel.addElement(2);
        carousel.addElement(3);
        carousel.addElement(1);

        CarouselRun run1 = carousel.run();

        System.out.println(run1.isFinished()); //false

        System.out.println(run1.next()); //2
        System.out.println(run1.next()); //3
        System.out.println(run1.next()); //1

        System.out.println(run1.next()); //1
        System.out.println(run1.next()); //2

        System.out.println(run1.next()); //1

        System.out.println(run1.isFinished()); //true
        System.out.println(run1.next()); //-1
        /**
         *Случай отказа от добавления дополнительных элементов:
         */
        DecrementingCarousel carousel2 = new DecrementingCarousel(3);

        System.out.println(carousel2.addElement(-2)); //false
        System.out.println(carousel2.addElement(0)); //false

        System.out.println(carousel2.addElement(2)); //true
        System.out.println(carousel2.addElement(3)); //true
        System.out.println(carousel2.addElement(1)); //true

//carousel is full
        System.out.println(carousel2.addElement(2)); //false

        CarouselRun run2 = carousel2.run();

        System.out.println(run2.next()); //2
        System.out.println(run2.next()); //3
        System.out.println(run2.next()); //1

        System.out.println(run2.next()); //1
        System.out.println(run2.next()); //2

        System.out.println(run2.next()); //1

        System.out.println(run2.isFinished()); //true
        System.out.println(run2.next()); //-1
        /**
         * Отказ от добавления дополнительных элементов после вызова "run":
         */
        DecrementingCarousel carousel3 = new DecrementingCarousel(10);

        System.out.println(carousel3.addElement(2)); //true
        System.out.println(carousel3.addElement(3)); //true
        System.out.println(carousel3.addElement(1)); //true

        carousel3.run();

        System.out.println(carousel3.addElement(2)); //false
        System.out.println(carousel3.addElement(3)); //false
        System.out.println(carousel3.addElement(1)); //false
        /**
         * Отказ от создания более, чем одного CarouselRun:
         */
        DecrementingCarousel carousel4 = new DecrementingCarousel(10);
        System.out.println(carousel4.run() == null); //false
        System.out.println(carousel4.run() == null); //true
        /**
         * для HalvingCarousel:
         */
        DecrementingCarousel carouselH = new HalvingCarousel(7);

        carouselH.addElement(20);
        carouselH.addElement(30);
        carouselH.addElement(10);

        CarouselRun runH = carouselH.run();

        System.out.println(runH.isFinished()); //false

        System.out.println(runH.next()); //20
        System.out.println(runH.next()); //30
        System.out.println(runH.next()); //10

        System.out.println(runH.next()); //10
        System.out.println(runH.next()); //15
        System.out.println(runH.next()); //5

        System.out.println(runH.next()); //5
        System.out.println(runH.next()); //7
        System.out.println(runH.next()); //2

        System.out.println(runH.next()); //2
        System.out.println(runH.next()); //3
        System.out.println(runH.next()); //1

        System.out.println(runH.next()); //1
        System.out.println(runH.next()); //1

        System.out.println(runH.isFinished()); //true
        System.out.println(runH.next()); //-1
        /**
         * для GraduallyDecreasingCarousel:
         */
        CarouselRun runGD = new GraduallyDecreasingCarousel(3).run();
        System.out.println(runGD.isFinished());
        System.out.println(runGD.next());

        DecrementingCarousel carouselGD = new GraduallyDecreasingCarousel(7);

        carouselGD.addElement(20);
        carouselGD.addElement(30);
        carouselGD.addElement(10);

        CarouselRun runG = carouselGD.run();

        System.out.println(runG.isFinished()); //false

        System.out.println(runG.next()); //20
        System.out.println(runG.next()); //30
        System.out.println(runG.next()); //10

        System.out.println(runG.next()); //19
        System.out.println(runG.next()); //29
        System.out.println(runG.next()); //9

        System.out.println(runG.next()); //17
        System.out.println(runG.next()); //27
        System.out.println(runG.next()); //7

        System.out.println(runG.next()); //14
        System.out.println(runG.next()); //24
        System.out.println(runG.next()); //4

        System.out.println(runG.next()); //10
        System.out.println(runG.next()); //20

        System.out.println(runG.next()); //5
        System.out.println(runG.next()); //15

        System.out.println(runG.next()); //9

        System.out.println(runG.next()); //2

        System.out.println(runG.isFinished()); //true
        System.out.println(runG.next()); //-1

    }
}
