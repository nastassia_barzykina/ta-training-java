package com.epam.training.student_nastassia_barzykina.java_basics.task5.carouselWithLimit;

/**
 * нужно реализовать класс GraduallyDecreasingCarousel. Этот подкласс должен уменьшать элементы
 * как обычный DecrementingCarousel. Разница в том, что эта реализация должна производить карусельный запуск, который
 * ограничивает количество вызовов следующего метода. При достижении лимита вызовов запуск карусели должен считаться завершенным.
 */
public class MainWithLimit {
    public static void main(String[] args) {
        DecrementingCarousel carousel = new DecrementingCarouselWithLimitedRun(7, 10);

        carousel.addElement(20);
        carousel.addElement(30);
        carousel.addElement(10);

        CarouselRun run = carousel.run();

        System.out.println(run.isFinished()); //false

        System.out.println(run.next()); //20
        System.out.println(run.next()); //30
        System.out.println(run.next()); //10

        System.out.println(run.next()); //19
        System.out.println(run.next()); //29
        System.out.println(run.next()); //9

        System.out.println(run.next()); //18
        System.out.println(run.next()); //28
        System.out.println(run.next()); //8

        System.out.println(run.next()); //17

        System.out.println(run.isFinished()); //true
        System.out.println(run.next()); //-1

    }
}
