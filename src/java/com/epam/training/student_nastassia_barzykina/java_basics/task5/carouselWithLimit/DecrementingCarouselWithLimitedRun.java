package com.epam.training.student_nastassia_barzykina.java_basics.task5.carouselWithLimit;

public class DecrementingCarouselWithLimitedRun extends DecrementingCarousel {
    private final int lim;//кол-во вызовов
    private int count = 0;//счетчик вызовов

    public DecrementingCarouselWithLimitedRun(int capacity, int lim) {
        super(capacity);
        this.lim = lim;
    }

    @Override
    public int getElement() {
        count++;
        return getParticularElement();
    }

    private int getParticularElement() {
        cycleIncrementK();
        if (array[k] > 0) {
            var elem = array[k];
            array[k] = elem - 1;
            return elem;
        } else {
            return getParticularElement();
        }
    }

    public boolean check() {//переопределение метода проверки
        boolean result = true;
        for (int elem : getArray()) {
            if (elem > 0 && count < lim) {
                result = false;
                break;
            }
        }
        return result;
    }
}
