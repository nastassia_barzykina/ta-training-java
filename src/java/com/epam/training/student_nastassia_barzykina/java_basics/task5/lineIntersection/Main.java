package com.epam.training.student_nastassia_barzykina.java_basics.task5.lineIntersection;

import java.util.Scanner;

/**
 * необходимо реализовать метод intersection в классе Line. Он должен возвращать точку пересечения двух линий (Point).
 * Если линии совпадают или не пересекаются, метод должен возвращать значение null.
 */

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int k1 = scanner.nextInt();
        int b1 = scanner.nextInt();
        int k2 = scanner.nextInt();
        int b2 = scanner.nextInt();

        Line line1 = new Line(k1,b1);
        Line line2 = new Line(k2,b2);

        System.out.println("result is " + line1.intersection(line2));

    }

}
