package com.epam.training.student_nastassia_barzykina.java_basics.task5.carousel;

public class DecrementingCarousel {
    protected final int[] array;
    private boolean blocked;
    private int iAdd;
    protected int k = -1;

    public DecrementingCarousel(int capacity) {
        this.array = new int[capacity];

    }

    public boolean addElement(int element) {
        if (!blocked && element > 0 && iAdd < array.length) {
            array[iAdd++] = element;
            return true;
        }
        return false;
    }

    public int getElement() {
        cycleIncrementK();
        if (array[k] > 0) {
            var elem = array[k];
            array[k] = elem - 1;
            return elem;
        } else {
            return getElement();
        }
    }

    protected void cycleIncrementK() {
        if (k < array.length - 1) {
            ++k;
        } else {
            k = 0;
        }
    }

    public CarouselRun run() {
        if (blocked) {
            return null;
        } else {
            blocked = true;
            return new CarouselRun(this);
        }
    }

    public int getK() {
        return k;
    }

    public void setK(int k) {
        this.k = k;
    }

    public int[] getArray() {
        return array;
    }
}
