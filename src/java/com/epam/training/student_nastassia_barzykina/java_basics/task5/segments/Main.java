package com.epam.training.student_nastassia_barzykina.java_basics.task5.segments;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x1 = sc.nextInt();
        int y1 = sc.nextInt();
        int x2 = sc.nextInt();
        int y2 = sc.nextInt();
        int x3 = sc.nextInt();
        int y3 = sc.nextInt();
        int x4 = sc.nextInt();
        int y4 = sc.nextInt();
        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        Point p3 = new Point(x3, y3);
        Point p4 = new Point(x4, y4);
        Segment s1 = new Segment(p1, p2);
        Segment s2 = new Segment(p3, p4);
        if (p1.equals(p2) || p3.equals(p4)) {
            throw new IllegalArgumentException();
        } else {
            Point middleS1 = s1.middle();
            Point middleS2 = s2.middle();
            System.out.println("middle S1 X = " + middleS1.getX());
            System.out.println("middle S1 Y= " + middleS1.getY());
            System.out.println("middle S2 X = " + middleS2.getX());
            System.out.println("middle S2 Y= " + middleS2.getY());

            double lS1 = s1.length();
            double lS2 = s2.length();
            System.out.println("length S1 = " + lS1);
            System.out.println("length S2 = " + lS2);
            Point intersectionsSegments = s1.intersection(s2);
            Point interSec = s1.intersection(s2);
            if (interSec == null){
                System.out.println("Segments don`t have intersection`s point");
            } else {
                System.out.println("test x = " + interSec.getX());
                System.out.println("test Y = " + interSec.getY());
            }
        }
    }
}
