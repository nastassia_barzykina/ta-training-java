package com.epam.training.student_nastassia_barzykina.java_basics.task5.carousel;

/**
 *Decrementing Carousel контейнер, принимающий элементы int. DecrementingCarousel имеет максимальную вместимость, указанную
 *в конструкторе. При создании DecrementingCarousel находится в состоянии накопления: вы можете добавлять элементы с помощью
 * метода addElement и создать объект CarouselRun с помощью метода run. После вызова метода run
 * DecrementingCarousel находится в рабочем состоянии: он отказывается добавлять дополнительные элементы.
 *
 * CarouselRun позволяет перебирать элементы карусели, уменьшая их один за другим с помощью метода next.
 * Данный метод возвращает значение текущего элемента, затем он уменьшает текущий элемент на единицу и переключается на
 * следующий элемент.
 * CarouselRun перебирает элементы в порядке их добавления. Когда элемент уменьшится до нуля, CarouselRun пропустит его
 * в дальнейших итерациях. Когда больше нет элементов, доступных для уменьшения, CarouselRun возвращает -1.
 *
 * CarouselRun также имеет метод isFinished который указывает, закончились ли в карусели элементы для уменьшения.
 *
 *
 * Детали спецификации
 * DecrementingCarousel имеет два public метода:
 *
 * boolean addElement(int element) – добавляет элемент. Если элемент отрицателен или равен нулю, не добавляет его. Если
 * контейнер заполнен, не добавляет элемент. Если метод run был вызван для создания CarouselRun, не добавляет элемент.
 * Если элемент добавлен успешно, возвращает true. В противном случае – false.
 * CarouselRun run() – возвращает CarouselRun для перебора элементов. Если метод run уже был вызван ранее, он должен вернуть
 * null: DecrementingCarousel может генерировать только один объект CarouselRun.
 * CarouselRun имеет два public метода:
 *
 * int next() – возвращает текущее значение текущего элемента, затем уменьшает текущий элемент на единицу и переключается
 * на следующий элемент в порядке добавления. Пропускает нулевые элементы. Когда больше нет элементов для уменьшения, возвращает -1.
 * boolean isFinished() – когда больше нет элементов для уменьшения, возвращает true. В противном случае возвращает false.
 */
public class CarouselRun {
    private final DecrementingCarousel decCar;

    public CarouselRun(DecrementingCarousel decCar) {
        this.decCar = decCar;

    }

    public int next() {
        if (isFinished()) {
            return -1;
        }
        return decCar.getElement();
    }


    public boolean isFinished() {
        boolean result = true;
        for (int elem : decCar.getArray()) {
            if (elem > 0) {
                result = false;
                break;
            }
        }
        return result;
    }


}
