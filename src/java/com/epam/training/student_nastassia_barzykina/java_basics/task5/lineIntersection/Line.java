package com.epam.training.student_nastassia_barzykina.java_basics.task5.lineIntersection;

public class Line {
    private int k;
    private int b;
    public Line(){

    }
    public Line (int k, int b){
        this.k = k;
        this.b = b;
    }

    public  Point intersection(Line l) {//метод возвращает точку пересечения прямых, если она есть
        int kn1 = this.getK();
        int bn1 = this.getB();
        int kn2 = l.getK();
        int bn2 = l.getB();
        if (kn1 == kn2) {
            return null;
        } else {
            int rezX = (bn2 - bn1) / (kn1 - kn2);
            int rezY = kn1 * (bn2 - bn1) / (kn1 - kn2) + bn1;
            return new Point(rezX, rezY);
        }
    }

    public int getK() {
        return k;
    }

    public int getB() {
        return b;
    }

    @Override
    public String toString() {
        return "Line{" +
                "k=" + k +
                ", b=" + b +
                '}';
    }
}
