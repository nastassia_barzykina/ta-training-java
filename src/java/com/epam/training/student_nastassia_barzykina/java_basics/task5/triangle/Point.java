package com.epam.training.student_nastassia_barzykina.java_basics.task5.triangle;

/**
 * Реализуйте методы класса Triangle:
 *
 * Конструктор, имеющий в качестве параметров координаты трех вершин.
 * Убедитесь, что эти точки относятся к вершинам треугольника.
 * Проверьте, что созданный треугольник существует и не является вырожденным.
 * Если это не так, используйте throw new IllegalArgumentException(), чтобы вызвать ошибку.
 * double area() – возвращает площадь треугольника.
 * Point centroid() – возвращает центроид треугольника.
 * Класс Point уже существует.
 */
class Point {
    private double x;
    private double y;

    public Point(final double x, final double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
    @Override
    public boolean equals(Object another) {
        if (another == null) {
            return true;
        }
        if (!(another instanceof Point)) {
            return false;
        }
        Point point = (Point) another;
        return x == point.getX() && y == point.getY();
    }
}
