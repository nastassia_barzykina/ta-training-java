package com.epam.training.student_nastassia_barzykina.java_basics.task5.carousel;

/**
 * реализуйте GraduallyDecreasingCarousel. Этот подкласс должен уменьшать элементы путем постепенного увеличения уменьшения.
 * Когда вам нужно уменьшить элемент в первый раз, уменьшите его на 1. В следующий раз, когда вам нужно уменьшить тот же элемент,
 * уменьшите его на 2. Далее уменьшите его на 3, затем на 4 и так далее. Вы не должны продолжать уменьшение с
 * неположительные элементами.
 */
public class GraduallyDecreasingCarousel extends DecrementingCarousel{
    private int step = 1;//шаг уменьшения
    public GraduallyDecreasingCarousel(final int capacity) {
        super(capacity);
    }
    @Override
    public int getElement () {
        cycleIncrementK();
        if (array[k] > 0) {
            var elem = array[k];
            array[k] = elem - step;
            return elem;
        } else {
            return getElement();
        }
    }
    @Override
    protected void cycleIncrementK () {
        if (k < array.length - 1) {
            ++k;
        } else {
            k = 0;
            step++;
        }
    }
}
