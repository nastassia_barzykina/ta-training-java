package com.epam.training.student_nastassia_barzykina.java_basics.task5.carouselWithLimit;

public class CarouselRun {
    protected final DecrementingCarousel decCar;

    public CarouselRun(DecrementingCarousel decCar) {
        this.decCar = decCar;
    }

    public int next() {
        if (isFinished()) {
            return -1;
        }
        return decCar.getElement();
    }

    public boolean isFinished() {
        return decCar.check();
    }

}
