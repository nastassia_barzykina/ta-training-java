package com.epam.training.student_nastassia_barzykina.java_basics;
/*
считывает последовательность целочисленных значений из стандартного ввода и находит среднее значение.

Подробности:

Вы должны считывать значения последовательности, пока следующее не станет 0. Нулевое значение означает конец входной последовательности и не является ее частью.
Последовательность гарантированно содержит хотя бы одно значение.
Среднее значение также является целым числом. Используйте целочисленные операции.
 */

import java.util.Scanner;

public class Average {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int input;// для вводимого числа
        int avg = 0;//для среднего значения

        for (int i = 0; ; i++) {//бесконечный цикл
            input = scanner.nextInt();
            avg += input;// avg+input
            if (input == 0) {//условие выхода из цикла
                avg = avg / i;
                break;
            }
        }
        System.out.println(avg);
    }
}
