package com.epam.training.student_nastassia_barzykina.java_basics;

public class MainCat {
    /**
     * Передача аргумента в метод по значению ссылки
     * В метод передается копия значения ссылки на объект, а не сам объект: изменения, производимые с содержимым объекта
     * в методе, будут доступны/видны из той части программы, где объект был создан.
     * Метод concat() вызывается из метода main() и получает копию значения ссылки на объект типа Cat. В итоге оба метода
     * имеют свои переменные, которые ссылаются на один и тот же объект. Потому изменения, производимые в методе concat(),
     * будут видны и в методе main().
     * @param cat
     */
    public static void concat (Cat cat) {
        System.out.println("2. Old value of \"collarStatus\" into \"concat\" method is:" + cat.getCollarStatus());
        cat.setCollarStatus(true);
        System.out.println("3. Old value of \"collarStatus\" into \"concat\" method is:" + cat.getCollarStatus());
    }
    public static void main(String[] args) {
        Cat kitty = new Cat();
        System.out.println("1. Old value of \"collarStatus\" into \"main\" method is:" + kitty.getCollarStatus());
        concat(kitty);
        System.out.println("4. Old value of \"collarStatus\" into \"main\" method is:" + kitty.getCollarStatus());
    }
}
